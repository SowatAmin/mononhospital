@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>		
		<a href="/home">Manage Users</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Add User</a></li>
</ul>
@endsection
@section('content')
<!--<form action="addUser" method="post" class="form-horizontal" role="form">	-->

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>		            
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
{!! Form::open(array('route' => 'users.store', 'class' =>'form-horizontal'))!!}
<div class="col-sm-20">
	<div class="form-group">
		<div class="col-sm-2">
			<label class="control-label">First Name: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="fname">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputEmail3" class="control-label">Last Name: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="lname">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputEmail3" class="control-label">Email: </label>
		</div>
		<div class="col-sm-10">
			<input type="email" class="form-control" name="email">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputEmail3" class="control-label">Username: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="username">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputPassword3" class="control-label">Password: </label>
		</div>
		<div class="col-sm-10">
			<input type="password" class="form-control" name="password">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputEmail3" class="control-label">Role: </label>
		</div> <br><br>
		<div style="margin-left: 15%;">
			<label><input type="radio" name="role" value="1">Admin</label>
			<label><input type="radio" name="role" value="2">Food Manager</label>
			<label><input type="radio" name="role" value="3">Hospital Manager</label>
			<label><input type="radio" name="role" value="4">Accounts Manager</label>	
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10" style="margin-left:20%">
			<button type="submit" class="btn btn-success">Add User</button>
		</div>
	</div>
</div>
{!!Form::close()!!}
<!--</form> -->

@endsection