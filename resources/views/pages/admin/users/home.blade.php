@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>		
		<a href="/home">Manage Users</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Show Users</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>		            
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr style="border-bottom:1px solid black">
						<th style="border-bottom:1px solid black">ID</th>
						<th style="border-bottom:1px solid black">First Name</th>
						<th style="border-bottom:1px solid black">Last Name</th>
						<th style="border-bottom:1px solid black">Email</th>
						<th style="border-bottom:1px solid black">Username</th>						
						<th style="border-bottom:1px solid black">Role</th>
						<th class="pull-right">Edit/Delete</th>
					</tr>
				</thead>
				<tbody>
					@foreach($tableData as $index => $tableData)
					<tr  style="border-bottom:1px solid black">
						<td style="border-bottom:1px solid black">{{$tableData->id}}</td>
						<td style="border-bottom:1px solid black">{{$tableData->fname}}</td>
						<td style="border-bottom:1px solid black">{{$tableData->lname}}</td>
						<td style="border-bottom:1px solid black">{{$tableData->email}}</td>
						<td style="border-bottom:1px solid black">{{$tableData->username}}</td>
						
						@if($tableData->role == 1)
						<td style="border-bottom:1px solid black">Admin</td>
						@elseif($tableData->role == 2)
						<td style="border-bottom:1px solid black">Food Manager</td>
						@elseif($tableData->role == 3)
						<td style="border-bottom:1px solid black">Hospital Manager</td>
						@elseif($tableData->role == 4)
						<td style="border-bottom:1px solid black">Accounts Manager</td>
						@endif
						<td class="pull-right">						
							
							

							{!! Form::open(array('url' => 'users/' . $tableData->id)) !!}
							{!! Form::hidden('_method', 'DELETE') !!}
							<a href="/users/{!! $tableData->id !!}/edit" class="btn btn-primary">Edit</a>
							<button type="submit" class="btn btn-danger">Delete</button>
							{!! Form::close() !!}
						</td> 
					</tr>
					@endforeach
				</tbody>
			</table>
			<a href="/users/create" class="btn btn-success" style="margin-left:20%">Add new User</a>
		</div>
	</div>
</div>

@endsection