@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>		
		<a href="/home">Manage Users</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Show Users</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>		            
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Username</th>						
						<th>Role</th>
						<th>Edit/Delete</th>
					</tr>
				</thead>
				<tbody>
					@foreach($tableData as $index => $tableData)
					<tr>
						<td>{{$tableData->id}}</td>
						<td>{{$tableData->fname}}</td>
						<td>{{$tableData->lname}}</td>
						<td>{{$tableData->email}}</td>
						<td>{{$tableData->username}}</td>
						
						@if($tableData->role == 1)
						<td>Admin</td>
						@elseif($tableData->role == 2)
						<td>Food Manager</td>
						@elseif($tableData->role == 3)
						<td>Hospital Manager</td>
						@elseif($tableData->role == 4)
						<td>Accounts Manager</td>
						@endif
						<td>						
							<a href="/users/{!! $tableData->id !!}/edit" class="btn btn-primary">Edit</a>
							
							<a href="/users/delete/{!! $tableData->id !!}" class="btn btn-danger">Delete</a>
						</td> 
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection