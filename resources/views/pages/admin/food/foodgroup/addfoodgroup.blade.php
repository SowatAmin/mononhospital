@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/foodadminpart/food-group">Food Group Management</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a>Add New Group</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		<h2 style="padding-left:15px">Add food group</h2>
		
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		<div class="box-content">
			<form action="/foodadminpart/food-group" class="form-horizontal" method="post">
			{!! csrf_field() !!}
				<fieldset>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Enter a food group</label>
					<div class="controls">
					  <input name="foodgroupname" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}">
					</div>
				  </div>				  
				  <div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="/foodadminpart/food-group" class="btn">Cancel</a>
				  </div>
				</fieldset>
			  </form>		
		</div>
	</div><!--/span-->

</div><!--/row-->
@endsection

@section('essentialScript')
<script>
$(document).ready(function() {    
	$(".alert.alert-success").fadeOut(3000);
});
</script>
@endsection