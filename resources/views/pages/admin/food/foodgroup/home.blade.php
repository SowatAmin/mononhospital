@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Food Group Management</a></li>
</ul>
@endsection
@section('content')
<div class="row-fluid">	

	<a href="/foodadminpart/add-food-group" class="quick-button metro yellow span2">
		<i class="icon-group"></i>
		<p>Add New Group</p>		
	</a>

	<a href="/foodadminpart/show-all-group" class="quick-button metro red span2">
		<i class="icon-briefcase"></i>
		<p>Show All Group</p>		
	</a>
		
	<div class="clearfix"></div>
					
</div><!--/row-->
@endsection