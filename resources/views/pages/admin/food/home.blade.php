@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Dashboard</a></li>
</ul>
@endsection
@section('content')
<div class="row-fluid">	

	<a href="/foodadminpart/food-group" class="quick-button metro yellow span2">
		<i class="icon-group"></i>
		<p>Food Group Management</p>		
	</a>
	<a href="/foodadminpart/food-item" class="quick-button metro red span2">
		<i class="icon-briefcase"></i>
		<p>Food Item Management</p>		
	</a>
	<!-- <a class="quick-button metro blue span2">
		<i class="glyphicons-icon pot"></i>
		<p>Salary</p>		
	</a>
	<a class="quick-button metro green span2">
		<i class="icon-list-alt"></i>
		<p>Journal</p>
	</a>
	<a class="quick-button metro pink span2">
		<i class="icon-book"></i>
		<p>Create Voucher</p>		
	</a>
	<a class="quick-button metro black span2">
		<i class="icon-tags"></i>
		<p>Sales</p>
	</a> -->
	
	<div class="clearfix"></div>
					
</div><!--/row-->
@endsection