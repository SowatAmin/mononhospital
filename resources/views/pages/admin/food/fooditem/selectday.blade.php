@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/foodadminpart/food-item">Food Item Manager</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a>Select Day</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon calendar"></i><span class="break"></span>Select Day</h2>
			
				
		<div class="box-content">
			<ul class="dashboard-list metro">
				<li>
					<a href="/foodadminpart/create-item/Friday">			
						Friday                                    
					</a>
				</li>
			  	<li>
					<a href="/foodadminpart/create-item/Saturday">				  
				  		Saturday
					</a>
			  	</li>
			  	<li>
					<a href="/foodadminpart/create-item/Sunday">				  
				  		Sunday                                    
					</a>
			  	</li>
			  	<li>
					<a href="/foodadminpart/create-item/Monday">				  
				  		Monday                                    
					</a>
			  	</li>
			  	<li>
					<a href="/foodadminpart/create-item/Tuesday">				  
				  		Tuesday                                    
					</a>
			  	</li>
			  	<li>
					<a href="/foodadminpart/create-item/Wednesday">				  
				  		Wednesday
					</a>
			  	</li>
			  	<li>
					<a href="/foodadminpart/create-item/Thursday">				  
				  		Thursday                                    
					</a>
			  	</li>			  
			</ul>
		</div>
	</div><!--/span-->

</div><!--/row-->
@endsection

@section('essentialScript')
<script>
$(document).ready(function() {    
	$(".alert.alert-success").fadeOut(3000);
});
</script>
@endsection