@extends('layouts.default')

@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Food Item Management</a></li>
</ul>
@endsection

@section('content')
<div class="row-fluid">	

	<a href="/foodadminpart/select-day" class="quick-button metro yellow span2">
		<i class="icon-group"></i>
		<p>Create New Item</p>		
	</a>

	<a href="/foodadminpart/show-all-item" class="quick-button metro red span2">
		<i class="icon-briefcase"></i>
		<p>Show All Item</p>		
	</a>
		
	<div class="clearfix"></div>
					
</div><!--/row-->
@endsection