@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/foodadminpart/food-item">Food Item Management</a>
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a href="/foodadminpart/show-all-item">Show All Item</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a>Edit Food Item Information</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon edit"></i><span class="break"></span>Edit</h2>
			
		
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		@if (session('alert'))
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('alert') }}
		    </div>
		@endif
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		<div class="box-content">
			<form action="/foodadminpart/update-food-item/{!! $itemInfo->id !!}" class="form-horizontal" method="post">
			<div class="span6">
			{!! csrf_field() !!}
				<fieldset>
				  <div class="control-group">
				  	<label class="control-label" for="focusedInput">Name Of Item</label>
				  	<div class="controls">
				  	  <input name="itemname" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $itemInfo->NameOfItem !!}">
				  	</div>
				  </div>
				  <div class="control-group">
				  	<label class="control-label" for="selectError">Select Day</label>
				  	<div class="controls">
				  	  <select name="day" id="selectError" data-rel="chosen">
				  	  	@if($itemInfo->DayFor=='Friday')
				  		<option value="Friday" selected="">Friday</option>
				  		@else
				  		<option value="Friday">Friday</option>
				  		@endif
				  		@if($itemInfo->DayFor=='Saturday')
				  		<option value="Saturday" selected="">Saturday</option>
				  		@else
				  		<option value="Saturday">Saturday</option>
				  		@endif
				  		@if($itemInfo->DayFor=='Sunday')
				  		<option value="Sunday" selected="">Sunday</option>
				  		@else
				  		<option value="Sunday">Sunday</option>
				  		@endif
				  		@if($itemInfo->DayFor=='Monday')
				  		<option value="Monday" selected="">Monday</option>
				  		@else
				  		<option value="Monday">Monday</option>
				  		@endif
				  		@if($itemInfo->DayFor=='Tuesday')
				  		<option value="Tuesday" selected="">Tuesday</option>
				  		@else
				  		<option value="Tuesday">Tuesday</option>
				  		@endif
				  		@if($itemInfo->DayFor=='Wednesday')
				  		<option value="Wednesday" selected="">Wednesday</option>
				  		@else
				  		<option value="Wednesday">Wednesday</option>
				  		@endif
				  		@if($itemInfo->DayFor=='Thursday')
				  		<option value="Thursday" selected="">Thursday</option>
				  		@else
				  		<option value="Thursday">Thursday</option>
				  		@endif				  						  		
				  	  </select>
				  	</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="selectError">Select Category</label>
					<div class="controls">
					  <select name="itemCategory" id="category" data-rel="chosen">					  	
					  @foreach($category as $data)
					  	@if($data->id==$itemInfo->CategoryId){
							<option value="{!! $data->id !!}" selected="">{!! $data->NameOfGroup !!}</option>		
					  	}
						@else
							<option value="{!! $data->id !!}">{!! $data->NameOfGroup !!}</option>
						@endif
					  @endforeach
					  </select>
					</div>
				  </div>
				  <div class="control-group">
				  	<label class="control-label" for="focusedInput">Quantity (gm)</label>
				  	<div class="controls">
				  	  <input name="qty" class="quantity input-xlarge focused" id="focusedInput" type="text" value="{!! $itemInfo->Quantity !!}">
				  	</div>
				  </div>
				  <div class="control-group">
				  	<label class="control-label" for="selectError">How many times</label>
					<div class="controls">
					  <select name="hmt" id="none" data-rel="chosen">
					  	@for($i=1;$i<=8;$i++)
					  	@if($itemInfo->HowManyTimes==$i)
					  	<option value="{!! $i !!}" selected="">{!! $i !!}</option>
					  	@else
					  	<option value="{!! $i !!}">{!! $i !!}</option>
					  	@endif
					  	@endfor											  
					  </select>
					</div>
				  </div>
				  <div class="control-group">
				  	<label class="control-label" for="focusedInput">Distribution Formula</label>
				  	<div class="controls">
				  	  <input name="formula" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $itemInfo->formula !!}">
				  	</div>
				  </div>
				 <!--  <div class="form-actions">
				 					<button type="submit" class="btn btn-primary">Submit</button>
				 					<a href="/foodadminpart/show-all-item" class="btn">Cancel</a>
				 </div> -->
				</fieldset>
			</div>
				<div class="span5">		
					<div class="tooltip-demo well">
					  <p class="muted" style="margin-bottom: 0;">
					  To create distribution formula you have to use only provided keyword which is given below:-
					  <br>
					  @foreach($keywords as $keyword)
					  {!! $keyword->keyword !!}<br>
					  @endforeach
					  </p>
					</div>                                  
				</div>

			  	<div class="form-actions span12" style="margin:0; width:102%; margin-left:-1%;">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="/foodadminpart/show-all-item" class="btn">Cancel</a>
				</div>
			</form>		
		</div>
	</div><!--/span-->
</div><!--/row-->
@endsection

@section('essentialScript')
<script>
$(document).ready(function() {
	var quantityValue = $(".quantity").val();
	$(".alert.alert-success").fadeOut(3000);
	$(".quantity").on("input",function(){
		if(isNaN(this.value)){
			alert('You Must Enter a Valid Number');
			$(this).val(quantityValue);
		}
	});	
});
</script>
@endsection