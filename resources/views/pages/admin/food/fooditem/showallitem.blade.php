@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/foodadminpart/food-item">Food Item Manager</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a>Show All Item</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">		
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon list"></i><span class="break"></span>Food Items</h2>
			
		
		<div class="box-content">
			@if (session('status'))
			    <div class="alert alert-success">
			    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			        {{ session('status') }}
			    </div>
			@endif
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					  <th>Serial</th>
					  <th>Name of Item</th>					  
					  <th>Day</th>
					  <th>Quantity</th>
					  <th>How Many Times</th>
					  <th>Category</th>
					  <th>Action</th>
				  </tr>
			  </thead>   
			  <tbody>
			  @foreach($foodlists as $index => $foodlist)
				<tr>
					<td>{!! $index+1 !!}</td>
					<td class="center">{!! $foodlist->NameOfItem !!}</td>					
					<td class="center">{!! $foodlist->DayFor !!}</td>
					<td class="center">{!! $foodlist->Quantity !!} gm</td>
					<td class="center">{!! $foodlist->HowManyTimes !!}</td>
					<td class="center">{!! $foodlist->NameOfGroup !!}</td>
					<td class="center">
						<!-- <a class="btn btn-success" href="#">
							<i class="halflings-icon white zoom-in"></i>  
						</a> -->
						<a href="/foodadminpart/item-edit-info/{!! $foodlist->id !!}" title data-rel="tooltip" class="btn btn-info" data-original-title="Edit">
							<i class="halflings-icon white edit"></i>  
						</a>
						<a class="btn btn-danger" href="/Del/{!! $foodlist->id !!}">
							<i class="halflings-icon white trash"></i> 
						</a>
					</td>
				</tr>
				@endforeach							
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->
	<!-- <div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Attention</h3>
		</div>
		<div class="modal-body">
			<p>Are you sure you want to delete the item?</p>
		</div>
		<div class="modal-footer">
			<a id="deleteConfirm" href="/foodadminpart/delete-food-item/" class="btn btn-danger">Yes</a>
			<a href="#" class="btn btn-primary" data-dismiss="modal">No</a>			
		</div>
	</div> -->
</div><!--/row-->
@endsection
@section('essentialScript')
<script>
/*$(document).ready(function() {    		
	$('.dataDelete').click(function(){
		var id = $(this).attr('data');
		var hrefModaLink = $('#deleteConfirm').attr('href');
		var link = hrefModaLink + id;
		$('#deleteConfirm').attr('href',link);
	});
});
*/

</script>
@endsection