@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/foodadminpart/food-item">Food Item Management</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/foodadminpart/select-day">Select Day</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a>Create Food Item</a>		
	</li>
</ul>
@endsection
@section('content')
  
<div class="row-fluid sortable">	  
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon edit"></i><span class="break"></span>Create Item</h2>
			
		
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		<div class="box-content">
			<form action="/foodadminpart/food-item" class="form-horizontal" method="post">
			<div class="span6">			
			{!! csrf_field() !!}			
				<fieldset>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Name Of Item</label>
					<div class="controls">
					  <input name="itemname" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('itemname') !!}">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="selectError">Select Category</label>
					<div class="controls">
					  <select name="itemCategory" id="selectError" data-rel="chosen">
						<option value="">Select One</option>
					  @foreach($category as $data)
						<option value="{!! $data->id !!}">{!! $data->NameOfGroup !!}</option>
					  @endforeach
					  </select>					  
					</div>							
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Day</label>
					<div class="controls">
					  <input name="day" class="input-xlarge disable" id="focusedInput" type="text" value="{!! $day !!}" readonly="">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Quantity(gram)</label>
					<div class="controls">
					  <input name="qty" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('qty') !!}">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Distribution Formula</label>
					<div class="controls">
					  <input name="df" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('df') !!}">
					</div>
				  </div>				  
				  <div class="control-group">
					<label class="control-label" for="selectError">How many times</label>
					<div class="controls">
					  <select name="hmt" id="none" data-rel="chosen">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>					  
					  </select>
					</div>
				  </div>				  
				  
				</fieldset>
			  
			  </div>

			  <div class="span5">		
				<div class="tooltip-demo well">
				  <p class="muted" style="margin-bottom: 0;">
				  To create distribution formula you have to use only provided keyword which is given below:-
				  <br>
				  @foreach($keywords as $keyword)
				  {!! $keyword->keyword !!}<br>
				  @endforeach
				  </p>
				</div>                                  
			  </div>

			  <div class="form-actions span12" style="margin:0; width:102%; margin-left:-1%;">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="/foodadminpart/select-day" class="btn">Cancel</a>
				  </div>
			</form>
		</div>
		

	</div><!--/span-->
</div><!--/row-->
@endsection

@section('essentialScript')
<script>
$(document).ready(function() {    
	$(".alert.alert-success").fadeOut(3000);
});
</script>
@endsection