@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Dashboard</a></li>
</ul>
@endsection
@section('content')
<div class="row-fluid">

	<a href="users" class="quick-button metro blue span2">
		<i class="icon-list"></i>
		<p>User <br> Management</p>		
	</a>
	<a href="servicesPart/" class="quick-button metro blue span2">
		<i class="icon-briefcase"></i>
		<p>Services<br> Management</p>
	</a>

	<a href="foodadminpart/" class="quick-button metro yellow span2">
		<i class="icon-group"></i>
		<p>Manage Food<br> Section</p>		
	</a>
	<a href="manageopdsection/" class="quick-button metro red span2">
		<i class="icon-briefcase"></i>
		<p>Manage OPD<br> Section</p>
	</a>
	<a href="doctorsAdmin/" class="quick-button metro green span2">
		<i class="icon-briefcase"></i>
		<p>Manage Doctor<br>Info</p>
	</a>

	<a href="/voucher/create" class="quick-button metro blue span2">
		<i class="icon-book"></i>
		<p>Todays' <br>Transaction</p>		
	</a>
	<div class="clearfix"></div>
					
</div><!--/row-->

<br><br><div class="row-fluid">
	<a href="/AdminViewVoucher" class="quick-button metro purple span2">
		<i class="icon-book"></i>
		<p>Voucher List</p>		
	</a>

</div><!--/row-->
@endsection