@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Doctor Management</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 style="margin-left:15px">Doctor Management</h2>
			<div style="width:100%; max-height:380px; overflow: auto; font-size:12px">
				<table width="100%" class="table table-bordered" style="font-size:12px">
					<tr>
						<th align="left">Name</th>
						<th align="center">Address</th>
						<th align="center">Phone</th>
						<th align="center">Specialization</th>
						<th align="center">Designation</th>
						<th align="center">Rate</th>
						<th width="30px" align="center">Vat (%)</th>
						<th width="50px" align="center">Service<br>Charge (%)</th>
						<th align="right">Edit/Delete</th>
					</tr>
					@foreach($doctors as $doctor)
					<tr>
						<td>{{$doctor->DocTitle}} {{$doctor->name}}</td>
						<td>{{$doctor->DocAddress}}</td>
						<td>{{$doctor->DocPhone}}</td>
						<td>{{$doctor->DocSpecialization}}</td>
						<td>{{$doctor->DocDesignation}}</td>
						<td>{{$doctor->Rate}}</td>
						<td>{{$doctor->VatRate}}</td>
						<td>{{$doctor->ServiceChargeRate}}</td>
						<td>
							{!! Form::open(array('url' => 'doctorsAdmin/' . $doctor->id)) !!}
							{!! Form::hidden('_method', 'DELETE') !!}

							<a href="/doctorsAdmin/{!! $doctor->id !!}/edit" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>

							<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>

							{!! Form::close() !!}
						</td>
					</tr>
					@endforeach
				</table>	
			</div>	
			<br>	

			<a href="/doctorsAdmin/create" class="btn btn-success" style="margin-left:20%">Add new Doctor</a>

		</div>
	</div>
</div>




@endsection





