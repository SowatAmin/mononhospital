@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>		
		<a href="/home">Manage Users</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Add User</a></li>
</ul>
@endsection
@section('content')
<!--<form action="addUser" method="post" class="form-horizontal" role="form">	-->

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>		            
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
{!! Form::open(array('route' => 'doctorsAdmin.store', 'class' =>'form-horizontal'))!!}
<div class="col-sm-12">
	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputEmail3"  class="control-label">Title: </label>
		</div>
		<div class="col-sm-2">
			<input type="text" class="form-control" name="DocTitle">
		</div>

		<div class="col-sm-1">
			<label class="control-label" align="right">Name: </label>
		</div>
		<div class="col-sm-7">
			<input type="text" class="form-control" name="name">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputPassword3" class="control-label">Phone Number: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="DocPhone">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputEmail3" class="control-label">Address: </label>
		</div>
		<div class="col-sm-10">
			<textarea name="DocAddress" class="form-control"></textarea>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputEmail3" class="control-label">Specialization: </label>
		</div>
		<div class="col-sm-4">
			<select name="DocSpecialization">
				@foreach($specializations as $specialization)

				<option>{!! $specialization->DocSpecialization !!}</option>

				@endforeach
			</select>
		</div>

		<div class="col-sm-2">
			<label for="inputEmail3" class="control-label">Designation: </label>
		</div>
		<div class="col-sm-4">
			<select width="100%" name="DocDesignation">
				@foreach($designations as $designation)

				<option>{!! $designation->DocDesignation !!}</option>

				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group">
		
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputPassword3" class="control-label">Rate: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="Rate">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputPassword3" class="control-label">Vat: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="VatRate">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-2">
			<label for="inputPassword3" class="control-label">Service Charge: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="ServiceChargeRate">
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10" style="margin-left:20%">
			<button type="submit" class="btn btn-success">Add Doctor</button>
		</div>
	</div>
</div>
{!!Form::close()!!}
<!--</form> -->

@endsection