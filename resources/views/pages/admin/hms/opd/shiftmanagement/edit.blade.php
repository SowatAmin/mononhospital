@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/manageopdsection">Manage OPD Section</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="{!! URL::previous() !!}">Shift Management</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="#">Create a new time shift</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon edit"></i><span class="break"></span>Create a New Time Shift</h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
		</div>
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		<div class="box-content">
			<form action="/opdshiftmanagement" class="form-horizontal" method="post">
			{!! csrf_field() !!}
				<fieldset>
				  <div class="control-group @if ($errors->has('shiftname'))error @endif">
					<label class="control-label" for="focusedInput">Enter a time shift name</label>
					<div class="controls">						
					  	<input name="shiftname" class="input-xlarge focused" id="focusedInput" type="text" >
					  	@if ($errors->has('shiftname'))
					  	<span class="help-inline">
					  	{!! $errors->first('shiftname') !!}
					  	</span>
					  	@endif
					</div>
				  </div>				  
				  <div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="{!! URL::previous() !!}" class="btn">Cancel</a>
				  </div>
				</fieldset>
			</form>		
		</div>
	</div><!--/span-->

</div><!--/row-->
@endsection

@section('essentialScript')
<script>
$(document).ready(function() {    
	$(".alert.alert-success").fadeOut(3000);
});
</script>
@endsection