@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/manageopdsection">Manage OPD Section</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="{!! URL::previous() !!}">Shift Management</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="#">Show all time shift</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon list"></i><span class="break"></span>All time shifts</h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
		</div>
		<div class="box-content">			
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					  <th>Serial</th>
					  <th>Name of the Time Shift</th>					  
					  <th>Action</th>
				  </tr>
			  </thead>   
			  <tbody>
			  	@foreach($allData as $index => $data)
				<tr>
					<td>{!! $index+1 !!}</td>
					<td class="center">{!! $data->shift_name !!}</td>					
					<td class="center">
						<!-- <a class="btn btn-success" href="#">
							<i class="halflings-icon white zoom-in"></i>  
						</a> -->
						<a href="/opdshiftmanagement/{!! $data->id !!}/edit" title data-rel="tooltip" class="btn btn-info" data-original-title="Edit">
							<i class="halflings-icon white edit"></i>  
						</a>
						<a class="dataDelete btn btn-danger" href="#myModal" data-toggle="modal" title data-rel="tooltip" data-original-title="Delete" data="{!! $data->id !!}">
							<i class="halflings-icon white trash"></i> 
						</a>
					</td>
				</tr>
				@endforeach							
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Attention</h3>
		</div>
		<div class="modal-body">
			<p>Are you sure you want to delete the time shift?</p>
		</div>
		<div class="modal-footer">
			<a id="deleteConfirm" href="/foodadminpart/delete-food-item/" class="btn btn-danger">Yes</a>
			<a href="#" class="btn btn-primary" data-dismiss="modal">No</a>			
		</div>
	</div>
</div><!--/row-->
@endsection
@section('essentialScript')
<script>
$(document).ready(function() {    		
	$('.dataDelete').click(function(){
		var id = $(this).attr('data');
		var hrefModaLink = $('#deleteConfirm').attr('href');
		var link = hrefModaLink + id;
		$('#deleteConfirm').attr('href',link);
	});
});
</script>
@endsection