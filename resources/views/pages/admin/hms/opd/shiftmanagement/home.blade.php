@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="{!! URL::previous() !!}">Manage OPD Section</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>Shift Management</li>
</ul>
@endsection
@section('content')
<div class="row-fluid">	

	<a href="/opdshiftmanagement/create" class="quick-button metro blue span2">
		<i class="icon-group"></i>
		<p>Create a new time shift</p>		
	</a>
	<a href="/opdshiftmanagement/showdata" class="quick-button metro green span2">
		<i class="icon-briefcase"></i>
		<p>Show all time shift</p>		
	</a>
	<div class="clearfix"></div>
					
</div><!--/row-->
@endsection