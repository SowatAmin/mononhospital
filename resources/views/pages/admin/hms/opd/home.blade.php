@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>Manage OPD Section</li>
</ul>
@endsection
@section('content')
<div class="row-fluid">	

	<a href="/opdshiftmanagement/" class="quick-button metro blue span2">
		<i class="icon-group"></i>
		<p>Time Shift Management</p>		
	</a>
	<a href="#" class="quick-button metro green span2">
		<i class="icon-briefcase"></i>
		<p>Service Management</p>		
	</a>
	<a href="#" class="quick-button metro red span2">
		<i class="icon-briefcase"></i>
		<p>OPD Fee</p>		
	</a>
	
	<div class="clearfix"></div>
					
</div><!--/row-->
@endsection