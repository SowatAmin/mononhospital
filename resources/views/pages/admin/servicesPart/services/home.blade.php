@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">

			@foreach($masterCategories as $masterCategory)
			<h2>{{$masterCategory->name}}</h2>
			
			@foreach($categories as $category)
			@if($category['masterCatId'] == $masterCategory['id'])

			<h3 style="margin-left:2%">{{$category->name}}</h3>

			<table class="table" id="" style="margin-left:4%">
				<thead>
					<tr  style="border-bottom:1px solid black">
						<th style="border-bottom:1px solid black">Services</th>
						<th style="border-bottom:1px solid black">Type</th>
						<th style="border-bottom:1px solid black">Rate</th>
						<th style="border-bottom:1px solid black">Service Charge</th>
						<th class="pull-right">Edit/Delete</th>
					</tr>
				</thead>
				@foreach($services as $service)
				@if($service['serviceCatId'] == $category['id'])			
				
				<tbody>
					<tr  style="border-bottom:1px solid black">
						<td style="border-bottom:1px solid black">{!!$service->name!!}</td>
						<td style="border-bottom:1px solid black; text-align:center">{!!$service->type!!}</td>
						<td style="border-bottom:1px solid black; text-align:center">{!!$service->rate!!}</td>
						<td style="border-bottom:1px solid black; text-align:center">{!!$service->sc!!}</td>

						<td class="pull-right">
							{!! Form::open(array('url' => 'servicesPart/services/' . $data->id)) !!}
							{!! Form::hidden('_method', 'DELETE') !!}
							<a href="/servicesPart/category/{!! $data->id !!}/edit" class="btn btn-primary">Edit</a>
							<button type="submit" class="btn btn-danger">Delete</button>
							{!! Form::close() !!}
						</td>
					</tr>
					
				</tbody>

				@endif
				@endforeach								
			</table>
			<br>
			@endif
			@endforeach

			@endforeach
			<a href="services/create" class="btn btn-success" style="margin-left:40%">Add new Services</a>

		</div>
	</div>
</div>




@endsection





