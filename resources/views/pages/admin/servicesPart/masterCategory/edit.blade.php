@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif

<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Personal Info</h3>
		
		{!! Form::open(['url'=>'patientAdmission']) !!}
		<div class="col-md-12">
			<div class="col-md-2">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:40px;text-align:left">Reg No.</span>
					<input type="text" name="id" id="id" class="form-control" style="height:auto" value="{{$patient->id}}">
				</div>
			</div>
			<div class="col-md-3">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:60px;text-align:left">Bed/cabin:</span>
					<select class="select form-control" name="mastercat" id="mastercat">
						@foreach($beds as $bed)

						<option value="{{$bed->name}}">{{$bed->name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:10px;text-align:left">*Name:</span>
					<input type="text" name="name" class="form-control" style="height:auto">
				</div>
			</div>
			<div class="col-md-3">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:80px;text-align:left">Status</span>
					<select class="select form-control" name="admissionStatus">
						<option value="admitted">Admitted</option>
						<option value="released">Released</option>
						<option value="booking">Booking</option>
					</select>
				</div>
			</div>



			<!--Nav Tabs-->
			<div style="margin-top:20px" id="patientFullDetails" class="col-md-12">
				<div id="navigation">
					<ul class="nav nav-tabs nav-justified">
						<li class="active">
							<a data-toggle="tab" href="#personalInfo">Personal</a>
						</li>
						<li>
							<a data-toggle="tab" href="#dailyRentAndCharge">Daily Rent &amp; Charge</a>
						</li>
						<li>
							<a data-toggle="tab" href="#accountsLedger">Accounts Ledger</a>
						</li>
						<li>
							<a data-toggle="tab" href="#investigation">Investigation</a>
						</li>
						<li>
							<a data-toggle="tab" href="#prescription">Prescription</a>
						</li>
						<li>
							<a data-toggle="tab" href="#visitor">Visitor</a>
						</li>
					</ul>
				</div>
				<div class="tab-content">
					<!--Personal Info-->
					<div id="personalInfo" class="tab-pane fade in active" style="margin-top:15px">
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Marital Status</span>
								<select name="maritalStatus" class="select form-control">
									<option value="Married">Married</option>
									<option value="Unmarried">Unmarried</option>
								</select>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Nationality</span>
								<select name="nationality" class="select form-control">
									<option value="Bangladeshi">Bangladeshi</option>
									<option value="Other">Other</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Sex</span>
								<select name="sex" class="select form-control">
									<option value="Male">Male</option>
									<option value="Female">Female</option>
									<option value="Others">Others</option>
								</select>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Religion</span>
								<select name="religion" class="select form-control">
									<option value="Islam">Islam</option>
									<option value="Christian">Christian</option>
									<option value="Hindu">Hindu</option>
									<option value="Shanatan">Shanatan</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Age</span>
								<input type="text" name="age" class="form-control" style="height:auto">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Blood Group</span>
								<select name="bloodGroup" class="select form-control">
									<option value="A+">A+</option>
									<option value="B+">B+</option>
									<option value="O+">O+</option>
									<option value="AB+">AB+</option>
									<option value="A-">A-</option>
									<option value="B-">B-</option>
									<option value="O-">O-</option>
									<option value="AB-">AB-</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Profession</span>
								<select class="select form-control" id="profession"
								name="profession">
								<option value="Architect">Architect</option>
								<option value="Businessman">Businessman</option>
								<option value="D.G.M Bangladesh Bank">D.G.M Bangladesh Bank</option>
								<option value="Housewife">Housewife</option>
								<option value="Phychisian">Phychisian</option>
								<option value="Service Holder">Service Holder</option>
								<option value="Student">Student</option>
								<option value="Teaching">Teaching</option>
							</select>
						</div>
						<div class="input-group" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Present Address</span>
							<textarea name="message2" rows="3" name="presentAddress" class="form-control"></textarea>
						</div>
						<div class="input-group" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Permanent Address</span>
							<textarea name="message2" rows="3" name="permanentAddress" class="form-control"></textarea>
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon">Diagnosis</span>
							<select class="select form-control" name="diagnosis">
								<option>Select an option</option>
								@foreach($diagnosis as $diag)
								<option value="{{$diag->name}}">{{$diag->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Guardian's Name</span>
							<input type="text" name="guardianName" class="form-control" aria-describedby="basic-addon1">


							<span class="input-group-btn" style="width:0px;"></span>
							<input type="text" name="guardianRel" class="form-control" aria-describedby="basic-addon1" placeholder="Relation">


						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">*Guardian's Phone</span>
							<input type="text" name="guardianPhone" class="form-control" aria-describedby="basic-addon1">
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Alt. Guardian Name</span>
							<input type="text" name="altGuardianName" class="form-control" aria-describedby="basic-addon1">

							<span class="input-group-btn" style="width:0px;"></span>
							<input type="text" name="altGuardianRel" class="form-control" aria-describedby="basic-addon1" placeholder="Relation">


						</div>
						<div class="input-group" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Alt.Guardian Address</span>
							<textarea name="altGuardianAddress" rows="3" class="form-control"></textarea>
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Alt. Guardian Phone</span>
							<input type="text" name="altGuardianPhone" class="form-control" aria-describedby="basic-addon1">
						</div>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>
					<div class="col-sm-6">
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">*Consultant</span>
							<select class="select form-control" name="assignedConsultant" >
								<option>Select an option</option>		
								@foreach($consultants as $consustant)
								<option value="{{$consustant->name}}">{{$consustant->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">*M/O on duty</span>
							<select class="select form-control" name="moOnDuty" >
								<option>Select an option</option>		
								@foreach($mos as $mo)
								<option value="{{$mo->name}}">{{$mo->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">*Manager</span>
							<select class="select form-control" name="manager" >
								<option>Select an option</option>		
								@foreach($managers as $manager)
								<option value="{{$manager->name}}">{{$manager->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group input-group-sm" style="margin-top:20px">
							<div class="col-sm-4">
								<input type="checkbox" value="Police Case" name="policeCase">Police Case
							</div>
							<div class="col-sm-4">
								<input type="checkbox" value="Incoming Call" name="incomingCall">Incoming Call
							</div>
							<div class="col-sm-4">
								<input type="checkbox" name="outgoingCall" value="outgoingCall">Outgoing Call
							</div>
						</div>
						<div class="form-group" style="margin-top:20px">
							<div class="col-sm-4">
								<input type="checkbox" value="Visitor" name="visitor">Visitor Allowed
							</div>
							<div class="col-sm-4">
								<input type="checkbox" value="" name="extraFood">Extra Food
							</div>
							<div class="col-sm-4">
								<input type="checkbox" name="cigarette" value="cigarette">Cigarette
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Booking Date &amp; Time</span>
							<input type="date" name="bookingDate" class="form-control inline">
							<input class="form-control" name="bookingTime" type="time">
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Admission Date &amp; Time</span>
							<input type="date" name="admissionDate" id="admissionDate" class="form-control">
							<input class="form-control" name="admissionTime" type="time">
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" id="basic-addon1">Discharge Date &amp; Time</span>
							<input type="date" name="releaseDate" class="form-control">
							<input class="form-control" name="releaseTime" type="time">
						</div>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>
				</div>




				<!--Daily Rent and Charge-->
				<div id="dailyRentAndCharge" class="tab-pane fade">
					<div class="col-sm-12">
						<div class="col-sm-6" style="margin-top:25px">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Master Category</span>
								<select class="select form-control" id="master_catagory" name="master_catagory">
									<option>Select an option</option>		
									@foreach($mastercatagories as $mastercategory)
									<option value="{{$mastercategory->id}}">{{$mastercategory->name}}</option>
									@endforeach
								</select>
							</div>


							<div class="input-group input-group-sm" style="margin-top:15px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Category</span>
								<select class="select form-control" name="subcategory" id="subcategory" >

								</select>
							</div>

							<div class="input-group input-group-sm" style="margin-top:15px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Services</span>
								<select class="select form-control" id="services" name="services">
									<option value="First Choice">
										First Choice
									</option>
									<option value="Second Choice">
										Second Choice
									</option>
									<option value="Third Choice">
										Third Choice
									</option>
								</select>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Quantity</span>
								<input type="text" class="form-control" style="height:auto">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Rate</span>
								<input type="text" class="form-control" style="height:auto" id="rate" name="rate">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Vat</span>
								<input type="text" class="form-control" style="height:auto" id="vat" name="vat">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Service Charge</span>
								<input type="text" class="form-control" style="height:auto" id="servicecharge" name="servicecharge">
							</div>
						</div>

						<div class="col-sm-12" >
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Description</span>
								<textarea name="message2" rows="2" class="form-control"></textarea>
							</div>
						</div>

						<div class="col-sm-12">									
							<div class="btn-group" style="margin-top:20px;float:right;">
								<button type="button" class="btn btn-primary">
									<span class="glyphicon glyphicon-plus"></span> Add
								</button>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>

					<!--Table of Data for Daily Rent and Charge-->
					<div class="col-sm-12">
						<table class="table table-bordered">
							<tr>
								<th>ID</th>
								<th>Reg No.</th>
								<th>Master Category</th>
								<th>Category</th>
								<th>Services</th>
								<th>Quantity</th>
								<th>Vat</th>
								<th>Service Charge</th>
								<th>Description</th>
							</tr>

						</table>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>
				</div>





				<!--Accounts Ledger-->
				<div id="accountsLedger" class="tab-pane fade">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Date</span>
								<input class="form-control" type="date" class="form-control" name="patient_dischargeDate">
							</div>									
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Master Category</span>
								<select class="select form-control" id="master_catagory" name="master_catagory">
									<option>Select an option</option>		
									@foreach($mastercatagories as $mastercategory)
									<option value="{{$mastercategory->id}}">{{$mastercategory->name}}</option>
									@endforeach
								</select>
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Category</span>
								<select class="select form-control" name="subcategory" id="subcategory" >

								</select>
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Services</span>
								<select class="select form-control" id="services" name="services">
									<option value="First Choice">
										First Choice
									</option>
									<option value="Second Choice">
										Second Choice
									</option>
									<option value="Third Choice">
										Third Choice
									</option>
								</select>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Quantity</span>
								<input type="text" class="form-control" style="height:auto">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Rate</span>
								<input type="text" class="form-control" style="height:auto">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Vat</span>
								<input type="text" class="form-control" style="height:auto">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Service Charge</span>
								<input type="text" class="form-control" style="height:auto">
							</div>
						</div>

						<div class="col-sm-12">
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Remarks</span>
								<textarea name="message2" rows="2" class="form-control"></textarea>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Non-Profit Bill</span>
								<label class="form-control">0</label>
							</div>										
						</div>
						<div class="col-sm-4">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Consultant Bill</span>
								<label class="form-control">0</label>
							</div>										
						</div>
						<div class="col-sm-4">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Hospital Bill</span>
								<label class="form-control">0</label>
							</div>										
						</div>
						<div class="col-sm-3">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Total Bill</span>
								<label class="form-control">0</label>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Payment</span>
								<label class="form-control">0</label>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Discount</span>
								<label class="form-control">0</label>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Due</span>
								<label class="form-control">0</label>
							</div>
						</div>
						<div class="col-sm-12">									
							<div class="btn-group btn-group-justified" style="margin-top:20px">
								<div class="btn-group">
									<button type="button" class="btn btn-primary">
										<span class="glyphicon glyphicon-pencil"></span> Add Service
									</button>
								</div>
								<div class="btn-group">
									<button type="submit" onclick="getSummaryBill()" class="btn btn-info">
										<span class="glyphicon glyphicon-list-alt"></span> Summary Bill
									</button>
								</div>
								<div class="btn-group">
									<button type="submit" onclick="getDetailedBill()" class="btn btn-info">
										<span class="glyphicon glyphicon-align-justify"></span> Detailed Bill
									</button>
								</div>
								<div class="btn-group">
									<button type="submit" onclick="getHospitalBill()" class="btn btn-info">
										<span class="glyphicon glyphicon-list"></span> Hospital Bill
									</button>
								</div>
								<div class="btn-group">
									<button type="submit" onclick="getConsultantBill()" class="btn btn-info">
										<span class="glyphicon glyphicon-list-alt"></span> Consultant Bill
									</button>
								</div>
								<div class="btn-group">
									<button type="submit" onclick="getNonProfitBill()" class="btn btn-info">
										<span class="glyphicon glyphicon-list-alt"></span> Non-Profitable Bill
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>

					<!--Table of Data for Accounts Ledger-->
					<div class="col-sm-12">
						<table class="table table-bordered">
							<tr>
								<th>Bill No.</th>
								<th>Reg No.</th>
								<th>Bed/Cabin No.</th>
								<th>Bill Date</th>
								<th>Master Category</th>
								<th>Category</th>
								<th>Services</th>
								<th>Quantity</th>
								<th>Vat</th>
								<th>Service Charge</th>
								<th>Billed By</th>
								<th>Remarks</th>
							</tr>

						</table>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>
				</div>








				<!--Investigations-->
				<div id="investigation" class="tab-pane fade">							
					<div class="col-sm-12">								
						<div class="col-sm-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon">Date</span>
								<input class="form-control" type="date" class="form-control" name="investigationDate">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Send Date and Time</span>
								<input type="date" class="form-control" name="investigationSendDate">
								<span class="input-group-btn" style="width:0px;"></span>
								<input class="form-control" name="investigationSendTime" type="time">

							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Delivery Date and Time</span>
								<input type="date" class="form-control" name="investigationDeliveryDate">
								<span class="input-group-btn" style="width:0px;"></span>
								<input class="form-control" name="investigationDeliveryTime" type="time">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon">Laboratory</span>
								<input class="form-control" type="text" class="form-control" name="laboratory">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Investigations</span>
								<textarea rows="7" class="form-control" name="Investigations"></textarea>
							</div>									
						</div>
						<div class="col-sm-12">									
							<div class="btn-group" style="margin-top:20px;float:right;">
								<button type="button" class="btn btn-primary">
									<span class="glyphicon glyphicon-plus"></span> Add
								</button>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>

					<!--Table of Data for Investigation-->
					<div class="col-sm-12">
						<table class="table table-bordered">
							<tr>
								<th>Bill No.</th>
								<th>Reg No.</th>
								<th>Bed/Cabin No.</th>
								<th>Date</th>
								<th>Description</th>
								<th>Laboratory</th>
								<th>Send Date</th>
								<th>Send Time</th>
								<th>Delivery Date</th>
								<th>Delivery Time</th>
								<th>Billed By</th>
							</tr>

						</table>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>
				</div>





				<!--Prescription-->
				<div id="prescription" class="tab-pane fade">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<label class="control-label">Treatment Discharge With Advice</label>								
							<textarea class="form-control" rows="6" name="tda" ></textarea>
						</div>
						<div class="col-sm-6">									
							<label class="control-label " for="investigation">Investigation</label>
							<textarea class="form-control" rows="6" name="investigation"></textarea>
						</div>
						<div class="col-sm-6">
							<label class="control-label " for="message4">Notes</label>
							<textarea class="form-control" rows="6" name="notes" ></textarea>
						</div>
						<div class="col-sm-6">
							<label class="control-label " for="treatment">Treatment Given</label>
							<textarea class="form-control" rows="6" id="treatment" name="treatment" ></textarea>
						</div>
					</div>

					<div class="col-sm-12" style="margin-top:20px">
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button type="button" onclick="getDorbForm()" class="btn btn-primary">
									<span class="glyphicon glyphicon-list-alt"></span>  DORB Form
								</button>
							</div>
							<div class="btn-group">
								<button type="submit" onclick="getEmsForm()" class="btn btn-primary">
									<span class="glyphicon glyphicon-list-alt"></span>  EMS Form
								</button>
							</div>
							<div class="btn-group">
								<button type="button" onclick="getPaRoleForm()" class="btn btn-primary">
									<span class="glyphicon glyphicon-list-alt"></span>  Pa-Role Form
								</button>
							</div>
							<div class="btn-group">
								<button type="button" onclick="getDischargeCertificate()" class="btn btn-primary">
									<span class="glyphicon glyphicon-list-alt"></span>  Discharge Certificate
								</button>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>
				</div>







				<!--Visitor-->
				<div id="visitor" class="tab-pane fade">
					<div class="col-sm-4">
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" style="min-width:10px;text-align:left">Visitor Name</span>
							<input type="text" name="visitorName" id="visitorName" class="form-control">
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" style="min-width:10px;text-align:left">Relation to the Patient</span>
							<input type="text" name="visitorRelation" id="visitorRelation" class="form-control">
						</div>
						<div class="input-group input-group-sm" style="margin-top:10px">
							<span class="input-group-addon" style="min-width:10px;text-align:left">Contact Number</span>
							<input type="text" name="visitorPhone" id="visitorPhone" class="form-control">
						</div>
						<div class="input-group" style="margin-top:10px">
							<span class="input-group-addon" style="min-width:10px;text-align:left">Remarks</span>
							<textarea name="visitorRemark" id="visitorRemark" rows="3" class="form-control"></textarea>
						</div>
						<div class="col-sm-12">									
							<div class="btn-group" style="margin-top:20px;float:right;">
								<button type="button" class="btn btn-primary" name="visitorAdd" id="visitorAdd">
									<span class="glyphicon glyphicon-plus"></span> Add
								</button>
							</div>
						</div>
					</div>

					<!--Table of Data for Visitors-->
					<div class="col-sm-8">
						<table class="table table-bordered" name="visitorTable" id="visitorTable">
							<tr>
								<th>Visitor's Name</th>
								<th>Relation to Patient</th>
								<th>Contact No.</th>
								<th>Remarks</th>
							</tr>


						</table>
					</div>
					<div class="col-sm-12">
						<hr style="border: 0;height: 1px;background: #333;">
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="btn-group btn-group-justified">
					<div class="btn-group">
						<button type="button" class="btn btn-primary">
							<span class="glyphicon glyphicon-pencil"></span> New
						</button>
					</div>
					<div class="btn-group">
						<button type="submit" class="btn btn-success">
							<span class="glyphicon glyphicon-save"></span> Save
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-danger">
							<span class="glyphicon glyphicon-trash"></span> Delete
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-primary">
							<span class="glyphicon glyphicon-folder-open"></span> Browse
						</button>
					</div>
					<div class="btn-group">
						<button type="button" onclick="getPatientInfoReport()" class="btn btn-info">
							<span class="glyphicon glyphicon-print"></span> View
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-success">
							<span class="glyphicon glyphicon-floppy-disk"></span> Release
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-primary">
							<span class="glyphicon glyphicon-retweet"></span> Re-Admit
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-info">
							<span class="glyphicon glyphicon-print"></span> Print
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-success">
							<span class="glyphicon glyphicon-briefcase"></span> Payments
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-primary">
							<span class="glyphicon glyphicon-minus"></span> Discount
						</button>
					</div>
					<div class="btn-group">
						<button type="button" class="btn btn-danger">
							<span class="glyphicon glyphicon-off"></span> Exit
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}

</div>

