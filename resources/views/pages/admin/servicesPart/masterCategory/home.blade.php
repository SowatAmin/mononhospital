@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr style="border-bottom:1px solid black">
						<th style="border-bottom:1px solid black">Master Categories</th>
						<th class="pull-right">Edit/Delete</th>
					</tr>
				</thead>
				<tbody>
					@foreach($tableData as $tableData)
					<tr style="border-bottom:1px solid black">
						<td  style="border-bottom:1px solid black">{{$tableData->name}}</td>						
						<td class="pull-right">						
							
							{!! Form::open(array('url' => 'servicesPart/masterCategory/' . $tableData->id)) !!}
							{!! Form::hidden('_method', 'DELETE') !!}
							<a href="masterCategory/{!! $tableData->id !!}/edit" class="btn btn-primary">Edit</a>
							<button type="submit" class="btn btn-danger">Delete</button>
							{!! Form::close() !!}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<a href="masterCategory/create" class="btn btn-success" style="margin-left:40%">Add new Master Category</a>
		</div>
	</div>
</div>


<!--<form action="addUser" method="post" class="form-horizontal" role="form">	-->

<!--{!! Form::open(array('route' => 'users.store', 'class' =>'form-horizontal'))!!}-->

<!--</form> -->

@endsection





