@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>		
		<a href="/home">Manage Users</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Add User</a></li>
</ul>
@endsection
@section('content')
<!--<form action="addUser" method="post" class="form-horizontal" role="form">	-->

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>		            
		<li>{{ $errors->first() }}</li>		            
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
{!! Form::open(array('route' => 'servicesPart.masterCategory.store', 'class' =>'form-horizontal'))!!}
<div class="col-sm-20">
	<div class="form-group">
		<div class="col-sm-3">
			<label class="control-label">New Master Category: </label>
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="name">
		</div>
	</div>

	
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10" style="margin-left:20%">
			<button type="submit" class="btn btn-success">Add Master Category</button>
		</div>
	</div>
</div>	
{!!Form::close()!!}
<!--</form> -->

@endsection