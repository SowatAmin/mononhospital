@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Voucher</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New Voucher</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Edit Voucher</h3>		

		{!! Form::model($voucher, array('route' => array('voucher.update', $voucher), 'class' => 'form-horizontal', 'method' => 'PUT')) !!}
		<div class="col-md-6">
			<div class="input-group input-group-sm">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Voucher No</span>
				<input type="text" name="voucherNumber" id="voucherNumber" class="form-control" style="height:auto" value="{{ $voucher->voucherNumber }}">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Voucher Type</span>
				<select name="voucherType" id="voucherType" class="form-control">
					
					<option>{{ $voucher->voucherType }}</option>

					@foreach($vouchertype as $type)
					<option value="{{ $type->accounttype}}">{{ $type->accounttype}}</option>
					@endforeach

				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Parent Head</span>
				<select name="parentHead" id="parentHead" class="form-control">
					

					<option>{{ $voucher->parentHead }}</option>

					@foreach($accounttype as $atype)
					<option value="{{ $atype->parentHead}}">{{ $atype->parentHead}}</option>
					@endforeach

				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Account Head</span>
				<select name="accountHead" id="accountHead" class="form-control">
					<option>{{ $voucher->accountHead }}</option>
				</select>
			</div>
		</div>

		<div class="col-md-6">
			<div class="input-group input-group-sm">
				<span class="input-group-addon" style="min-width:135px;text-align:left">Date</span>
				<input type="text" name="date" id="date" class="form-control" style="height:auto" value="{{$voucher->date}}">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Payment Mode</span>
				<select name="paymentMode" id="paymentMode" class="form-control">
					<option>{{ $voucher->paymentMode }}</option>
					<option value=""></option>
					<option value="Cash">Cash</option>
					<option value="Bank">Bank</option>
					
				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Available Cash/Bank</span>
				<select name="available_cash_bank" id="available_cash_bank" class="form-control">
					<option>{{ $voucher->available_cash_bank }}</option>
				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Amount</span>
				<input type="text" name="amount" id="amount" class="form-control" style="height:auto" value="{{ $voucher->amount }}">
			</div>
		</div>

		<div class="col-md-12">
			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Description</span>
				<textarea rows="3" name="description" id="description" class="form-control">{{ $voucher->description }}</textarea>
			</div>
		</div>

		<div class="col-sm-12" style="margin-top: 15px">
			<div class="btn-group btn-group-justified">
				<div class="btn-group">
					<a class="btn btn-primary" href="/voucher">
						<span class="glyphicon glyphicon-retweet"></span> New
					</a>
				</div>
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<span class="glyphicon glyphicon-save"></span> Save
					</button>
				</div>
				<div class="btn-group">
					<button type="button" class="btn btn-danger">
						<span class="glyphicon glyphicon-trash"></span> Delete
					</button>
				</div>
				<div class="btn-group">
					<a class="btn btn-primary" href="{{ URL::to('voucher/' . $voucher->voucherNumber) }}">
						<span class="glyphicon glyphicon-book"></span> View
					</a>
				</div>
				<div class="btn-group">
					<a class="btn btn-danger" href="/home">
						<span class="glyphicon glyphicon-off"></span> Exit
					</a>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>


@section('essentialScript')

<script type="text/javascript">
	$("#date").datepicker({ dateFormat: 'yy-mm-dd' });	


	//account head ajax;
	$(document).ready(function() {
		$("#parentHead").on('change',function(e){
			console.log(e);
			var phead=e.target.value;


			$.get('/edit/ajax-phead?phead='+phead,function(data){

				console.log(data);
				$("#accountHead").empty();

			//$("#subcategory").select2({allowClear: true});
			$.each(data,function(index,subcatObj){
				$("#accountHead").append(' <option value="'+subcatObj.childHead+'">'+subcatObj.childHead+'</option> ');

			});
		});

		});

	});

	$('#paymentMode').on('change',function(){

	var mode=$('#paymentMode').val();

	$.get('/edit/ajax-pmode?mode='+mode,function(data){


		console.log(data);
		$("#available_cash_bank").empty();
		$.each(data,function(index,subcatObj){
				$("#available_cash_bank").append(' <option value="'+subcatObj.availableCash_bank+'">'+subcatObj.availableCash_bank+'</option> ');



	});
	});
});



</script>

@endsection

@endsection