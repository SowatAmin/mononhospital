@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Edit Payment</h3>
		<form action="/patientDiscount/update" class="form-horizontal" method="post"> {!! csrf_field() !!}
			<div class="col-md-12">			
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Date</span>
						<input type="input" name="date" id="date" class="form-control" value="{{$discount->date}}">
					</div>
				</div>

				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Ref. of</span>
						<select name="refBy" class="select form-control">
							<option>Hospital</option>
							<option>{{$discount->refBy}}</option>

							@foreach($doctors as $doctor)							
							<option>{{$doctor->name}}</option>							
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Amount</span>
						<input type="text" name="amount" class="form-control" value="{{$discount->amount}}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Description</span>
						<textarea rows="3" name="desc" class="form-control">{{$discount->desc}}</textarea>
					</div>
				</div>
				<input type="hidden" name="regNo" value="{!!$discount->regNo!!}">
				<input type="hidden" name="id" value="{!!$discount->id!!}">
			</div>

			<div class="col-sm-12">
				<hr style="border: 0;height: 1px;background: #333;">
			</div>

			<div class="col-sm-12">
				<div class="btn-group">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<span class="glyphicon glyphicon-pencil"></span> Edit Discount
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@section('essentialScript')
<script>
$("#date").datepicker({ dateFormat: 'yy-mm-dd' });

</script>
@endsection

@endsection