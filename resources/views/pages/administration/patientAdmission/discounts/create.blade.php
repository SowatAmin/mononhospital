@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Payment</h3>
		<form action="/patientDiscount/patient-discount" class="form-horizontal" method="post"> {!! csrf_field() !!}
			<div class="col-md-12">			
				<div class="col-md-4">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Date</span>
						<input type="input" name="date" id="date" class="form-control">
					</div>
				</div>

				<div class="col-md-4">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Ref. of</span>
						<select name="refBy" class="select form-control">
							<option>Hospital</option>

							@foreach($doctors as $doctor)							
							<option>{{$doctor->name}}</option>							
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Amount</span>
						<input type="text" name="amount" class="form-control">
					</div>
				</div>
				<div class="col-md-12" style="margin-top:15px">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Description</span>
						<textarea rows="2" name="desc" class="form-control"></textarea>
					</div>
				</div>
				<input type="hidden" name="regNo" value="{!!$id!!}">
			</div>

			<div class="col-sm-12">
				<hr style="border: 0;height: 1px;background: #333;">
			</div>

			<div class="col-sm-12">
				<div class="btn-group">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<span class="glyphicon glyphicon-plus"></span> Add Discount
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@section('essentialScript')
<script>
$("#date").datepicker({ dateFormat: 'yy-mm-dd' }).datepicker("setDate", new Date());

</script>
@endsection

@endsection