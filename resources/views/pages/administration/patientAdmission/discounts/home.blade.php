@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Patient Admissions</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Discounts</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<div class="btn-group">
			<a class="btn btn-success" href="/patientAdmission/{!! $patient->id !!}/edit"><span class="glyphicon glyphicon-arrow-left"></span> Back to Patient Admission</a>
		</div>
		<h3>Discounts for {{$patient->name}}</h3>
		<div style="margin-top:10px; width:100%; max-height:400px; font-size:13px; overflow: auto">
			<table class="table table-bordered">
				<tr valign="middle">
					<th align="center">ID</th>
					<th align="center">Reg No.</th>
					<th align="center">Date</th>
					<th align="center">Bed Cabin No.</th>
					<th align="center">Description</th>
					<th align="center">Amount</th>
					<th align="center">Recieved By</th>
					<th align="center">Referenced By</th>
					<th align="center">Edit/Delete</th>
				</tr>
				@foreach($discounts as $discount)
				<tr>
					<td align="center">{{$discount->id}}</td>
					<td align="center">{{$discount->regNo}}</td>
					<td align="center">{{$discount->date}}</td>
					<td align="center">{{$discount->bedCabin}}</td>
					<td align="center">{{$discount->desc}}</td>
					<td align="center">{{$discount->amount}}</td>
					<td align="center">{{$discount->discountBy}}</td>
					<td align="center">{{$discount->refBy}}</td>
					<td align="center">
						<a class="btn btn-primary" href="/patientDiscount/edit-patient-discount/{!! $discount->id !!}"><span class="glyphicon glyphicon-pencil"></span></a>
						<a class="btn btn-danger" href="/patientDiscount/delete-patient-discount/{!! $discount->id !!}"><span class="glyphicon glyphicon-remove"></span></a>
					</td>
				</tr>
				@endforeach
			</table>
		</div>

		<hr style="border: 0;height: 1px;background: #333;">

		<div class="col-sm-12">
			
			<div class="btn-group">
				<a class="btn btn-primary" href="/patientDiscount/create-patient-discount/{!! $patient->id !!}"><span class="glyphicon glyphicon-plus"></span> Add New Discount</a>
			</div>
			
		</div>
	</div>
</div>

@section('essentialScript')
<script>

</script>
@endsection

@endsection