@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Edit Daily Rent & Charge</h3>
		<form action="/dailyRent/update" class="form-horizontal" method="post"> {!! csrf_field() !!}
			<div class="col-sm-12">
				<div class="col-sm-6" style="margin-top:25px">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Master Category</span>
						<select class="select form-control" id="master_category" name="master_category">
							<option value="{{ $dailyRent->master_category }}">{{$mCat}}</option>
							@foreach($mastercatagories as $mastercategory)
							<option value="{{$mastercategory->id}}">{{$mastercategory->name}}</option>
							@endforeach
						</select>
					</div>

					<input type="hidden" name="pid" id="pid" value="{{$dailyRent->pid}}">
					<input type="hidden" name="id" id="id" value="{{$dailyRent->id}}">


					<div class="input-group input-group-sm" style="margin-top:15px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Category</span>
						<select class="select form-control" name="subcategory" id="subcategory" >
							<option>{{$dailyRent->subcategory}}</option>
							@foreach($subCatagories as $subCatagory)
							<option value="{{$subCatagory->id}}">{{$subCatagory->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="input-group input-group-sm" style="margin-top:15px">
						<span class="input-group-addon" style="min-width:120px;text-align:left" selected="selected">Services</span>
						<select class="select form-control" id="services" name="services">
							<option>{{$dailyRent->services}}</option>
						</select>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Quantity</span>
						<input type="text" class="form-control" style="height:auto" id="quantity" name="quantity" value="{{$dailyRent->quantity}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Rate</span>
						<input type="text" class="form-control" style="height:auto" id="rate" name="rate" value="{{$dailyRent->rate}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Vat</span>
						<input type="text" class="form-control" style="height:auto" id="vat" name="vat" value="{{$dailyRent->vat}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Service Charge</span>
						<input type="text" class="form-control" style="height:auto" id="servicecharge" name="servicecharge" value="{{$dailyRent->servicecharge}}">
					</div>
				</div>

				<div class="col-sm-12" >
					<div class="input-group" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Description</span>
						<textarea name="dailydesc" id="dailydesc" rows="2" class="form-control"></textarea>
					</div>
				</div>

				<div class="col-sm-12">									
					<div class="btn-group" style="margin-top:20px;float:right;">
						<button type="submit" class="btn btn-primary" id="editDailyTable">
							<span class="glyphicon glyphicon-pencil"></span> Save
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@section('essentialScript')
<script type="text/javascript">
	//daily rent master category

	$(document).ready(function() {
		$("#master_category").on('change',function(e){
			console.log(e);
			var mcat_id=e.target.value;
		//alert(mcat_id);


		$.get('/edit/ajax-subcat?mcat_id='+mcat_id,function(data){

			console.log(data);
			$("#subcategory").empty();
			$.each(data,function(index,subcatObj){
				$("#subcategory").append(' <option value="'+subcatObj.name+'">'+subcatObj.name+'</option> ');



			});
		});
	});
	});

	//daily rent sub-category edit page


	$(document).ready(function() {



		$('#subcategory').on('change',function(e){
			console.log(e);
			var mcat_name=$("#master_category").val();
		//alert(mcat_name);
		var cat_name=$("#subcategory").val();
		//alert(cat_name);
		var tablename="";
		if(mcat_name==1)      {tablename="Bed";}
		else if(mcat_name==2) {tablename="Doctors";}
		else if(mcat_name==3) {tablename="Hospitalservices";}
		else				  {tablename="Nonprofitservices";}

		//alert(tablename);

		$.get('/edit/ajax-cat?cat_name='+cat_name+'&tablename='+tablename,function(data){
			
			console.log(data);
			X=data;
			$("#services").empty();
			$.each(data,function(index,serviceObj){
				
				$("#services").append("<option value="+'"'+serviceObj.name+'"'+">"+serviceObj.name+"</option>");



			});
			
			

		});
	});
	});



	//daily rent&charge services

	$(document).ready(function(){
		$("#services").on('change',function(){

			var indexx=$('#services').val();
		//alert($('#services').val());
		//alert(indexx);

		var count = Object.keys(X).length;

		//console.log(OR);
		for(i=0;i<count;i++)
		{
			if(indexx===X[i].name){

				$("#rate").val(X[i].Rate);
				$("#vat").val(X[i].VatRate);
				$("#servicecharge").val(X[i].ServiceChargeRate);


				//alert($("#rate").val());

				break;
			}		
		}

		ON=X[i].name;
		OR=X[i].Rate;
		OV=X[i].VatRate;
		OSC=X[i].ServizceChargeRate;
		


	});

	});
</script>
@endsection

@endsection