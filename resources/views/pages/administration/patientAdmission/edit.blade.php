@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Patient Admission</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Existing Patient</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">

	<div class="row">
		<h3 style="padding-left:15px">Personal Info</h3>
		
		{!! Form::model($patient, array('route' => array('patientAdmission.update', $patient), 'class' => 'form-horizontal', 'method' => 'PUT')) !!}
		
		<input type="hidden" name="username" id="username" value="{{ $user }}">
		<div class="col-md-12">
			<div class="col-md-2">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:40px;text-align:left">Reg No.</span>
					<input type="text" name="id" id="id" class="form-control" value="{!!$patient['id']!!}" disabled>
					
				</div>
			</div>
			<div class="col-md-3">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:60px;text-align:left">Bed/cabin:</span>
					<input type="text" name="bedCabin" id="bedCabin" class="form-control" style="height:auto" value="{!! $patient->bedCabin !!}" >		
				</div>
			</div>

			<div class="col-md-4">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:10px;text-align:left">*Name:</span>
					<input type="text" name="name" class="form-control" style="height:auto" value="{!!$patient->name!!}">
				</div>
			</div>
			<div class="col-md-3">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:80px;text-align:left">Status</span>
					<select class="select form-control" name="admissionStatus">
						<option value="{{$patient->admissionStatus}}">{{$patient->admissionStatus}}</option>
						<option value="Admitted">Admitted</option>
						<option value="Released">Released</option>
						<option value="Booking">Booking</option>
					</select>
				</div>
			</div>



			<!--Nav Tabs-->
			<div style="margin-top:20px" id="patientFullDetails" class="col-md-12">
				<div id="navigation">
					<ul class="nav nav-tabs nav-justified">
						<li class="active">
							<a data-toggle="tab" href="#personalInfo">Personal</a>
						</li>
						<li>
							<a data-toggle="tab" href="#dailyRentAndCharge">Daily Rent &amp; Charge</a>
						</li>
						<li>
							<a data-toggle="tab" href="#accountsLedger">Accounts Ledger</a>
						</li>
						<li>
							<a data-toggle="tab" href="#investigation">Investigation</a>
						</li>
						<li>
							<a data-toggle="tab" href="#prescription">Prescription</a>
						</li>
						<li>
							<a data-toggle="tab" href="#visitor">Visitor</a>
						</li>
					</ul>
				</div>
				<div class="tab-content">
					

					<!--Personal Info-->
					<div id="personalInfo" class="tab-pane fade in active" style="margin-top:15px">
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Marital Status</span>
								<select name="maritalStatus" class="select form-control">
									<option value="{{$patient->maritalStatus}}">{{$patient->maritalStatus}}</option>
									<option value="Married">Married</option>
									<option value="Unmarried">Unmarried</option>
								</select>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Nationality</span>
								<select name="nationality" class="select form-control">
									<option value="{{$patient->nationality}}">{{$patient->nationality}}</option>
									<option value="Bangladeshi">Bangladeshi</option>
									<option value="Other">Other</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Sex</span>
								<select name="sex" class="select form-control">
									<option value="{{$patient->sex}}">{{$patient->sex}}</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
									<option value="Others">Others</option>
								</select>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Religion</span>
								<select name="religion" class="select form-control">
									<option value="{{$patient->religion}}">{{$patient->religion}}</option>
									<option value="Islam">Islam</option>
									<option value="Christian">Christian</option>
									<option value="Hindu">Hindu</option>
									<option value="Shanatan">Shanatan</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Age</span>
								<input type="text" name="age" class="form-control" style="height:auto" value="{{$patient->age}}">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Blood Group</span>
								<select name="bloodGroup" class="select form-control">
									<option value="{{$patient->bloodGroup}}">{{$patient->bloodGroup}}</option>
									<option value=""></option>
									<option value="A+">A+</option>
									<option value="B+">B+</option>
									<option value="O+">O+</option>
									<option value="AB+">AB+</option>
									<option value="A-">A-</option>
									<option value="B-">B-</option>
									<option value="O-">O-</option>
									<option value="AB-">AB-</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Education</span>
								<select name="education" id="education" class="select form-control">
									<option value="{{$patient->education}}">{{$patient->education}}</option>
									<option value=""></option>
									<option value="SCHOOL">SCHOOL</option>
									<option value="SSC">SSC</option>
									<option value="HSC">HSC</option>
									<option value="O-LEVEL">O-LEVEL</option>
									<option value="A-LEVEL">A-LEVEL</option>
									<option value="UNIVERSITY">UNIVERSITY</option>
								</select>
							</div>



							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Profession</span>
								<select class="select form-control" id="profession" name="profession">
									<option value="{{$patient->profession}}">{{$patient->profession}}</option>
									<option value=""></option>
									<option value="Architect">Architect</option>
									<option value="Businessman">Businessman</option>
									<option value="D.G.M Bangladesh Bank">D.G.M Bangladesh Bank</option>
									<option value="Housewife">Housewife</option>
									<option value="Phychisian">Phychisian</option>
									<option value="Service Holder">Service Holder</option>
									<option value="Student">Student</option>
									<option value="Teaching">Teaching</option>
								</select>
							</div>
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Present Address</span>
								<textarea rows="3" name="presentAddress" class="form-control"> {{$patient->presentAddress}}</textarea>
							</div>
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Permanent Address</span>
								<textarea rows="3" name="permanentAddress" class="form-control"> {{$patient->permanentAddress}}</textarea>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon">Diagnosis</span>
								<select class="select form-control" name="diagnosis">
									<option value="{{$patient->diagnosis}}">{{$patient->diagnosis}}</option>
									@foreach($diagnosis as $diag)
									<option value="{{$diag->name}}">{{$diag->name}}</option>
									@endforeach
								</select>
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">History Of Drug Abuse</span>
								<select class="select form-control" id="drugabuse" name="drugabuse">
									
									<option value="{{$patient->drugabuse}}">{{$patient->drugabuse}}</option>
									<option value=""></option>
									<option value="Yes">Yes</option>
									<option value="No">No</option>
								</select>
							</div>

						</div>
						<div class="col-md-6">

							<div class="input-group input-group-sm" style="margin-top:10px">
								
								<span class="input-group-addon" id="basic-addon1">Date Of Birth</span>
								<input type="text" name="dob" id="dob" value="{{$patient->dob}}" class="form-control" style="height:auto">
								
							</div>


							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Guardian's Name</span>
								<input type="text" name="guardianName" class="form-control" value="{{$patient->guardianName}}">


								<span class="input-group-btn" style="width:0px;"></span>
								<input type="text" name="guardianRel" class="form-control" value="{{$patient->guardianRel}}">


							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">*Guardian's Phone</span>
								<input type="text" name="guardianPhone" class="form-control" value="{{$patient->guardianPhone}}">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Alt. Guardian Name</span>
								<input type="text" name="altGuardianName" class="form-control" value="{{$patient->altGuardianName}}">

								<span class="input-group-btn" style="width:0px;"></span>
								<input type="text" name="altGuardianRel" class="form-control" value="{{$patient->altGuardianRel}}">


							</div>
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Alt.Guardian Address</span>
								<textarea name="altGuardianAddress" rows="3" class="form-control">{{$patient->altGuardianAddress}}</textarea>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Alt. Guardian Phone</span>
								<input type="text" name="altGuardianPhone" class="form-control" value="{{$patient->altGuardianPhone}}">
							</div>

							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Family History Of Psychotic Illness</span>
								<select class="select form-control" id="illness" name="illness">

									<option value="{{$patient->illness}}">{{$patient->illness}}</option>
									<option value=""></option>
									<option value="Father">Father</option>
									<option value="Mother">Mother</option>
									<option value="Sister">Sister</option>
									<option value="Brother">Brother</option>
									<option value="Grandfather">Grandfather</option>
									<option value="Grandmother">Grandmother</option>
								</select>
							</div>


						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
						<div class="col-sm-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">*Consultant</span>
								<select class="select form-control" name="assignedConsultant" id="assignedConsultant" >
									<option value="{{$patient->assignedConsultant}}">{{$patient->assignedConsultant}}</option>		
									@foreach($consultants as $consustant)
									<option value="{{$consustant->name}}">{{$consustant->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">*M/O on duty</span>
								<select class="select form-control" name="moOnDuty" >
									<option value="{{$patient->moOnDuty}}">{{$patient->moOnDuty}}</option>
									@foreach($mos as $mo)
									<option value="{{$mo->name}}">{{$mo->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">*Manager</span>
								<select class="select form-control" name="manager" >
									<option value="{{$patient->manager}}">{{$patient->manager}}</option>
									@foreach($managers as $manager)
									<option value="{{$manager->name}}">{{$manager->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group input-group-sm" style="margin-top:20px">
								<div class="col-sm-4">
									@if($patient->policeCase == 1)
									<input type="checkbox" value="Police Case" checked name="policeCase">Police Case
									@else
									<input type="checkbox" value="Police Case" name="policeCase">Police Case
									@endif									
								</div>
								<div class="col-sm-4">
									@if($patient->incomingCall == 1)
									<input type="checkbox" checked value="Incoming Call" name="incomingCall">Incoming Call
									@else
									<input type="checkbox" value="Incoming Call" name="incomingCall">Incoming Call
									@endif
								</div>
								<div class="col-sm-4">
									@if($patient->outgoingCall == 1)
									<input type="checkbox" name="outgoingCall" checked value="outgoingCall">Outgoing Call
									@else
									<input type="checkbox" name="outgoingCall" value="outgoingCall">Outgoing Call
									@endif
								</div>
							</div>
							<div class="form-group" style="margin-top:20px">
								<div class="col-sm-4">
									@if($patient->visitor == 1)
									<input type="checkbox" value="Visitor" checked name="visitor">Visitor Allowed
									@else
									<input type="checkbox" value="Visitor" name="visitor">Visitor Allowed
									@endif
								</div>
								<div class="col-sm-4">
									@if($patient->extraFood == 1)
									<input type="checkbox" value="" checked name="extraFood">Extra Food
									@else
									<input type="checkbox" value="" name="extraFood">Extra Food
									@endif									
								</div>
								<div class="col-sm-4">
									@if($patient->incomingCall == 1)
									<input type="checkbox" name="cigarette" checked value="cigarette">Cigarette
									@else
									<input type="checkbox" name="cigarette" value="cigarette">Cigarette
									@endif
								</div>
								
							</div>
						</div>
						<div class="col-sm-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Booking Date &amp; Time</span>
								<input type="text" name="bookingDate" id="bookingDate" class="form-control inline" value="{!!$patient->bookingDate!!}">
								<input class="form-control" name="bookingTime" type="time" value="{{$patient->bookingTime}}">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Admission Date &amp; Time</span>
								<input type="text" name="admissionDate" id="admissionDate" class="form-control" value="{{$patient->admissionDate}}">
								<input class="form-control" name="admissionTime" type="time" value="{{$patient->admissionTime}}">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Discharge Date &amp; Time</span>
								<input type="text" name="releaseDate" id="releaseDate" class="form-control" value="{{$patient->releaseDate}}">
								<input class="form-control" name="releaseTime" type="time" value="{{$patient->releaseTime}}">
							</div>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
					</div>




					<!--Daily Rent and Charge-->
					<div id="dailyRentAndCharge" class="tab-pane fade">
						<div class="col-sm-12">
							<div class="col-sm-6" style="margin-top:25px">
								<div class="input-group input-group-sm">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Master Category</span>
									<select class="select form-control" id="master_catagory" name="master_catagory" style="width:100%">
										<option>Select an option</option>
										@foreach($mastercatagories as $mastercategory)
										<option value="{{$mastercategory->id}}">{{$mastercategory->name}}</option>
										@endforeach
									</select>
								</div>


								<div class="input-group input-group-sm" style="margin-top:15px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Category</span>
									<select class="select form-control" name="subcategory" id="subcategory" style="width:100%">
										<option>Select an option</option>
									</select>
								</div>

								<div class="input-group input-group-sm" style="margin-top:15px">
									<span class="input-group-addon" style="min-width:120px;text-align:left" selected="selected" >Services</span>
									<select class="select form-control" id="services" name="services" style="width:100%;overflow:hidden">
										<option>Select an option</option>

									</select>
								</div>
								<div id="hiddendiv">
									<div class="input-group" style="margin-top:10px">
										<span class="input-group-addon" style="min-width:120px;text-align:left">Doctor Name</span>
										<input type="text" class="form-control" style="height:auto" id="visitingdoc" name="visitingdoc">
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-success" id="hide">
											<span class="glyphicon glyphicon-menu-up"></span> Hide
										</button>
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-warning" id="hiddenaddbtn">
											<span class="glyphicon glyphicon-user"></span> Add Doctor
										</button>
									</div>
								</div>


								<div id="hiddendiv_consultant">

									<div class="input-group" style="margin-top:10px">
										<span class="input-group-addon" style="min-width:120px;text-align:left">Consultancy Name:</span>
										<input type="text" class="form-control" style="height:auto" id="servicename" name="servicename">
									</div>

									<div class="input-group" style="margin-top:10px">
										<span class="input-group-addon" style="min-width:120px;text-align:left">Doctor Name</span>
										<input type="text" class="form-control" style="height:auto" id="docname" name="docname">
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-success" id="hide_consuntant">
											<span class="glyphicon glyphicon-menu-up"></span> Hide
										</button>
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-warning" id="addconsultantbtn">
											<span class="glyphicon glyphicon-user"></span> Add Service
										</button>
									</div>

								</div>








							</div>

							<div class="col-sm-6">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Quantity</span>
									<input type="text" class="form-control" style="height:auto" id="quantity" name="quantity" value=1 placeholder="1">
								</div>

								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Rate</span>
									<input type="text" class="form-control" style="height:auto" id="rate" name="rate">

									<span class="input-group-addon" style="min-width:80px;text-align:left">Total</span>
									<input type="text" class="form-control" style="height:auto"  id="total_daily" name="rate_acc">
								</div>

								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Vat</span>
									<input type="text" class="form-control" style="height:auto" id="vat" name="vat">
								</div>

								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Service Charge</span>
									<input type="text" class="form-control" style="height:auto" id="servicecharge" name="servicecharge">
								</div>


							</div>

							<div class="col-sm-12" >
								<div class="input-group" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Description</span>
									<textarea name="dailydesc" id="dailydesc" rows="2" class="form-control"></textarea>
								</div>
							</div>

							<div class="col-sm-12">									
								<div class="btn-group" style="margin-top:20px;float:right;">
									<button type="button" class="btn btn-primary" id="addDailyTable">
										<span class="glyphicon glyphicon-plus"></span> Add
									</button>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>


						<!--Table of Data for Daily Rent and Charge-->
						<div style="width:100%; max-height:260px; overflow: auto; font-size:12px">
							<table class="table table-bordered" id="dailyTable">
								<tr>
									<th>ID</th>
									<th>Reg No.</th>
									<th>Master Category</th>
									<th>Category</th>
									<th>Services</th>
									<th>Quantity</th>
									<th>Rate</th>
									<th>Vat</th>
									<th>Service Charge</th>
									<th>Description</th>
									<th>Edit/Delete</th>
								</tr>
								@foreach($dailyRents as $index => $dailyRent)
								<tr>
									<td>{!! $index + 1 !!}</td>
									<td>{!! $dailyRent->pid !!}</td>
									<td>{!! $dailyRent->master_category !!}</td>
									<td>{!! $dailyRent->subcategory !!}</td>
									<td>{!! $dailyRent->services !!}</td>
									<td>{!! $dailyRent->quantity !!}</td>
									<td>{!! $dailyRent->rate !!}</td>
									<td>{!! $dailyRent->vat !!}</td>
									<td>{!! $dailyRent->servicecharge !!}</td>
									<td>{!! $dailyRent->description !!}</td>
									<td>
										<a href="/dailyRent/edit/{{$dailyRent->id}}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
										<a href="/dailyRent/delete/{{$dailyRent->id}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>

									</td>
								</tr>
								@endforeach
							</table>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
					</div>





					<!--Accounts Ledger-->
					<div id="accountsLedger" class="tab-pane fade">
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Date</span>
									<input class="form-control" type="input" class="form-control" name="bdate" id="bdate">
								</div>									
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Master Category</span>
									<select class="select form-control" id="master_catagory_acc" name="master_catagory_acc" style="width:100%">
										<option>Select an option</option>		
										@foreach($mastercatagories as $mastercategory)
										<option value="{{$mastercategory->id}}">{{$mastercategory->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Category</span>
									<select class="select form-control" name="subcategory_acc" id="subcategory_acc" style="width:100%">
										<option>Select an option</option>
									</select>
								</div>
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Services</span>
									<select class="select form-control" id="services_acc" name="services_acc" style="width:100%">
										<option>Select an option</option>
									</select>
								</div>


								<div id="hiddendiv_acc">
									<div class="input-group" style="margin-top:10px">
										<span class="input-group-addon" style="min-width:120px;text-align:left">Doctor Name</span>
										<input type="text" class="form-control" style="height:auto" id="visitingdoc_acc" name="visitingdoc_acc">
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-success" id="hide_acc">
											<span class="glyphicon glyphicon-menu-up"></span> Hide
										</button>
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-warning" id="hiddenaddbtn_acc">
											<span class="glyphicon glyphicon-user"></span> Add Doctor
										</button>
									</div>
								</div>


								<div id="hiddendiv_consultant_acc">

									<div class="input-group" style="margin-top:10px">
										<span class="input-group-addon" style="min-width:120px;text-align:left">Consultancy Name:</span>
										<input type="text" class="form-control" style="height:auto" id="servicename_acc" name="servicename_acc">
									</div>

									<div class="input-group" style="margin-top:10px">
										<span class="input-group-addon" style="min-width:120px;text-align:left">Doctor Name</span>
										<input type="text" class="form-control" style="height:auto" id="docname_acc" name="docname_acc">
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-success" id="hide_consuntant_acc">
											<span class="glyphicon glyphicon-menu-up"></span> Hide
										</button>
									</div>

									<div class="btn-group" style="margin-top:20px;float:right;">
										<button type="button" class="btn btn-warning" id="addconsultantbtn_acc">
											<span class="glyphicon glyphicon-user"></span> Add Service
										</button>
									</div>

								</div>




							</div>

							<div class="col-sm-6">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Quantity</span>
									<input type="text" class="form-control" style="height:auto" id="quantity_acc" name="quantity_acc" value=1 placeholder="1">
								</div>

								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:80px;text-align:left">Rate</span>
									<input type="text" class="form-control" style="height:auto"  id="rate_acc" name="rate_acc">

									<span class="input-group-addon" style="min-width:80px;text-align:left">Total</span>
									<input type="text" class="form-control" style="height:auto"  id="total_acc" name="rate_acc">
								</div>

								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Vat</span>
									<input type="text" class="form-control" style="height:auto" id="vat_acc" name="vat_acc">
								</div>

								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Service Charge</span>
									<input type="text" class="form-control" style="height:auto" id="servicecharge_acc" name="servicecharge_acc">
								</div>
							</div>





							






							<div class="col-sm-12">
								<div class="input-group" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Remarks</span>
									<textarea name="remarks_acc" id="remarks_acc" rows="2" class="form-control"></textarea>
								</div>
							</div>

							<div class="col-sm-4">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Non-Profit Bill</span>
									<label class="form-control" id="nonprofit"> {!! $nonProfitBill !!} </label>
								</div>										
							</div>
							<div class="col-sm-4">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Consultant Bill</span>
									<label class="form-control" id="consultantbill"> {!! $consultantBill !!} </label>
								</div>										
							</div>
							<div class="col-sm-4">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Hospital Bill</span>
									<label class="form-control" id="hospitalbill"> {!! $hospitalServices !!} </label>
								</div>										
							</div>
							<div class="col-sm-3">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:10px;text-align:left">Total Bill</span>
									<label class="form-control" id="totalbill"> {!! $total !!} </label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:10px;text-align:left">Payment</span>
									<label class="form-control"> {!! $payment !!} </label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:10px;text-align:left">Discount</span>
									<label class="form-control"> {!! $discount !!} </label>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:10px;text-align:left">Due</span>
									<label class="form-control"> {!! $due !!} </label>
								</div>
							</div>
							<div class="col-sm-12">									
								<div class="btn-group btn-group-justified" style="margin-top:20px">
									<div class="btn-group">
										<button type="button" class="btn btn-primary" id="addledgerbtn">
											<span class="glyphicon glyphicon-pencil"></span> Add Service
										</button>
									</div>
									<div class="btn-group">
										<button type="button" onclick="getSummaryBill()" class="btn btn-info">
											<span class="glyphicon glyphicon-list-alt"></span> Summary Bill
										</button>
									</div>
									<div class="btn-group">
										<button type="button" onclick="getDetailedBill()" class="btn btn-info">
											<span class="glyphicon glyphicon-align-justify"></span> Detailed Bill
										</button>
									</div>
									<div class="btn-group">
										<button type="button" onclick="getHospitalBill()" class="btn btn-info">
											<span class="glyphicon glyphicon-list"></span> Hospital Bill
										</button>
									</div>
									<div class="btn-group">
										<button type="button" onclick="getConsultantBill()" class="btn btn-info">
											<span class="glyphicon glyphicon-list-alt"></span> Consultant Bill
										</button>
									</div>
									<div class="btn-group">
										<button type="button" onclick="getNonProfitBill()" class="btn btn-info">
											<span class="glyphicon glyphicon-list-alt"></span> Non-Profitable Bill
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>

						<!--Table of Data for Accounts Ledger-->
						<div style="width:100%; max-height:220px; overflow: auto; font-size:12px">
							<table class="table table-bordered" id="accountledgertbl">
								<tr>
									<th>Bill ID.</th>
									<th>Bed/Cabin No.</th>
									<th width="85px">Bill Date</th>
									<th>Master Category</th>
									<th>Category</th>
									<th>Services</th>
									<th>Rate</th>
									<th>Quantity</th>
									<th>Vat</th>
									<th>SC</th>
									<th>Billed By</th>
									<th>Edit/Delete</th>
								</tr>
								@foreach($bills as $index => $bill)
								<tr>
									<td>{!! $index + 1 !!}</td>
									<td>{!! $patient->bedCabin !!}</td>
									<td>{!! $bill->date !!}</td>
									<td>{!! $bill->masterCategory !!}</td>
									<td>{!! $bill->category !!}</td>
									<td>{!! $bill->services !!}</td>
									<td>{!! $bill->rate !!}</td>
									<td>{!! $bill->quantity !!}</td>
									<td>{!! $bill->vat !!}</td>
									<td>{!! $bill->serviceCharge !!}</td>
									<td>{!! $bill->billBy !!}</td>
									<td>
										<a href="/accountLedger/edit/{{$bill->id}}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
										<a href="/accountLedger/delete/{{$bill->id}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
									</td>
								</tr>
								@endforeach
								<input type="hidden" name="bedC" id="bedC" value="{!! $patient->bedCabin !!}">
							</table>
						</div>
						<hr style="margin-top:-2px; border: 0;height: 1px;background: #333;">
					</div>








					<!--Investigations-->
					<div id="investigation" class="tab-pane fade">							
						<div class="col-sm-12">								
							<div class="col-sm-6">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon">Date</span>
									<input class="form-control" type="text" class="form-control" name="investigationDate" id="investigationDate" >
								</div>

								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" id="basic-addon1">Send Date and Time</span>
									<input type="text" class="form-control" name="investigationSendDate" id="investigationSendDate">
									<span class="input-group-btn" style="width:0px;"></span>
									<input class="form-control" name="investigationSendTime" id="investigationSendTime" type="time">

								</div>
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" id="basic-addon1">Delivery Date and Time</span>
									<input type="text" class="form-control" name="investigationDeliveryDate" id="investigationDeliveryDate">
									<span class="input-group-btn" style="width:0px;"></span>
									<input class="form-control" type="time" name="investigationDeliveryTime" id="investigationDeliveryTime">
								</div>
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon">Laboratory</span>
									<input class="form-control" type="text" class="form-control" name="laboratory" id="laboratory">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="input-group" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">Investigations</span>
									<textarea rows="7" class="form-control" name="Investigations" id="Investigations"></textarea>
								</div>									
							</div>
							<div class="col-sm-12">									
								<div class="btn-group" style="margin-top:20px;float:right;">
									<button type="button" class="btn btn-primary" id="investigationbtn">
										<span class="glyphicon glyphicon-plus"></span> Add
									</button>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>

						<!--Table of Data for Investigation-->
						<div style="width:100%; max-height:220px; overflow: auto; font-size:12px">
							<table class="table table-bordered" id="investigationTbl">
								<tr>
									<th>Bill No.</th>
									<th>Reg No.</th>
									<th>Bed/Cabin No.</th>
									<th>Date</th>
									<th>Laboratory</th>
									<th>Investigation</th>
									<th>Send Date</th>
									<th>Send Time</th>
									<th>Delivery Date</th>
									<th>Delivery Time</th>
									<th>Billed By</th>
								</tr>

								@foreach($investigations as $investigation)
								<tr>									
									<th>{{$investigation->id}}</th>
									<th>{{$investigation->pid}}</th>
									<th>{{$investigation->bedcabin}}</th>
									<th>{{$investigation->todaydate}}</th>
									<th>{{$investigation->lab}}</th>
									
									<th>{{$investigation->investigation}}</th>
									<th>{{$investigation->senddate}}</th>
									
									
									<th>{{$investigation->investigationSendTime}}</th>
									<th>{{$investigation->deliverydate}}</th>
									<th>{{$investigation->investigationDeliveryTime}}</th>
									<th>{{$investigation->billby}}</th>
								</tr>
								@endforeach

							</table>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
					</div>





					<!--Prescription-->
					<div id="prescription" class="tab-pane fade">
						<div class="col-sm-12">
							<div class="col-sm-6">
								<label class="control-label">Treatment Discharge With Advice</label>								
								<textarea class="form-control" rows="6" name="tda"> {!! $prescription->tda !!} </textarea>
							</div>
							<div class="col-sm-6">									
								<label class="control-label " for="investigation">Investigation</label>
								<textarea class="form-control" rows="6" name="investigation"> {!! $prescription->investigation !!} </textarea>
							</div>
							<div class="col-sm-6">
								<label class="control-label " for="message4">Notes</label>
								<textarea class="form-control" rows="6" name="notes" > {!! $prescription->notes !!} </textarea>
							</div>
							<div class="col-sm-6">
								<label class="control-label " for="treatment">Treatment Given</label>
								<textarea class="form-control" rows="6" id="treatment" name="treatment"> {!! $prescription->treatment !!} </textarea>
							</div>
						</div>

						<div class="col-sm-12" style="margin-top:20px">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button type="button" onclick="getDorbForm()" class="btn btn-primary">
										<span class="glyphicon glyphicon-list-alt"></span>  DORB Form
									</button>
								</div>
								<div class="btn-group">
									<button type="submit" onclick="getEmsForm()" class="btn btn-primary">
										<span class="glyphicon glyphicon-list-alt"></span>  EMS Form
									</button>
								</div>
								<div class="btn-group">
									<button type="button" onclick="getPaRoleForm()" class="btn btn-primary">
										<span class="glyphicon glyphicon-list-alt"></span>  Pa-Role Form
									</button>
								</div>
								<div class="btn-group">
									<button type="button" onclick="getDischargeCertificate()" class="btn btn-primary">
										<span class="glyphicon glyphicon-list-alt"></span>  Discharge Certificate
									</button>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
					</div>







					<!--Visitor-->
					<div id="visitor" class="tab-pane fade">
						<div class="col-sm-4">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Visitor Name</span>
								<input type="text" name="visitorName" id="visitorName" class="form-control">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Relation to the Patient</span>
								<input type="text" name="visitorRelation" id="visitorRelation" class="form-control">
							</div>
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Contact Number</span>
								<input type="text" name="visitorPhone" id="visitorPhone" class="form-control">
							</div>
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Remarks</span>
								<textarea name="visitorRemark" id="visitorRemark" rows="3" class="form-control"></textarea>
							</div>
							<div class="col-sm-12">									
								<div class="btn-group" style="margin-top:20px;float:right;">
									<button type="button" class="btn btn-primary" name="visitorAdd" id="visitorAdd">
										<span class="glyphicon glyphicon-plus"></span> Add
									</button>
								</div>
							</div>
						</div>

						<!--Table of Data for Visitors-->
						<div class="col-sm-8">
							<div style="width:100%; max-height:600px; overflow: auto; font-size:12px">
								<table class="table table-bordered" name="visitorTable" id="visitorTable">
									<tr>
										<th>Visitor's Name</th>
										<th>Relation to Patient</th>
										<th>Contact No.</th>
										<th>Remarks</th>
									</tr>
									@foreach($visitors as $visitor)
									<tr>
										<td> {{$visitor->visitorName}} </td>
										<td> {{$visitor->visitorRelation}} </td>
										<td> {{$visitor->visitorPhone}} </td>
										<td> {{$visitor->visitorRemark}} </td>
									</tr>
									@endforeach

								</table>
							</div>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<a class="btn btn-primary" href="/patientAdmission">New</a>
						</div>
						<div class="btn-group">
							<button type="submit" class="btn btn-success">
								<span class="glyphicon glyphicon-save"></span> Save
							</button>
						</div>
						
						<div class="btn-group">
							<button type="button" onclick="getPatientInfoReport()" class="btn btn-info">
								<span class="glyphicon glyphicon-print"></span> View
							</button>
						</div>
						<div class="btn-group">
							<a class="btn btn-primary" href="/patientAdmission/release/{!!$patient['id']!!}"><span class="glyphicon glyphicon-floppy-disk"></span> Release</a>
								<!-- <button type="button" class="btn btn-success">
									<span class="glyphicon glyphicon-floppy-disk"></span> Release
								</button> -->
							</div>
							<div class="btn-group">
								<a class="btn btn-success" href="/patientPayment/home/{!!$patient['id']!!}"><span class="glyphicon glyphicon-briefcase"></span> Payments</a>
								<!--<button type="button" class="btn btn-success">
									<span class="glyphicon glyphicon-briefcase"></span> Payments
								</button>-->
							</div>
							<div class="btn-group">
								<a class="btn btn-primary" href="/patientDiscount/home/{!!$patient['id']!!}"><span class="glyphicon glyphicon-minus"></span> Discount</a>
								<!--<button type="button" class="btn btn-primary">
									<span class="glyphicon glyphicon-minus"></span> Discount
								</button>-->
							</div>
							<div class="btn-group">
								<a href="/home" class="btn btn-danger" ><span class="glyphicon glyphicon-off"></span>Exit</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			{!! Form::close() !!}

		</div>


		@section('essentialScript')
		<script>

		$('select').select2();


//hidden doc add div for daily rent n account ledger 
$(document).ready(function() {
	$("#hiddendiv").hide();
	$("#hiddendiv_consultant").hide();
	$("#hiddendiv_consultant_acc").hide();

});


function visitingdoc(){
	$("#hiddendiv").show();

}



$("#hiddenaddbtn").click(function(){

	var x = document.getElementById("services");
	var option = document.createElement("option");
	option.text = $("#visitingdoc").val();
	x.add(option);
	var y=$("#visitingdoc").val();
	$("#services").val(y);
	$("#hiddendiv").hide();
	$('select').select2();
});



$("#hide").click(function(){
	$("#hiddendiv").hide();
});







//hiddden div for custom consultancy services daily rent


$("#addconsultantbtn").click(function(){

	var x = $("#servicename").val();
	$("#subcategory").append("<option value="+'"'+x+'"'+"selected='selected'>"+x+"</option>");
	var y=$("#docname").val();
	$("#services").append("<option value="+'"'+y+'"'+"selected='selected'>"+y+"</option>");
	$("#hiddendiv_consultant").hide();
	

	$('select').select2();

	
});


$("#hide_consuntant").click(function(){
	$("#hiddendiv_consultant").hide();
});











//hiddden div for custom consultancy services account ledger


$("#addconsultantbtn_acc").click(function(){

	var x = $("#servicename_acc").val();
	$("#subcategory_acc").append("<option value="+'"'+x+'"'+"selected='selected'>"+x+"</option>");
	var y=$("#docname_acc").val();
	$("#services_acc").append("<option value="+'"'+y+'"'+"selected='selected'>"+y+"</option>");
	$("#hiddendiv_consultant_acc").hide();
	

	$('select').select2();

	
});


$("#hide_consuntant_acc").click(function(){
	$("#hiddendiv_consultant_acc").hide();
});











//hidden doc add div for account ledger


$(document).ready(function() {
	$("#hiddendiv_acc").hide();
});
function visitingdoc_acc(){
	$("#hiddendiv_acc").show();

}

$("#hiddenaddbtn_acc").click(function(){

	var x = document.getElementById("services_acc");
	var option = document.createElement("option");
	option.text = $("#visitingdoc_acc").val();
	x.add(option);
	var y=$("#visitingdoc_acc").val();
	$("#services_acc").val(y);
	$("#hiddendiv_acc").hide();
	$('select').select2();



});

$("#hide_acc").click(function(){
	$("#hiddendiv_acc").hide();
});




//global variables

var X,Y,I;
var OR,OV,OSC,ON;
var OR_ACC,OV_ACC,OSC_ACC;
var CABIN="";


var NON_PROFIT=$("#nonprofit").text();
var CONSULTANT=$("#consultantbill").text();
var HOSPITAL=$("#hospitalbill").text();
var TOTAL_BILL=$("#totalbill").text();



//X-CSRF-Token
$.ajaxSetup({
	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});



$(document).ready(function() {
	var x="";
	if($("#assignedConsultant").val()=="select an option")
	{
		$("#assignedConsultant").val(x);		
	}
});



//daily rent master category edit page

$(document).ready(function() {
	$("#master_catagory").on('change',function(e){
		console.log(e);
		var mcat_id=e.target.value;
		//alert(mcat_id);


		$.get('/edit/ajax-subcat?mcat_id='+mcat_id,function(data){

			console.log(data);
			$("#subcategory").empty();
			$("#subcategory").select2({allowClear: true});
			//$("#services").select2({allowClear: true});
			$.each(data,function(index,subcatObj){
				$("#subcategory").append(' <option value="'+subcatObj.name+'">'+subcatObj.name+'</option> ');



			});
		});
	});
});






//daily rent sub-category edit page


$(document).ready(function() {



	$('#subcategory').on('change',function(e){
		console.log(e);
		var mcat_name=$("#master_catagory").val();
		//alert(mcat_name);
		var cat_name=$("#subcategory").val();
		//alert(cat_name);
		var tablename="";
		if(mcat_name==1)      {tablename="Bed";}
		else if(mcat_name==2) {tablename="Doctors";}
		else if(mcat_name==3) {tablename="Hospitalservices";}
		else				  {tablename="Nonprofitservices";}

		//alert(tablename);

		$.get('/edit/ajax-cat?cat_name='+cat_name+'&tablename='+tablename,function(data){
			
			console.log(data);
			X=data;
			$("#services").empty();
			$("#services").select2({allowClear: true});
			$.each(data,function(index,serviceObj){
				
				$("#services").append("<option value="+'"'+serviceObj.name+'"'+">"+serviceObj.name+"</option>");



			});
			
			

		});

		if($('#subcategory').val()=="Other Consultant Service")
		{
			
			customconsultant();
		}
		
		
	});
});


function customconsultant()
{
	$("#hiddendiv_consultant").show();
}







//daily rent&charge services

$(document).ready(function(){
	$("#services").on('change',function(){

		var indexx=$('#services').val();
		bedcabin(indexx);
		//alert($('#services').val());
		//alert(indexx);

		var count = Object.keys(X).length;

		//console.log(OR);
		for(i=0;i<count;i++)
		{
			if(indexx===X[i].name)
			{

				$("#rate").val(X[i].Rate);
				$("#vat").val(X[i].VatRate);
				$("#servicecharge").val(X[i].ServiceChargeRate);
				$("#total_daily").val(X[i].Rate);


				//alert($("#rate").val());

				break;
			}		
		}

		ON=X[i].name;
		OR=X[i].Rate;
		OV=X[i].VatRate;
		OSC=X[i].ServiceChargeRate;

		
		if($("#services").val()=="Other")
		{
			visitingdoc();
		}
		
		


	});
	
});


function bedcabin(bedno){
	if($("#master_catagory").val()==1)
	{
		CABIN=bedno;
		//alert("Cabin no"+CABIN);
		
	}
	else
	{
		return; 
	}

}


//daily rent&charge rate,vat,sc
$("#quantity").keyup(function(){


	if($("#quantity").val()=="")
	{
		$("#rate").val(OR);
		$("#vat").val(OV);
		$("#servicecharge").val(OSC);
		$("#total_daily").val(1);


	}
	var q=$("#quantity").val();
	var r=$("#rate").val();
	$("#total_daily").val(parseInt(q)*parseInt(r));

	/*else
	{


		$("#rate").val($("#quantity").val()*OR);

	}*/






});

$("#rate").keyup(function(){

	var q=$("#quantity").val();
	var r=$("#rate").val();
	$("#total_daily").val(parseInt(q)*parseInt(r));


});




//daily rent and charge table


$("#addDailyTable").click(function(){

	
	
	var regno=$("#id").val();
	var mastercat=$("#master_catagory").val();
	var cat=$("#subcategory").val();
	var service=$("#services").val();

	var quantity=$("#quantity").val();
	var rate=$("#rate").val();
	//var rate=OR;
	var vat=$("#vat").val();
	var servicecharge=$("#servicecharge").val();
	var description=$("#dailydesc").val();

	//added new
	var bedNo=$("#bedCabin").val();
	var date=$("#bdate").val();
	var billBy=$("#username").val();

	
	

	






	$.ajax({

		url  : "/edit/editsave_dailyrent",
		type : "GET",

		data: {
			'pid':regno,
			'master_category':mastercat,
			'subcategory':cat,
			'services':service,
			'quantity':quantity,
			'rate':rate,
			'vat':vat,
			'servicecharge':servicecharge,
			'description':description,
			'bedNo':bedNo,
			'date':date,
			'billBy':billBy,






		},
		success: function(re){
						//console.log(re);
						//alert(re);
						location.reload();
						
						
					}
				});
	/*$("#dailyTable").append("<tr><td>"+regno+"</td><td>"+regno+"</td><td>"+mastercat+"</td><td>"+cat+"</td><td>"+service+"</td><td>"+quantity+"</td><td>"+rate+"</td><td>"+vat+"</td><td>"+servicecharge+"</td><td>"+description+"</td><td><button class='btn btn-xs btn-primary' id='btndummy'><span class='glyphicon glyphicon-pencil'></span></button><button class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-remove'></span></button></td></tr>");*/
	
	$("#quantity").val(1);
	




});





//account ledger master category

$(document).ready(function() {
	$("#master_catagory_acc").on('change',function(e){
		console.log(e);
		var mcat_id=e.target.value;


		$.get('/edit/ajax-subcat?mcat_id='+mcat_id,function(data){

			console.log(data);
			$("#subcategory_acc").empty();
			$("#subcategory_acc").select2({allowClear: true});
			$.each(data,function(index,subcatObj){
				$("#subcategory_acc").append(' <option value="'+subcatObj.name+'">'+subcatObj.name+'</option> ');



			});

		});
	});
});



//account ledger sub-category
$(document).ready(function() {



	$('#subcategory_acc').on('change',function(e){
		console.log(e);
		var mcat_name=$("#master_catagory_acc").val();
		//alert(mcat_name);
		var cat_name=$("#subcategory_acc").val();
		//alert(cat_name);
		var tablename="";
		if(mcat_name==1)      {tablename="Bed";}
		else if(mcat_name==2) {tablename="Doctors";}
		else if(mcat_name==3) {tablename="Hospitalservices";}
		else				  {tablename="Nonprofitservices";}

		//alert(tablename);

		$.get('/edit/ajax-cat?cat_name='+cat_name+'&tablename='+tablename,function(data){
			
			console.log(data);
			Y=data;
			$("#services_acc").empty();
			$("#services_acc").select2({allowClear: true});
			$.each(data,function(index,serviceObj){
				$("#services_acc").append("<option value="+'"'+serviceObj.name+'"'+">"+serviceObj.name+"</option>");
					//X=JSON.parse(serviceObj);
					
				});
			
			

		});
		if($('#subcategory_acc').val()=="Other Consultant Service")
		{
			
			customconsultant_acc();
		}
	});
});

function customconsultant_acc(){
	$("#hiddendiv_consultant_acc").show();
}



//account ledger services
$(document).ready(function(){
	$("#services_acc").on('change',function(){
		var indexx=$('#services_acc').val();
		//alert(indexx);

		var count = Object.keys(Y).length;
		

		for(i=0;i<count;i++)
		{
			if(indexx===Y[i].name)
			{

				$("#rate_acc").val(Y[i].Rate);
				$("#vat_acc").val(Y[i].VatRate);
				$("#servicecharge_acc").val(Y[i].ServiceChargeRate);


			//alert($("#rate_acc").val());
			
			break;
		}		
	}

	ON_ACC=Y[i].name;
	OR_ACC=Y[i].Rate;
	OV_ACC=Y[i].VatRate;
	OSC_ACC=Y[i].ServiceChargeRate;



		//console.log(X);
		if($("#services_acc").val()=="Other")
		{
			visitingdoc_acc();
		}

	});
	
});





//Account ledger rate,vat,sc
$("#quantity_acc").keyup(function(){






	if($("#quantity_acc").val()=="")
	{
		$("#rate_acc").val(OR_ACC);
		$("#vat_acc").val(OV_ACC);
		$("#servicecharge_acc").val(OSC_ACC);
		$("#total_acc").val(1);


	}

	var q=$("#quantity_acc").val();
	var r=$("#rate_acc").val();

	$("#total_acc").val(parseInt(q)*parseInt(r));



});

$("#rate_acc").keyup(function(){

	var q=$("#quantity_acc").val();
	var r=$("#rate_acc").val();
	$("#total_acc").val(parseInt(q)*parseInt(r));


});







//Account ledger addbtn
$("#addledgerbtn").click(function(){
	var pid=$("#id").val();
	//alert(pid);
	var bedcabinNo=$("#bedC").val();
	var master_catagory_acc=$("#master_catagory_acc").val();
	var subcategory_acc=$("#subcategory_acc").val();
	var services_acc=$("#services_acc").val();
	var quantity_acc=$("#quantity_acc").val();
	var rate_acc=$("#rate_acc").val();
	//var rate_acc=OR_ACC;
	var vat_acc=$("#vat_acc").val();
	var servicecharge_acc=$("#servicecharge_acc").val();
	var remarks_acc=$("#remarks_acc").val();
	var bdate=$("#bdate").val();
	//alert($("#bdate").val());
	var billingby=$("#username").val();
	var admissionStatus=$("#admissionStatus").val();
	var isbilled="true";
	var x="";


	$("#accountledgertbl").append("<tr><td>"+x+"</td><td>"+bedcabinNo+"</td><td>"+bdate+"</td><td>"+master_catagory_acc+"</td><td>"+subcategory_acc+"</td><td>"+services_acc+"</td><td>"+rate_acc+"</td><td>"+quantity_acc+"</td><td>" +vat_acc+ "</td><td>"+servicecharge_acc+"</td><td>" +billingby+ "</td><td>"+remarks_acc+"</td></tr>");
	

	$.ajax({

		url  : "/edit/editsaveAccountLedger",
		type : "GET",

		data: {
			
			'regNo':pid,
			'bedNo':bedcabinNo,
			'masterCategory':master_catagory_acc,
			'category':subcategory_acc,
			'services':services_acc,
			'remarks':remarks_acc,
			'date':bdate,
			'quantity':quantity_acc,
			'rate':rate_acc,
			'vat':vat_acc,
			'serviceCharge':servicecharge_acc,
			'billBy':billingby,
			

		},
		success: function(re){
			//console.log(re);
						//alert(re);
						location.reload();
						
						
					}
				});

	
	//billCalc(master_catagory_acc);


});





//ajax for visitor info

$(document).ready(function(){
	$("#visitorAdd").click(function(){
		var pid=$("#id").val();
		//alert(pid);
		var v_name=$("#visitorName").val();
		//alert(v_name);
		var v_rel=$("#visitorRelation").val();
		//alert(v_rel);
		var v_phone=$("#visitorPhone").val();
		//alert(v_phone);
		var v_remark=$("#visitorRemark").val();
		//alert(v_remark);
		var y="";

		
		
		
		$.ajax({

			url  : "/edit/editsave",
			type : "GET",

			data: {
				'pid':pid,
				'visitorName':v_name,
				'visitorRelation':v_rel,
				'visitorPhone':v_phone,
				'visitorRemark':v_remark

			},
			success: function(re){
						//console.log(re);
						alert(re);
						
						
					}
				});
		
		$("#visitorTable").append("<tr><td>"+v_name+"</td><td>"+v_rel+"</td><td>"+v_phone+"</td><td>"+v_remark+"</td></tr>");
		$("#visitorName").val(y);

		$("#visitorRelation").val(y);
		
		$("#visitorPhone").val(y);

		$("#visitorRemark").val(y);
		

		
		
		


	});

});



	//datepicker

	/*$("#bookingDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#admissionDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#releaseDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#bdate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#investigationDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#investigationSendDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#investigationDeliveryDate").datepicker({ dateFormat: 'yy-mm-dd' });*/
	//$("#patient_dischargeDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#bookingDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#admissionDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#releaseDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#bdate").datepicker({ dateFormat: 'yy-mm-dd' }).datepicker("setDate", new Date());
	$("#investigationDate").datepicker({ dateFormat: 'yy-mm-dd' }).datepicker("setDate", new Date());
	$("#investigationSendDate").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#investigationDeliveryDate").datepicker({ dateFormat: 'yy-mm-dd' });

	




	//investigation

	$("#investigationbtn").click(function(){
		var pid=$("#id").val();
		//alert(pid);
		var todaydate=$("#investigationDate").val();
		//alert(todaydate);
		var senddate=$("#investigationSendDate").val();
		//alert(senddate);
		var deliverydate=$("#investigationDeliveryDate").val();
		//alert(deliverydate);

		var investigationSendTime=$("#investigationSendTime").val();
		
		var investigationDeliveryTime=$("#investigationDeliveryTime").val();
		


		var lab=$("#laboratory").val();
		//alert(lab);
		var investigation=$("#Investigations").val();
		
		var bedcabin=$("#bedCabin").val();

		var billingby=$("#username").val();



		$.ajax({

			url  : "/edit/editsaveInvestigation",
			type : "GET",

			data: {
				'pid':pid,
				'todaydate':todaydate,
				'senddate':senddate,
				'deliverydate':deliverydate,
				'investigationSendTime':investigationSendTime,
				'investigationDeliveryTime':investigationDeliveryTime,
				'lab':lab,
				'bedcabin':bedcabin,					
				'investigation':investigation,
				'billby': billingby

			},
			success: function(re){
				console.log(re);
						//alert(re);
						
						
					}
				});



		$("#investigationTbl").append("<tr><td>"+pid+"</td><td>"+pid+"</td><td>"+bedcabin+"</td><td>"+todaydate+"</td><td>"+lab+"</td><td>"+investigation+"</td><td>"+senddate+"</td><td>"+investigationSendTime+"</td><td>"+deliverydate+"</td><td>"+investigationDeliveryTime+"</td><td>"+billingby+"</td></tr>")

	});







//Rabbir's code
function getPatientInfoReport() {
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-info-report/" + id;
		window.open(url, '_blank');
		//window.location.href = url;		
	}
	else{
		var url = "/patientReports/patient-info-report/0";
		window.location.href = url;			
	}
}

function getSummaryBill() {
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-summary-bill/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-summary-bill/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getDetailedBill(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-detailed-bill/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-detailed-bill/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getHospitalBill(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-hospital-bill/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-hospital-bill/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getHospitalBill(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-hospital-bill/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-hospital-bill/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getConsultantBill(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-consultant-bill/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-hospital-bill/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getNonProfitBill(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-non-profit-bill/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-non-profit-bill/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getDorbForm(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-dorb-form/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-dorb-form/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getEmsForm(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-ems-form/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-ems-form/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getPaRoleForm(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-pa-role-form/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-pa-role-form/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}

function getDischargeCertificate(){
	var id = document.getElementById("id").value;

	if(id){
		var url = "/patientReports/patient-discharge-certificate/" + id;
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}	
	}
	else{
		var url = "/patientReports/patient-discharge-certificate/0";
		var win = window.open(url, '_blank');
		if(win){
			win.focus();
		}else{
			alert('Please allow popups for this site');
		}
	}
}




//bedcabin finder
function bedcabin(bedno){
	if($("#master_catagory").val()==1 || $("#master_catagory_acc").val()==1)
	{
		CABIN=bedno;
		$("#bedCabin").val(CABIN);
		
	}
	else
	{
		return; 
	}

}


//dob picker
$( "#dob" ).datepicker({
	dateFormat : 'yy/mm/dd',
	changeMonth : true,
	changeYear : true,
	yearRange: '-100y:c+nn'

});




</script>


@endsection

@endsection