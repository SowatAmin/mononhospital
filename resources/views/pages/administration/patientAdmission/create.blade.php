@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Search Patient</a> 
		<i class="icon-angle-right"></i>
	</li>
	
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="row-fluid sortable">		
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon list"></i><span class="break"></span>Existing Patient List</h2>
			
		
		<div class="box-content">
		
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
								<th>Patient Number</th>
								<th>Patient Name</th>
								<th>Guardian Contact Number</th>
								<th>Edit</th>
				 </tr>
			  </thead>   
			  <tbody>
			 @foreach($patients as $patient)

					<tr>
								<td>{{$patient->id}}</td>
								<td>{{$patient->name}}</td>
								<td>{{$patient->guardianPhone}}</td>
								<td><a href="/patientAdmission/{!! $patient->id !!}/edit" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a></td>
								
					</tr>
				@endforeach							
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->
	
</div><!--/row-->

@section('essentialScript')

<script type="text/javascript">
	

	
	

</script>

@endsection

@endsection