<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>EMS Form</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
			padding-top: 10px;
		}
		td{
			padding-top: 15px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">
		<h2 align="center">MONON Psychiatric Hospital</h2>
		<h3 align="center" style="margin-top:-30px">Treatment Center for Mental Illness and Drug Addiction</h3>
		<h4 align="center">
			20/20 Tajmahal Road, Mohammadpur, Dhaka<br>
			Telephone - 9114550, 0171243820
		</h4>

		<h3 align="center" style="margin-top:-30px">
			Pa-Role Form
		</h3>

		<table width="100%" style="font-size:15px">
			<tr>
				<td width="30px">Date:</td>
				<td colspan="5" style="border-bottom: 1px dashed black">
					{{$today}}
				</td>
			</tr>
			<tr>
				<td colspan="2" width="200px">Name of Patient:</td>
				<td colspan="2" style="border-bottom: 1px dashed black"> {{$patient->name}} </td>
				<td width="20px" align="right">Bed no:</td>
				<td style="border-bottom: 1px dashed black" width="130px">{{$patient->bedCabin}}</td>
			</tr>
			<tr>
				<td colspan="2">Name of Consultant:</td>
				<td colspan="4" style="border-bottom: 1px dashed black">
					{{$patient->assignedConsultant}}
				</td>
			</tr>
			<tr>
				<td colspan="6">
					I am taking the above named patient, under the treatment of the mentioned consultant at
					my own risk and shall not hold the hospital management responsible for any matter
					regarding the patient during this time. In case the patient does not return, I shall clear all
					due bills and then take a discharge certificate.
				</td>
			</tr>
			<tr>
				<td colspan="2">Check out time:</td>
				<td colspan="1" style="border-bottom: 1px dashed black"></td>
				<td colspan="2" width="120px" align="right">Due check in time:</td>
				<td style="border-bottom: 1px dashed black"></td>
			</tr>
			<tr>
				<td colspan="2">Signature of Guardian:</td>
				<td colspan="4" style="border-bottom: 1px dashed black">
					
				</td>
			</tr>
			<tr>
				<td colspan="2">Name of Guardian:</td>
				<td colspan="4" style="border-bottom: 1px dashed black">
					
				</td>
			</tr>
			<tr>
				<td colspan="2">Relation with patient:</td>
				<td colspan="4" style="border-bottom: 1px dashed black">
					
				</td>
			</tr>
			<tr>
				<td colspan="2">Contact Address & Tel no:</td>
				<td colspan="4" rowspan="2">
					
				</td>
			</tr>
			<tr>
				<td colspan="2"><br></td>				
			</tr>

			<tr>
				<td colspan="2">Name of checking out manager:</td>
				<td colspan="1" style="border-bottom: 1px dashed black"></td>
				<td colspan="2" align="right">Signature:</td>
				<td style="border-bottom: 1px dashed black"></td>
			</tr>
			<tr>
				<td colspan="2">Name of M/O on duty:</td>
				<td colspan="1" style="border-bottom: 1px dashed black"></td>
				<td colspan="2" align="right">Signature:</td>
				<td style="border-bottom: 1px dashed black"></td>
			</tr>
			
			<tr>
				<td colspan="6" style="padding-top:40px;border-bottom: 3px dotted black"></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:40px">Actual check in time:</td>
				<td colspan="4" style="padding-top:40px;border-bottom:1px dashed black">
					
				</td>
			</tr>
			<tr>
				<td colspan="2">Check out time:</td>
				<td colspan="1" style="border-bottom: 1px dashed black"></td>
				<td colspan="2" align="right">Due check in time:</td>
				<td style="border-bottom: 1px dashed black"></td>
			</tr>
			<tr>
				<td colspan="2">Name of M/O on duty:</td>
				<td colspan="1" style="border-bottom: 1px dashed black"></td>
				<td colspan="2" align="right">Signature:</td>
				<td style="border-bottom: 1px dashed black"></td>
			</tr>
			<tr>
				<td colspan="2">Name of manager on duty:</td>
				<td colspan="1" style="border-bottom: 1px dashed black"></td>
				<td colspan="2" align="right">Signature:</td>
				<td style="border-bottom: 1px dashed black"></td>
			</tr>
		</table>
	</div>
</body>