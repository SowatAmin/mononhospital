<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Detailed Bill for {!! $patient->name !!}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 15px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 14px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 20px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">
		<div style="margin-top:10px;">
			<table width="80%" style="margin:0px auto">
				<tr>
					<td id="logo"><!--Logo--></td>
					<td style="padding-left:10px">
						<h3 align="center" style="margin-bottom:-10px">
							<strong>Monon Psychiatric Hospital</strong>
						</h3>
						<h4 align="center" style="margin-bottom:-10px">
							<strong>Treatment Centre for Mental illness and Drug Addiction</strong>
							
						</h4>
						<h5 align="center">
							<strong>20/20 Tajmohal Road, Muhammadpur, Dhaka</strong>
						</h5>
					</td>
				</tr>
			</table>
		</div>

		<hr style="border: 0;height: 2px;background: #333;">

		<h4 align="center" style="margin: 0px auto; margin-bottom:10px; width:10%;padding:5px 20px 5px 20px;border:3px double black">
			Total Bill
		</h4>
		
		<table width="100%" style="margin:0px auto">
			<tr>
				<th width="140px">Reg No.</th>
				<td>{!! $patient->id !!}</td>

				<th width="80px">Bill No.</th>
				<td>{!! $patient->billNo !!}</td>

				<th width="110px">Bed/Cabin No.</th>
				<td>{!! $patient->bedCabin !!}</td>
			</tr>
			<tr>
				<th>Patient's Name</th>
				<td colspan="5">{!! $patient->name !!}</td>
			</tr>
			<tr>
				<th>Guardian's Name</th>
				<td colspan="5">{!! $patient->guardianName !!}</td>
			</tr>
			<tr>
				<th>Admission Date</th>
				<td colspan="2">{!! $patient->admissionDate !!}</td>

				<th width="120px">Discharge Date</th>
				<td colspan="2">{!! $patient->releaseDate !!}</td>
			</tr>
		</table>
		
		<hr style="border: 0;height: 2px;background: #333;">
		<div style="margin-bottom:10px">
			<table id="verticalTable">
				<tr>
					<th align="left" width="200px">Particulars</th>
					<th width="75px" align="center">Date</th>
					<th align="center">Qty</th>
					<th align="center">Rate</th>
					<th align="center">Amount</th>
					<th align="center">VAT</th>
					<th align="center">VAT <br> Amt</th>
					<th align="center">Service <br> Charge<br>(%)</th>
					<th align="center">Service <br> Charge<br>Amt</th>
					<th align="center">Total</th>
				</tr>
				@foreach($masterCategories as $masterCategory)
				<tr>
					<td style="font-size:15px" colspan="10" border="0" align="left"><strong>{{$masterCategory->masterCategory}}</strong></td>
				</tr>

				@foreach($bills as $bill)
				@if($bill->masterCategory == $masterCategory->masterCategory)
				<tr>
					<td align="left">{{$bill->services}}</td>
					<td align="center">{{$bill->date}}</td>
					<td align="center">{{$bill->quantity}}</td>
					<td align="center">{{$bill->rate}}</td>
					<td align="center">{!! $bill->rate * $bill->quantity !!}</td>
					<td align="center">{{$bill->vat}}</td>
					<td align="center">{{$bill->vat * ($bill->rate * $bill->quantity)  / 100 }}</td>
					<td align="center">{{$bill->serviceCharge}}</td>
					<td align="center">{{$bill->serviceCharge * ($bill->rate * $bill->quantity) / 100}}</td>
					<td align="center">{{($bill->rate * $bill->quantity) + ($bill->vat * ($bill->rate * $bill->quantity)  / 100) + ($bill->serviceCharge * ($bill->rate * $bill->quantity) / 100) }}</td>
				</tr>

				@endif
				@endforeach

				<tr>
					<td style="font-size:15px" colspan="9"><strong>Total for {!! $masterCategory->masterCategory !!}</strong></td>
					<td style="font-size:15px" align="center"><strong>{{ $masterCategory->total }}</strong></td>
				</tr>
				<tr>
					<td colspan="10"><br></td>
				</tr>


				@endforeach
				<tr style="font-size:15px">
					<td colspan="4" align="left"> <strong>Net Total</strong> </td>
					<td align="center"> <strong>{!! $totalRate !!}</strong></td>
					<td align="center"></td>
					<td align="center"><strong>{!! $totalVatAmt !!}</strong></td>
					<td align="center"></td>
					<td align="center"><strong>{!! $totalServiceAmt !!}</strong></td>
					<td align="center"><strong>{!! $total!!}</strong></td>
				</tr>
			</table>
		</div>

	</div>

</div>

</body>