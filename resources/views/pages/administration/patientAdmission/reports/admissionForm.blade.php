<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Admission Form for {!! $patient->name !!}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{
			font-size: 15px;
		}		
		.page{
			background: white;			
			padding:30px 30px 10px 30px;
		}
		.verticalTable th{
			text-align: center;
			border: 1px solid black;
		}
		.verticalTable td{
			border-right: 1px solid black;
			border-left: 1px solid black;
		}
		.verticalTable{
			padding: 1px;
			margin: 0px;
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 13px;
		}
		th{
			text-align: left;
		}
		#logo{
			background-image: url("monon_logo.jpg");
			background-repeat: no-repeat;
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">
		<table style="margin-left:30px">
			<tr>
				<td><<img src="monon_logo.jpg"></td> <!--<img src="monon_logo.jpg">-->
				<td style="padding-left:30px">
					<h3 align="center" style="margin-bottom:-20px">
						<strong>Monon Psychiatric Hospital</strong>
					</h3>
					<h4 align="center">
						<strong>Treatment Centre for Mental illness and Drug Addiction</strong>
						<br>
						20/20 Tajmohal Road, Muhammadpur, Dhaka
					</h4>
				</td>
			</tr>
		</table>
		<hr style="border: 0;height: 2px;background: #333;">
		<h4 align="center" style="margin: 0 auto; margin-bottom:10px; width:20%;padding:5px 50px 5px 50px;border:3px double black">
			Admission Form
		</h4>
		<table width="100%">
			<tr>
				<th width="120px">Reg No.:</th>
				<td width="200px">{!! $patient->id !!}</td>				
				
				<th>Bed/Cabin No.:</th>
				<td>{!! $patient->bedCabin !!}</td>
			</tr>
			<tr>
				<th>Ref. by:</th>
				<td>{!! $patient->assignedConsultant !!}</td>				
				
				<th>Age:</th>
				<td>{!! $patient->age !!}</td>
			</tr>
			<tr>
				<th>Patient's name: </th>
				<td>{!! $patient->name !!}</td>				

				<th>Sex:</th>
				<td>{!! $patient->sex !!}</td>
			</tr>
			<tr>
				<th>Guardian's name:</th>
				<td>{!! $patient->guardianName !!}</td>				

				<th>Marital status:</th>
				<td>{!! $patient->maritalStatus !!}</td>
			</tr>
			<tr>
				<th rowspan="3">Present address:</th>
				<td style="word-wrap: break-word;" rowspan="3">{!! $patient->presentAddress !!}</td>

				<th>Blood group:</th>
				<td>{!! $patient->bloodGroup !!}</td>
			</tr>
			<tr>
				<th>Profession:</th>
				<td>{!! $patient->profession !!}</td>
			</tr>
			<tr>
				<th>Nationality:</th>
				<td>{!! $patient->nationality !!}</td>
			</tr>
			<tr>
				<th rowspan="3">Permanent Address:</th>
				<td word-wrap: break-word; rowspan="3">{!! $patient->permanentAddress!!}</td>

				<th>Religion:</th>
				<td>{!! $patient->religion !!}</td>
			</tr>
			<tr>
				<th>Police case:</th>
				<td>
					@if($patient->policeCase == 1)
					Yes
					@else
					No
					@endif
				</td>
			</tr>
			<tr>
				<th style="width:120px">Signature Of Guardian:</th>
				<td></td>
			</tr>
			<tr>
				<th>Phone number:</th>
				<td colspan="3">{!! $patient->guardianPhone !!}</td>
			</tr>
			<tr>
				<th>Admission date:</th>
				<td>{!! $patient->admissionDate !!}</td>

				<th>Time:</th>
				<td>{!! $patient->admissionTime !!}</td>
			</tr>
			<tr>
				<th>Discharge date:</th>
				<td>{!! $patient->releaseDate !!}</td>

				<th>Time:</th>
				<td>{!! $patient->releaseTime !!}</td>
			</tr>
		</table>

		

		<h3 style="text-decoration: underline; margin-bottom: 5px">
			<strong>Visitor List:</strong>
		</h3>

		<table width="100%" class="verticalTable">
			<tr>
				<th>No.</th>
				<th>Name Of Visitor</th>
				<th>Relation with Patient</th>
				<th>Contact No.</th>
				<th>Remarks</th>
			</tr>
			
			@foreach($visitors as $index => $visitor)
			
			<tr>
				<td align="center">{!! $index+1 !!}</td>
				<td>{!! $visitor->visitorName !!}</td>
				<td>{!! $visitor->visitorRelation !!}</td>
				<td>{!! $visitor->visitorPhone !!}</td>
				<td>{!! $visitor->visitorRemark !!}</td>
			</tr>

			@endforeach			
			
		</table>

		<table width="100%" style="margin-top:5px;">
			<tr>
				<th width="150px">M/O on duty:</th>
				<td width="230px">{!! $patient->moOnDuty !!}</td>

				<th>Signature of M/O:</th>
				<td></td>
			</tr>
		</table>

		<table width="100%">
			<tr>
				<th width="150px">Manager on duty:</th>
				<td width="230px">{!! $patient->manager !!}</td>

				<th>Signature of manager:</th>
				<td><br></td>
			</tr>		
		</table>

		<table width="100%" style="margin-top:2px;">
			<tr style="padding-top:50px">
				<th width="150px">Visitors allowed:</th>
				<td colspan="3">
					@if($patient->visitor == 1)
					Yes
					@else
					No
					@endif
				</td>
			</tr>

			<tr valign="top">
				<th width="170px" rowspan="2" colspan="2">Telephone call allowed:</th>				
				<th width="80px">Outgoing:</th>
				<td >
					@if($patient->outgoingCall == 1)
					Yes
					@else
					No
					@endif
				</td>
			</tr>
			<tr>
				<th width="50px">Incoming:</th>
				<td>
					@if($patient->incomingCall == 1)
					Yes
					@else
					No
					@endif
				</td>
			</tr>
			<tr>
				<th width="150px">Extra food allowed:</th>
				<td colspan="3">
					@if($patient->extraFood == 1)
					Yes
					@else
					No
					@endif
				</td>
			</tr>
			<tr>
				<th width="150px">Cigarettes allowed:</th>
				<td colspan="3">
					@if($patient->cigarette == 1)
					Yes
					@else
					No
					@endif
				</td>
			</tr>
			<tr>
				<th width="150px">Diagnosis:</th>
				<td colspan="3">{!! $patient->diagnosis !!}</td>
			</tr>
		</table>
	</div>

	<div class="page" style="page-break-before: always;">
		<h2 align="center">
			<strong>Rules and Regulations for Admitted Patients</strong>
		</h2>
		<div style="margin-top:5px">
			<ol>
				<li>
					An advance payment for at least one week has to be deposited on admission.
				</li>
				<li>
					The hospital management will not carry out any investigations; provide regular meals/extra food or medicine if admitted patients have any due. Under such circumstances, the hospital authority will provide food from the hospital staff mess until further order.
				</li>
				<li>
					If dues are not cleared off even after notification, the hospital management holds the right to discharge a patient & hand him/her over at the address provided during admission.
				</li>
				<li>
					Visiting Hours are as follows: Morning: 10.00am to 11.00am and Evening: 5.00pm to 7.00pm
				</li>
				<li>
					Male visitors to female patients are not allowed to enter the female wards or cabins. If the patient is ambulatory, male visitors can visit the patient in the hospital lobby.
				</li>
				<li>
					Attendants are not allowed to stay with the patients.
				</li>
				<li>
					Guardians need to provide necessary daily commodities for respective patient.
				</li>
				<li>
					Guardians are urged not to keep any valuable belongings with respective patients’. Hospital management will not be responsible for the loss or damage of any such item.
				</li>
				<li>
					Patients are not allowed to make or receive phone call unless otherwise mentioned.
				</li>
				<li>
					Guardians should call the hospital as convenient to be informed about respective patient.
				</li>
				<li>
					The hospital authority will call up or contact guardians by other possible means at any time of need in the context of patient’s need and all such calls or contacting expenses will be billed.
				</li>
				<li>
					Only enlisted visitors will be allowed to visit patients.
				</li>
			</ol>
		</div>

		<div>
			<hr style="border: 0;height: 1px;background: #333;">
		</div>

		<table width="100%"s>
			<tr>
				<td width="250px">Name of Guardian on admission:</td>
				<td>{!! $patient->guardianName !!}</td>
				<td align="right">Relation with Pt.:</td>
				<td>{!! $patient->guardianRel !!}</td>
			</tr>
			<tr>
				<td>Name of Alternate Guardian:<br>(In absence of guardian on admission)</td>
				<td width="150px"><br></td>
				<td align="right">Relation with Pt.:</td>
				<td><br></td>
			</tr>
			<tr>
				<td valign="top" rowspan="2">Address of Alternate Guardian:</td>
				<td rowspan="2" colspan="3"><br></td>
			</tr>
			<tr></tr>
			<tr>
				<td>Contact of Alternate Guardian:</td>
				<td valign="top" colspan="3"><br></td>
			</tr>
		</table>

		<div class="col-sm-12">
			<hr style="border: 0;height: 1px;background: #333;">
		</div>

		<div class="col-sm-12" style="margin-top:5px">
			<div class="col-sm-12">
				<p>
					By signing below I certify that all information provided regarding the patient and guardianships is true 
					and agree to abide by the above mentioned rules & regulations for my admitted patient. I also certify 
					that this admission is made with my full consent & is not the result of any influence.
				</p>
			</div>
		</div>

		<div class="col-sm-12" style="margin-top:70px;margin-bottom:1px">
			<div class="col-sm-12" style="margin-bottom:1px">
				<div class="col-sm-5" style="margin-bottom:1px">
					<hr style="border: 0;height: 1px;background: #333; margin-bottom:1px">
				</div>
			</div>

			<div class="col-sm-12" style="margin-top:1px">
				<div class="col-sm-12" style="margin-left:10px">
					<span>Signature of Guardian on admission</span>
				</div>
			</div>
		</div>

	</div>

	<div class="page" style="page-break-before: always;">
		<h2 align="center">
			<strong>Treatment on Admission</strong>
		</h2>
		
		<hr style="border: 0;height: 1px;background: #333;">		

		<table width="100%" style="margin-top:15px">
			<tr>
				<th>Patient's Name:</th>
				<td width="180px">{!! $patient->name !!}</td>

				<th width="40px">Age:</th>
				<td width="20px">{!! $patient->age !!}</td>

				<th>Bed/Cabin No.:</th>
				<td width="120px">{!! $patient->bedCabin !!}</td>
			</tr>
			<tr>
				<th>Admission Date:</th>
				<td>{!! $patient->admissionDate !!}</td>

				<th>Time:</th>
				<td colspan="3">{!! $patient->admissionTime !!}</td>
			</tr>
			<tr>
				<th>Ref. Doctor:</th>
				<td colspan="5">{!! $patient->assignedConsultant !!}</td>
			</tr>
			<tr>
				<th>Diagnosis:</th>
				<td colspan="5">{!! $patient->diagnosis !!}</td>
			</tr>
		</table>

		<table class="verticalTable" width="100%" style="margin-top:40px">
			<tr>
				<th>Consultant's Notes/Advice</th>
				<th>Treatment on admission</th>
			</tr>
			<tr>
				<td>{{$prescription->notes}}</td>
				<td>{{$prescription->treatment}}</td>
			</tr>
		</table>
	</div>
</body>