<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Consultant Bill for {!! $patient->name !!}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{
			font-size: 12px;
		}
		th{
			text-align: left;
		}
		tr{
			margin-top: 10px;
		}
		#verticalTable th{
			text-align: center;
			border: 1px solid black;
		}
		#verticalTable td{
			
		}
		#verticalTable{
			width: 100%;
			padding: 20px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}

		#logo{
			background-image: url("monon_logo.png");
			background-repeat: no-repeat;
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">
		<table width="100%" >
			<tr>
				<td style="border-right:1px dashed black; padding-right:20px">
					<div style="margin-top:10px;">
						<table width="90%" style="margin:0px auto">
							<tr>
								<td id="logo"><!--Logo--></td>
								<td style="padding-left:10px">
									<h3 align="center" style="margin-bottom:-10px">
										<strong>Monon Psychiatric Hospital</strong>
									</h3>
									<h4 align="center" style="margin-bottom:-10px">
										<strong>Treatment Centre for Mental illness and Drug Addiction</strong>

									</h4>
									<h5 align="center">
										<strong>20/20 Tajmohal Road, Muhammadpur, Dhaka</strong>
									</h5>
								</td>
							</tr>
						</table>
					</div>

					<hr style="border: 0;height: 2px;background: #333;">

					<h4 align="center" style="margin: 0px auto; margin-bottom:5px; width:100px; padding:5px 15px 5px 15px;border:3px double black">
						Consultant Bill
					</h4>

					<div style="margin-top:10px;margin-bottom:10px">
						<table width="100%" style="margin:0px auto">
							<tr>
								<th>Reg No.</th>
								<td>{!! $patient->id !!}</td>

								<th>Bed/Cabin No.</th>
								<td>{!! $patient->bedCabin !!}</td>
							</tr>
							<tr>
								<th>Patient's Name</th>
								<td colspan="3">{!! $patient->name !!}</td>
							</tr>
							<tr>
								<th>Guardian's Name</th>
								<td colspan="3">{!! $patient->guardianName !!}</td>
							</tr>
							<tr>
								<th>Admission Date</th>
								<td>{!! $patient->admissionDate !!}</td>

								<th>Discharge Date</th>
								<td>{!! $patient->releaseDate !!}</td>
							</tr>
						</table>
					</div>

					<hr style="border: 0;height: 2px;background: #333;">
					<div style="margin-top:10px;margin-bottom:10px">
						<table id="verticalTable">
							<tr>
								<th align="left">Particulars</th>
								<th align="center">Upto</th>
								<th align="left">Quantity</th>
								<th align="right">Amount</th>
							</tr>

							@foreach($masterCategories as $masterCategory)

							@foreach($serviceCategories as $category)
							@if($category->masterCategory == $masterCategory->masterCategory)
							<tr>
								<td colspan="4" align="left"><strong> {{$category->category}} </strong></td>
							</tr>

							@foreach($bills as $bill)
							@if($bill->category == $category->category)

							<tr>
								<td align="left">{{$bill->services}}</td>
								<td align="center">{{$bill->date}}</td>
								<td align="center">{{$bill->sumOfQuantity}}</td>
								<td align="right">{!! $bill->total !!}</td>
							</tr>

							@endif
							@endforeach

							@endif
							@endforeach

							@endforeach

							<tr>
								<td colspan="4"><br></td>
							</tr>
							<tr>
								<td style="border-top:2px solid black" colspan="3" align="right"><strong>Total:</strong></td>
								<td style="border-top:2px solid black" align="right">{!! $total !!}</td>
							</tr>
							<tr>
								<td colspan="3" align="right"><strong>Discount:</strong></td>
								<td align="right">{{$discount}}</td>
							</tr>
							<tr>
								<td colspan="3" align="right"><strong>Due:</strong></td>
								<td align="right">{{$due}}</td>
							</tr>
						</table>
					</div>
				</td>






				<td style="padding-left:20px">
					<div style="margin-top:10px;">
						<table width="90%" style="margin:0px auto">
							<tr>
								<td id="logo"><!--Logo--></td>
								<td style="padding-left:10px">
									<h3 align="center" style="margin-bottom:-10px">
										<strong>Monon Psychiatric Hospital</strong>
									</h3>
									<h4 align="center" style="margin-bottom:-10px">
										<strong>Treatment Centre for Mental illness and Drug Addiction</strong>

									</h4>
									<h5 align="center">
										<strong>20/20 Tajmohal Road, Muhammadpur, Dhaka</strong>
									</h5>
								</td>
							</tr>
						</table>
					</div>

					<hr style="border: 0;height: 2px;background: #333;">

					<h4 align="center" style="margin: 0px auto; margin-bottom:5px; width:100px; padding:5px 15px 5px 15px;border:3px double black">
						Consultant Bill
					</h4>

					<div style="margin-top:10px;margin-bottom:10px">
						<table width="100%" style="margin:0px auto">
							<tr>
								<th>Reg No.</th>
								<td>{!! $patient->id !!}</td>

								<th>Bed/Cabin No.</th>
								<td>{!! $patient->bedCabin !!}</td>
							</tr>
							<tr>
								<th>Patient's Name</th>
								<td colspan="3">{!! $patient->name !!}</td>
							</tr>
							<tr>
								<th>Guardian's Name</th>
								<td colspan="3">{!! $patient->guardianName !!}</td>
							</tr>
							<tr>
								<th>Admission Date</th>
								<td>{!! $patient->admissionDate !!}</td>

								<th>Discharge Date</th>
								<td>{!! $patient->releaseDate !!}</td>
							</tr>
						</table>
					</div>

					<hr style="border: 0;height: 2px;background: #333;">
					<div style="margin-top:10px;margin-bottom:10px">
						<table id="verticalTable">
							<tr>
								<th align="left">Particulars</th>
								<th align="center">Upto</th>
								<th align="left">Quantity</th>
								<th align="right">Amount</th>
							</tr>

							@foreach($masterCategories as $masterCategory)

							@foreach($serviceCategories as $category)
							@if($category->masterCategory == $masterCategory->masterCategory)
							<tr>
								<td colspan="4" align="left"><strong> {{$category->category}} </strong></td>
							</tr>

							@foreach($bills as $bill)
							@if($bill->category == $category->category)

							<tr>
								<td align="left">{{$bill->services}}</td>
								<td align="center">{{$bill->date}}</td>
								<td align="center">{{$bill->sumOfQuantity}}</td>
								<td align="right">{!! $bill->total !!}</td>
							</tr>

							@endif
							@endforeach

							@endif
							@endforeach

							@endforeach

							<tr>
								<td colspan="4"><br></td>
							</tr>
							<tr>
								<td style="border-top:2px solid black" colspan="3" align="right"><strong>Total:</strong></td>
								<td style="border-top:2px solid black" align="right">{!! $total !!}</td>
							</tr>
							<tr>
								<td colspan="3" align="right"><strong>Discount:</strong></td>
								<td align="right">{{$discount}}</td>
							</tr>
							<tr>
								<td colspan="3" align="right"><strong>Due:</strong></td>
								<td align="right">{{$due}}</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>