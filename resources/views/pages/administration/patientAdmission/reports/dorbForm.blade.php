<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DORB Form</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">
		<h2 align="center">Discharge on Own Risk Bond</h2>
		<table width="100%" style="font-size:18px">
			<tr style="">
				<td colspan="2"></td>
				<td align="right">Date:</td>
				<td style="border-bottom: 1px dashed black"> {{$today}} </td>
			</tr>
			<tr style="padding-bottom:10px">
				<td width="180px" style="padding-top:8px">Name of Patient:</td>
				<td colspan="3" style="border-bottom: 1px dashed black;padding-top:8px">{{$patient->name}}</td>
			</tr>
			<tr style="padding-bottom:10px">
				<td style="padding-top:8px">Bed No:</td>
				<td colspan="3" style="border-bottom: 1px dashed black;padding-top:8px">{{$patient->bedCabin}}</td>
			</tr>
			<tr style="padding-bottom:10px">
				<td style="padding-top:8px;padding-bottom:8px">Age:</td>
				<td colspan="3" style="border-bottom: 1px dashed black;padding-top:8px;margin-bottom:8px">
					{{$patient->age}}
				</td>
			</tr>
			<tr valign="top" style="padding-bottom:10px">
				<td style="padding-top:8px">Address:</td>
				<td colspan="3" rowspan="2" style="padding-top:8px">{{$patient->presentAddress}}</td>
			</tr>
			<tr>
				<td style="padding-top:8px"><br></td>
			</tr>
			<tr style="padding-bottom:10px">
				<td style="padding-top:8px">Name of the Consultant:</td>
				<td colspan="3" style="border-bottom: 1px dashed black;padding-top:8px">{{$patient->assignedConsultant}}</td>
			</tr>
			<tr style="padding-bottom:10px">
				<td style="padding-top:8px">Name of Guardian:</td>
				<td colspan="3" style="border-bottom: 1px dashed black;padding-top:8px">{{$patient->guardianName}}</td>
			</tr>
			<tr style="padding-bottom:10px">
				<td style="padding-top:8px">Relation with patient:</td>
				<td colspan="3" style="border-bottom: 1px dashed black;padding-top:8px;margin-bottom:8px";padding-bottom:8px>{{$patient->guardianRel}}</td>
			</tr>
			<tr valign="top" style="padding-bottom:10px">
				<td style="padding-top:8px">Address:</td>
				<td colspan="3" rowspan="2" style="padding-top:8px">{{$patient->permanentAddress}}</td>
			</tr>
			<tr>
				<td style="padding-top:8px"><br></td>
			</tr>
			<tr>
				<td style="padding-top:8px">Contact phone numbers:</td>
				<td colspan="3" style="border-bottom: 1px dashed black;padding-top:8px">
					{{$patient->guardianPhone}}, 
					@if($patient->altGuardianPhone)
						{{$patient->altGuardianPhone}}(Alt.)
					@endif
				</td>
			</tr>
			<tr>
				<td colspan="4" style="padding-top:50px">
					This is to certify that the mentioned guardian of the patient is taking the patient at his/her own risk.
				</td>	
			</tr>
			<tr>
				<td colspan="4" style="padding-top:15px">
					The hospital authority or the consultant of the patient shall not be responsible for any consequence after discharge.
				</td>
			</tr>
			<tr valign="top">
				<td style="border-bottom: 1px solid black;padding-top:120px"></td>
				<td colspan="3"><br></td>
			</tr>
			<tr>
				<td align="center">Signature of Guardian</td>
				<td colspan="3"><br></td>
			</tr>
		</table>
	</div>
</body>