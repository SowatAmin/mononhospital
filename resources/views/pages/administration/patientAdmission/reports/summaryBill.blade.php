<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Summary Bill for {!! $patient->name !!}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{
			font-size: 15px;
		}
		th{
			text-align: left;
		}
		tr{
			margin-top: 10px;
		}
		#verticalTable th{
			border: 1px solid black
		}
		#verticalTable td{
		}
		#verticalTable{
			width: 95%;
			padding: 20px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
			font-size: 14px;
		}

		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">
		<div style="margin-top:10px;">
			<table width="80%" style="margin:0px auto">
				<tr>
					<td id="logo"><!--Logo--></td>
					<td style="padding-left:10px">
						<h3 align="center" style="margin-bottom:-10px">
							<strong>Monon Psychiatric Hospital</strong>
						</h3>
						<h4 align="center" style="margin-bottom:-10px">
							<strong>Treatment Centre for Mental illness and Drug Addiction</strong>
							
						</h4>
						<h5 align="center">
							<strong>20/20 Tajmohal Road, Muhammadpur, Dhaka</strong>
						</h5>
					</td>
				</tr>
			</table>
		</div>

		<hr style="border: 0;height: 2px;background: #333;">

		<h4 align="center" style="margin: 0px auto; margin-bottom:10px; width:10%;padding:5px 20px 5px 20px;border:3px double black">
			Total Bill
		</h4>		

		<div>
			<table width="100%" style="margin:0px auto">
				<tr>
					<th width="140px">Reg No.</th>
					<td>{!! $patient->id !!}</td>

					<th width="80px">Bill No.</th>
					<td>{!! $patient->billNo !!}</td>

					<th width="110px">Bed/Cabin No.</th>
					<td>{!! $patient->bedCabin !!}</td>
				</tr>
				<tr>
					<th>Patient's Name</th>
					<td colspan="5">{!! $patient->name !!}</td>
				</tr>
				<tr>
					<th>Guardian's Name</th>
					<td colspan="5">{!! $patient->guardianName !!}</td>
				</tr>
				<tr>
					<th>Admission Date</th>
					<td colspan="2">{!! $patient->admissionDate !!}</td>

					<th width="120px">Discharge Date</th>
					<td colspan="2">{!! $patient->releaseDate !!}</td>
				</tr>
			</table>	
		</div>
		
		<hr style="border: 0;height: 2px;background: #333;">

		<div style="margin-bottom:10px;margin-top:-20px" >
			<table id="verticalTable">
				<tr>
					<th align="left">Particulars</th>
					<th align="center">Upto</th>
					<th align="center">Quantity</th>
					<th align="center">Amount</th>
					<th align="center">Total</th>
				</tr>
				@foreach($masterCategories as $masterCategory)
				<tr>
					<td colspan="5" style="color: green;" align="left"><strong>{{$masterCategory->masterCategory}}</strong></td>
				</tr>

				@foreach($serviceCategories as $category)

				@if($category->masterCategory == $masterCategory->masterCategory)
				<tr>
					<td colspan="5" align="left"><strong> {{$category->category}} </strong></td>
				</tr>

				@foreach($bills as $bill)
				@if($bill->category == $category->category)
				<tr>
					<td align="left">{{$bill->services}}</td>
					<td align="center">{{$bill->date}}</td>
					<td align="center">{{$bill->sumOfQuantity}}</td>
					<td align="right">{!! $bill->total !!}</td>
				</tr>
				@endif
				@endforeach

				@endif
				@endforeach

				<tr>
					<td colspan="4"></td>
					<td style="color: green; min-width:15px" align="right"><strong>{{$masterCategory->total}}</strong></td>					
				</tr>

				@endforeach

			</table>

			<hr style="border: 0;height: 2px;background: #333;">

			<table align="right" style="margin-right:55px">
				<tr>
					
					<td align="right" style="padding-right:20px">Sub Total: </td>
					<td align="right"><strong> {!! $total !!}</strong></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:20px">Payment: </td>
					<td align="right"><strong> {{$payment}}</strong></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:20px">Discount: </td>
					<td align="right"><strong> {{$discount}}</strong></td>
				</tr>
				<tr>
					<td align="right" style="padding-right:20px">Due: </td>
					<td align="right"><strong> {{$due}}</strong></td>
				</tr>
			</table>
		</div>

	</div>

</div>

</body>