<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>EMS Form</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
			padding-top: 10px;
		}
		td{
			padding-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">
		<h2 align="center">MONON Psychiatric Hospital</h2>
		<h3 align="center" style="margin-top:-30px">Treatment Center for Mental Illness and Drug Addiction</h3>
		<h4 align="center">
			20/20 Tajmahal Road, Mohammadpur, Dhaka<br>
			Telephone - 9114550, 0171243820
		</h4>

		<h3 align="center" style="margin-top:-30px">
		 EMS Form
		</h3>

		<table width="100%" style="font-size:18px">
			<tr>
				<th colspan="7">Patient Particulars</th>
			</tr>
			<tr>
				<th colspan="2" width="150px">Name of patient:</th>
				<td colspan="5" style="border-bottom: 1px dashed black">{{$patient->name}}</td>
			</tr>
			<tr>
				<th width="20px">Age:</th>
				<td width="100px" align="center" style="border-bottom: 1px dashed black">{{$patient->age}}</td>
				<th align="right">Sex:</th>
				<td align="center" style="border-bottom: 1px dashed black">{{$patient->sex}}</td>
				<th align="right" colspan="2">Marital Status:</th>
				<td align="center" style="border-bottom: 1px dashed black">{{$patient->maritalStatus}}</td>
			</tr>
			<tr valign="top">
				<th colspan="2">Address of patient:</th>
				<td rowspan="2" colspan="5">{{$patient->presentAddress}}</td>
			</tr>
			<tr><td colspan="2"><br></td></tr>
			<tr>
				<th colspan="2">Advised by (Consultant):</th>
				<td colspan="5" style="border-bottom: 1px dashed black">{{$patient->assignedConsultant}}</td>
			</tr>
			
			<tr>
				<th colspan="7">Guardian's particulars and undertaking</th>
			</tr>
			<tr>
				<th colspan="2">Name of Guardian:</th>
				<td colspan="5" style="border-bottom: 1px dashed black" >{{$patient->guardianName}}</td>
			</tr>
			<tr>
				<th colspan="2">Relation with patient:</th>
				<td colspan="5" style="border-bottom: 1px dashed black">{{$patient->guardianRel}}</td>
			</tr>
			<tr>
				<th colspan="2">Address of Guardian:</th>
				<td colspan="5"></td>
			</tr>
			<tr>
				<th colspan="7">Contact telephone numbers:</th>
			</tr>
			<tr>
				<td colspan="7" style="border-bottom: 1px dashed black"><br></td>
			</tr>
			<tr>
				<td colspan="7" style="border-bottom: 1px dashed black"><br></td>
			</tr>
			<tr>
				<td colspan="7">
					I am making a request for Emergency Medical Service for the above named patient to be treated by the mentioned consultant.
				</td>
			</tr>
			<tr valign="bottom">
				<th style="padding-top:100px" colspan="2">Signature of Guardian</th>
				<td colspan="2"><br></td>
				<th align="center">Date:</th>
				<td colspan="2"></td>
			</tr>
		</table>
	</div>
</body>