<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Discharge Certificate</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		tr{
			
		
		}
		td{
			padding-top: 14px;
		}
		#sec_table{
			height: 100%;
			width: 100%;
		}

	</style>
</head>

<body>
	<div class="page">
		<table width="100%">
			<tr>
				<td colspan="2" width="480px"><h4 align="left">TREATMENT ON DISCHARGE WITH ADVICE</h4></td>
				<td colspan="4" align="center"><h3>MONON PSYCHIATRIC HOSPITAL</h3></td>
			</tr>
			<tr valign="top">
				<td colspan="2" rowspan="11">{{$prescription->tda}}</td>
				<td colspan="4" align="center">
					<h4>Treatment Center for Mental Illness & Drug Addiction</h4>
					<strong>
						House no - 20/20, Block-C, Mohammadpur, Dhaka <br>
						Tel: 9114550, Mobile No - 01711243820,01718333133 <br>
						E-mail address - monon98@dhaka.net
					</strong>
				</td>
			</tr>
			<tr>
				<td colspan="4" align="center"><h3>DISCHARGE CERTIFICATE</h3></td>
			</tr>
			<tr>
				<td width="50px">ID No:</td>
				<td colspan="3" style="border-bottom:1px dashed black"> {{$patient->id}} </td>
			</tr>
			<tr>
				<td colspan="2" width="120px">Name of Patient:</td>
				<td colspan="2" style="border-bottom:1px dashed black">{{$patient->name}}</td>
			</tr>
			<tr>
				<td>Age:</td>
				<td style="border-bottom:1px dashed black">{{$patient->age}}</td>
				<td align="right">Sex:</td>
				<td style="border-bottom:1px dashed black">{{$patient->sex}}</td>
			</tr>
			<tr>
				<td colspan="2">Bed No/Cabin No:</td>
				<td colspan="2" style="border-bottom:1px dashed black">{{$patient->bedCabin}}</td>
			</tr>
			<tr>
				<td>Address:</td>
				<td colspan="3" rowspan="2">{{$patient->address}}</td>
			</tr>
			<tr><td><br></td></tr>
			<tr>
				<td colspan="2">Date of Admission:</td>
				<td colspan="2" style="border-bottom:1px dashed black">{{$patient->admissionDate}}</td>
			</tr>
			<tr>
				<td colspan="2">Date of Discharge:</td>
				<td colspan="2" style="border-bottom:1px dashed black">{{$patient->releaseDate}}</td>
			</tr>
			<tr>
				<td width="50px">Consultant:</td>
				<td colspan="3" style="border-bottom:1px dashed black">{{$patient->assignedConsultant}}</td>
			</tr>
			<tr>
				<td width="50px">Medical Officer</td>
				<td >{{$patient->moOnDuty}}</td>
				<td width="50px">Diagnosis:</td>
				<td colspan="3" style="border-bottom:1px dashed black">{{$patient->diagnosis}}</td>
			</tr>
		</table>
	</div>
	<div class="page" style="page-break-before: always;">
		<table width="100%">
			<tr>
				<td width="480px"><h4>NOTICE:</h4></td>
				<td><h4>INVESTIGATIONS:</h4></td>
			</tr>
			<tr valign="top">
				<td rowspan="3">{{$prescription->notes}}</td>
				<td height="200px">{{$prescription->investigation}}</td>
			</tr>
			<tr>
				<td><h4>TREATMENT GIVEN:</h4></td>
			</tr>
			<tr valign="top">
				<td height="250px">{{$prescription->treatment}}</td>
			</tr>
		</table>
	</div>
</body>