@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Edit Daily Rent & Charge</h3>
		<form action="/accountLedger/update" class="form-horizontal" method="post"> {!! csrf_field() !!}
			<div class="col-sm-12">
				<div class="col-sm-6">
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Date</span>
						<input class="form-control" type="input" class="form-control" name="bdate" id="bdate" value="{{$bill->date}}">
					</div>									
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Master Category</span>
						<select class="select form-control" id="master_category_acc" name="master_category_acc">
							<option value="{{ $bill->masterCategory }}">{{ $bill->masterCategory }}</option>
							@foreach($mastercatagories as $mastercategory)
							<option value="{{ $mastercategory->id }}">{{$mastercategory->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Category</span>
						<select class="select form-control" name="subcategory_acc" id="subcategory_acc" >
							<option value="{{ $bill->category }}">{{ $bill->category }}</option>
						</select>
					</div>
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Services</span>
						<select class="select form-control" id="services_acc" name="services_acc">
							<option value="{{ $bill->services }}">{{ $bill->services }}</option>
						</select>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Quantity</span>
						<input type="text" class="form-control" style="height:auto" id="quantity_acc" name="quantity_acc" value="{{$bill->quantity}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Rate</span>
						<input type="text" class="form-control" style="height:auto"  id="rate_acc" name="rate_acc" value="{{$bill->rate}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Vat</span>
						<input type="text" class="form-control" style="height:auto" id="vat_acc" name="vat_acc" value="{{$bill->vat}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Service Charge</span>
						<input type="text" class="form-control" style="height:auto" id="servicecharge_acc" name="servicecharge_acc" value="{{$bill->serviceCharge}}">
					</div>
				</div>

				<div class="col-sm-12">
					<div class="input-group" style="margin-top:10px">
						<span class="input-group-addon" style="min-width:120px;text-align:left">Remarks</span>
						<textarea name="remarks_acc" id="remarks_acc" rows="2" class="form-control"></textarea>
					</div>
				</div>

				<input type="hidden" name="regNo" id="regNo" value="{{$bill->regNo}}">
				<input type="hidden" name="id" id="id" value="{{$bill->id}}">



				<div class="col-sm-12">									
					<div class="btn-group" style="margin-top:20px;float:right;">
						<button type="submit" class="btn btn-primary" id="editBill">
							<span class="glyphicon glyphicon-pencil"></span> Save
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@section('essentialScript')
<script type="text/javascript">
//account ledger master category

$(document).ready(function() {
	$("#master_category_acc").on('change',function(e){
		console.log(e);
		var mcat_id=e.target.value;


		$.get('/edit/ajax-subcat?mcat_id='+mcat_id,function(data){

			console.log(data);
			$("#subcategory_acc").empty();
			$.each(data,function(index,subcatObj){
				$("#subcategory_acc").append(' <option value="'+subcatObj.name+'">'+subcatObj.name+'</option> ');



			});

		});
	});
});



//account ledger sub-category
$(document).ready(function() {



	$('#subcategory_acc').on('change',function(e){
		console.log(e);
		var mcat_name=$("#master_category_acc").val();
		//alert(mcat_name);
		var cat_name=$("#subcategory_acc").val();
		//alert(cat_name);
		var tablename="";
		if(mcat_name==1)      {tablename="Bed";}
		else if(mcat_name==2) {tablename="Doctors";}
		else if(mcat_name==3) {tablename="Hospitalservices";}
		else				  {tablename="Nonprofitservices";}

		//alert(tablename);

		$.get('/edit/ajax-cat?cat_name='+cat_name+'&tablename='+tablename,function(data){
			
			console.log(data);
			Y=data;
			$("#services_acc").empty();
			$.each(data,function(index,serviceObj){
				$("#services_acc").append("<option value="+'"'+serviceObj.name+'"'+">"+serviceObj.name+"</option>");
					//X=JSON.parse(serviceObj);
					
				});
			
			

		});
	});
});




//account ledger services
$(document).ready(function(){
	$("#services_acc").on('change',function(){
		var indexx=$('#services_acc').val();
		//alert(indexx);

		var count = Object.keys(Y).length;
		

		for(i=0;i<count;i++)
		{
			if(indexx===Y[i].name)
			{

				$("#rate_acc").val(Y[i].Rate);
				$("#vat_acc").val(Y[i].VatRate);
				$("#servicecharge_acc").val(Y[i].ServiceChargeRate);


			//alert($("#rate_acc").val());
			
			break;
		}		
	}

	ON_ACC=Y[i].name;
	OR_ACC=Y[i].Rate;
	OV_ACC=Y[i].VatRate;
	OSC_ACC=Y[i].ServiceChargeRate;

		//console.log(X);

	});
	
});


$("#bdate").datepicker({ dateFormat: 'yy-mm-dd' });
</script>
@endsection

@endsection