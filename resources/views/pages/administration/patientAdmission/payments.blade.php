@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Payment</h3>
		{!! Form::open(['url'=>'patientPayments']) !!}
		<form class="form-horizontal">

			<div class="col-md-12">			
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Date</span>
						<input type="date" name="paymentDate" class="form-control">
					</div>
				</div>

				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Mode</span>
						<select name="desc" class="select form-control">
							<option>Bank 01</option>
							<option>Cash in Hand</option>
							<option>IOU</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Amount</span>
						<input type="text" name="payment" class="form-control">
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Description</span>
						<select name="" class="select form-control">
							<option>Advance Return</option>
							<option>Bank Deposit</option>
							<option>Bill</option>
							<option>Cash in Hand</option>
							<option>Cheque</option>
							<option>Cheque No : J S B 6947374</option>
							<option>Cheque No : J S B 6947375</option>
							<option>Cheque No : J S B 6947378</option>
							<option>Cheque No 2568241</option>
							<option>Cheque No 3006013</option>
							<option>Seat Rent</option>
							<option>T T</option>
						</select>
					</div>
				</div>
				
			</div>

			<div class="col-sm-12">
				<hr style="border: 0;height: 1px;background: #333;">
			</div>

			<div class="col-sm-12">
				<div class="btn-group">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<span class="glyphicon glyphicon-plus"></span> Add
						</button>
					</div>
					<div class="btn-group">
						<a class="btn btn-success" href="/patientAdmission/{!! $patient->id !!}/edit"><span class="glyphicon glyphicon-arrow-left"></span> Back to Patient Admission</a>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<hr style="border: 0;height: 1px;background: #333;">
			</div>

			<!--Table of Data for Payment-->
			<div style="width:100%; max-height:300px; overflow: auto; font-size:12px">
				<table class="table table-bordered">
					<tr>
						<th align="center">ID</th>
						<th align="center">Reg No.</th>
						<th align="center">Payment Date</th>
						<th align="center">Bed Cabin No.</th>
						<th align="center">Description</th>
						<th align="center">Amount</th>
						<th align="center">Recieved By</th>
						<th align="center">Posted</th>
						<th align="center">Edit/Delete</th>
					</tr>

					@foreach($payments as $payment)
					<tr>
						<td align="center">$payment->id</td>
						<td align="center">$payment->regNo</td>
						<td align="center">$payment->paymentDate</td>
						<td align="center">$payment->bedCabin</td>
						<td align="center">$payment->desc</td>
						<td align="center">$payment->payment</td>
						<td align="center">$payment->recievedBy</td>
						<td align="center">$payment->posted</td>
						<td align="center">
							{!! Form::open(array('url' => '/patientPayment/' . $data->id)) !!}
							{!! Form::hidden('_method', 'DELETE') !!}
							<a href="/patientPayment/{!! $payment->id !!}/edit"><span class="glyphicon glyphicon-pencil"></span></a>
							<button type="submit" class="btn btn-danger">Delete</button>
							{!! Form::close() !!}
						</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</form>
</div>
</div>

@section('essentialScript')
<script type="text/javascript">

</script>
@endsection

@endsection