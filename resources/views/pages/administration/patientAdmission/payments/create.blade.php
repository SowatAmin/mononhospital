@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Services Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Master Categories</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">Payment</h3>
		<form action="/patientPayment/patient-payment" class="form-horizontal" method="post"> {!! csrf_field() !!}
			<div class="col-md-12">
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Date</span>
						<input type="input"  name="paymentDate" id="paymentDate" class="form-control">
					</div>
				</div>

				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Mode</span>
						<select name="desc" class="select form-control">
							<option>Bank 01</option>
							<option>Cash in Hand</option>
							<option>IOU</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Amount</span>
						<input type="text" name="payment" class="form-control">
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Description</span>
						<select name="" class="select form-control">
							<option>Advance Return</option>
							<option>Bank Deposit</option>
							<option>Bill</option>
							<option>Cash in Hand</option>
							<option>Cheque</option>
							<option>Cheque No : J S B 6947374</option>
							<option>Cheque No : J S B 6947375</option>
							<option>Cheque No : J S B 6947378</option>
							<option>Cheque No 2568241</option>
							<option>Cheque No 3006013</option>
							<option>Seat Rent</option>
							<option>T T</option>
						</select>
					</div>
				</div>
				<input type="hidden" name="regNo" value="{!!$id!!}">				
			</div>

			<div class="col-sm-12">
				<hr style="border: 0;height: 1px;background: #333;">
			</div>

			<div class="col-sm-12">
				<div class="btn-group">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">
							<span class="glyphicon glyphicon-plus"></span> Add Payment
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@section('essentialScript')
<script>
$("#paymentDate").datepicker({ dateFormat: 'yy-mm-dd' }).datepicker("setDate", new Date());

</script>
@endsection

@endsection