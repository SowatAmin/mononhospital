<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Discount List Between {{$from}} To {{$to}}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 14px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 12px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 5px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		<h4>Discount List Between {{$from}} To {{$to}}</h4>
		<table width="100%" id="verticalTable">
			<tr>
				<th align="center">Reg<br>No</th>
				<th width="160px" align="left">Name</th>
				<th width="70px" align="center">Date</th>
				<th width="100px" align="center">Bed/Cabin</th>
				<th align="center">Consultant<br>Discount</th>
				<th align="center">Hospital<br>Discount</th>
				<th align="center">Total <br> Discount</th>
				<th align="center" width="120px">Referenced By</th>
			</tr>

			@foreach($consultants as $consultant)

			<tr>
				<td colspan="8">
					<strong>{{ $consultant->assignedConsultant }}</strong>
				</td>
			</tr>

			@foreach($discounts as $discount)
			@if($discount->consultant == $consultant->assignedConsultant)

			<tr>
				<td align="center">{{$discount->regNo}}</td>
				<td style="padding-left:5px" align="left">{{$discount->name}}</td>
				<td align="center">{{$discount->date}}</td>
				<td style="padding-left:5px" align="left">{{$discount->bedCabin}}</td>
				<td style="padding-right:5px" align="right">{{$discount->consultDisc}}</td>
				<td style="padding-right:5px" align="right">{{$discount->hospDisc}}</td>
				<td style="padding-right:5px" align="right">{{$discount->totalDisc}}</td>
				<td style="padding-left:5px" align="left">{{$discount->refBy}}</td>
			</tr>

			@endif
			@endforeach

			<tr>
				<td colspan="4"style="padding-left:5px" align="left">
					<strong>Subtotal: <span style="padding-left:4em">{{$consultant->patients}}</span></strong>
				</td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->consultDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->hospDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->totalDisc}}</strong></td>
				<td></td>
			</tr>

			<tr><td colspan="8"><br></td></tr>

			@endforeach

			<tr>
				<td colspan="4"style="padding-left:5px" align="left">
					<strong>Total: <span style="padding-left:5em">{{$total->patients}}</span></strong>
				</td>
				<td style="padding-right:5px" align="right"><strong>{{$total->consultDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$total->hospDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$total->totalDisc}}</strong></td>
				<td></td>
			</tr>

		</table>

	</div>

</div>

</body>