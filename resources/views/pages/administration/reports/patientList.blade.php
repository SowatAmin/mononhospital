<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Patient List</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 15px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 14px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 5px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		<h4>Patient List Between {{$from}} To {{$to}}</h4>
		<table width="100%" id="verticalTable">
			<tr>
				<th>Reg No</th>
				<th align="left" width="200px">Name</th>
				<th width="75px" align="center">Ad. Date</th>
				<th align="center">Age</th>
				<th align="center">Sex</th>
				<th align="center">M. Status</th>
				<th align="center">Phone</th>
				<th align="center">Bed/Cabin</th>
				<th align="center">Terrief</th>
				<th align="center">Ref. Doctor</th>
			</tr>
			@foreach($patients as $patient)
			<tr>
				<td>{{$patient->id}}</td>
				<td align="left" width="200px">{{$patient->name}}</td>
				<td width="75px" align="center">{{$patient->admissionDate}}</td>
				<td align="center">{{$patient->age}}</td>
				<td align="center">{{$patient->sex}}</td>
				<td align="center">{{$patient->maritalStatus}}</td>
				<td align="center">{{$patient->guardianPhone}}</td>
				<td align="center">{{$patient->bedCabin}}</td>
				<td align="center">{{$patient->bedRate}}</td>
				<td align="center">{{$patient->assignedConsultant}}</td>
			</tr>				
			@endforeach
			<tr>
				<td><strong>Total</strong></td>
				<td> {{$count}} </td>
				<td colspan="8"></td>
			</tr>
		</table>

	</div>

</div>

</body>