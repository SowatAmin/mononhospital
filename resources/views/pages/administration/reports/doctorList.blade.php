<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Doctor's List</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 15px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 14px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 5px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		<h4>Doctor's List</h4>

		<div>
			<table width="100%" id="verticalTable">
				<tr>
					<th>Serial No</th>
					<th align="left" width="200px">Name</th>
					<th align="center">Address</th>
					<th align="center">Phone</th>
					<th align="center">Designation</th>
					<th align="center">Specializaton</th>
				</tr>
				@foreach($doctors as $doctor)
				<tr>
					<td>{{$doctor->id}}</td>
					<td align="left" width="200px">{{$doctor->name}}</td>
					<td align="center">{{$doctor->DocAddress}}</td>
					<td align="center">{{$doctor->DocPhone}}</td>
					<td align="center">{{$doctor->DocDesignation}}</td>
					<td align="center">{{$doctor->DocSpecialization}}</td>
				</tr>				
				@endforeach
				<tr>
					<td><strong>Total</strong></td>
					<td> {{$count}} </td>
					<td colspan="4"></td>
				</tr>
			</table>
		</div>

	</div>

</div>

</body>