<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Discount List Between {{$from}} To {{$to}}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 14px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 12px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 5px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		<h4>Discount List Between {{$from}} To {{$to}}</h4>
		<table width="100%" id="verticalTable">
			<tr>
				<th align="center">Reg<br>No</th>
				<th width="155px" align="left">Name</th>
				<th width="65px" align="center">Date</th>
				<th align="center">Seat<br>Rent</th>
				<th align="center">Hospital<br>Discount</th>
				<th align="center">Consul.<br>bill</th>
				<th align="center">Consul.<br>Discount</th>
				<th align="center">Hospital Bill<br>Without Disc.</th>
				<th align="center">Consul. Bill<br>Without Disc.</th>
				<th align="left" width="100px">Patient of</th>
			</tr>

			@foreach($consultants as $consultant)

			@foreach($patients as $patient)
			@if($patient->assignedConsultant == $consultant->assignedConsultant)

			<tr>
				<td align="center">{{$patient->id}}</td>
				<td style="padding-left:5px" align="left">{{$patient->name}}</td>
				<td align="center">{{$patient->admissionDate}}</td>
				<td style="padding-left:5px" align="left">{{$patient->bedCharge}}</td>
				<td style="padding-right:5px" align="right">{{$patient->hospDisc}}</td>
				<td style="padding-right:5px" align="right">{{$patient->consultCharge}}</td>
				<td style="padding-right:5px" align="right">{{$patient->consultDisc}}</td>
				<td style="padding-right:5px" align="right">{{$patient->hospital}}</td>
				<td style="padding-right:5px" align="right">{{$patient->consultant}}</td>
				<td style="padding-left:5px" align="left">{{$patient->assignedConsultant}}</td>
			</tr>

			@endif
			@endforeach

			<tr>
				<td colspan="3" style="padding-left:5px" align="left">
					<strong>Consultant Total: <span style="padding-left:4em">{{$consultant->patients}}</span></strong>
				</td>
				<td style="padding-left:5px" align="left"><strong>{{$consultant->bedCharge}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->hospDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->consultCharge}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->consultDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->hospital}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$consultant->consultant}}</strong></td>
				<td></td>
			</tr>

			<tr><td colspan="10"><br></td></tr>

			@endforeach

			<tr>
				<td colspan="3"style="padding-left:5px" align="left">
					<strong>Total: <span style="padding-left:9em">{{$total->patients}}</span></strong>
				</td>
				<td style="padding-left:5px" align="left"><strong>{{$total->bedCharge}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$total->hospDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$total->consultCharge}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$total->consultDisc}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$total->hospital}}</strong></td>
				<td style="padding-right:5px" align="right"><strong>{{$total->consultant}}</strong></td>
				<td></td>
			</tr>

		</table>

	</div>

</div>

</body>