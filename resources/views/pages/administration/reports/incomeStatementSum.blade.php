<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Income Statement Sum Between {{$from}} To {{$to}}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 15px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 13px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		<h4>Income Statement Sum Between {{$from}} To {{$to}}</h4>
		<table width="100%" id="verticalTable">
			<tr>
				<th rowspan="2" align="center">Consultant</th>
				<th rowspan="2" align="center">Number of<br>Patients</th>
				
				<th colspan="4" align="center">Hospital Bill</th>
				
				<th rowspan="2" align="center">Total</th>				
				<th colspan="2" align="center">Discount</th>

				<th rowspan="2" align="center">Gross<br>Income</th>
			</tr>
			<tr>
				<th>Bed</th>
				<th>MSC</th>
				<th>OAC</th>
				<th>A. Fee</th>

				<th>Consultant</th>
				<th>Hospital</th>
			</tr>

			@foreach($consultants as $consultant)

			<tr>
				<td align="left">{{$consultant->assignedConsultant}}</td>
				<td align="center">{{$consultant->patients}}</td>

				<td align="right">{{$consultant->bedFee}}</td>
				<td align="right">{{$consultant->msc}}</td>
				<td align="right">{{$consultant->oac}}</td>
				<td align="right">{{$consultant->aFee}}</td>

				<td align="right">{{$consultant->total}}</td>
				<td align="right">{{$consultant->hospDisc}}</td>
				<td align="right">{{$consultant->consultDisc}}</td>
				<td align="right">{{$consultant->gross}}</td>
			</tr>

			@endforeach

			<tr>
				<td align="left"><strong>Total</strong></td>
				<td align="center"><strong> {{$total->patients}} </strong></td>

				<td align="right"><strong>{{$total->bedFee}}</strong></td>
				<td align="right"><strong>{{$total->msc}}</strong></td>
				<td align="right"><strong>{{$total->oac}}</strong></td>
				<td align="right"><strong>{{$total->aFee}}</strong></td>
				
				<td align="right"><strong>{{$total->total}}</strong></td>
				<td align="right"><strong>{{$total->hospDisc}}</strong></td>
				<td align="right"><strong>{{$total->consultDisc}}</strong></td>
				<td align="right"><strong>{{$total->gross}}</strong></td>			
			</tr>
		</table>
	</div>
</div>

</body>