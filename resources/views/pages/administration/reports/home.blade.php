@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Hospital Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Reports</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<div style="margin-top:20px" id="patientFullDetails" class="col-md-12">
			<div id="navigation">
				<ul class="nav nav-tabs nav-justified">
					<li class="active">
						<a data-toggle="tab" href="#patientList">Patient <br> List</a>
					</li>
					<li>
						<a data-toggle="tab" href="#doctorsList">Doctor's <br> list</a>
					</li>
					<li>
						<a data-toggle="tab" href="#discountList">Discount <br> List</a>
					</li>
					<li>
						<a data-toggle="tab" href="#patientRegister">Patient <br> Register</a>
					</li>
					<li>
						<a data-toggle="tab" href="#duePatientList">Due Patient <br> List</a>
					</li>
					<li>
						<a data-toggle="tab" href="#discountListWithCharges">Discount List <br> with Charges</a>
					</li>
					<li>
						<a data-toggle="tab" href="#incomeStatement">Income <br> Statement</a>
					</li>
					<li>
						<a data-toggle="tab" href="#incomeStatementSum">Income Statement<br>Sum</a>
					</li>
				</ul>
			</div>
			<div class="tab-content">

				<!--Patient List-->
				<div id="patientList" class="tab-pane fade in active" style="margin-top:5px">
					<form action="/reports/patient-list" class="form-horizontal" target="_blank" method="post"> {!! csrf_field() !!}
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">Status</span>
								<select class="select form-control" name="admissionStatus" id="admissionStatus">
									<option value="Admitted">Admitted</option>
									<option value="Released">Released</option>
									<option value="Booking">Booking</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="input" class="form-control" name="fromDate" id="fromDate">
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="input" class="form-control" name="toDate" id="toDate">
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-save"></span> View Patient List
								</button>
							</div>
						</div>
					</form>
				</div>
				





				<div id="doctorsList" class="tab-pane fade">
					<form action="/reports/doctor-list" target="_blank" class="form-horizontal" method="post">
					{!! csrf_field() !!}
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:10px">
									<span class="glyphicon glyphicon-save"></span> View Doctors List
								</button>
							</div>
						</div>
					</form>
				</div>





				<div id="discountList" class="tab-pane fade">
					<form action="/reports/discount-list" target="_blank" class="form-horizontal" method="post"> {!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="input" class="form-control" name="fromDate" id="fromDate1" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="input" class="form-control" name="toDate" id="toDate1" >
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-save"></span> View Discount List
								</button>
							</div>
						</div>
					</form>
				</div>





				<div id="patientRegister" class="tab-pane fade">
					<form action="/reports/patient-register" target="_blank" class="form-horizontal" method="post"> {!! csrf_field() !!}
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
									<input class="form-control" type="input" class="form-control" name="fromDate" id="fromDate2" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group input-group-sm" style="margin-top:10px">
									<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
									<input class="form-control" type="input" class="form-control" name="toDate" id="toDate2" >
								</div>
							</div>
						</div>						
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-save"></span> View Patient Register
								</button>
							</div>
						</div>
					</form>
				</div>





				<div id="duePatientList" class="tab-pane fade">
					<form action="/reports/due-patient-list" target="_blank" class="form-horizontal" method="post"> {!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="input" class="form-control" name="fromDate" id="fromDate3" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="input" class="form-control" name="toDate" id="toDate3" >
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-save"></span> View Due Patient List
								</button>
							</div>
						</div>
					</form>
				</div>





				<div id="discountListWithCharges" class="tab-pane fade">
					<form action="/reports/discount-list-with-charges" target="_blank" class="form-horizontal" method="post"> {!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="input" class="form-control" name="fromDate" id="fromDate4" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="input" class="form-control" name="toDate"  id="toDate4">
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-save"></span> View Discount List with Charges
								</button>
							</div>
						</div>
					</form>
				</div>





				<div id="incomeStatement" class="tab-pane fade">
					<form action="/reports/income-statement" target="_blank" class="form-horizontal" method="post"> {!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="input" class="form-control" name="fromDate" id="fromDate5" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="input" class="form-control" name="toDate" id="toDate5" >
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-save"></span> View Income Statement
								</button>
							</div>
						</div>
					</form>
				</div>






				<div id="incomeStatementSum" class="tab-pane fade">
					<form action="/reports/income-statement-sum" target="_blank" class="form-horizontal" method="post"> {!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="input" class="form-control" name="fromDate" id="fromDate6" class="from_date" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="input" class="form-control" name="toDate" id="toDate6"  class="to_date">
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-save"></span> View Income Statement Sum
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@section('essentialScript')

<script type="text/javascript">

	$( "#fromDate,#toDate,#fromDate1,#toDate1,#fromDate2,#toDate2,#fromDate3,#toDate3,#fromDate4,#toDate4,#fromDate5,#toDate5,#fromDate6,#toDate6" ).datepicker({
	dateFormat : 'yy/mm/dd',
	changeMonth : true,
	changeYear : true,
	yearRange: '-100y:c+nn'
	
});

</script>

@endsection

@endsection