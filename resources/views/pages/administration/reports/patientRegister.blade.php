<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Patient Register Between {{$from}} To {{$to}}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 12px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 11px;
			border: 1px dashed black;
			padding: 1px;
		}
		#verticalTable{
			width: 100%;
			
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		<h4>Patient Register Between {{$from}} To {{$to}}</h4>
		<table width="100%" id="verticalTable">
			<tr>
				<th rowspan="2" width="50px" align="center">Reg No</th>
				<th rowspan="2" align="left">Name</th>
				<th rowspan="2" width="60px" align="center">Admission <br> Date</th>
				<th rowspan="2" align="center">Bed</th>
				<th rowspan="2" align="center">Consultant</th>
				<th rowspan="2" align="center">Consultancy</th>
				
				<th colspan="4" align="center">Hospital Bill</th>
				
				<th colspan="5" align="center">Non-Profit Bill</th>
				
				<th rowspan="2" align="center">Total</th>
				<th rowspan="2" align="center">VAT</th>
				<th rowspan="2" align="center">Payment</th>
				<th rowspan="2" align="center">Discount</th>
				<th rowspan="2" align="center">Due</th>
			</tr>
			<tr>
				<th>Bed</th>
				<th>MSC</th>
				<th>OAC</th>
				<th>A. Fee</th>

				<th>Food</th>
				<th>Medicine</th>
				<th>P.P.</th>
				<th>Inv.</th>
				<th style="width:40px">O. Con.</th>
			</tr>
			@foreach($patients as $patient)
			<tr>
				<td align="center">{{$patient->id}}</td>
				<td width="150px" align="left">{{$patient->name}}</td>
				<td align="center">{{$patient->admissionDate}}</td>
				<td align="left">{{$patient->bedCabin}}</td>
				<td align="left">{{$patient->assignedConsultant}}</td>
				<td align="right">{{$patient->consult}}</td>
				<td align="right">{{$patient->bedFee}}</td>
				<td align="right">{{$patient->msc}}</td>
				<td align="right">{{$patient->oac}}</td>
				<td align="right">{{$patient->aFee}}</td>
				<td align="right">{{$patient->food}}</td>
				<td align="right">{{$patient->medicine}}</td>
				<td align="right">{{$patient->pp}}</td>
				<td align="right">{{$patient->inv}}</td>
				<td align="right">{{$patient->oConsult}}</td>
				<td align="right">{{$patient->total}}</td>
				<td align="right">{{$patient->vat}}</td>
				<td align="right">{{$patient->payment}}</td>
				<td align="right">{{$patient->discount}}</td>
				<td align="right">{{$patient->due}}</td>
			</tr>				
			@endforeach
			<tr>
				<td align="center"><strong>Total</strong></td>
				<td align="center"><strong> {{$count}} </strong></td>
				<td colspan="3"></td>
				<td align="right"><strong>{{$total->consult}}</strong></td>
				<td align="right"><strong>{{$total->bedFee}}</strong></td>
				<td align="right"><strong>{{$total->msc}}</strong></td>
				<td align="right"><strong>{{$total->oac}}</strong></td>
				<td align="right"><strong>{{$total->aFee}}</strong></td>
				<td align="right"><strong>{{$total->food}}</strong></td>
				<td align="right"><strong>{{$total->medicine}}</strong></td>
				<td align="right"><strong>{{$total->pp}}</strong></td>
				<td align="right"><strong>{{$total->inv}}</strong></td>
				<td align="right"><strong>{{$total->oConsult}}</strong></td>
				<td align="right"><strong>{{$total->total}}</strong></td>
				<td align="right"><strong>{{$total->vat}}</strong></td>
				<td align="right"><strong>{{$total->payment}}</strong></td>
				<td align="right"><strong>{{$total->discount}}</strong></td>
				<td align="right"><strong>{{$total->due}}</strong></td>			
			</tr>
		</table>

	</div>

</div>

</body>