@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">O.P.D List</a> 
		<i class="icon-angle-right"></i>
	</li>
	
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="row-fluid sortable">		
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon list"></i><span class="break"></span>O.P.D List</h2>
			
		
		<div class="box-content">
		
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					 			<th>SL No.</th>
								<th>Patient Name</th>
								<th>Date</th>
								<th>Edit</th>

				  </tr>
			  </thead>   
			  <tbody>
			@foreach($infos as $info)
			<tr>
								<td>{{$info->opdNo}}</td>
								<td>{{$info->pName}}</td>
								<td>{{$info->date}}</td>
								<td><a href="/opd/{{$info->opdNo}}/edit/" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a></td>
								
								
								
			</tr>
				@endforeach							
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->
	
</div><!--/row-->


@section('essentialScript')

<script type="text/javascript">
	

	
	

</script>

@endsection

@endsection