	@extends('layouts.default')
	@section('path')
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="#">Home</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<i class="icon-home"></i>
			<a href="#">O.P.D Report</a> 
			<i class="icon-angle-right"></i>
		</li>
		
	</ul>
	@endsection

	@section('content')

	@if (count($errors) > 0)
	<div class="alert alert-error">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif		
	@if (session('status'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ session('status') }}
	</div>
	@endif


	<div class="container">


		<div id="patientList" class="tab-pane fade in active" style="margin-top:5px">
			
			<form action="/opdReport" target="_blank" class="form-horizontal" method="post"> {!! csrf_field() !!}


				<div class="col-md-4">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
						<input type="input" class="form-control" name="fromDate" id="fromDate" >
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
						<input type="input" class="form-control" name="toDate" id="toDate" >
					</div>
				</div>
				<div class="col-sm-12">			
					<div class="btn-group">
						<button type="submit" id="viewbtn" class="btn btn-success" style="margin-top:20px">
							<span class="glyphicon glyphicon-save"></span> View O.P.D Report 
						</button>
					</div>
				</div>
			</form>
			
				
		</div>





	</div>


	@section('essentialScript')

	<script type="text/javascript">

	$( "#toDate,#fromDate" ).datepicker({
	dateFormat : 'yy/mm/dd',
	changeMonth : true,
	changeYear : true,
	yearRange: '-100y:c+nn'
	
});


/*$("#viewbtn").click(function(){
var x=$("#toDate").val();
alert(x);
});*/



	</script>

	@endsection

	@endsection









