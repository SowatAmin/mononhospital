@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">O.P.D.</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New O.P.D.</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">O.P.D.</h3>
		<form class="form-horizontal">

			<div class="col-md-12">

				<div class="col-md-2">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:40px;text-align:left">OPD No.</span>
						<input type="text" name="opdNo" id="opdNo" class="form-control" style="height:auto" value="{{ $opdid }}" >
					</div>
				</div>

				<div class="col-md-4">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Name</span>
						<input type="text" name="pName" id="pName" class="form-control" style="height:auto" value="{{$name}}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Date</span>
						<input type="text" name="date" id="date" class="form-control" value="{{ $date }}">
					</div>
				</div>
				



				<div class="col-md-3" style="margin-top:15px">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Hospital Fee</span>
						<input type="text" name="hospitalfee" id="hospitalfee" class="form-control" style="height:auto" value=100>
					</div>
				</div>
				<div class="col-md-3" style="margin-top:15px">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Doctor's Fee</span>
						<input type="text" name="doctorsfee" id="doctorsfee" class="form-control" value=0 style="height:auto">
					</div>
				</div>
				
				<div class="col-md-3" style="margin-top:15px">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Total Fee</span>
						<input type="text" name="totalfee" id="totalfee" class="form-control" style="height:auto">
						<input type="hidden" name="words" id="words">
					</div>





				</div>
				<div class="col-md-3" style="margin-top:15px">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="min-width:10px;text-align:left">Consultant</span>
						<select class="select form-control" name="consultant" id="consultant" >
							<option>Select a name</option>
							@foreach($consultants as $consustant)

							<option value="{{$consustant->name}}">{{$consustant->name}}</option>
							@endforeach
							
						</select>
					</div>
				</div>
				<div class="col-md-4"></div>
			</div>

			<div class="col-sm-12">
				<hr style="border: 0;height: 1px;background: #333;">
			</div>

			<div class="col-sm-2">                 
				<div class="btn-group" style="margin-top:15px;float:right;">
					<button type="button" class="btn btn-primary" id="opdAddbtn">
						<span class="glyphicon glyphicon-plus"></span> Add
					</button>
				</div>
			</div>

			<!--Nav Tabs-->
			<div style="margin-top:20px" id="patientFullDetails" class="col-md-12">
				<div id="navigation">
					<ul class="nav nav-tabs nav-left">
						<li class="active">
							<a data-toggle="tab" href="#personalInfo">Patient Bill</a>
						</li>
						
					</ul>
				</div>
				<div class="tab-content">

					<!--Personal Info-->
					<div id="personalInfo" class="tab-pane fade in active" style="margin-top:15px">
						

						


						
						
						
						
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
						<div class="col-sm-12">
							<table class="table table-bordered" id="opdtbl">
								<tr>
									
									<th>Reg No.</th>
									<th>Consultant.</th>
									<th>Doctor's Fee</th>
									<th>Hospital Fee</th>
									<th>Total Fee.</th>
									<th>Date</th>
								</tr>

							</table>
						</div>
						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
					</div>


					
				</div>



				<div class="col-sm-12">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="button" class="btn btn-primary" id="newbtn">
								<span class="glyphicon glyphicon-pencil"></span> New
							</button>
						</div>



						<div class="btn-group">
							<button type="button" class="btn btn-warning" id="view">
								<span class="glyphicon glyphicon-retweet"></span> View Bill
							</button>
						</div>

						<div class="btn-group">
							<a href="/home" class="btn btn-danger" ><span class="glyphicon glyphicon-off"></span>Exit</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>



@section('essentialScript')
<script>

var TOTAL=0;
var WORDS;

$('select').select2();



/*$(document).ready(function(){
	$("#doctorsfee").keyup(function(){
		
		if($("#doctorsfee").val()==0 || $("#doctorsfee").val()==0  )
		{
			$("#totalfee").val($("#hospitalfee").val());
			
		}
		else
		{
			$("#totalfee").val(parseInt($("#doctorsfee").val())+parseInt($("#hospitalfee").val()));
		}	
		
	});
});

*/



$("#view").click(function(){


var id=$("#opdNo").val();

//window.location.href = "/opdBill/"+id+"/"+WORDS+"/bill";
url="/opdBill/"+id+"/"+WORDS+"/bill";
window.open(url, '_blank')

});



//sum of hospital and doctor fee with plugin
$("input[id=doctorsfee],input[id=hospitalfee]").sum("keyup", "#totalfee");





//opd add button

$("#opdAddbtn").click(function(){
	var opdNo=$("#opdNo").val();
	var consultant=$("#consultant").val();
	var doctors_fee=$("#doctorsfee").val();
	var hospital_fee=$("#hospitalfee").val();
	var total_fee=$("#totalfee").val();
	var date=$("#date").val();
	TOTAL=parseInt(TOTAL)+parseInt(total_fee);
	



	$.ajax({

		url  : "saveOpdAll",
		type : "GET",

		data: {
			'opdNo':opdNo,
			'consultant':consultant,
			'doctors_fee':doctors_fee,
			'hospital_fee':hospital_fee,
			'total_fee':total_fee,					
			'date':date

		},
		success: function(re){
			console.log(re);
						//alert(re);

						$("#hospitalfee").val(0);
						$("#totalfee").val(0);
						$("#doctorsfee").val(0);


						inWords(TOTAL);
						
						
						
						
					}
				});




	$("#opdtbl").append("<tr><td>"+opdNo+"</td><td>"+consultant+"</td><td>"+doctors_fee+"</td><td>"+hospital_fee+"</td><td>"+total_fee+"</td><td>"+date+"</td></tr>");


});




$("#newbtn").click(function(){
	location.reload();
});







var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];


function inWords(num) {
	if ((num = num.toString()).length > 9) return 'overflow';
	n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
	if (!n) return;
	var str = '';
	str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
	str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
	str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
	str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
	str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + ' ' : '';
	var result=str+"taka only.";
    //alert(str);
    WORDS=result;

}









</script>
@endsection

@endsection