<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>OPD BILL</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
	body {
	}
	table{

	}
	th{
		text-align: left;
	}
	#verticalTable th{
		text-align: center;
		font-size: 15px;
		border: 1px solid black;
	}
	#verticalTable td{
		font-size: 14px;
		border: 1px dashed black;
	}
	#verticalTable{
		width: 100%;
		padding: 5px 0px 0px 0px;
		margin: 0px auto;
		border-collapse: collapse;
	}
	tr{
		margin-top: 10px;
	}
	#logo{
		width: 112px;
		height: 113px;
		padding-left:30px;
	}

	td{
		padding: 5px;
		

		
	}


}

	

	</style>


</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		<hr size=2 style="background-color:black">
		
		
		<h4 align="center" style="margin: 0px auto; margin-bottom:10px; width:20%;padding:5px 10px 5px 10px;border:3px double black">Money Receipt</h4>
		

		<table  style=" width:100%">
			<caption><b>OFFICE COPY</b></caption>
			<tr>
				<td style="font-size:large;"><i><b>SL No:</b></i>{{$info->opdNo}}</td>
				
				<td align="right" style="font-size:large;"><i><b>Date:{{$date}}</b></i> </td>

			</tr>


		</table>

				<table style="border-collapse: collapse;width:100%">

			
			<tr>
				<td style="border-bottom: 1px solid #ddd; "><i><b>Received with thanks from Mr. / Miss/ Mrs:</b></i>{{$info->pName}}</td>
				

			</tr>
			<tr>
				<td style="border-bottom: 1px solid #ddd;"><i><b>For:</b></i></td>
			</tr>

			<tr>
				<td style="border-bottom: 1px solid #ddd;"><i><b>Net Payable Amount: </b></i>{{$totalfee}}/-</td>
			</tr>

			<tr>
				<td style="border-bottom: 1px solid #ddd;"><i><b>The Sum of Taka: </b></i>{{ $words }}</td>
			</tr>
			<tr>
				<td>As Consultation fee/ Hospital stay/ Medicine Charge/ Advance/ Dues/ Other Charges. 
				</td>
			</tr>


		</table>
		
		<br>
		<p>_ _ _ _ _ _ _ _ _ _ _ _</p>
		<p>Manager</p>





		


<hr size=2 style="background-color:black">
		
		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
<h4 align="center" style="margin: 0px auto; margin-bottom:10px; width:20%;padding:5px 10px 5px 10px;border:3px double black">Money Receipt</h4>
		<br>
		<table  style=" width:100%">
			<caption><b>PATIENT COPY</b></caption>
			<tr>
				<td style="font-size:large;"><i><b>SL No: </b></i>{{$info->opdNo}}</td>
				
				<td align="right" style="font-size:large;"><i><b>Date:{{$date}}</b></i> </td>

			</tr>


		</table>

		
		<table style="border-collapse: collapse;width:100%">

			
			<tr>
				<td style="border-bottom: 1px solid #ddd; "><i><b>Received with thanks from Mr. / Miss/ Mrs: </b></i>{{$info->pName}}</td>
				

			</tr>
			<tr>
				<td style="border-bottom: 1px solid #ddd;"><i><b>For:</b></i></td>
			</tr>

			<tr>
				<td style="border-bottom: 1px solid #ddd;"><i><b>Net Payable Amount:</b></i>{{$totalfee}}/-</td>
			</tr>

			<tr>
				<td style="border-bottom: 1px solid #ddd;"><i><b>The Sum of Taka: </b></i>{{ $words }}</td>
			</tr>
			<tr>
				<td>As Consultation fee/ Hospital stay/ Medicine Charge/ Advance/ Dues/ Other Charges. 
				</td>
			</tr>


		</table>
		<br>
		<p>_ _ _ _ _ _ _ _ _ _ _ _</p>
		<p>Manager</p>


		
		
		

	</div>

</div>







</body>

</html>