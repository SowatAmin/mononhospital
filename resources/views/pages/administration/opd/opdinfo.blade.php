@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">O.P.D.</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New O.P.D.</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif

{!! Form::open(['url'=>'opd']) !!}
<div class="container-fluid">
	<div class="row">
		<h3 style="padding-left:15px">O.P.D.</h3>
		<form class="form-horizontal">

			<!--Nav Tabs-->
			<div style="margin-top:20px" id="patientFullDetails" class="col-md-12">
				<div id="navigation">
					<ul class="nav nav-tabs nav-left">
						<li class="active">
							<a data-toggle="tab" href="#personalInfo">Patient Personal</a>
						</li>
						
					</ul>
				</div>
				<div class="tab-content">

					<!--Personal Info-->
					<div id="personalInfo" class="tab-pane fade in active" style="margin-top:15px">
						


						<div class="col-md-2">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:40px;text-align:left">OPD No.</span>
								<input type="text" name="opdNo" id="opdNo" class="form-control" style="height:auto" value="{{$opdid['opdNo']+1}}" >
							</div>
						</div>

						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Name</span>
								<input type="text" name="pName" id="pName" class="form-control" style="height:auto" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Date</span>
								<input type="text" name="date" id="date" class="form-control">
							</div>
						</div>
						<div class="col-md-3">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:10px;text-align:left">Shift</span>
								<select class="select form-control" name="shift" id="shift">

									@foreach($shfts as $shfts)
									<option value="{{$shfts->shift_name}}">{{$shfts->shift_name}}</option>
									@endforeach
								</select>
							</div>
						</div>







						<div class="col-md-4" style="margin-top:15px">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Sex</span>
								<select name="sex" id="sex" class="select form-control">
									<option value="Male">Male</option>
									<option value="Female">Female</option>
									<option value="Others">Others</option>
								</select>
							</div>
						</div>

						<div class="col-md-4" style="margin-top:15px">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Age</span>
								<input type="text" name="age" id="age"  class="form-control" style="height:auto">
							</div>
						</div>

						<div class="col-md-4" style="margin-top:15px">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="basic-addon1">Phone</span>
								<input type="text" name="phone" id="phone" class="form-control" >
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="input-group input-group-sm" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Occupation</span>
								<select class="select form-control" id="occupation" name="occupation">
									<option value="">Select an option</option>
									<option value="Architect">Architect</option>
									<option value="Doctor">Doctor</option>
									<option value="Businessman">Businessman</option>
									<option value="D.G.M Bangladesh Bank">D.G.M Bangladesh Bank</option>
									<option value="Housewife">Housewife</option>
									<option value="Phychisian">Phychisian</option>
									<option value="Service Holder">Service Holder</option>
									<option value="Student">Student</option>
									<option value="Teaching">Teaching</option>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group" style="margin-top:10px">
								<span class="input-group-addon" id="basic-addon1">Address</span>
								<textarea name="address" id="address" rows="3" class="form-control"></textarea>
							</div>
						</div>

						<div class="col-sm-12">
							<hr style="border: 0;height: 1px;background: #333;">
						</div>
						
						
					</div>


					
				</div>



				<div class="col-sm-12">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="button" class="btn btn-primary" id="newbtn">
								<span class="glyphicon glyphicon-pencil"></span> New
							</button>
						</div>
						<div class="btn-group">
							<button type="submit" class="btn btn-success">
								<span class="glyphicon glyphicon-save"></span> Save
							</button>
						</div>

						<div class="btn-group">
							<a href="/home" class="btn btn-danger" ><span class="glyphicon glyphicon-off"></span>Exit</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
{!! Form::close() !!}


@section('essentialScript')
<script>

$("#date").datepicker({ dateFormat: 'yy-mm-dd' }).datepicker("setDate", new Date());



$("#newbtn").click(function(){
	location.reload();
});


</script>
@endsection

@endsection