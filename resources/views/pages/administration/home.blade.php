@extends('layouts.default')

@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Dashboard</a></li>
</ul>
@endsection

@section('content')
<div class="row-fluid">	

	<a href="/opd" class="quick-button metro yellow span2">
		<i class="icon-edit"></i>
		<p><strong>New O.P.D.</strong><br><br></p>		
	</a>
	<a href="/opd/create" class="quick-button metro yellow span2">
		<i class="icon-edit"></i>
		<p><strong>Existing O.P.D.</strong><br><br></p>		
	</a>

	<a href="/patientAdmission" class="quick-button metro purple span2">
		<i class="glyphicon glyphicon-user"></i>
		<p><strong>New Patient Admission</strong></p>
	</a>

	<a href="/patientAdmission/create" class="quick-button metro blue span2">
		<i class="icon-list"></i>
		<p><strong>Existing Patient</strong><br>Info</p>
	</a>

	<a href="/reports" class="quick-button metro green span2">
		<i class="glyphicon glyphicon-list-alt"></i>
		<p><strong>Reports</strong><br><br></p>
	</a>

	
	<a href="/bed" class="quick-button metro red span2">
		<i class="glyphicon glyphicon-bed"></i>
		<p><strong>Bed Cabin List</strong><br><br></p>
	</a>

	


	<div class="clearfix"></div>
					
</div>





<br><br><div class="row-fluid">	

	<a href="/opdReportShow" class="quick-button metro pink span2">
		<i class="glyphicon glyphicon-bed"></i>
		<p><strong>O.P.D Report</strong><br><br></p>
	</a>

	<div class="clearfix"></div>
					
</div>


<!--/row-->
@endsection