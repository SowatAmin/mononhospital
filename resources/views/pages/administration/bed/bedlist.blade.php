@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Bed Cabin List</a> 
		<i class="icon-angle-right"></i>
	</li>
	
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container">
    
    	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Bed Cabin List</h3>
						<div class="pull-right">
							<span class="clickable filter" data-toggle="tooltip" title="Search Bed Cabin" data-container="body">
								
							</span>
						</div>
					</div>
					<div class="panel-body">
						<input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Bed Cabin Name or Number" />
					</div>
					<table class="table table-hover" id="dev-table">
						<thead>
							<tr>
								<th>Bed Cabin Number</th>
								<th>Bed Cabin Type</th>
								<th>Status</th>
								<th>Occupied By Patient No.</th>
							</tr>
						</thead>
						
							@foreach($beds as $bed)
							<tr>
								<td>{{$bed->name}}</td>
								<td>{{$bed->BedCabinType}}</td>
								<td>{{$bed->Status}}</td>
								<td>{{$bed->RegNo}}</td>
								
								
							</tr>
							@endforeach

							
							
						</tbody>
					</table>

				</div>
				
			</div>



		
		</div>
	</div>


@section('essentialScript')

<script type="text/javascript">
	

	
	

</script>

@endsection

@endsection