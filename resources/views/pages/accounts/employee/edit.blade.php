@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Search Employee</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Existing Employee</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		{!! Form::model($employee, array('route' => array('employee.update', $employee), 'class' => 'form-horizontal', 'method' => 'PUT')) !!}
		<h3 style="padding-left:15px">Exisiting Employee Informations</h3>

		<div class="col-md-12">
			<div class="col-md-6">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:100px;text-align:left">Employee ID</span>
					<label name="employeeNo" id="employeeNo" class="form-control" > {{ $employee->employeeNo }} </label>				
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left">Name</span>
					<input type="text" name="name" id="name" class="form-control" style="height:auto" value="{{ $employee->name }}">
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left">Father's Name</span>
					<input type="text" name="fathersName" id="fathersName" class="form-control" style="height:auto" value="{{ $employee->fathersName }}">
				</div>				
			</div>

			<div class="col-md-6">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Sex</span>
					<select name="sex" id="sex" class="form-control">
						<option> {{ $employee->sex }} </option>
						<option></option>
						<option>Male</option>
						<option>Female</option>
					</select>

					<span class="input-group-addon" style="min-width:100px;text-align:left">Marital Status</span>
					<select name="maritalStatus" id="maritalStatus" class="form-control">
						<option> {{ $employee->maritalStatus }} </option>
						<option value=""></option>
						<option value="Married">Married</option>
						<option value="Unmarried">Unmarried</option>
						<option value="Single">Single</option>
					</select>
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left">Date of Birth</span>
					<input type="text" name="dob" id="dob" class="form-control" style="height:auto" value="{{ $employee->dob }}">

					<span class="input-group-addon" style="min-width:100px;text-align:left">Joining Date</span>
					<input type="text" name="jDate" id="jDate" class="form-control" style="height:auto" value="{{ $employee->jDate }}">
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Address</span>
					<textarea name="address" id="address" class="form-control"></textarea>
				</div>
			</div>

			<div class="col-md-12">
				<hr>
			</div>

			
			<div class="col-md-6">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Designation</span>
					<select name="designation" id="designation" class="form-control">
						<option> {{ $employee->designation }} </option>
						<option value=""></option>		
						@foreach($designation as $desig)
						<option value="{{$desig->designation}}">{{$desig->designation}}</option>
						@endforeach	




					</select>
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Grade</span>
					<select name="grade" id="grade" class="form-control">
						<option> {{ $employee->grade }} </option>
						<option value=""></option>
						<option value="Director">Director</option>
						<option value="Hospital Staff">Hospital Staff</option>
						<option value="Medical Officer">Medical Officer</option>
					</select>
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Section</span>
					<select name="section" id="section" class="form-control">
						<option> {{ $employee->section }} </option>
						<option value=""></option>
						<option value="Accounts">Accounts</option>
						<option value="Adminstration">Adminstration</option>
						<option value="Medical">Medical</option>
					</select>
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Pay Type</span>
					<select name="payType" id="payType" class="form-control">
						<option> {{ $employee->payType }} </option>
						<option value=""></option>
						<option value="Pay Scale">Pay Scale</option>
						<option value="Piece">Piece</option>
						<option value="Consolidated">Consolidated</option>
					</select>

					<span class="input-group-addon" style="min-width:100px;text-align:left;">Order ID</span>
					<input type="text" name="oId" id="oId" class="form-control" style="height:auto" value="{{ $employee->oId }}">
				</div>
			</div>

			<div class="col-md-6">
				<div class="input-group input-group-sm">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Basic</span>
					<input type="text" name="basic" id="txt" class="form-control" style="height:auto" value="{{ $employee->basic }}">
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">House Rent</span>
					<input type="text" name="houseRent" id="txt" class="form-control" style="height:auto" value="{{ $employee->houseRent }}">
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left;">Medical Alowance</span>
					<input type="text" name="medAllow" id="txt" class="form-control" style="height:auto" value="{{ $employee->medAllow }}">

					<span class="input-group-addon" style="min-width:100px;text-align:left;">Conveyence</span>
					<input type="text" name="conveyence" id="txt" class="form-control" style="height:auto" value="{{ $employee->conveyence }}">
				</div>

				<div class="input-group input-group-sm" style="margin-top: 15px">
					<span class="input-group-addon" style="min-width:100px;text-align:left;background-color:#89E9F9;">Gross Salary</span>
					<input type="text" name="gSalary" id="gSalary" class="form-control" style="height:auto" placeholder="0" value="{{ $employee->gSalary }}">
				</div>
			</div>

			<div class="col-md-12">
				<hr>
			</div>

			<div class="col-md-2">
				<input type="checkbox" value="" name="active" >Active
			</div>
			<div class="col-md-2">
				<input type="checkbox" value="" name="bonus">Bonus
			</div>
			<div class="col-md-8">
				<input type="checkbox" value="" name="pf">PF
			</div>
			<div class="col-md-2" style="margin-top: 15px">
				<input type="checkbox" value="" name="ot">OT
			</div>
			<div class="col-md-10" style="margin-top: 15px">
				<input type="checkbox" value="" name="attBonus">Attendance Bonus
			</div>			
		</div>

		<div class="col-md-12">
			<hr>
		</div>

		<div class="col-sm-12" style="margin-top: 15px">
			<div class="btn-group btn-group-justified">
				<div class="btn-group">
					<a class="btn btn-primary" href="/voucher">
						<span class="glyphicon glyphicon-retweet"></span> New
					</a>
				</div>
				<div class="btn-group">
					<button type="submit" class="btn btn-success">
						<span class="glyphicon glyphicon-save"></span> Save
					</button>
				</div>

				<div class="btn-group">
					<a class="btn btn-danger" href="/home">
						<span class="glyphicon glyphicon-off"></span> Exit
					</a>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>


@section('essentialScript')

<script>

//select2

$('select').select2();


//new button to reload

$('#newbtn').click(function() {
    location.reload();
});




//datepicker

$( "#dob,#jDate" ).datepicker({
	dateFormat : 'yy/mm/dd',
	changeMonth : true,
	changeYear : true,
	yearRange: '-100y:c+nn'
	
});


//oid copy

$(document).ready(function(){
	var id=$("#employeeNo").text();
	$('#oId').val(id);

});






/*$("#gSalary").click(function(){
	var basic=$("#basic").val();
	var houserent=$("#houseRent").val();;
	var allowance=$("#medAllow").val();;
	var con=$("#conveyence").val();;
	var total=0;
	if(basic=="")
	{
		basic=0;
	}
	 if(houserent=="")
	{
		houserent=0;
	}
	 if(allowance=="")
	{
		allowance=0;	
	}
	 if(con=="")
	{
		con=0;	
	}

	else
	{
		total=parseInt(basic)+parseInt(houserent)+parseInt(allowance)+parseInt(con);
		
		$("#gSalary").val(total);
	}

	});*/

   
 
    
 
  //sum of inputs  

$("input[id=txt]").sum("keyup", "#gSalary");


$.Calculation.setDefaults({
	
	 onParseError: null,
	 onParseClear: null
});




</script>

@endsection

@endsection