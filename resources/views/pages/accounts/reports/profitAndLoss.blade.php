<?php ini_set('max_execution_time', 300); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Profit & Loss Statement</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		
		.tableV th{
			text-align: center;
			font-size: 13px;
			border: 1px solid black;
		}
		.tableV td{
			font-size: 12px;
			border: 1px dashed black;
		}
		.tableV{
			width: 100%;
			padding: 0px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
	</style>
</head>

<body>
	<div class="page">
		<div style="margin-top:10px;">
			<h3 align="center" style="margin-bottom:-10px">
				<strong>Monon Psychiatric Hospital (Pvt.) LTD.</strong>
			</h3>
			<h5 align="center" style="margin-top:-1px">
				<strong>20/20 Tajmohal Road, Block-C, Muhammadpur, Dhaka</strong>
			</h5>
		</div>

		<hr style="border: 0;height: 2px;background: #333;">

		<h4 align="center" style="margin: 0px auto; margin-bottom:10px; width:20%;padding:5px 10px 5px 10px;border:3px double black">Profit & Loss Account</h4>


		<div style="margin-bottom:10px">
			<h4>Profit & Loss Account from {!! $from !!} to {!! $to !!}</h4>
			<table class="tableV">
				<tr>
					<th rowspan="3" width="280px">Particulars</th>
					<th colspan="4">Debit</th>
					<th rowspan="3" width="1px" style="background-color: black"><br></th>					
					<th colspan="4">Credit</th>
				</tr>
				<tr>
					<th colspan="3">Bank</th>
					<th rowspan="2">Cash</th>
					
					<th colspan="3">Bank</th>
					<th rowspan="2">Cash</th>
				</tr>
				<tr>
					<th>Sonali Bank</th>
					<th>Islami Bank</th>
					<th>Fixed Deposit</th>
					
					<th>Sonali Bank</th>
					<th>Islami Bank</th>
					<th>Fixed Deposit</th>
				</tr>

				@foreach($parentHeads as $parentHead)
				<tr>
					<td style="font-size: 13px;padding-left: 5px"><strong> {!! $parentHead->parentHead !!} </strong></td>
					<td colspan="4"></td>
					<th width="1px" style="background-color: black"><br></th>
					<td colspan="4"></td>
				</tr>

				@foreach($vouchers as $voucher)
				
				@if($voucher->parentHead == $parentHead->parentHead)

				<tr>
					<td style="padding-left: 20px">{!! $voucher->accountHead !!}</td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->debitSonaliBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->debitIslamiBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->debitNationalBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->debitCash !!} </td>					
					<th width="1px" style="background-color: black"><br></th>
					<td align="right" style="padding-right: 5px"> {!! $voucher->creditSonaliBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->creditIslamiBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->creditNationalBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->creditCash !!} </td>

				</tr>

				@endif

				@endforeach

				<tr style="font-size: 13px">
					<td style="padding-left: 5px"><strong>Totals for {!! $parentHead->parentHead !!} </strong></td>
					<td align="right" colspan="4" style="padding-right: 5px"><strong>{!! $parentHead->debit !!}</strong></td>
					<th align="right" width="1px" style="background-color: black"><br></th>
					<td align="right" colspan="4" style="padding-right: 5px"><strong>{!! $parentHead->credit !!}</strong></td>
				</tr>

				<tr style="font-size: 13px;padding-left: 5px">
					<td align="right" colspan="5"></td>
					<th align="right" width="1px" style="background-color: black"><br></th>
					<td align="right" colspan="4"></td>
				</tr>

				@endforeach

				<tr style="font-size: 13px">
					<td style="padding-left: 5px"><strong>Net Total</strong></td>
					<td align="right" colspan="4" style="padding-right: 5px"><strong>{!! $total->debit !!}</strong></td>
					<th align="right" width="1px" style="background-color: black"><br></th>
					<td align="right" colspan="4" style="padding-right: 5px"><strong>{!! $total->credit !!}</strong></td>
				</tr>

			</table>

			<table  align="center" width="100%" style="margin-top: 50px">
				<tr>
					<td><hr size=2 style="background-color:black"></td>
					<td></td>
					<td><hr size=2 style="background-color:black"></td>
				</tr>

				<tr>
					<td>Prepared By</td>
					<td></td>
					<td>Approved By</td>
				</tr>
			</table>
		</div>

	</div>

</div>

</body>