@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Hospital Management</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Reports</a></li>
</ul>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif
@if (session('alert'))
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('alert') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		<div style="margin-top:20px" id="patientFullDetails" class="col-md-12">
			<div id="navigation">
				<ul class="nav nav-tabs nav-justified">
					<li class="active">
						<a data-toggle="tab" href="#patientList">Profit<br>& Loss</a>
					</li>
					<li>
						<a data-toggle="tab" href="#doctorsList">Payment<br>& Recieved</a>
					</li>
					<li>
						<a data-toggle="tab" href="#discountList">Balance<br>Sheet</a>
					</li>
					
				</ul>
			</div>
			<div class="tab-content">

				<!--Patient List-->
				<div id="patientList" class="tab-pane fade in active" style="margin-top:5px">
					<form action="/accountsReports/profit-and-loss" class="form-horizontal" target="_blank" method="post"> 
						{!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="text" class="form-control" name="fromDate" id ="PALFrom">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="text" class="form-control" name="toDate" id ="PALTo">
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-list-alt"></span> View Profit & Loss Statement
								</button>
							</div>
						</div>
					</form>
				</div>				





				<div id="doctorsList" class="tab-pane fade">
					<form action="/accountsReports/payment-and-recieved" class="form-horizontal" target="_blank" method="post"> 
						{!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="text" class="form-control" name="fromDate" id ="PARFrom">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="text" class="form-control" name="toDate" id ="PARTo">
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-list-alt"></span> View Payment & Recieved
								</button>
							</div>
						</div>
					</form>
				</div>





				<div id="discountList" class="tab-pane fade">
					<form action="/accountsReports/balance-sheet" class="form-horizontal" target="_blank" method="post"> 
						{!! csrf_field() !!}
						<div class="col-md-6">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">From</span>
								<input class="form-control" type="text" class="form-control" name="fromDate" id ="BlShFrom">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" style="min-width:120px;text-align:left">To</span>
								<input class="form-control" type="text" class="form-control" name="toDate" id ="BlShTo">
							</div>
						</div>
						<div class="col-sm-12">			
							<div class="btn-group">
								<button type="submit" class="btn btn-success" style="margin-top:20px">
									<span class="glyphicon glyphicon-list-alt"></span> View Balance Sheet
								</button>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>

@section('essentialScript')

<script type="text/javascript">

	$("#PALFrom").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#PALTo").datepicker({ dateFormat: 'yy-mm-dd' });

	$("#PARFrom").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#PARTo").datepicker({ dateFormat: 'yy-mm-dd' });

	$("#BlShFrom").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#BlShTo").datepicker({ dateFormat: 'yy-mm-dd' });

</script>

@endsection

@endsection