<?php ini_set('max_execution_time', 300); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Balance Sheet</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		
		.tableV th{
			text-align: center;
			font-size: 13px;
			border: 1px solid black;
		}
		.tableV td{
			font-size: 12px;
			border: 1px dashed black;
		}
		.tableV{
			width: 100%;
			padding: 0px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
	</style>
</head>

<body>
	<div class="page">
		<div style="margin-top:10px;">
			<h3 align="center" style="margin-bottom:-10px">
				<strong>Monon Psychiatric Hospital (Pvt.) LTD.</strong>
			</h3>
			<h5 align="center" style="margin-top:-1px">
				<strong>20/20 Tajmohal Road, Block-C, Muhammadpur, Dhaka</strong>
			</h5>
		</div>

		<hr style="border: 0;height: 2px;background: #333;">

		<h4 align="center" style="margin: 0px auto; margin-bottom:10px; width:20%;padding:5px 10px 5px 10px;border:3px double black">Balance Sheet</h4>


		<div style="margin-bottom:10px">
			<h4>Balance Sheet from {!! $from !!} to {!! $to !!}</h4>
			<table class="tableV">
				<tr>
					<th rowspan="3" width="280px">Account Head</th>
					<th colspan="5">Amount</th>
					<th rowspan="3">Total</th>
				</tr>
				<tr>
					<th colspan="3">Bank</th>
					<th rowspan="2">Cash</th>
					<th rowspan="2">Acc.<br>Total</th>
				</tr>
				<tr>
					<th>Sonali Bank</th>
					<th>Islami Bank</th>
					<th>Fixed Deposit</th>
				</tr>

				@foreach($accountTypes as $accountType)

				<tr>
					<td style="font-size: 15px; font-weight: bold; color: green" colspan="7">{!! $accountType->type !!}</td>
				</tr>

				@foreach($parentHeads as $parentHead)
				@if($accountType->type == $parentHead->accType)

				<tr>
					<td style="font-size: 13px; font-weight: bold; color: red; padding-left: 20px" colspan="7">{!! $parentHead->parentHead !!}</td>
				</tr>

				@foreach($vouchers as $voucher)
				@if($voucher->parentHead == $parentHead->parentHead)

				<tr>
					<td style="padding-left: 40px">{!! $voucher->accountHead !!}</td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->sonaliBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->islamiBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->nationalBank !!} </td>
					<td align="right" style="padding-right: 5px"> {!! $voucher->cash !!} </td>
					<td></td>
					<td></td>
				</tr>

				@endif
				@endforeach

				<tr>
					<td style="font-size: 13px; font-weight: bold; color: red;" align="right" colspan="6">
						{!! $parentHead->total !!}
					</td>
					<td></td>
				</tr>
				
				
				@endif
				@endforeach

				<tr>
					<td style="font-size: 15px; font-weight: bold; color: green" align="right" colspan="7">{!! $accountType->total !!}</td>
				</tr>

				@endforeach
				<!-- <tr>
					<td colspan="7"><br></td>
				</tr>
				<tr>
					<td style="font-size: 15px; font-weight: bold; color: green" align="right" colspan="7">{!! $total !!}</td>
				</tr> -->
			</table>


			<table  align="center" width="100%" style="margin-top: 50px">

				<tr>
					<td><hr size=2 style="background-color:black"></td>
					<td></td>
					<td><hr size=2 style="background-color:black"></td>
					<td></td>
					<td><hr size=2 style="background-color:black"></td>
				</tr>

				<tr>
					<td>Prepared By</td>
					<td></td>
					<td>Approved By</td>
					<td></td>
					<td>Accountant</td>
				</tr>
			</table>

		</div>
	</div>
</div>

</body>