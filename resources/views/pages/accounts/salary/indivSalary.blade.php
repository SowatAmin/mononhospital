@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Voucher</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New Voucher</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">		
		<h3 style="padding-left:15px; margin-bottom:20px">Salary Sheet for {{$salary->name}} for {{$salary->month}} {{$salary->year}}</h3>

		<form class="form-horizontal" role="form" method="POST" action="{{ url('salary/edit-salary') }}">
			{!! csrf_field() !!}

			<input type="hidden" name="id" value="{{$salary->id}}">

			<div class="col-md-12">
				<div class="col-md-6">
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Basic</span>
						<input type="text" name="basic" class="form-control" value="{{$salary->basic}}">

						<span class="input-group-addon" style="min-width:40px;text-align:left">House Rent</span>
						<input type="text" name="houseRent" class="form-control" value="{{$salary->houseRent}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Medical Allowance</span>
						<input type="text" name="medAllow" class="form-control" value="{{$salary->medAllow}}">

						<span class="input-group-addon" style="min-width:40px;text-align:left">Conveyence</span>
						<input type="text" name="conveyence" class="form-control" value="{{$salary->conveyence}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon" id="basic-addon1">Gross Salary</span>
						<input type="text" name="gSalary" class="form-control" value="{{$salary->gSalary}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Others Add</span>
						<input type="text" name="othersAdd" class="form-control" value="{{$salary->othersAdd}}">
					</div>
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Total Overtime (Hrs)</span>
						<input type="text" name="otHour" class="form-control" value="{{$salary->otHour}}">
					</div>
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Festival Overtime (Hrs)</span>
						<input type="text" name="festivalOT" class="form-control" value="{{$salary->festivalOT}}">
					</div>					
				</div>

				<div class="col-md-6">
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Meal Deduct</span>
						<input type="text" name="mealDeduct" class="form-control" value="{{$salary->mealDeduct}}">
					</div>
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Fine Deduct</span>
						<input type="text" name="fineDeduct" class="form-control" value="{{$salary->fineDeduct}}">
					</div>
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Advance Deduct</span>
						<input type="text" name="advanceDeduct" class="form-control" value="{{$salary->advanceDeduct}}">
					</div>					
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Others Deduct</span>
						<input type="text" name="othersDeduct" class="form-control" value="{{$salary->othersDeduct}}">
					</div>

					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Total LWP (Hrs)</span>
						<input type="text" name="lwpHour" class="form-control" value="{{$salary->lwpHour}}">
					</div>
					<div class="input-group input-group-sm" style="margin-top:10px">
						<span class="input-group-addon">Total Absent (Hrs)</span>
						<input type="text" name="uaHour" class="form-control" value="{{$salary->uaHour}}">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-1" style="margin-top:20px">
					<button type="submit" class="btn btn-primary">
						<span class="glyphicon glyphicon-save"></span> Save Salary
					</button>
				</div>
			</div>
		</form>

	</div>
</div>


@section('essentialScript')



@endsection

@endsection