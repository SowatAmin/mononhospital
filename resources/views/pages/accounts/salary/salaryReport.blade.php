<?php ini_set('max_execution_time', 300); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Salary Sheet</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">

		.tableV th{
			text-align: center;
			font-size: 12px;
			border: 1px solid black;
		}
		.tableV td{
			font-size: 11px;
			border: 1px solid black;
		}
		.tableV{
			width: 100%;
			padding: 20px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
	</style>
</head>

<body>
	<div class="page">
		<div style="margin-top:10px;">
			<table width="80%" style="margin:0px auto">
				<tr>
					<td id="logo"></td>
					<td style="padding-left:10px">
						<h3 align="center" style="margin-bottom:-10px">
							<strong>Monon Psychiatric Hospital</strong>
						</h3>
						<h4 align="center" style="margin-bottom:-10px">
							<strong>Treatment Centre for Mental illness and Drug Addiction</strong>
							
						</h4>
						<h5 align="center">
							<strong>20/20 Tajmohal Road, Muhammadpur, Dhaka</strong>
						</h5>
					</td>
				</tr>
			</table>
		</div>

		<hr style="border: 0;height: 2px;background: #333;">

		<h4 align="center" style="margin: 0px auto; margin-bottom:10px; width:10%;padding:5px 20px 5px 20px;border:3px double black">
			Salary Sheet
		</h4>

		<div style="margin-bottom:10px">
			<table class="tableV">
				<tr>
					<th align="center">Emp<br>No</th>
					<th align="center" width="100px">Name</th>
					<th align="center">Basic</th>
					<th align="center">House<br>Rent</th>
					<th align="center">Med.<br>Allow</th>
					<th align="center">Conv.</th>
					<th align="center">Gross</th>

					<th align="center">Allow<br>/hr</th>
					<th align="center">Allow<br>Amt.</th>

					<th align="center">OT<br>Hr.</th>
					<th align="center">Fest.<br>OT</th>
					<th align="center">OT<br>Rate</th>
					<th align="center">OT<br>Amt.</th>
					<th align="center">Fest.<br>Amt.</th>
					<th align="center">OT<br>Allow</th>
					<th align="center">Total<br>OT</th>

					<th align="center">Others<br>Add</th>

					<th align="center">LWP.<br>Hrs.</th>
					<th align="center">LWP.<br>Rate</th>
					<th align="center">LWP.<br>Amt.</th>
					<th align="center">LWP.<br>Allow</th>
					<th align="center">LWP.<br>Total</th>

					<th align="center">U.A.<br>Hrs.</th>
					<th align="center">U.A.<br>Rate</th>
					<th align="center">U.A.<br>Amt.</th>
					<th align="center">U.A.<br>Allow</th>

					<th align="center">Meal<br>Deduct</th>
					<th align="center">Advance<br>Deduct</th>
					<th align="center">Fine<br>Deduct</th>

					<th align="center">Others<br>Deduct</th>

					<th align="center">Stamp</th>

					<th align="center">Payable<br>Salary</th>

					<th align="center" width="100px">Signature</th>

				</tr>
				@foreach($salaries as $salary)
				<tr height="50px">
					<td align="center">{!! $salary->employeeNo !!}</td>
					<td align="center">{!! $salary->name !!}</td>
					<td align="right">{!! $salary->basic !!}</td>
					<td align="right">{!! $salary->houseRent !!}</td>
					<td align="right">{!! $salary->medAllow !!}</td>
					<td align="right">{!! $salary->conveyence !!}</td>
					<td align="right">{!! $salary->gSalary !!}</td>

					<td align="right">{!! $salary->allowPhr !!}</td>
					<td align="right">{!! $salary->allowAmount !!}</td>

					<td align="right">{!! $salary->totalOT !!}</td>
					<td align="right">{!! $salary->festivalOT !!}</td>
					<td align="right">{!! $salary->otRate !!}</td>
					<td align="right">{!! $salary->otAmount !!}</td>
					<td align="right">{!! $salary->festivalPayment !!}</td>
					<td align="right">{!! $salary->otAllow !!}</td>
					<td align="right">{!! $salary->totalOT !!}</td>

					<td align="right">{!! $salary->othersAdd !!}</td>

					<td align="right">{!! $salary->lwpHour !!}</td>
					<td align="right">{!! $salary->lwpRate !!}</td>
					<td align="right">{!! $salary->lwpAmount !!}</td>
					<td align="right">{!! $salary->lwpAllow !!}</td>
					<td align="right">{!! $salary->totalLwp !!}</td>

					<td align="right">{!! $salary->uaHour !!}</td>
					<td align="right">{!! $salary->uaRate !!}</td>
					<td align="right">{!! $salary->uaAmount !!}</td>
					<td align="right">{!! $salary->uaAllow !!}</td>

					<td align="right">{!! $salary->mealDeduct !!}</td>
					<td align="right">{!! $salary->fineDeduct !!}</td>
					<td align="right">{!! $salary->advanceDeduct !!}</td>

					<td align="right">{!! $salary->othersDeduct !!}</td>

					<td align="right">{!! $salary->stamp !!}</td>

					<td align="right">{!! $salary->payableSalary !!}</td>

					<td></td>
				</tr>
				
				@endforeach

				<tr style="font-weight:bold">
					<td colspan="2" align="center">Total</td>

					<td align="right">{!! $total->basic !!}</td>
					<td align="right">{!! $total->houseRent !!}</td>
					<td align="right">{!! $total->medAllow !!}</td>
					<td align="right">{!! $total->conveyence !!}</td>
					<td align="right">{!! $total->gSalary !!}</td>

					<td align="right">{!! $total->allowPhr !!}</td>
					<td align="right">{!! $total->allowAmount !!}</td>

					<td align="right">{!! $total->totalOT !!}</td>
					<td align="right">{!! $total->festivalOT !!}</td>
					<td align="right">{!! $total->otRate !!}</td>
					<td align="right">{!! $total->otAmount !!}</td>
					<td align="right">{!! $total->festivalPayment !!}</td>
					<td align="right">{!! $total->otAllow !!}</td>
					<td align="right">{!! $total->totalOT !!}</td>

					<td align="right">{!! $total->othersAdd !!}</td>

					<td align="right">{!! $total->lwpHour !!}</td>
					<td align="right">{!! $total->lwpRate !!}</td>
					<td align="right">{!! $total->lwpAmount !!}</td>
					<td align="right">{!! $total->lwpAllow !!}</td>
					<td align="right">{!! $total->totalLwp !!}</td>

					<td align="right">{!! $total->uaHour !!}</td>
					<td align="right">{!! $total->uaRate !!}</td>
					<td align="right">{!! $total->uaAmount !!}</td>
					<td align="right">{!! $total->uaAllow !!}</td>

					<td align="right">{!! $total->mealDeduct !!}</td>
					<td align="right">{!! $total->fineDeduct !!}</td>
					<td align="right">{!! $total->advanceDeduct !!}</td>

					<td align="right">{!! $total->othersDeduct !!}</td>

					<td align="right">{!! $total->stamp !!}</td>

					<td align="right">{!! $total->payableSalary !!}</td>

					<td></td>					
				</tr>

			</table>

			<table  align="center" width="100%" style="margin-top: 20px">
				<tr>
					<td><hr size=2 style="background-color:black"></td>
					<td><hr size=2 style="background-color:black"></td>
				</tr>

				<tr>
					<td>Accounts Supervisor</td>
					<td>Director (Finance)</td>
				</tr>
			</table>

		</div>

	</div>

</div>

</body>