@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Voucher</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New Voucher</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		
		<h3 style="padding-left:15px; margin-bottom:20px">Pick Month and Year Salary Sheet</h3>

		<form class="form-horizontal" role="form" method="POST" action="{{ url('salary/make-salary-sheet') }}">
			{!! csrf_field() !!}

			<div class="form-group">
				<label class="col-md-2">Month</label>

				<div class="col-md-6">
					<select name="month" id="month" class="form-control">
						<option></option>
						<option value="January">January</option>
						<option value="February">February</option>
						<option value="March">March</option>
						<option value="April">April</option>
						<option value="May">May</option>
						<option value="June">June</option>
						<option value="July">July</option>
						<option value="August">August</option>
						<option value="September">September</option>
						<option value="October">October</option>
						<option value="November">November</option>
						<option value="December">December</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2">Year</label>

				<div class="col-md-6">
					<select name="year" id="year" class="form-control">
						<option></option>
						@foreach($years as $year)
						<option value="{{$year}}">{{$year}}</option>
						@endforeach
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-6 col-md-offset-2">
					<button type="submit" class="btn btn-primary">
						<span class="glyphicon glyphicon-list-alt"></span> View Salary Sheet
					</button>
				</div>
			</div>
		</form>		
	</div>
</div>


@section('essentialScript')



@endsection

@endsection