@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Voucher</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New Voucher</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">		
		<h3 style="padding-left:15px; margin-bottom:20px">Salary Sheet for {{$month}} {{$year}}</h3>

		<form class="form-horizontal" target="_blank" role="form" method="POST" action="{{ url('salary/view-salaries') }}"> 
			{!! csrf_field() !!}
			
			<input type="hidden" name="month" value="{{$month}}">
			<input type="hidden" name="year" value="{{$year}}">

			<div class="form-group">
				<div class="col-md-6 col-md-offset-1" style="margin-top:20px">
					<button type="submit" class="btn btn-primary">
						<span class="glyphicon glyphicon-save"></span> View Salaries
					</button>
				</div>
			</div>
		</form>

		<div style="max-width:1200px; max-height:500px; overflow: auto; font-size:12px">
			<table class="table table-bordered" id="dailyTable">
				<tr>
					<th align="center">Employee<br>No</th>
					<th align="center">Name</th>
					<th align="center">Basic</th>
					<th align="center">House<br>Rent</th>
					<th align="center">Medical<br>Allowance</th>
					<th align="center">Conveyence</th>
					<th align="center">Gross<br>Salary</th>

					<th align="center">OT<br> Hrs.</th>
					<th align="center">Fest.<br>OT</th>
					<th align="center">Others<br>Add</th>

					<th align="center">Meal<br>Deduct</th>
					<th align="center">Adv.<br>Deduct</th>
					<th align="center">Fine<br>Deduct</th>
					<th align="center">Others<br>Deduct</th>

					<th align="center">Total<br>LWP</th>	
					<th align="center">Total<br>Absent</th>
					
					<th align="center" width="120px">Edit/delete</th>
				</tr>
				@foreach($salaries as $salary)
				<tr>
					<td>{!! $salary->employeeNo !!}</td>
					<td>{!! $salary->name !!}</td>
					<td>{!! $salary->basic !!}</td>
					<td>{!! $salary->houseRent !!}</td>
					<td>{!! $salary->medAllow !!}</td>
					<td>{!! $salary->conveyence !!}</td>
					<td>{!! $salary->gSalary !!}</td>

					<td>{!! $salary->otHour !!}</td>
					<td>{!! $salary->festivalOT !!}</td>
					<td>{!! $salary->othersAdd !!}</td>

					<td>{!! $salary->mealDeduct !!}</td>
					<td>{!! $salary->advanceDeduct !!}</td>
					<td>{!! $salary->mealDeduct !!}</td>
					<td>{!! $salary->othersDeduct !!}</td>

					<td>{!! $salary->lwpHour !!}</td>
					<td>{!! $salary->uaHour !!}</td>
					
					<td>
						<a href="/salary/edit-salary/{{$salary->id}}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
						<a href="/salary/delete-salary/{{$salary->id}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
					</td>
				</tr>
				@endforeach
			</table>
		</div>	
	</div>
</div>


@section('essentialScript')



@endsection

@endsection