<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Journal Report</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 15px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 14px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 5px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		


		<table border=2 align="center" style="height:50px" >
			<tr>
				<td align="center"><strong>Transaction</strong></td>
			</tr>
		</table>
		<br><br><br>


		<table width="100%">
			<tr>
					<td><b>Accounts: Monon Account</b></td>
					<td><b>Source: {{$journal->source}}</b></td>
			</tr>
			<tr>
					<td><b>Particular: {{$journal->particular}}</b> </td>
					<td><b>Date: {{$journal->date }}</b></td>
			</tr>		
		</table>


		<br><br><br>

		<table width="100%" id="verticalTable">
			
			<tr>
				
				<th align="center">Account Head</th>
				<th align="center">Debit</th>
				<th align="center" >Credit</th>
				
				
			</tr>
			@foreach($journal_datas as $journal_data)
			<tr>
				
				<td align="left" >{{$journal_data->acchead}}</td>
				<td align="right" >{{$journal_data->debit}}</td>
				<td align="right">{{$journal_data->credit}}</td>
				
				
			</tr>				
			@endforeach
			<tr>
				
				
				<td></td>
				<td align="center"><strong>Debit Total={{$debit_total}}</strong></td>
				<td align="center"><strong>Credit Total={{$credit_total}}</strong></td>
				

			</tr>
		</table>

	</div>

</div>

</body>