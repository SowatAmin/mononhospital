@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">New Journal</a> 
		
	</li>
	
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">

	{!! Form::open(['url'=>'journal']) !!}

	<div class="row">		
		<h3 style="padding-left:10px">New Journal</h3>

		<div class="col-md-6">
			<div class="input-group input-group-sm">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Journal Number</span>
				<input type="text" name="journalNo" id="journalNo" class="form-control" style="height:auto"  value="{{ $id->id+1 }}">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Date</span>
				<input type="text" name="date" id="date" class="form-control" style="height:auto">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Parent Head</span>
				<select name="parentHead" id="parentHead" class="form-control" required>
					<option value=""></option>
					@foreach($accounttype as $atype)
					<option value="{{ $atype->parentHead}}">{{ $atype->parentHead}}</option>
					@endforeach

				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Debit</span>
				<input type="text" name="debit" id="debit" class="form-control" style="height:auto">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Total Debit</span>
				<input type="text" name="total_debit" id="total_debit" class="form-control" style="height:auto" value="0">
			</div>
		</div>

		<div class="col-md-6">
			<div class="input-group input-group-sm">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Source</span>
				<input type="text" name="source" id="source" class="form-control" style="height:auto">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Particular</span>
				<textarea rows="3" name="particular" id="particular" class="form-control"></textarea>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Account Head</span>
				<select name="accountHead" id="accountHead" class="form-control" required>
					
				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Credit</span>
				<input type="text" name="credit" id="credit" class="form-control" style="height:auto">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Total Credit</span>
				<input type="text" name="total_credit" id="total_credit" class="form-control" style="height:auto" value="0">
			</div>
		</div>

		<div class="col-md-12">
			<div class="btn-group" style="margin-top:20px;float:right;">
				<button type="button" class="btn btn-warning" id="addbtn">
					<span class="glyphicon glyphicon-plus"></span> Add
				</button>
			</div>			
		</div>

		<div class="col-md-12" style="margin-top: 15px">
			<div style="max-height:500px; overflow: auto; font-size:12px">
				<table class="table table-bordered" id="jounalTable">
					<tr>
						<th>Journal Number</th>
						<th>Account Head</th>
						<th>Debit</th>
						<th>Credit</th>
					</tr>
				</table>
			</div>
		</div>
	</div>



	<br><br><br>	
	<div class="col-sm-12" style="margin-top: 15px">
		<div class="btn-group btn-group-justified">
			<div class="btn-group">
				<a class="btn btn-primary" href="#">
					<span class="glyphicon glyphicon-retweet"></span> New
				</a>
			</div>
			<div class="btn-group">
				<button type="submit" class="btn btn-success">
					<span class="glyphicon glyphicon-save"></span> Save
				</button>
			</div>

			<div class="btn-group">
				<a class="btn btn-danger" href="/home">
					<span class="glyphicon glyphicon-off"></span> Exit
				</a>
			</div>
		</div>
	</div>

	{!! Form::close() !!}

</div>

@section('essentialScript')

<script type="text/javascript">

$('select').select2();

$("#date").datepicker({ dateFormat: 'yy-mm-dd' });

	//account head ajax;
	$(document).ready(function() {
		$("#parentHead").on('change',function(e){
			console.log(e);
			var phead=e.target.value;


			$.get('ajax-phead?phead='+phead,function(data){

				console.log(data);
				$("#accountHead").empty();

				$("#accountHead").select2({allowClear: true});
				$.each(data,function(index,subcatObj){
					$("#accountHead").append(' <option value="'+subcatObj.childHead+'">'+subcatObj.childHead+'</option> ');

				});
				$('select').select2();
			});

		});

	});

	$("#addbtn").click(function(){
		var debit=$("#debit").val();
		var credit=$("#credit").val();
		var acchead=$("#accountHead").val();
		var id=$("#journalNo").val();

		$("#jounalTable").append("<tr><td>"+id+"</td><td>"+acchead+"</td><td>"+debit+"</td><td>"+credit+"</td></tr>");






		$.ajax({

			url  : "save_journaldata",
			type : "GET",

			data: {
				'jid':id,
				'acchead':acchead,
				'debit':debit,
				'credit':credit,




			},
			success: function(re){
						//console.log(re);
						//alert(re);

						var total_c=$("#total_credit").val();
						var total_d=$("#total_debit").val();

						total_c=parseFloat(total_c)+parseFloat(credit);
						$("#total_credit").val(total_c);
						total_d=parseFloat(total_d)+parseFloat(debit);
						$("#total_debit").val(total_d);						


						
						
					}
				});


		
	});





	</script>

	@endsection

	@endsection