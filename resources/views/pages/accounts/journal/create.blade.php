@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Search Journal</a> 
		<i class="icon-angle-right"></i>
	</li>
	
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="row-fluid sortable">		
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon list"></i><span class="break"></span>Journal List</h2>
			
		
		<div class="box-content">
		
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					 			<th>Journal Number</th>
								<th>Source</th>
								<th>Particular</th>
								<th>Date</th>
								<th>Action</th>
			  </thead>   
			  <tbody>
					@foreach($journals as $journal)
				<tr>
								<td>{{ $journal->id }}</td>
								<td>{{ $journal->source }}</td>
								<td>{{ $journal->particular }}</td>
								<td>{{ $journal->date }}</td>
								<td><a href="/journal/{{$journal->id}}/edit" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a></td>
								
				</tr>
				
				@endforeach							
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->
	
</div><!--/row-->

@section('essentialScript')

<script type="text/javascript">
	

	
	

</script>

@endsection

@endsection