@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Voucher</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New Voucher</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
	
		
		

		<div class="col-sm-12">
			<hr style="border: 0;height: 1px;background: #333;">
		</div>

		<div style=" width:100%; max-height:400px; overflow: auto; font-size:12px">
			<h3 style="padding-left:15px">Vouchers Added Today</h3>

			<table class="table table-bordered" style="margin-top:20px">
				<tr>
					<th>Voucher <br> Number</th>
					<th>Voucher <br> Type</th>
					<th>Parent <br> Head</th>
					<th>Account <br> Head</th>
					<th>Date</th>
					<th>Payment <br> Mode</th>
					<th>Cash/Bank</th>
					<th>Amount</th>
					<th>Description</th>
					<th>Edit/Delete</th>
				</tr>
				@foreach($vouchers as $voucher)
				<tr>
					<td>{!! $voucher->voucherNumber !!}</td>
					<td>{!! $voucher->voucherType !!}</td>
					<td>{!! $voucher->parentHead !!}</td>
					<td>{!! $voucher->accountHead !!}</td>
					<td>{!! $voucher->date !!}</td>
					<td>{!! $voucher->paymentMode !!}</td>
					<td>{!! $voucher->available_cash_bank !!}</td>
					<td>{!! $voucher->amount !!}</td>
					<td>{!! $voucher->description !!}</td>
					<td>
						<a href="/voucher/{{$voucher->id}}/edit/" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>

						{!! Form::open(array('url' => 'voucher/' . $voucher->voucherNumber, 'class' => 'pull-right')) !!}
						{!! Form::hidden('_method', 'DELETE') !!}
						<button type="submit" class="btn btn-danger">
							<span class="glyphicon glyphicon-remove"></span>
						</button>
						{!! Form::close() !!}

						

					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
</div>

@section('essentialScript')

<script type="text/javascript">
	
	function getVoucher() {
		var voucherNumber = document.getElementById("voucherNumber").value;
		if(voucherNumber){
			var url = "/voucher/" + voucherNumber + "/edit";
			window.location.href = url;		
		}
		else{
			var url = "/voucher/0/edit";
			window.location.href = url;			
		}
	}

</script>

@endsection

@endsection