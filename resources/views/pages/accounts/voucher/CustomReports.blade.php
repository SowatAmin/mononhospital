<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Voucher Report</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 13px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 12px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 5px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}

	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		
		<h4>Voucher Between {{$from}} To {{$to}}</h4>
		<table width="100%" id="verticalTable">
			<tr>
				<th>SL.<br>No</th>
				<th align="center">V.<br>No</th>
				<th align="center">Voucher Type</th>
				<th align="center">Parent Head</th>
				<th align="center" >Account Head</th>
				<th align="center">Payment Mode</th>
				<th align="center">Available Cash Bank</th>
				<th align="center">Amount</th>
				<th align="center" style="width:80px;">Date</th>
				
			</tr>
			@foreach($vouchers as $index => $voucher)
			<tr>
				<td>{{$index}}</td>
				<td align="left" >{{$voucher->voucherNumber}}</td>
				<td align="left" >{{$voucher->voucherType}}</td>
				<td align="left" >{{$voucher->parentHead}}</td>
				<td align="left" >{{$voucher->accountHead}}</td>
				<td align="center">{{$voucher->paymentMode}}</td>
				<td align="center">{{$voucher->available_cash_bank}}</td>

				<td align="center">{{$voucher->amount}}</td>
				<td align="center">{{$voucher->date}}</td>
				
			</tr>				
			@endforeach
			<tr>
				
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align="center"><strong>Total={{$total}}</strong></td>
				<td></td>
				
				
				
				
				


			</tr>
		</table>

	</div>

</div>

</body>