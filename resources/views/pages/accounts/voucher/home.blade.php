@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Voucher</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">New Voucher</a></li>
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="container-fluid">
	<div class="row">
		
		<h3 style="padding-left:15px">New Voucher</h3>
		<div class="col-md-6">
			<div class="input-group input-group-sm">
				<span class="input-group-addon" style="min-width:100px;text-align:left">Voucher No</span>
				<input type="text" name="voucherNumber" id="voucherNumber" class="form-control" style="height:auto" value="{{ $voucher->voucherNumber+1 }}">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Voucher Type</span>
				<select name="voucherType" id="voucherType" class="form-control" required>
					<option>Select an option</option>
					
					@foreach($vouchertype as $type)
					<option value="{{ $type->accounttype}}">{{ $type->accounttype}}</option>
					@endforeach

				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Parent Head</span>
				<select name="parentHead" id="parentHead" class="form-control" required>
					<option>Select an option</option>
					@foreach($accounttype as $atype)
					<option value="{{ $atype->parentHead}}">{{ $atype->parentHead}}</option>
					@endforeach

				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:100px;text-align:left;">Account Head</span>
				<select name="accountHead" id="accountHead" class="form-control" required>
					
				</select>
			</div>
		</div>

		<div class="col-md-6">
			<div class="input-group input-group-sm">
				<span class="input-group-addon" style="min-width:135px;text-align:left">Date</span>
				<input type="text" name="date" id="date" class="form-control" style="height:auto">
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Payment Mode</span>
				<select name="paymentMode" id="paymentMode" class="form-control" required>
					<option value="">Select an option</option>
					<option value="Cash">Cash</option>
					<option value="Bank">Bank</option>
					
				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Available Cash/Bank</span>
				<select name="available_cash_bank" id="available_cash_bank" class="form-control" required>
					
				</select>
			</div>

			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Amount</span>
				<input type="text" name="amount" id="amount" class="form-control" style="height:auto" required>
				<input type="hidden" name="words" id="words">
			</div>
		</div>

		<div class="col-md-12">
			<div class="input-group input-group-sm" style="margin-top: 15px">
				<span class="input-group-addon" style="min-width:135px;text-align:left;">Description</span>
				<textarea rows="3" name="description" id="description" class="form-control"></textarea>
			</div>
		</div>


		<div class="col-md-12">
			<div class="btn-group" style="margin-top:20px;float:right;">
				<button type="button" class="btn btn-warning" id="addbtn">
					<span class="glyphicon glyphicon-plus"></span> Add
				</button>
			</div>

		</div>



		<div class="col-md-12" style="margin-top: 15px">
			<div style="max-height:500px; overflow: auto; font-size:12px">
				<table class="table table-bordered" id="vouchertable">
					<tr>
						<th>Voucher Number</th>
						<th>Voucher Type</th>
						<th>Parent Head</th>
						<th>Account Head</th>
						<th>Payment Mode</th>
						<th>Available Cash/ Bank</th>
						<th>Amount</th>
						<th>Description</th>
						<th>Date</th>
						
						
					</tr>
				</table>
			</div>
		</div>

		<div class="col-sm-12" style="margin-top: 15px">
			<div class="btn-group btn-group-justified">
				<div class="btn-group">
					<a class="btn btn-primary" href="/voucher">
						<span class="glyphicon glyphicon-retweet"></span> New
					</a>
				</div>
				
				<div class="btn-group">
					<a class="btn btn-danger" href="/home">
						<span class="glyphicon glyphicon-off"></span> Exit
					</a>
				</div>
			</div>
		</div>
		
	</div>
</div>


@section('essentialScript')

<script type="text/javascript">
$('select').select2();



$("#addbtn").click(function(){



	var voucherNumber=$("#voucherNumber").val();
	var voucherType=$("#voucherType").val();
	var parentHead=$("#parentHead").val();
	var accountHead=$("#accountHead").val();
	var paymentMode=$("#paymentMode").val();
	var available_cash_bank=$("#available_cash_bank").val();
	var amount=$("#amount").val();
	var description=$("#description").val();
	var date=$("#date").val();

		

		






		$.ajax({

			url  : "save_voucherdata",
			type : "GET",

			data: {
				
				'voucherType':voucherType,
				'parentHead':parentHead,
				'accountHead':accountHead,
				'paymentMode':paymentMode,
				'available_cash_bank':available_cash_bank,
				'amount':amount,
				'description':description,
				'date':date
				
				




			},
			success: function(re){
						
						inWords(amount);
						var words=$("#words").val();
						$("#vouchertable").append("<tr><td>"+voucherNumber+"</td><td>"+voucherType+"</td><td>"+parentHead+"</td><td>"+accountHead+"</td><td>"+paymentMode+"</td><td>"+available_cash_bank+"</td><td>"+amount+"</td><td>"+description+"</td><td>"+date+"</td></tr>");
						
						voucherNumber=parseInt(voucherNumber)+1;
						
						$("#voucherNumber").val(voucherNumber);				


						
						
					}
				});


		
	});







$("#date").datepicker({ dateFormat: 'yy-mm-dd' });

	//account head ajax;
	$(document).ready(function() {
		$("#parentHead").on('change',function(e){
			console.log(e);
			var phead=e.target.value;


			$.get('ajax-phead?phead='+phead,function(data){

				console.log(data);
				$("#accountHead").empty();

			//$("#accountHead").select2({allowClear: true});
			$.each(data,function(index,subcatObj){
				$("#accountHead").append(' <option value="'+subcatObj.childHead+'">'+subcatObj.childHead+'</option> ');

			});
		});

		});

	});

	$('#paymentMode').on('change',function(){


	var mode=$('#paymentMode').val();

	$.get('ajax-pmode?mode='+mode,function(data){


		console.log(data);
		$("#available_cash_bank").empty();
		

		$.each(data,function(index,subcatObj){
				$("#available_cash_bank").append(' <option value="'+subcatObj.availableCash_bank+'">'+subcatObj.availableCash_bank+'</option> ');



	});
		$('select').select2();
	});
});



$(document).ready(function(){
		$("#amount").on('change',function(){
			var amount=$("#amount").val();
		inWords(amount);
		

		});
		
	});


var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];


function inWords(num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return;
    var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + ' ' : '';
    var result=str+"taka only.";
    //alert(str);
    $('#words').val(result);
}









	</script>

	@endsection

	@endsection