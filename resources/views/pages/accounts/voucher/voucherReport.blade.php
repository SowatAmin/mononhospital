<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Voucher</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
	body {
	}
	table{

	}
	th{
		text-align: left;
	}
	#verticalTable th{
		text-align: center;
		font-size: 15px;
		border: 1px solid black;
	}
	#verticalTable td{
		font-size: 14px;
		border: 1px dashed black;
	}
	#verticalTable{
		width: 100%;
		padding: 5px 0px 0px 0px;
		margin: 0px auto;
		border-collapse: collapse;
	}
	tr{
		margin-top: 10px;
	}
	#logo{
		width: 112px;
		height: 113px;
		padding-left:30px;
	}

	td{
		padding: 8px;
		font-size: 23px;
		
		
	}

	</style>


</head>

<body>
	<div class="page">

		<h3 align="center">
			MONON Psychiatric Hospital			
		</h3>
		<h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4>
		<hr size=2 style="background-color:black">
		<br><br>
		
		<table border=2 align="center" style="height:50px" >
			<tr>
				<td align="center"><strong>Payment Voucher</strong></td>
			</tr>
		</table>
		<br><br><br><br>

		


		<table style="width:100%">
			<tr>
				
				<td align="right" >Date:{{$voucher->date}}</td>
			</tr>
			


		</table>
		<br><br><br><br><br><br>

		<table style="border-collapse:collapse;width:100%">

			<tr>
				<td align="left" style="font-family:sans-serif;"><b><i>Voucher No:</i></b>{{$voucher->voucherNumber}}</td>
			</tr>
			<tr>
				<td align="left" style="border-bottom: 1px solid #ddd; "><b><i>Accounts:</i></b>Monon Account</td>	

			</tr>

			<tr>
				<td style="border-bottom: 1px solid #ddd; "><b><i>Account Head:</i></b>{{$voucher->accountHead}}</td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid #ddd; "><b><i>Amount:</i></b>{{$voucher->amount}}/-</td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid #ddd; "><b><i>Received from :</i></b>{{$voucher->available_cash_bank }}</td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid #ddd; "><b><i>Description :</i></b>{{$voucher->description}}</td>
			</tr>




		</table>

		<br><br><br><br><br><br>

		<table width=100%>
			<tr>
				<td>.......................</td>
				<td>.......................</td>
				<td>.......................</td>
			</tr>
			<tr>
				<td>Prepared By</td>
				<td>Approved By</td>
				<td>Accountent</td>
			</tr>


		</table>
		
		
		

	</div>

</div>







</body>