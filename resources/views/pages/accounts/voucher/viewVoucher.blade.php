@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="#">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<i class="icon-home"></i>
		<a href="#">Search Voucher</a> 
		<i class="icon-angle-right"></i>
	</li>
	
</ul>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-error">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif		
@if (session('status'))
<div class="alert alert-success">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{{ session('status') }}
</div>
@endif


<div class="row-fluid sortable">		
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon list"></i><span class="break"></span>Voucher List</h2>
			
		
		<div class="box-content">
		
			<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					 		<th>SL. No</th>
							<th>Voucher Number</th>
							<th>Voucher Type</th>
							<th>Parent Head</th>
							<th>Account Head</th>
							<th>Payment Mode</th>
							<th>Available Cash Bank</th>
							<th>Amount</th>
							<th>Date</th>
							<th>View Voucher</th>
				  </tr>
			  </thead>   
			  <tbody>
			@foreach($vouchers as $index => $voucher)
				<tr>
						<td>{{$index+1}}</td>
						<td>{{$voucher->voucherNumber}}</td>
						<td>{{$voucher->voucherType}}</td>
						<td>{{$voucher->parentHead}}</td>
						<td>{{$voucher->accountHead}}</td>
						<td>{{$voucher->paymentMode}}</td>
						<td>{{$voucher->available_cash_bank}}</td>
						<td>{{$voucher->amount}}</td>
						<td>{{$voucher->date}}</td>
						<td><a href="/voucher/{{$voucher->voucherNumber}}/invoice" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-comment"></span></a></td>
								
							</tr>
				@endforeach							
			  </tbody>
		  </table>            
		</div>
	</div><!--/span-->
	
</div><!--/row-->


@section('essentialScript')

<script type="text/javascript">


</script>

@endsection

@endsection