@extends('layouts.default')

@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Dashboard</a></li>
</ul>
@endsection

@section('content')
<div class="row-fluid">	

	<a href="/voucher" class="quick-button metro yellow span2">
		<i class="glyphicon glyphicon-text-background"></i>
		<p>Voucher</p>	
	</a>

	<a href="/journal" class="quick-button metro red span2">
		<i class="glyphicon glyphicon-briefcase"></i>
		<p>New Journal</p>	
	</a>

	<a href="/journal/show" class="quick-button metro pink span2">
		<i class="glyphicon glyphicon-folder-close"></i>
		<p>Existing Journal</p>	
	</a>
	

	<a href="/employee" class="quick-button metro blue span2">
		<i class="icon-user"></i>
		<p>New Employee</p>
	</a>
	<a href="/employee/create" class="quick-button metro orange span2">
		<i class="icon-user"></i>
		<p>Existing Employee</p>
	</a>

	<a href="/salary/index" class="quick-button metro blue span2">
		<i class="icon-briefcase"></i>
		<p>Salary</p>
	</a>

	
	<div class="clearfix"></div>
					
</div><!--/row-->
<br>
<div class="row-fluid">

	<a href="/ViewVoucher" class="quick-button metro purple span2">
		<i class="icon-list-alt"></i>
		<p>Existing Voucher</p>
	</a>

	<a href="/VoucherReport" class="quick-button metro black span2">
		<i class="icon-list-alt"></i>
		<p>Voucher Reports</p>
	</a>

	<a href="/accountsReports/homepage" class="quick-button metro green span2">
		<i class="icon-list-alt"></i>
		<p>Reports</p>
	</a>

<div class="clearfix"></div>	

</div>
@endsection