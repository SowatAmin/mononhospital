@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a>Create Food Chart</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid">
  	@foreach($clientinfos as $clientinfo)
	<div class="span6">
		<h3>{!! $day !!}</h3>
		<p>Total Patient: {!! $clientinfo->total_patient!!}</p>
		<p>Num. of DH: NDP: {!! $clientinfo->dh_ndp !!} + DP: {!! $clientinfo->dh_dp !!} = {!! $clientinfo->dh_ndp+$clientinfo->dh_dp !!}</p>
		<p>Num. of catering meal: L: {!! $clientinfo->number_of_catering_meal_lunch !!} + D: {!! $clientinfo->number_of_catering_meal_dinner !!} = {!! $clientinfo->number_of_catering_meal_lunch+$clientinfo->number_of_catering_meal_dinner !!}</p>
		<p>Num. of staff meal: L: {!! $clientinfo->number_of_staff_meal_lunch !!} + D: {!! $clientinfo->number_of_staff_meal_dinner !!} = {!! $clientinfo->number_of_staff_meal_lunch+$clientinfo->number_of_staff_meal_dinner !!}</p>
	</div>
	<div class="span6">
		<div class="span6">
			<h3>Date: {!! date_format(date_create($date),'d-M-Y') !!}</h3>
			<p>Num. of NDP (R): {!! $clientinfo->number_of_ndp_regular !!} &nbsp;&nbsp;&nbsp; DP (R): {!! $clientinfo->number_of_dp_regular !!}</p>
			<p>Num. of NDP (L): {!! $clientinfo->number_of_ndp_low_cost !!} &nbsp;&nbsp;&nbsp; DP (L): {!! $clientinfo->number_of_dp_low_cost !!}</p>
			
		</div>
		<div class="span6">
			<h3>Total</h3>
			<p>= {!! $clientinfo->number_of_ndp_regular+$clientinfo->number_of_dp_regular !!} </p>
			<p>= {!! $clientinfo->number_of_ndp_low_cost+$clientinfo->number_of_dp_low_cost !!}</p>
		</div>
		<div class="span12" style="border-top:1px solid #000; margin:0;">
			<p>Total <span style="margin-left:80px;">{!! $clientinfo->number_of_ndp_regular+$clientinfo->number_of_ndp_low_cost !!}</span> <span style="margin-left:60px;">{!! $clientinfo->number_of_dp_low_cost+$clientinfo->number_of_dp_regular !!}</span></p>

			<p><a href="/food/edit-client/{!! $clientinfo->id !!}" data-rel="tooltip" data-original-title="update client">Do you want to change the number of client?</a></p>
		</div>
	</div>
	@endforeach
</div>	
<div class="row-fluid sortable">
  	  
	<div class="box span12">
			<div>
			<h2><i class="halflings-icon list"></i><span class="break"></span>Create Food Charts</h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
			
		
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		@if (session('customAlert'))
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('customAlert') }}
		    </div>
		@endif
		<div class="box-content">
			<form action="/food/daily-chart" method="post">
			{!! csrf_field() !!}
				<table class="table">
				  <thead>
					  <tr>
						  <th>Name of Item</th>
						  <th>Qty/meal(gm)&nbsp;&nbsp;&times;&nbsp;&nbsp;num.of client</th>
						  <th>Total Quantity</th>					  
					  </tr>
				  </thead>   
				  <tbody>
				  <?php 
				  	$autoClientCheck = 0;				  	
				  ?>
				  @foreach($items as $index => $item)
					  <?php					  	
					  	$resultQuantity = 1;
					  ?>
					<tr>
						<input type="hidden" name="foodid[]" value="{!! $item->id !!}">
						<td>{!! $item->NameOfItem !!}</td>
						<td class="center">
							<input name="qty[]" style="width:60px" class="multiply" id="value_{!! (2*$index)+1 !!}" type="text" value="{!! $item->Quantity !!} gm" readonly="">
							<?php $resultQuantity = $resultQuantity*$item->Quantity; ?>
							&times;
							@if($item->HowManyTimes>1)
								<input name="hmt[]" style="width:60px" class="multiply" id="times_{!! ((2*$index)+1)*(($index*2)+2) !!}" type="text" value="{!! $item->HowManyTimes !!}" readonly="">
								<?php $resultQuantity = $resultQuantity*$item->HowManyTimes; ?>
								@if($autoClient[$autoClientCheck]!="fixed")
									&times;
								@endif
							@else
								<input name="hmt[]" style="width:60px" class="multiply" id="times_{!! ((2*$index)+1)*(($index*2)+2) !!}" type="hidden" value="{!! $item->HowManyTimes !!}" >
								<?php $resultQuantity = $resultQuantity*$item->HowManyTimes; ?>
							@endif
							@if($autoClientCheck<=sizeof($autoClient)-1 && $autoClientItemId[$autoClientCheck]==$item->id)
								@if($autoClient[$autoClientCheck]!="fixed")								
									<input style="width:60px" name="client[]" class="multiply" id="value_{!! ($index*2)+2 !!}" type="text" placeholder="0" value="{!! $autoClient[$autoClientCheck] !!}" >
									<?php $resultQuantity = $resultQuantity*$autoClient[$autoClientCheck]; ?>
								@else
									<input style="width:60px" name="client[]" class="multiply" id="value_{!! ($index*2)+2 !!}" type="hidden" placeholder="0" value="1" >
								@endif
								<?php $autoClientCheck++; ?>
							@else
								<input style="width:60px" name="client[]" class="multiply" id="value_{!! ($index*2)+2 !!}" type="text" placeholder="0" >
								<?php $resultQuantity = 0; ?>
							@endif							
						</td>
						<td id="result_{!! ((2*$index)+1)+(($index*2)+2) !!}" class="center">{!! $resultQuantity !!}gm ({!! $resultQuantity/1000 !!}kg)</td>					
					</tr>				
				  @endforeach		
				  </tbody>
			  	</table>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="/food/food-chart" class="btn">Cancel</a>
				</div>
		  </form>
		</div>
	</div><!--/span-->
</div><!--/row-->
@endsection

@section('essentialScript')
<script type="text/javascript">
$(document).ready(function(){

    $("input").on("input",function(){
        var idName = $(this).attr('id');
        var idNumber = idName.substring(6);
        var findAnotherValue = idNumber-1;
        var resultAddress = parseInt(idNumber) + parseInt(findAnotherValue);
        var commonThingsOfAnId = "value_";
        var anotherId = commonThingsOfAnId.concat(findAnotherValue);        
        var firstValueWithString = $('#'+anotherId).attr('value');
        var firstValue = firstValueWithString.substr(0,firstValueWithString.indexOf(' '));
    	var secondValue = $(this).val();
    	var timesAddress = parseInt(idNumber) * parseInt(findAnotherValue);
    	var timesValue = $('#times_'+timesAddress).attr('value');
    	var result = firstValue*timesValue*secondValue;

    	if(isNaN(secondValue)){
    		alert('You Must Enter a Valid Number');
    		$(this).val('');
    	}
    	else $("#result_"+resultAddress).html(result + "gm (" + result/1000 + "kg)");
    });
});
</script>
@endsection