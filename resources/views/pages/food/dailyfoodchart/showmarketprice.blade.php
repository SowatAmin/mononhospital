@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>		
		<a href="/food/show-calendar-market">Calendar</a> 
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a>Show price chart</a>		
	</li>
</ul>
@endsection

@section('content')
<div class="row-fluid sortable">  	  
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon list"></i><span class="break"></span>Food Price Chart of {!! date('d-M-Y',strtotime($date)) !!}</h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
		</div>		
		<div class="box-content">			
			<table class="table table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					  <th>Name of Item</th>
					  <th>Qty</th>
					  <th>Price</th>
					  @if($date==date('Y-m-d'))
					  <th>Action</th>
					  @endif					  
				  </tr>
			  </thead>   
			  <tbody>			  
			  @foreach($infos as $index => $info)
			  @if($index==0)
			  	<?php continue; ?>
			  @endif			  
				<tr>
					<input type="hidden" name="foodname[]" value="{!! $info->item_name !!}">
					<td>{!! $info->item_name !!}</td>
					<td class="center">
						@if($date==date('Y-m-d'))
						<input style="width:auto" name="qty[]" class="multiply" value="{!! $info->quantity !!}" type="text" readonly="">
						@else
						{!! $info->quantity !!}
						@endif
					</td>
					<td class="center">
						@if($date==date('Y-m-d'))
						<input style="width:60px" name="price[]" class="multiply" id="price_{!! $index !!}" type="text" placeholder="0" value="{!! $info->price !!} Tk" readonly="">
						@else
						{!! $info->price !!} Tk
						@endif
					</td>
					@if(strtotime($date)>=strtotime(date('Y-m-d')))
					<td class="center">						
						<a href="/food/edit-price/{!! $info->id !!}" title data-rel="tooltip" class="btn btn-info" data-original-title="Edit">
							<i class="halflings-icon white edit"></i>  
						</a>
					</td>
					@endif					
				</tr>				
			  @endforeach			  		
			  </tbody>
		  	</table>				
		</div>
	</div><!--/span-->
</div><!--/row-->
@endsection