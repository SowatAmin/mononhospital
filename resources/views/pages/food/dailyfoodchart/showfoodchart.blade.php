@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a href="/food/show-calendar">Calendar</a>
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a>Food Chart</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid">
	@foreach($clientInfos as $clientinfo)
	<div class="span6">
		<h3>{!! date('l', strtotime($date)) !!}</h3>
		<p>Total Patient: {!! $clientinfo->total_patient!!}</p>
		<p>Num. of DH: NDP: {!! $clientinfo->dh_ndp !!} + DP: {!! $clientinfo->dh_dp !!} = {!! $clientinfo->dh_ndp+$clientinfo->dh_dp !!}</p>
		<p>Num. of catering meal: L: {!! $clientinfo->number_of_catering_meal_lunch !!} + D: {!! $clientinfo->number_of_catering_meal_dinner !!} = {!! $clientinfo->number_of_catering_meal_lunch+$clientinfo->number_of_catering_meal_dinner !!}</p>
		<p>Num. of staff meal: L: {!! $clientinfo->number_of_staff_meal_lunch !!} + D: {!! $clientinfo->number_of_staff_meal_dinner !!} = {!! $clientinfo->number_of_staff_meal_lunch+$clientinfo->number_of_staff_meal_dinner !!}</p>
	</div>
	<div class="span6">
		<div class="span6">
			<h3>Date: {!! date('d-M-Y', strtotime($date)) !!}</h3>
			<p>Num. of NDP (R): {!! $clientinfo->number_of_ndp_regular !!} &nbsp;&nbsp;&nbsp; DP (R): {!! $clientinfo->number_of_dp_regular !!}</p>
			<p>Num. of NDP (L): {!! $clientinfo->number_of_ndp_low_cost !!} &nbsp;&nbsp;&nbsp; DP (L): {!! $clientinfo->number_of_dp_low_cost !!}</p>
			
		</div>
		<div class="span6">
			<h3>Total</h3>
			<p>= {!! $clientinfo->number_of_ndp_regular+$clientinfo->number_of_dp_regular !!} </p>
			<p>= {!! $clientinfo->number_of_ndp_low_cost+$clientinfo->number_of_dp_low_cost !!}</p>
		</div>
		<div class="span12" style="border-top:1px solid #000; margin:0;">
			<p>Total <span style="margin-left:80px;">{!! $clientinfo->number_of_ndp_regular+$clientinfo->number_of_ndp_low_cost !!}</span> <span style="margin-left:60px;">{!! $clientinfo->number_of_dp_low_cost+$clientinfo->number_of_dp_regular !!}</span></p>

			<p>
				<a href="#myModal" class="dataDelete" data-rel="tooltip" data-toggle="modal" data-original-title="Delete chart" data="{!! $date !!}" >Delete the chart?</a> 
				&nbsp;&nbsp;&nbsp; 
				<a href="/food/chart-report/{!! $date !!}" data-rel="tooltip" data-original-title="download pdf">Download the chart report</a>
			</p>
		</div>
	</div>
	@endforeach
</div>	
<div class="row-fluid sortable">

	<div class="box span12">
		
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon list"></i><span class="break"></span>Food Charts</h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
		</div>		
		<div class="box-content">			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Name of Item</th>
						<th>Qty/meal(gm)&nbsp;&nbsp;&times;&nbsp;&nbsp;num.of client</th>
						<th>Total Quantity</th>
						<th>Act. pur qty</th>
						@if((strtotime(date('Y-m-d'))<=strtotime($date)) && (strtotime($date)<=strtotime("+2 day", strtotime(date('Y-m-d')))))
						<th>Action</th>
						@endif		  
					</tr>
				</thead>   
				<tbody>
					{{-- */$category=0;/* --}}
					@foreach($chartInfos as $index => $info)			  	
					<tr>						
						<td>{!! $info->NameOfItem !!}</td>
						<td class="center">
							<input style="width:60px" class="multiply" id="value_{!! (2*$index)+1 !!}" type="text" value="{!! $info->Quantity !!} gm" readonly="">
							&times;
							@if($info->HowManyTimes>1)
							<input style="width:60px" class="multiply" id="times_{!! ((2*$index)+1)*(($index*2)+2) !!}" type="text" value="{!! $info->HowManyTimes !!}" readonly="">
							&times;
							@else
							<input style="width:60px" class="multiply" id="times_{!! ((2*$index)+1)*(($index*2)+2) !!}" type="hidden" value="{!! $info->HowManyTimes !!}" >
							@endif
							<input style="width:60px" class="multiply" id="value_{!! ($index*2)+2 !!}" type="text" placeholder="0" value="{!! $info->number_of_client !!}" readonly="">
						</td>
						<td id="result" class="center">
							{!! $info->number_of_client*$info->HowManyTimes*$info->Quantity !!} gm ({!! ($info->number_of_client*$info->Quantity*$info->HowManyTimes)/1000 !!} kg)

						</td>
						@if($category != $info->CategoryId)
						{{-- */$category=$info->CategoryId;$add=0;/* --}}
						{{-- */$rowspan=0;/* --}}
						@foreach($chartInfos as $infoForAddition)							
						@if($infoForAddition->CategoryId==$category)
						{{-- */$add=$add+($infoForAddition->number_of_client*$infoForAddition->Quantity*$infoForAddition->HowManyTimes); /*--}}
						{{-- */$rowspan++;/* --}}							
						@endif
						@endforeach
						<td class="center" rowspan="{!! $rowspan; !!}" style="vertical-align:middle">		
							{!! $add !!} gm ({!! $add/1000 !!} kg)										
						</td>
						@endif
						<!-- (strtotime(date('Y-m-d'))<=strtotime($date)) && (strtotime($date)<=strtotime("+2 day", strtotime(date('Y-m-d')))) -->
						@if(strtotime($date)>=strtotime(date('Y-m-d')))
						<td class="center">
						<!-- <a class="btn btn-success" href="#">
							<i class="halflings-icon white zoom-in"></i>  
						</a> -->
						<a href="/food/edit-chart/{!! $info->id !!}" title data-rel="tooltip" class="btn btn-info" data-original-title="Edit">
							<i class="halflings-icon white edit"></i>  
						</a>
					</td>
						@endif
					</tr>								
					@endforeach		
			</tbody>
		</table>		  
	</div>
</div><!--/span-->

<div class="modal hide fade" id="myModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Attention</h3>
	</div>
	<div class="modal-body">
		<p>Are you sure you want to delete the chart?</p>
	</div>
	<div class="modal-footer">
		<a id="deleteConfirm" href="/food/delete-chart/" class="btn btn-danger">Yes</a>
		<a href="#" class="btn btn-primary" data-dismiss="modal">No</a>			
	</div>
</div>
</div><!--/row-->
@endsection

@section('essentialScript')
<script>
	$(document).ready(function() {    		
		$('.dataDelete').click(function(){
			var id = $(this).attr('data');
			var hrefModaLink = $('#deleteConfirm').attr('href');
			var link = hrefModaLink + id;
			$('#deleteConfirm').attr('href',link);
		});
	});
</script>
@endsection