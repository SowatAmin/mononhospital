@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a>Pick a date</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
	@if (session('alert'))
	    <div class="alert alert-error">
	    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        {{ session('alert') }}
	    </div>
	@endif
	  
		  <h2 style="padding-left:15px"><i class="halflings-icon calendar"></i><span class="break"></span>Please pick a date to create a market list</h2>
	  
	  
	  <div class="box-content">
		<!-- <div class='panel-heading'><h3 class='panel-title'>Calendar</h3></div> -->
            <div class='panel-body'>                 
                <div id="cldr"></div>
            </div>
	</div>
</div><!--/row-->
@endsection

@section('essentialScript')
<script>
$(document).ready(function() {
	var url = "/food/create-market-list/";
    $('#cldr').fullCalendar({
    	defaultDate: '{!! date('Y-m-d') !!}',
    	dayClick: function(date, allDay, jsEvent, view) {
    		var month = ("0" + (date.getMonth() + 1)).slice(-2);
    		var formatedDate = ("0" + (date.getDate())).slice(-2);    		
    		var collectDate = date.getFullYear() + "-" + month + "-" + formatedDate;    		
    		window.location.href = url + collectDate;
    	}
    });           
});
</script>
@endsection