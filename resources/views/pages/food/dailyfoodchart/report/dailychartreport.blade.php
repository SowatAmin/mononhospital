<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="padding: 50px">
<table width="100%">
	<tr>
    	<td> 
        	<table>
            	<tr>
                	<td colspan="4">{!! date('l', strtotime($date)) !!} </td>
                </tr>
                @foreach($clientInfos as $clientinfo)
                <tr>
                	<td>Total patient: {!! $clientinfo->total_patient !!}</td>
                    <td colspan="2"></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Number of DH:</td>
                    <td>NDP: {!! $clientinfo->dh_ndp !!} </td>
                    <td>+DP: {!! $clientinfo->dh_dp !!} </td>
                    <td>= {!! $clientinfo->dh_dp+$clientinfo->dh_ndp !!} </td>
                </tr>
                <tr>
                	<td>Number of cat Meal:</td>
                    <td>L: {!! $clientinfo->number_of_catering_meal_lunch !!} </td>
                    <td>+D: {!! $clientinfo->number_of_catering_meal_dinner !!} </td>
                    <td>= {!! $clientinfo->number_of_catering_meal_dinner + $clientinfo->number_of_catering_meal_lunch !!} </td>
                </tr>
                <tr>
                	<td>Number of stf Meal:</td>
                    <td>L: {!! $clientinfo->number_of_staff_meal_lunch !!} </td>
                    <td>+D: {!! $clientinfo->number_of_staff_meal_dinner !!} </td>
                    <td>= {!! $clientinfo->number_of_staff_meal_lunch+$clientinfo->number_of_staff_meal_dinner !!} </td>
                </tr>
                @endforeach
            </table>
        </td>
        
        <td> 
        	<table align="right">
            	<tr align="right">
                	<td colspan="5">Date: {!! date('d-M-Y',strtotime($date)) !!} </td>
                </tr>
                @foreach($clientInfos as $clientinfo)
                <tr>
                	<td>Num.of :NDP (R): </td>
                    <td> {!! $clientinfo->number_of_ndp_regular !!}</td>
                    <td>DP (R):</td>
                    <td> {!! $clientinfo->number_of_dp_regular !!}</td>
                    <td>= {!! $clientinfo->number_of_dp_regular + $clientinfo->number_of_ndp_regular !!} </td>
                </tr>
                <tr>
                	<td>Num.of :NDP (L): </td>
                    <td> {!! $clientinfo->number_of_ndp_low_cost !!}</td>
                    <td>DP (L):</td>
                    <td> {!! $clientinfo->number_of_dp_low_cost !!}</td>
                    <td>= {!! $clientinfo->number_of_dp_low_cost + $clientinfo->number_of_ndp_low_cost !!} </td>
                </tr>
                <tr>
                	<td style="border-top:1px solid #000;">Total</td>
                    <td style="border-top:1px solid #000;">{!! $clientinfo->number_of_ndp_low_cost + $clientinfo->number_of_ndp_regular !!} </td>
                    <td style="border-top:1px solid #000;"> </td>
                    <td style="border-top:1px solid #000;">{!! $clientinfo->number_of_dp_low_cost + $clientinfo->number_of_dp_regular !!} </td>
                    <td style="border-top:1px solid #000;"> </td>
                </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>
<table width="100%"  border="1">
    {{-- */$category=0;/* --}}    
	<tr>
    	<td rowspan="2">Name of Item </td>
        <td>Qty./meal(gm) X </td>
        <td rowspan="2">Total qty. </td>
        <td rowspan="2">Act. pur.qty. </td>
    </tr>
    <tr>
    	
        <td>num. of client.</td>
        
    </tr>
    @foreach($chartInfos as $index => $info)
    <tr>
    	<td>{!! $info->NameOfItem !!}</td>
        <td>
            {!! $info->Quantity !!} gm
            &times;
            @if($info->HowManyTimes>1)
            {!! $info->HowManyTimes !!}
            &times;            
            @endif
            {!! $info->number_of_client !!}
        </td>
        <td>{!! $info->number_of_client*$info->HowManyTimes*$info->Quantity !!} gm ({!! ($info->number_of_client*$info->Quantity*$info->HowManyTimes)/1000 !!} kg)</td>
        @if($category != $info->CategoryId)
        {{-- */$category=$info->CategoryId;$add=0;/* --}}
        {{-- */$rowspan=0;/* --}}
        @foreach($chartInfos as $infoForAddition)                           
            @if($infoForAddition->CategoryId==$category)
                {{-- */$add=$add+($infoForAddition->number_of_client*$infoForAddition->Quantity*$infoForAddition->HowManyTimes); /*--}}
                {{-- */$rowspan++;/* --}}                           
            @endif
        @endforeach
        <td rowspan="{!! $rowspan; !!}">{!! $add !!} gm ({!! $add/1000 !!} kg)</td>
        @endif
    </tr>
    @endforeach
    
</table>