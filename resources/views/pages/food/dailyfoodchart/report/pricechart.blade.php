<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="padding: 50px">
<table width="100%">
	<tr>
    	<td colspan="2">Daily Food Market Price Chart</td>
    </tr>
    <tr>
    	<td>{!! date('l', strtotime($date)) !!} </td>
        <td>Date : {!! $date !!} </td>
    </tr>
</table>
<table width="100%" border="1">
    <tr>
    	<td>
        	Item
        </td>
        <td>
        	Qty
        </td>
        <td>
        	Price
        </td>
    </tr>
    @foreach($food as $index => $item)
    @if($index==0)
        <?php continue; ?>
    @endif
    <tr>
    	<td>
        	{!! $item !!}
        </td>
        <td>
        	{!! $foodamount[$index] !!} gm ({!! $foodamount[$index]/1000 !!} kg)
        </td>
        <td>
        </td>
    </tr>
    @endforeach
</table>
</body>
</html>