@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="{!! URL::previous() !!}">Pick a date</a>
		<i class="icon-angle-right"></i>		
	</li>	
	<li>
		<a>Today's Client</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		
			<h2 style="padding-left:15px"><i class="halflings-icon edit"></i><span class="break"></span>Today's Client</h2>
			
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>		            
		            <li>{{ $errors->first() }}</li>		            
		        </ul>
		    </div>
		@endif		
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		@if (session('alert'))
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('alert') }}
		    </div>
		@endif
		@if ($existence == 'true')
		    <div class="row-fluid">
		  	  <div class="span12">
		  		<h2 style="color:red">Alert!! You already have created today's food chart</h2>		  		
		  		<div class="tooltip-demo well">
		  		  <p class="muted" style="margin-bottom: 0;">Dear user, you already have created today's food chart. So, you don't need to create it again. To change your today's daily chart, you have to <a href="#" data-rel="tooltip" data-original-title="update food chart">click here.</a><br><br>Thank you!
		  		  </p>
		  		</div>                                  
		  	  </div>
		    </div>
		@else
		<div class="box-content">
			<div class="page-header">
				@if($date==date('Y-m-d'))
				<h1>Welcome, <small>Today is {!! $day !!}, {!! date_format(date_create($date),'d-M-Y') !!}
				@else
				<h1>Welcome, <small>You are going to create a chart for {!! $day !!}, {!! date_format(date_create($date),'d-M-Y') !!}</small></h1>
				@endif
			</div> 
			<form action="/food/daily-client" class="form-horizontal" method="post">
			{!! csrf_field() !!}
				<fieldset>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Day Hospital (NDP)</label>
					<div class="controls">
					  <input name="dhndp" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <input name="date" class="input-xlarge focused" id="focusedInput" type="hidden" value="{!! $date !!}">
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Day Hospital (DP)</label>
					<div class="controls">
					  <input name="dhdp" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of catering meal (Lunch)</label>
					<div class="controls">
					  <input name="ncml" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of catering meal (Dinner)</label>
					<div class="controls">
					  <input name="ncmd" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of staff meal (Lunch)</label>
					<div class="controls">
					  <input name="nsml" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of staff meal (Dinner)</label>
					<div class="controls">
					  <input name="nsmd" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Nondiabetic Patient (Regular)</label>
					<div class="controls">
					  <input name="nndpr" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Nondiabetic Patient (Low cost)</label>
					<div class="controls">
					  <input name="nndpl" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Diabetic Patient (Regular)</label>
					<div class="controls">
					  <input name="ndpr" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Diabetic Patient (Low cost)</label>
					<div class="controls">
					  <input name="ndpl" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of guest meal</label>
					<div class="controls">
					  <input name="ngm" class="input-xlarge focused" id="focusedInput" type="text" value="{!! Request::old('foodgroupname') !!}" placeholder="0">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Total Client</label>
					<div class="controls">
					  <input name="tc" class="input-xlarge focused" id="focusedInput" type="text" value="0" readonly="">
					</div>
				  </div>				  
				  <div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="/food/food-chart" class="btn">Cancel</a>
				  </div>
				</fieldset>
			  </form>		
		</div>
		@endif
	</div><!--/span-->

</div><!--/row-->
@endsection

@section('essentialScript')
<script>
    $(document).ready(function(){ 
        $("input").on("input",function(){
        	if(isNaN(this.value)){
        		alert('You Must Enter a Valid Number');
        		$(this).val('');
        	}
            calculateSum();
        }); 
    });
 
    function calculateSum() { 		
        var sum = 0;
        var counter = 1;
        //iterate through each textboxes and add the values
        $(".input-xlarge.focused").each(function() {
 			if(counter==12)return false;
            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                sum = sum + parseInt(this.value);                                
            }
            counter++;
 
        });
        $("[name=tc]").val(sum);            
    }
</script>
@endsection