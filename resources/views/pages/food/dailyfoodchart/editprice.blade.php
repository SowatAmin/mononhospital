@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a href="/food/show-calendar-market">Calendar</a>
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a href="{!! URL::previous() !!}">Show price chart</a>
		<i class="icon-angle-right"></i>
	</li>
	<li>
		<a>Edit Info</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Price</h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
		</div>
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		<div class="box-content">
			<form action="/food/update-market-price" class="form-horizontal" method="post">
			{!! csrf_field() !!}
				<fieldset>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Food Name</label>
					<div class="controls">
					  <input name="food" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $info->item_name !!}" readonly="">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Amount</label>
					<div class="controls">
					  <input name="amount" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $info->quantity !!}" readonly="">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Price BDT</label>
					<div class="controls">
					  <input name="price" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $info->price !!}">
					</div>
				  </div>
				  <input name="id" type="hidden" value="{!! $info->id !!}">				  
				  <div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="{!! URL::previous() !!}" class="btn">Cancel</a>
				  </div>
				</fieldset>
			  </form>		
		</div>
	</div><!--/span-->

</div><!--/row-->
@endsection

@section('essentialScript')
<script>
$(document).ready(function() {    
	$(".alert.alert-success").fadeOut(3000);
});
</script>
@endsection