@extends('layouts.default')
@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li>		
		<a href="/food/pick-date-for-market-list">Pick a date</a> 
		<i class="icon-angle-right"></i>
	</li>	
	<li>
		<a>Create Food Price Chart</a>		
	</li>
</ul>
@endsection
@section('content')
<div class="row-fluid sortable">
  	  
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon list"></i><span class="break"></span>Create Food Price Chart for {!! date('d-M-Y',strtotime($date)) !!} ({!! date('l',strtotime($date)) !!}) &nbsp; &nbsp;<a href="/food/price-chart-report/{!! $date !!}" data-rel="tooltip" data-original-title="download pdf">Download the price chart</a></h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
		</div>
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		@if (session('alert'))
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('alert') }}
		    </div>
		@endif
		@if($exist)
				<div class="row-fluid">
			  	  <div class="span12">
			  		<h2 style="color:red">Alert!! You already have created today's price chart</h2>		  		
			  		<div class="tooltip-demo well">
			  		  <p class="muted" style="margin-bottom: 0;">Dear user, you already have created today's price chart. So, you don't need to create it again. To change your today's price chart, you have to <a href="#" data-rel="tooltip" data-original-title="update price chart">click here.</a><br><br>Thank you!
			  		  </p>
			  		</div>                                  
			  	  </div>
			    </div>
			@else
		<div class="box-content">
			<form action="/food/daily-market-price" method="post">
			<input type="hidden" name="date" value="{!! $date !!}">
			{!! csrf_field() !!}
				<table class="table table-bordered">
				  <thead>
					  <tr>
						  <th>Name of Item</th>
						  <th>Qty</th>
						  <th>Price</th>					  
					  </tr>
				  </thead>   
				  <tbody>
				  @foreach($food as $index => $item)
				  @if($index==0)
				  	<?php continue; ?>
				  @endif
					<tr>
						<input type="hidden" name="foodname[]" value="{!! $item !!}">
						<td>{!! $item !!}</td>
						<td class="center">
							<input style="width:auto" name="qty[]" class="multiply" value="{!! $foodamount[$index] !!} gm ({!! $foodamount[$index]/1000 !!} kg)" type="text" readonly="">			
						</td>
						<td class="center">
							<input style="width:60px" name="price[]" class="multiply" id="price_{!! $index !!}" type="text" placeholder="0" > Tk
						</td>					
					</tr>				
				  @endforeach		
				  </tbody>
			  	</table>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="/home" class="btn">Cancel</a>
				</div>
		  </form>
		</div>
		@endif
	</div><!--/span-->
</div><!--/row-->
@endsection

@section('essentialScript')
<script type="text/javascript">
$(document).ready(function(){
    $("input").on("input",function(){
        var check = $(this).val();        

    	if(isNaN(check)){
    		alert('You Must Enter a Valid Number');
    		$(this).val('');
    	}    	
    });
});
</script>
@endsection