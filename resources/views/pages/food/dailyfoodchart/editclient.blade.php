@extends('layouts.default')
@section('path')
@endsection
@section('content')
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Client</h2>
			<div class="box-icon">
				<!-- <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> -->
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<!-- <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> -->
			</div>
		</div>
		@if (count($errors) > 0)
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <ul>		            
		            <li>{{ $errors->first() }}</li>		            
		        </ul>
		    </div>
		@endif		
		@if (session('status'))
		    <div class="alert alert-success">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('status') }}
		    </div>
		@endif
		@if (session('alert'))
		    <div class="alert alert-error">
		    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session('alert') }}
		    </div>
		@endif
		<div class="box-content">
			<div class="page-header">
				<h1><small>{!! date('l',strtotime($editclientinfo->date)) !!}, {!! date_format(date_create($editclientinfo->date),'d-M-Y') !!}</small></h1>
			</div> 
			<form action="/food/update-client" class="form-horizontal" method="post">
			{!! csrf_field() !!}
				<fieldset>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Day Hospital (NDP)</label>
					<div class="controls">
						@if($editclientinfo->dh_ndp==0)
						<input name="dhndp" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="dhndp" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->dh_ndp !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <input name="id" type="hidden" value="{!! $editclientinfo->id !!}">				  
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Day Hospital (DP)</label>
					<div class="controls">
						@if($editclientinfo->dh_dp==0)
						<input name="dhdp" class="input-xlarge focused" id="focusedInput" type="text"placeholder="0">
						@else
						<input name="dhdp" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->dh_dp !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of catering meal (Lunch)</label>
					<div class="controls">
						@if($editclientinfo->number_of_catering_meal_lunch==0)
						<input name="ncml" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="ncml" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_catering_meal_lunch !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of catering meal (Dinner)</label>
					<div class="controls">
						@if($editclientinfo->number_of_catering_meal_dinner==0)
						<input name="ncmd" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="ncmd" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_catering_meal_dinner !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of staff meal (Lunch)</label>
					<div class="controls">
						@if($editclientinfo->number_of_staff_meal_lunch==0)
						<input name="nsml" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="nsml" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_staff_meal_lunch !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of staff meal (Dinner)</label>
					<div class="controls">
						@if($editclientinfo->number_of_staff_meal_dinner==0)
						<input name="nsmd" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_staff_meal_dinner !!}" placeholder="0">
						@else
						<input name="nsmd" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_staff_meal_dinner !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Nondiabetic Patient (Regular)</label>
					<div class="controls">
						@if($editclientinfo->number_of_ndp_regular==0)
						<input name="nndpr" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="nndpr" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_ndp_regular !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Nondiabetic Patient (Low cost)</label>
					<div class="controls">
						@if($editclientinfo->number_of_ndp_low_cost==0)
						<input name="nndpl" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="nndpl" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_ndp_low_cost !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Diabetic Patient (Regular)</label>
					<div class="controls">
						@if($editclientinfo->number_of_dp_regular==0)
						<input name="ndpr" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="ndpr" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_dp_regular !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of Diabetic Patient (Low cost)</label>
					<div class="controls">
						@if($editclientinfo->number_of_dp_low_cost==0)
						<input name="ndpl" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="ndpl" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_dp_low_cost !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Number of guest meal</label>
					<div class="controls">
						@if($editclientinfo->number_of_guest_meal==0)
						<input name="ngm" class="input-xlarge focused" id="focusedInput" type="text" placeholder="0">
						@else
						<input name="ngm" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->number_of_guest_meal !!}" placeholder="0">
						@endif					  
					</div>
				  </div>
				  <input name="date" class="input-xlarge focused" id="focusedInput" type="hidden" value="{!! $editclientinfo->date !!}">
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Total Client</label>
					<div class="controls">
					  <input name="tc" class="input-xlarge focused" id="focusedInput" type="text" value="{!! $editclientinfo->total_client !!}" readonly="">
					</div>
				  </div>				  
				  <div class="form-actions">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="/food/food-chart" class="btn">Cancel</a>
				  </div>
				</fieldset>
			  </form>		
		</div>
	</div><!--/span-->

</div><!--/row-->
@endsection

@section('essentialScript')
<script>
    $(document).ready(function(){ 
        $("input").on("input",function(){
        	if(isNaN(this.value)){
        		alert('You Must Enter a Valid Number');
        		$(this).val('');
        	}
            calculateSum();
        }); 
    });
 
    function calculateSum() { 		
        var sum = 0;
        var counter = 1;
        //iterate through each textboxes and add the values
        $(".input-xlarge.focused").each(function() {
 			if(counter==12)return false;
            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                sum = sum + parseInt(this.value);                                
            }
            counter++;
 
        });
        $("[name=tc]").val(sum);            
    }
</script>
@endsection