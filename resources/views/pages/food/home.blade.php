@extends('layouts.default')

@section('path')
<ul class="breadcrumb">
	<li>
		<i class="icon-home"></i>
		<a href="/home">Home</a> 
		<i class="icon-angle-right"></i>
	</li>
	<li><a href="#">Dashboard</a></li>
</ul>
@endsection

@section('content')
<div class="row-fluid">	

	<a href="/food/date-picker" class="quick-button metro yellow 

span2">
		<i class="icon-edit"></i>
		<p>Create Food Chart</p>		
	</a>	

	<a href="/food/show-calendar" class="quick-button metro blue 

span2">
		<i class="icon-list"></i>
		<p>Show Food Chart</p>		
	</a>

	<a href="/food/pick-date-for-market-list" class="quick-button metro green 
	
	span2">
		<i class="icon-edit"></i>
		<p>Create Market List</p>		
	</a>
	
	<a href="/food/show-calendar-market" class="quick-button metro red 
	
	span2">
		<i class="icon-list"></i>
		<p>Show Market List</p>		
	</a>
		
	<div class="clearfix"></div>
					
</div><!--/row-->
@endsection