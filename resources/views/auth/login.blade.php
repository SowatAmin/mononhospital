@extends('layouts.signIn')
@section('content')
	<div class="container-fluid-full">
	<div class="row-fluid">
				
		<div class="row-fluid">
			@if ($errors->has())
			    <div class="alert alert-danger">
			        <strong>Whoops!</strong> There were some problems with your input.<br><br>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<div class="login-box">
				<div class="icons">
					Monon Psychiatric Hospital
				</div>
				<h2>Login to your account</h2>
				<form class="form-horizontal" action="/auth/login" method="post">
				{!! csrf_field() !!}
					<fieldset>
						
						<div class="input-prepend" title="Username">
							<span class="add-on"><i class="halflings-icon user"></i></span>
							<input class="input-large span10" name="email" id="username" type="text" placeholder="type email"/>
						</div>
						<div class="clearfix"></div>

						<div class="input-prepend" title="Password">
							<span class="add-on"><i class="halflings-icon lock"></i></span>
							<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
						</div>
						<div class="clearfix"></div>
						
						<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>

						<div class="button-login">	
							<button type="submit" class="btn btn-primary">Login</button>
						</div>
						<div class="clearfix"></div>
				</form>
				<hr>
				<h3>Forgot Password?</h3>
				<p>
					No problem, <a href="#">click here</a> to get a new password.
				</p>	
			</div><!--/span-->
		</div><!--/row-->
		

</div><!--/.fluid-container-->

	</div><!--/fluid-row-->
@endsection