<!-- start: Meta -->
<meta charset="utf-8">
<title>Monon Psychiatric Hospital</title>
<meta name="description" content="Monon Psychiatric Hospital Management System">
<meta name="author" content="Webpers">
<meta name="keyword" content="">
<!-- end: Meta -->

<!-- start: Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- end: Mobile Specific -->

<!-- start: CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
{!! HTML::style('css/bootstrap.min.css', ['id'=>'bootstrap-style']) !!}

{!! HTML::style('css/bootstrap-responsive.min.css') !!}
{!! HTML::style('css/style.css', ['id'=>'base-style']) !!}
{!! HTML::style('css/style-responsive.css', ['id'=>'base-style-responsive']) !!}
{!! HTML::style('css/font.css') !!}



<!-- {!! HTML::style('css/formemp.css') !!} -->


<!-- end: CSS -->

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

<!--[if IE 9]>
	<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
	
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
	<!-- end: Favicon -->

	<style type="text/css">
		input.form-control{
			height: auto;
		}
		.input-group-addon {
			min-width: 165px;
			text-align:left;
		}

		.form-horizontal .control-label{
			/* text-align:right; */
			text-align:left;
			width: auto;
		}
	</style>