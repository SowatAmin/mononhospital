<!-- start: Main Menu -->
<div id="sidebar-left" class="span2">
	<div class="nav-collapse sidebar-nav">
		@if(Auth::user()->role==1)
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li>
				<a href="/home">
					<i class="icon-home"></i>
					<span class="hidden-tablet"> Home</span>
				</a>
			</li>	
			<!-- <li><a href="#"><i class="icon-list-alt"></i><span class="hidden-tablet"> Journal</span></a></li>
			<li><a href="#"><i class="icon-briefcase"></i><span class="hidden-tablet"> Purchase Info</span></a></li>
			<li><a href="#"><i class="icon-tags"></i><span class="hidden-tablet"> Sales</span></a></li>
			<li><a href="/home/add-employee"><i class="icon-group"></i><span class="hidden-tablet"> Add Employee</span></a></li>			
			<li><a href="#"><i class="icon-gift"></i><span class="hidden-tablet"> Salary</span></a></li> -->
			<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Manage User</span></a>
				<ul>
					<li><a class="submenu" href="/users/create"><i class="icon-file-alt"></i><span class="hidden-tablet"> Add New User</span></a></li>
					<li><a class="submenu" href="/users"><i class="icon-file-alt"></i><span class="hidden-tablet"> Show all Users</span></a></li>						
				</ul>	
			</li>
			<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Manage Food Section</span></a>
				<ul>
					<li><a class="submenu" href="/foodadminpart/food-group"><i class="icon-file-alt"></i><span class="hidden-tablet"> Food Group Management</span></a></li>
					<li><a class="submenu" href="/foodadminpart/food-item"><i class="icon-file-alt"></i><span class="hidden-tablet"> Food Item Management</span></a></li>						
				</ul>
			</li>
			<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Manage Services</span></a>
				<ul>
					<li><a class="submenu" href="/servicesPart/masterCategory"><i class="icon-file-alt"></i><span class="hidden-tablet">Manage Master Category</span></a></li>
					<li><a class="submenu" href="/servicesPart/services"><i class="icon-file-alt"></i><span class="hidden-tablet">Manage Services</span></a></li>					
				</ul>	
			</li>
			<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Manage Doctors</span></a>
				<ul>
					<li><a class="submenu" href="/doctorsAdmin/create"><i class="icon-file-alt"></i><span class="hidden-tablet"> Add New Doctor</span></a></li>
					<li><a class="submenu" href="/doctorsAdmin"><i class="icon-file-alt"></i><span class="hidden-tablet"> Show all Doctors</span></a></li>						
				</ul>	
			</li>
			<li>
				<a href="/voucher/create">
					<i class="icon-file-alt"></i>
					<span class="hidden-tablet">Existing Account Voucher</span>
				</a>
			</li>



			
		</ul>
		@elseif(Auth::user()->role==2)
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li><a href="/home"><i class="icon-home"></i><span class="hidden-tablet"> Home</span></a></li>
			
			<!-- <li><a href="/food/food-group"><i class="icon-book"></i><span class="hidden-tablet">Food Group</span></a></li> -->						
		</ul>
		@elseif(Auth::user()->role==3)
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li>
				<a href="/home">
					<i class="icon-home"></i>
					<span class="hidden-tablet"> Home</span>
				</a>
			</li>	
			<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">O.P.D.</span></a>
				<ul>
					<li><a class="submenu" href="/opd"><i class="icon-file-alt"></i><span class="hidden-tablet"> New O.P.D.</span></a></li>
					<li><a class="submenu" href="/opd/create"><i class="icon-file-alt"></i><span class="hidden-tablet"> Existing O.P.D.</span></a></li>						
					<li><a class="submenu" href="/opdReportShow"><i class="icon-file-alt"></i><span class="hidden-tablet"> O.P.D Report</span></a></li>						
				</ul>	
			</li>
			<li>
				<a class="dropmenu" href="/patientAdmission"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Patient Info</span></a>
				<ul>
					<li><a class="submenu" href="/patientAdmission"><i class="icon-file-alt"></i><span class="hidden-tablet">New Patient</span></a></li>
					<li><a class="submenu" href="/patientAdmission/create"><i class="icon-file-alt"></i><span class="hidden-tablet">Existing Patient</span></a></li>
				</ul>	
			</li>


			<li>
				<a href="/reports">
					<i class="icon-file-alt"></i>
					<span class="hidden-tablet">Report</span>
				</a>
			</li>

			<li>
				<a href="/bed">
					<i class="icon-file-alt"></i>
					<span class="hidden-tablet"> Bed Cabin List</span>
				</a>
			</li>
			




		</ul>
		@elseif(Auth::user()->role==4)
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li>
				<a href="/home">
					<i class="icon-home"></i>
					<span class="hidden-tablet"> Home</span>
				</a>
			</li>	
			<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Voucher</span></a>
				<ul>
					<li><a class="submenu" href="/voucher"><i class="icon-file-alt"></i><span class="hidden-tablet"> New Voucher</span></a></li>
					<li><a class="submenu" href="/ViewVoucher"><i class="icon-file-alt"></i><span class="hidden-tablet"> Existing Voucher</span></a></li>
					<li><a class="submenu" href="/VoucherReport"><i class="icon-file-alt"></i><span class="hidden-tablet"> Voucher Report</span></a></li>						

				</ul>	
			</li>

			<li>
				<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Journal</span></a>
				<ul>
					<li><a class="submenu" href="/journal"><i class="icon-file-alt"></i><span class="hidden-tablet"> New Journal</span></a></li>
					<li><a class="submenu" href="/journal/show"><i class="icon-file-alt"></i><span class="hidden-tablet"> Existing Journal</span></a></li>
					

				</ul>	
			</li>


			<li>
				<a class="dropmenu" href="/patientAdmission"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Employee</span></a>
				<ul>
					<li><a class="submenu" href="/employee"><i class="icon-file-alt"></i><span class="hidden-tablet">New Employee</span></a></li>
					<li><a class="submenu" href="/employee/create"><i class="icon-file-alt"></i><span class="hidden-tablet">Existing Employee</span></a></li>
				</ul>	
			</li>

			
			<li>
				<a href="/salary/index">
					<i class="icon-file-alt"></i>
					<span class="hidden-tablet">Salary</span>
				</a>
			</li>


			<li>
				<a href="/accountsReports/homepage">
					<i class="icon-file-alt"></i>
					<span class="hidden-tablet">Reports</span>
				</a>
			</li>


		</ul>
		@endif
	</div>
</div>
<!-- end: Main Menu -->