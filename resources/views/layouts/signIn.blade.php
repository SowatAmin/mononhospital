<!DOCTYPE html>
<html lang="en">
  <head>
  @include('includes.head')  
  <style type="text/css">
  	body { background: url(img/bg-login.jpg) !important; }
  </style>
  </head>

  <body>
  @yield('content')
  </body>
</html>