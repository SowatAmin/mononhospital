<!DOCTYPE html>
<html lang="en">
<head>
@include('includes.head')
</head>

<body>
	@include('includes.header')
	<div class="container-fluid-full">
		<div class="row-fluid">
			@include('includes.mainmenu')
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			<!-- start: Content -->
			<div id="content" class="span10">
				@yield('path')
				@yield('content')
			</div><!--/#content.span10-->
			<div class="clearfix"></div>
		</div><!--/.row-fluid-->
	</div><!--/.container-fluid-full-->
	@include('includes.javascript')
	@include('includes.footer')
</body>

</html>