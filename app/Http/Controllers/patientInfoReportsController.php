<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Illuminate\Support\Facades\Auth;

use Illuminate\html;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PatientInfo;
use App\Models\VisitorInfo;
use App\Models\Prescription;
use App\Models\Investigation;
use App\Models\Bill;
use App\Models\serviceMasterCategory;
use App\Models\serviceCategory;
use App\Models\patientPaymentHistory;
use App\Models\patientDiscount;
use Carbon\Carbon;

class patientInfoReportsController extends Controller
{
    public function getIndex(){
        //return 'hello';
    }

    public function getPatientInfoReport($id){
        if($id){
            if(is_numeric($id)){
                $counter = patientInfo::where('id',$id)->count();

                if($counter == 0){
                    //need to add alert to patientAdmission homepage to use this
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }
                
                $patient = patientInfo::where('id',$id)->get()->last();

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }
                

                $visitors = VisitorInfo::where('pid',$id)->get();

                $prescription = Prescription::where('id',$id)->get()->last();

                if(!$prescription){
                    $prescription = new Prescription;
                }

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.admissionForm',[
                    'patient' => $patient,
                    'prescription' => $prescription,
                    'visitors' => $visitors
                    ]);
                return $pdf->stream($id.'_Admission_Form.pdf');
            }
            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->with('alert',$alert);
            }
        }
        else{
            $alert = "Cannot search for empty data, you may not have saved the data before viewing, please check again if you have input the correct Reg No.";
            return back()->with('alert',$alert);
        }
    }




    public function getPatientSummaryBill($id){

        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                //return view('pages.administration.patientAdmission.reports.summaryBill');
                $patient = patientInfo::where('id',$id)->get()->last();                

                $masterCategories = Bill::selectRaw('masterCategory')
                ->where('regNo', $id)
                ->groupBy('masterCategory')
                ->orderBy('masterCategory', 'asc')
                ->get();



                $serviceCategories = Bill::selectRaw('masterCategory, category')
                ->where('regNo', $id)
                ->groupBy('category')
                ->orderBy('category', 'asc')
                ->get();



                $bills = Bill::selectRaw('masterCategory, category, date, services, sum(quantity) as sumOfQuantity, rate, vat, serviceCharge')
                ->where('regNo', $id)
                ->orderBy('date', 'asc')
                ->groupBy('services')                
                ->get();

                foreach ($bills as $bill){
                    $lastDate = Bill::selectRaw('regNo, services, date')->where('services', $bill->services)->where('regNo', $id)->orderBy('date','asc')->get()->last();
                    //echo $lastDate."\xA\xA";

                    //echo $bill->services." = ".$bill->date."\xA\xA";
                    //echo $lastDate->services." = ".$lastDate->date."\xA\xA";
                    $bill->date = $lastDate->date;
                    $bill->date = date("d-m-Y", strtotime($bill->date));
                    //echo $lastDate->services." = ".$bill->date." | ".$lastDate->date."\xA"."\xA";
                }

                //return $bills;

                $rates = Bill::selectRaw('masterCategory, category, date, services, quantity, rate, vat, serviceCharge')
                ->where('regNo', $id)               
                ->get();

                $total = 0;

                foreach ($masterCategories as $masterCategory){
                    $masterCategory->total = 0;
                    foreach ($bills as $bill) {
                        if($bill->masterCategory == $masterCategory->masterCategory){                            
                            $bill->total = 0;

                            
                            foreach ($rates as $index => $rate) {
                                if($bill->services == $rate->services){
                                    $temp = $rate->rate * $rate->quantity;
                                    $vat = ($rate->vat * $rate->rate) / 100;
                                    $sc = ($rate->serviceCharge * $rate->rate) / 100;
                                    $bill->total = $bill->total + $temp + $vat + $sc;
                                    //echo ($index+1).". ".$rate->services." ".$temp." ".$vat." ".$sc." ".$bill->total." "."\xA";//.$total."\xA";
                                }
                            }
                            //echo "\xA";
                            $masterCategory->total = $masterCategory->total + $bill->total;

                            
                            //echo $masterCategory->total."\xA"."\xA";
                        }                        
                    } 
                    $total = $total + $masterCategory->total;
                }

                //return $masterCategories;
                

                $payment = patientPaymentHistory::selectRaw('sum(payment) totalPayment')
                ->where('regNo', $id)
                ->get()
                ->last();

                if(!$payment->totalPayment){
                    $payment = 0;
                }
                else{
                    $payment = $payment->totalPayment;
                }

                $discount = patientDiscount::selectRaw('sum(amount) totalAmount')
                ->where('regNo', $id)
                ->get()
                ->last();

                if(!$discount->totalAmount){
                    $discount = 0;
                }
                else{
                    $discount = $discount->totalAmount;
                }

                $due = $total - $discount - $payment;

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }

                //return view('pages.administration.patientAdmission.reports.summaryBill');

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.summaryBill',[
                    'patient' => $patient,
                    'masterCategories' => $masterCategories,
                    'serviceCategories' => $serviceCategories,
                    'bills' => $bills,
                    'payment' => $payment,
                    'discount' => $discount,
                    'total' => $total,
                    'due' => $due
                    ]);

                return $pdf->stream($id.'_Summary_Bill.pdf');
            }
            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }



    public function getPatientDetailedBill($id){
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                //return view('pages.administration.patientAdmission.reports.summaryBill');
                $patient = patientInfo::where('id',$id)->get()->last();

                $masterCategories = Bill::selectRaw('masterCategory')
                ->groupBy('masterCategory')
                ->orderBy('masterCategory', 'asc')
                ->where('regNo', $id)
                ->get();

                $bills = Bill::orderBy('date', 'asc')
                ->where('regNo', $id)
                ->get();

                foreach($masterCategories as $masterCategory) {
                    $masterCategory->total = 0;
                    foreach ($bills as $bill) {
                        if($bill->masterCategory == $masterCategory->masterCategory){
                            $temp = $bill->rate * $bill->quantity;
                            $vat = (($bill->vat)*$temp)/100;
                            $sc = (($bill->serviceCharge)*$temp)/100;
                            $masterCategory->total = $masterCategory->total + $temp + $vat + $sc;
                        }
                    }
                }

                //return $bills;

                $total = 0;
                $totalRate = 0;
                $totalVatAmt = 0;
                $totalServiceAmt = 0;

                foreach($bills as $bill) {
                    $bill->date = date("d-m-Y", strtotime($bill->date));

                    $temp = $bill->rate *  $bill->quantity;
                    $totalRate = $totalRate + $temp;
                    $totalVatAmt = $totalVatAmt + ($bill->vat)*$temp/100;
                    $totalServiceAmt = $totalServiceAmt + ($bill->serviceCharge)*$temp/100;

                    $temp = $temp + ($bill->vat)*$temp/100 + ($bill->serviceCharge)*$temp/100;
                    $total = $total + $temp;
                }

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.detailedBill',[
                    'patient' => $patient,
                    'masterCategories' => $masterCategories,
                    'bills' => $bills,
                    'totalRate' => $totalRate,
                    'totalVatAmt' => $totalVatAmt,
                    'totalServiceAmt' => $totalServiceAmt,
                    'total' => $total
                    //add payment
                    //add discount
                    ]);

                return $pdf->stream($id.'_Detailed_Bill.pdf');
            }

            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }





    public function getPatientHospitalBill($id){
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                //return view('pages.administration.patientAdmission.reports.summaryBill');
                $patient = patientInfo::where('id',$id)->get()->last();

                $masterCategories = Bill::selectRaw('masterCategory')
                ->where('regNo', $id)
                ->where('masterCategory', 'Bed & Cabin Services')
                ->orWhere('masterCategory', 'Hospital Services')
                ->where('regNo', $id)
                ->groupBy('masterCategory')
                ->orderBy('masterCategory', 'asc')
                ->get();

                //return $masterCategories;

                $serviceCategories = Bill::selectRaw('regNo,masterCategory, category,rate')                                
                ->where('regNo', $id)
                ->where('masterCategory', 'Bed & Cabin Services')
                ->orWhere('masterCategory', 'Hospital Services')
                ->where('regNo', $id)
                ->groupBy('category')
                ->orderBy('category', 'asc')
                ->get();

                //return $serviceCategories;

                $bills = Bill::selectRaw('regNo, masterCategory, category, date, services, sum(quantity) as sumOfQuantity, rate, vat, serviceCharge')
                ->where('regNo', $id)
                ->groupBy('services')
                ->get();

                foreach ($bills as $bill){
                    $lastDate = Bill::selectRaw('regNo, services, date')->where('services', $bill->services)->where('regNo', $id)->orderBy('date','desc')->get()->first();
                    $bill->date = $lastDate->date;
                    $bill->date = date("d-m-Y", strtotime($bill->date));
                }

                $rates = Bill::selectRaw('regNo, masterCategory, services, rate, quantity, vat, serviceCharge')
                ->where('regNo', $id)
                ->where('masterCategory', 'Bed & Cabin Services')
                ->orWhere('masterCategory', 'Hospital Services')
                ->where('regNo', $id)
                ->get();

                $total = 0;

                foreach ($bills as $index => $bill) {
                    $bill->total = 0;
                    foreach ($rates as $rate) {
                        if ($bill->services == $rate->services){
                            $temp = $rate->rate * $rate->quantity;
                            $vat = (($rate->vat) * $temp)/100;
                            $sc = (($rate->serviceCharge) * $temp)/100;
                            $bill->total = $bill->total + $temp + $vat + $sc;
                            $total = $total + $temp + $vat +$sc;
                            //echo $rate->services." ".$temp." ".$rate->quantity." ".$vat." ".$sc." "."\xA"."\xA";
                        }

                    }
                    
                    //echo $bill->total." ".$total."\xA"."\xA";
                }
                


                //return $bills;

                $discount = patientDiscount::selectRaw('sum(amount) totalAmount')
                ->where('regNo', 2)
                ->where('refBy', 'Hospital')
                ->get()
                ->last();

                if(!$discount->totalAmount){
                    $discount = 0;
                }
                else{
                    $discount = $discount->totalAmount;
                }

                $due = $total - $discount;

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.hospitalBill',[
                    'patient' => $patient,
                    'masterCategories' => $masterCategories,
                    'serviceCategories' => $serviceCategories,
                    'bills' => $bills,
                    'due' => $due,
                    'discount' => $discount,
                    'total' => $total

                    ])->setOrientation('landscape');

                return $pdf->stream($id.'_'.$patient->name.'_Hospital_Bill.pdf');
            }

            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }



    public function getPatientConsultantBill($id){
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                //return view('pages.administration.patientAdmission.reports.summaryBill');
                $patient = patientInfo::where('id',$id)->get()->last();

                $masterCategories = Bill::selectRaw('masterCategory')
                ->where('regNo', $id)
                ->groupBy('masterCategory')
                ->orderBy('masterCategory', 'asc')
                ->get();

                $serviceCategories = Bill::selectRaw('masterCategory, category')
                ->where('regNo', $id)
                ->where('masterCategory', 'Consultant Services')
                ->groupBy('category')
                ->orderBy('category', 'asc')
                ->get();

                $bills = Bill::selectRaw('masterCategory, category, date, services, sum(quantity) as sumOfQuantity, rate, vat, serviceCharge')
                ->where('masterCategory', 'Consultant Services')
                ->where('regNo', $id)
                ->groupBy('services')
                ->orderBy('date', 'asc')
                ->get();

                foreach ($bills as $bill){
                    $lastDate = Bill::selectRaw('regNo, services, date')->where('services', $bill->services)->where('regNo', $id)->orderBy('date','desc')->get()->first();
                    $bill->date = $lastDate->date;
                    $bill->date = date("d-m-Y", strtotime($bill->date));
                }

                $rates = Bill::where('masterCategory', 'Consultant Services')
                ->where('regNo', $id)
                ->get();

                $total = 0;

                foreach ($bills as $bill){
                    $bill->total = 0;
                    foreach ($rates as $rate){
                        if ($bill->services == $rate->services) {
                            $temp = $rate->rate * $rate->quantity;
                            $vat = (($rate->vat) * $temp)/100;
                            $sc = (($rate->serviceCharge) * $temp)/100;
                            $bill->total = $bill->total + $temp + $vat + $sc;
                        }
                    }
                    $total= $total + $bill->total;
                }
                

                $discount = patientDiscount::selectRaw('sum(amount) totalAmount')
                ->where('regNo', $id)
                ->whereNotIn('refBy', ['Hospital'])
                ->get()
                ->last();

                if(!$discount->totalAmount){
                    $discount = 0;
                }
                else{
                    $discount = $discount->totalAmount;
                }

                $due = $total - $discount;

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.consultantBill',[
                    'patient' => $patient,
                    'masterCategories' => $masterCategories,
                    'serviceCategories' => $serviceCategories,
                    'bills' => $bills,
                    'due' => $due,
                    'discount' => $discount,
                    'total' => $total
                    ])->setOrientation('landscape');

                return $pdf->stream($id.'_'.$patient->name.'_Consultant_Bill.pdf');
            }

            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }



    public function getPatientNonProfitBill($id){
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                //return view('pages.administration.patientAdmission.reports.summaryBill');
                $patient = patientInfo::where('id',$id)->get()->last();

                $masterCategories = Bill::selectRaw('masterCategory')
                ->groupBy('masterCategory')
                ->orderBy('masterCategory', 'asc')
                ->where('regNo', $id)
                ->get();

                $serviceCategories = Bill::selectRaw('masterCategory, category')
                ->groupBy('category')
                ->orderBy('category', 'asc')                
                ->where('masterCategory', 'Non Profitable Services')
                ->where('regNo', $id)
                ->get();

                $bills = Bill::selectRaw('masterCategory, category, date, services, sum(quantity) as sumOfQuantity, rate, vat, serviceCharge')
                ->groupBy('services')
                ->orderBy('date', 'asc')
                ->where('masterCategory', 'Non Profitable Services')
                ->where('regNo', $id)
                ->get();

                foreach ($bills as $bill){
                    $lastDate = Bill::selectRaw('regNo, services, date')->where('services', $bill->services)->where('regNo', $id)->orderBy('date','desc')->get()->first();
                    $bill->date = $lastDate->date;
                    $bill->date = date("d-m-Y", strtotime($bill->date));
                }

                $rates = Bill::where('masterCategory', 'Non Profitable Services')
                ->where('regNo', $id)
                ->get();

                $total = 0;

                foreach ($bills as $bill){
                    $bill->total = 0;
                    foreach ($rates as $rate){
                        if ($bill->services == $rate->services) {
                            $temp = $rate->rate * $rate->quantity;
                            $vat = (($rate->vat) * $temp)/100;
                            $sc = (($rate->serviceCharge) * $temp)/100;
                            $bill->total = $bill->total + $temp + $vat + $sc;
                        }
                    }
                    $total= $total + $bill->total;
                }

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.nonProfitBill',[
                    'patient' => $patient,
                    'masterCategories' => $masterCategories,
                    'serviceCategories' => $serviceCategories,
                    'bills' => $bills,
                    'total' => $total
                    ])->setOrientation('landscape');

                return $pdf->stream($id.'_'.$patient->name.'_Non_Profit_Bill.pdf');
            }

            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }

    public function getPatientDorbForm($id){
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }


                $patient = patientInfo::where('id',$id)->get()->last();

                $today = Carbon::today()->toDateString();

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }

                $today = date("d-m-Y", strtotime($today));

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.dorbForm',[
                    'patient' => $patient,
                    'today' => $today
                    ]);

                return $pdf->stream($id.'_DORB_Form.pdf');
            }
            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }



    public function getPatientEmsForm($id){
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                $patient = patientInfo::where('id',$id)->get()->last();

                $today = Carbon::today()->toDateString();

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));

                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }
                
                $today = date("d-m-Y", strtotime($today));

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.emsForm',[
                    'patient' => $patient,
                    'today' => $today
                    ]);

                return $pdf->stream($id.'_DORB_Form.pdf');
            }
            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }




    public function getPatientPaRoleForm($id){
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }
        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                $patient = patientInfo::where('id',$id)->get()->last();

                $today = Carbon::today()->toDateString();

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }
                $today = date("d-m-Y", strtotime($today));

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.paRoleForm',[
                    'patient' => $patient,
                    'today' => $today
                    ]);

                return $pdf->stream($id.'_DORB_Form.pdf');
            }
            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }

    public function getPatientDischargeCertificate($id){

        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }

        if($id){
            if(is_numeric($id)){
                $counter = Bill::where('regNo',$id)->count();

                if($counter == 0){
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }

                //return view('pages.administration.patientAdmission.reports.dischargeCertificate');

                $patient = patientInfo::where('id',$id)->get()->last();
                $prescription = Prescription::where('id',$id)->get()->last();

                if(!$prescription){
                    $prescription = new Prescription;
                }

                $patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));
                if ($patient->bookingDate != "0000-00-00") {
                    $patient->bookingDate = date("d-m-Y", strtotime($patient->bookingDate));
                }                
                if($patient->releaseDate != "0000-00-00"){
                    $patient->releaseDate = date("d-m-Y", strtotime($patient->releaseDate));    
                }

                $pdf = PDF::loadView('pages.administration.patientAdmission.reports.dischargeCertificate',[
                    'patient' => $patient,
                    'prescription' => $prescription
                    ])->setOrientation('landscape');

                return $pdf->stream($id.'_Discharge_Certificate.pdf');
            }
            else{
                $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
                return back()->withInput()->with('alert',$alert);
            }
        }
        else{
            $alert = "No data for Reg No. = ".$id." please check if you have input the correct Reg No.";
            return back()->withInput()->with('alert',$alert);
        }
    }
}