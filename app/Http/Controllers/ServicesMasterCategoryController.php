<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator, Input, Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\serviceMasterCategory;

class ServicesMasterCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $tableData = serviceMasterCategory::orderBy('id','asc')->get();

        return view('pages.admin.servicesPart.masterCategory.home')->with('tableData', $tableData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        return view('pages.admin.servicesPart.masterCategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request){
        $input = $request->all();

        serviceMasterCategory::create($input);

        return redirect('servicesPart/masterCategory')->with('status','New Master Category has been added successfully.');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        $serviceMasterCategory = serviceMasterCategory::find($id);

        return \View::make('pages.admin.servicesPart.masterCategory.edit')->with('serviceMasterCategory', $serviceMasterCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id){

        $rules = array(
            'name' => 'required',
            );
        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            redirect('pages.admin.servicesPart.masterCategory.edit')
            ->withErrors($validator)
            ->with('status','Edit Not Successful.');
        }
        else {
            // store
            $serviceMasterCategory = serviceMasterCategory::find($id);
            $serviceMasterCategory->name = Input::get('name');
            $serviceMasterCategory->save();

            $tableData = serviceMasterCategory::orderBy('id','asc')->get();
            return redirect()->back()->with('status','Edit successful.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        


        $tableData = serviceMasterCategory::where('id',$id)->get();

        return view('pages.admin.servicesPart.masterCategory.home')->with('tableData', $tableData);
    }
}
