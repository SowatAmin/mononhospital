<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use PDF;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFoodGroupRequest;
use App\Http\Requests\StoreFoodItemRequest;
use App\Http\Requests\StoreDailyChartRequest;
use App\Http\Requests\StoreFoodClientRequest;
use App\Http\Requests\UpdateFoodChartRequest;
use App\Http\Requests\UpdateFoodClientRequest;
use App\Http\Requests\DatePickerRequest;
use App\Http\Requests\UpdatePriceChart;
use Illuminate\Support\Facades\Auth;
use App\Models\AddFoodGroup;
use App\Models\FoodList;
use App\Models\DailyChart;
use App\Models\FoodClient;
use App\Models\FoodMarket;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */    

    //Start Daily food charts method
    public function getFoodChart(){
        return view('pages.food.home');
    }

    public function getCreateFoodChart(){       

        $date = session()->get('pickedDate');

        $day = date("l",strtotime($date));
        $countTodaysClientInput = FoodClient::where('Date',$date)
                                                ->count();
        if($countTodaysClientInput == 0){
            return redirect('food/create-food-client');
        }
        $collectFoodClientInfo = FoodClient::where('Date',$date)
                                                ->get();
        $collectItemOfTheDay = FoodList::where('DayFor',$day)
                                        ->orderBy('CategoryId')
                                        ->get();
        $itemId = array();
        $formulaResult = array();
        
        foreach($collectItemOfTheDay as $data){
            if($data->formula != ""){
                if($data->formula != 'fixed'){                   
                    $result = DB::select('select '. $data->formula .' as calculate from foodclient where date = :date', ['date' => $date]);
                    
                    foreach($result as $find){
                        $formulaResult[] = $find->calculate;
                    }
                }else{
                    $formulaResult[] = 'fixed';
                }                                
                $itemId[] = $data->id;
            }
        }
        return view('pages.food.dailyfoodchart.createchart',['items' => $collectItemOfTheDay, 'clientinfos' => $collectFoodClientInfo, 'autoClientItemId' => $itemId, 'autoClient' => $formulaResult, 'date' => $date, 'day' => $day]);
        //return view('pages.food.dailyfoodchart.createchart',['items' => $collectItemOfTheDay, 'clientinfos' => $collectFoodClientInfo]);
    }

    public function postDailyChart(Request $request){
        
        $foodids = $request->foodid;
        $clientNumbers = $request->client;
        $quantity = $request->qty;
        $howManyTimes = $request->hmt;        

        //check if array input is empty
        $empty = true;        
        foreach($clientNumbers as $clientNumber){
            if($clientNumber!=""){
                $empty = false;
            }
        }        
        if($empty){            
            return redirect('food/create-food-chart')->with('customAlert','Sorry, You do not have any food client!!' );
        }

        //check if Todays Entry is already exist        
        else if(session()->has('pickedDate')){
            $date = session()->get('pickedDate');        
            foreach($foodids as $index => $foodid){

                $dailychart = new DailyChart;            
                $dailychart->food_id = $foodid;
                $dailychart->date = $date;
                $quantityExplode = explode(" ",$quantity[$index]);
                $dailychart->quantity = $quantityExplode[0];                
                $dailychart->how_many_times = $howManyTimes[$index];
                $dailychart->number_of_client = $clientNumbers[$index];
                
                $dailychart->save();            
            }
            session()->forget('pickedDate');
            return redirect('food/show-daily-chart/'.$date)->with('status', 'Your data has been saved successfully!!');            
        }else{
            return redirect('food/create-food-chart')->with('customAlert','Sorry it looks something went to be wrong!!' );
        }
    }

    public function getDeleteChart($date){
        
        FoodClient::where('date',$date)
                    ->delete();

        DailyChart::where('date',$date)
                    ->delete();

        return redirect('food/show-calendar');
    }

    public function getCreateFoodClient($d){
        $date = $d;

        if(strtotime($date)<strtotime(date('Y-m-d'))){
            return redirect('food/date-picker')->with('alert','You can\'t select a date before today!');
        }

        $countTodaysEntry = DailyChart::where('Date',$date)
                                            ->count();

        $countTodaysClientInput = FoodClient::where('Date',$date)
                                                ->count();

        if($countTodaysEntry > 0){            
            return redirect('food/date-picker')->with('alert','Chart has been created already.');
        }
        if($countTodaysClientInput > 0){
            session()->forget('pickedDate');
            session()->put('pickedDate',$date);
            return redirect('food/create-food-chart');
        }        
        $day = date('l',strtotime($date));
        return view('pages.food.dailyfoodchart.createclient',['day' => $day, 'date' => $date ,'existence' => 'false']);
    }

    public function postDailyClient(StoreFoodClientRequest $request){
        
        $foodclient = new FoodClient;

        $date = $request->date;

        $foodclient->date = $date;
        //Non-patient
        $foodclient->dh_ndp = $request->dhndp;
        $foodclient->dh_dp = $request->dhdp;
        $foodclient->number_of_catering_meal_lunch = $request->ncml;
        $foodclient->number_of_catering_meal_dinner = $request->ncmd;
        $foodclient->number_of_staff_meal_lunch = $request->nsml;
        $foodclient->number_of_staff_meal_dinner = $request->nsmd;
        $foodclient->number_of_guest_meal = $request->ngm;
        //Patient
        $foodclient->number_of_ndp_regular = $request->nndpr;
        $foodclient->number_of_ndp_low_cost = $request->nndpl;
        $foodclient->number_of_dp_regular = $request->ndpr;
        $foodclient->number_of_dp_low_cost = $request->ndpl;
        //total        
        $tp = $request->nndpr + $request->nndpl + $request->ndpr + $request->ndpl;
        $tc = $tp+$request->dhndp+$request->dhdp+$request->ncml+$request->ncmd+$request->nsml+$request->nsmd+$request->ngm;
        
        $foodclient->total_patient = $tp;
        $foodclient->total_client = $tc;

        //save
        $foodclient->save();

        session()->forget('pickedDate');
        session()->put('pickedDate',$date);
        return redirect('food/create-food-chart');

    }

    public function getEditClient($id){
        $collectEditClientInfo = FoodClient::find($id);

        return view('pages.food.dailyfoodchart.editclient',['editclientinfo' => $collectEditClientInfo]);
    }

    public function postUpdateClient(UpdateFoodClientRequest $request){
        $foodclient = FoodClient::find($request->id);

        $date = $request->date;
        //Non-patient
        $foodclient->dh_ndp = $request->dhndp;
        $foodclient->dh_dp = $request->dhdp;
        $foodclient->number_of_catering_meal_lunch = $request->ncml;
        $foodclient->number_of_catering_meal_dinner = $request->ncmd;
        $foodclient->number_of_staff_meal_lunch = $request->nsml;
        $foodclient->number_of_staff_meal_dinner = $request->nsmd;
        $foodclient->number_of_guest_meal = $request->ngm;
        //Patient
        $foodclient->number_of_ndp_regular = $request->nndpr;
        $foodclient->number_of_ndp_low_cost = $request->nndpl;
        $foodclient->number_of_dp_regular = $request->ndpr;
        $foodclient->number_of_dp_low_cost = $request->ndpl;
        //total        
        $tp = $request->nndpr + $request->nndpl + $request->ndpr + $request->ndpl;
        $tc = $tp+$request->dhndp+$request->dhdp+$request->ncml+$request->ncmd+$request->nsml+$request->nsmd+$request->ngm;
        
        $foodclient->total_patient = $tp;
        $foodclient->total_client = $tc;

        //save
        $foodclient->save();

        session()->forget('pickedDate');
        session()->put('pickedDate',$date);
        return redirect('food/create-food-chart');

    }

    public function getShowCalendar(){
        return view('pages.food.dailyfoodchart.showcalendar');
    }

    public function getShowDailyChart($date){
        
        if($date==''){
            return redirect('food/show-calendar');
        }
        $currentDate = date('Y-m-d');
        //$compareDate = date ("Y-m-d", strtotime("+2 day", strtotime($currentDate)));
       /* if(strtotime($date)<strtotime(date)){
            return redirect('food/show-calendar')->with('alert','Your selected date is too future!');
        }*/

        $checkDataExistOrNot = DailyChart::where('date',$date)
                                            ->count();

        if($checkDataExistOrNot == 0){
            return redirect('food/show-calendar')->with('alert','No data has been found!');
        }

        $info = DB::table('dailychart')
                    ->join('foodlists', 'dailychart.food_id', '=', 'foodlists.id')
                    ->where('dailychart.date',$date)     
                    ->select('dailychart.*', 'foodlists.NameOfItem', 'foodlists.Quantity', 'foodlists.CategoryId','foodlists.HowManyTimes')
                    ->orderBy('foodlists.CategoryId')
                    ->get();

        $collectFoodClientInfo = FoodClient::where('Date',$date)
                                                ->get();

        return view('pages.food.dailyfoodchart.showfoodchart', ['chartInfos' => $info, 'clientInfos' => $collectFoodClientInfo, 'date' => $date]);
    }

    public function getEditChart($id){
        $info = DailyChart::find($id);
        $foodInfo = FoodList::find($info->food_id);
        return view('pages.food.dailyfoodchart.edit',['info' => $info, 'foodInfo' => $foodInfo, 'id' => $id]);
    }

    public function postEditFoodChart(UpdateFoodChartRequest $request){
        $id = $request->id;
        $update = DailyChart::find($id);
        $update->number_of_client = $request->clientnumber;
        $update->save();

        return redirect('food/show-daily-chart/'.date('Y-m-d'));
    }

    public function getPickDateForMarketList(){
        return view('pages.food.dailyfoodchart.pickadateformarketlist');
    }

    public function getCreateMarketList($d){

        $date = $d;

        if(strtotime($date)<strtotime(date('Y-m-d'))){
            return redirect('food/pick-date-for-market-list')->with('alert','You can\'t select a date before today!');
        }
        
        $checkAlreadyExist = FoodMarket::where('date',$date)
                                        ->count();

        $checkDailyChartExist = DailyChart::where('date',$date)
                                            ->count();

        if($checkDailyChartExist == 0){
            return redirect('food/pick-date-for-market-list')->with('alert','No food chart has been created yet!');
        }

        if($checkAlreadyExist>0){
            return view('pages.food.dailyfoodchart.marketprice',['exist'=>true]);
        }

        $info = DB::table('dailychart')
                    ->join('foodlists','dailychart.food_id','=','foodlists.id')
                    ->where('dailychart.number_of_client','<>','0')
                    ->where('dailychart.Date',$date)
                    ->select('foodlists.CategoryId')
                    ->groupBy('foodlists.CategoryId')
                    ->get();
        $food[] = NULL;
        $foodAmount[] = NULL;        
        foreach($info as $data){
            $foodGroupInfo = AddFoodGroup::find($data->CategoryId);
            $food[] = $foodGroupInfo->NameOfGroup;            
            $calculateQuantitySql = DB::table('dailychart')
                                        ->join('foodlists','dailychart.food_id','=','foodlists.id')
                                        ->where('dailychart.number_of_client','<>','0')
                                        ->where('foodlists.CategoryId',$data->CategoryId)
                                        ->where('dailychart.Date',$date)
                                        ->select('foodlists.*','dailychart.number_of_client')                                        
                                        ->get();
            $foodAmountCount = 0;
            foreach ($calculateQuantitySql as $value) {
                $foodAmountCount+=($value->number_of_client*$value->Quantity*$value->HowManyTimes);       
            }
            $foodAmount[] = $foodAmountCount;
        }

        return view('pages.food.dailyfoodchart.marketprice',['food'=>$food,'foodamount'=>$foodAmount, 'exist'=>false,'date'=>$date]);
    }

    public function postDailyMarketPrice(Request $request){
        $prices = $request->price;
        $foodname = $request->foodname;
        $qty = $request->qty;
        $empty = true;

        foreach($prices as $price){
            if($price!=''){
                $empty = false;
            }
        }

        if($empty){
            return redirect()->back()->with('alert','You didn\'t enter any price!');
        }        

        foreach($foodname as $index => $data){
            $foodmarket = new FoodMarket;            
            $foodmarket->item_name = $data;
            $foodmarket->price = $prices[$index];
            $foodmarket->quantity = $qty[$index];
            $foodmarket->date = date('Y-m-d');

            $foodmarket->save();
        }

        return redirect('food/show-market-list/'.date('Y-m-d'));
    }

    public function getShowCalendarMarket(){
        return view('pages.food.dailyfoodchart.showcalendar2');
    }

    public function getShowMarketList($date){

        if($date==''){
            return redirect('food/show-calendar-market');
        }
        $currentDate = strtotime(date('Y-m-d'));
        if(strtotime($date)<$currentDate){
            return redirect('food/show-calendar-market')->with('alert','You can\'t select a date before today');
        }        

        $checkAlreadyExist = FoodMarket::where('date',$date)
                                        ->count();

        if($checkAlreadyExist==0){
            return redirect('food/show-calendar-market')->with('alert','Sorry, No data found on this day');
        }

        $infos = FoodMarket::where('date',$date)
                            ->get();

        return view('pages.food.dailyfoodchart.showmarketprice',['infos'=>$infos,'date'=>$date]);
    }

    public function getEditPrice($id){
        $info = FoodMarket::find($id);        

        return view('pages.food.dailyfoodchart.editprice',['info'=>$info]);
    }

    public function postUpdateMarketPrice(UpdatePriceChart $request){
        $updatePrice = FoodMarket::find($request->id);
        $updatePrice->price = $request->price;
        $updatePrice->save();

        return redirect('food/show-market-list/'.date('Y-m-d'));
    }

    public function getDatePicker(){
        $date = date('Y-m-d');        
        $dateArray = array();
        $clientExistDate = array();

        for($i=1;$i<=3;$i++){            
            $checkEntryAlreadyExist = DailyChart::where('Date',$date)
                                            ->count();
            $checkClientAlreadyExist = FoodClient::where('date',$date)
                                            ->count();
            if($checkEntryAlreadyExist==0){
                $dateArray[] = date_format(date_create($date),'d-M-Y');
            }
            if($checkClientAlreadyExist > 0){
                $clientExistDate[] = date_format(date_create($date),'d-M-Y');
            }
            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

        //print_r($clientExistDate);

        return view('pages.food.dailyfoodchart.pickadate',['date' => $dateArray, 'clientExist' => $clientExistDate]);
    }
    //End Daily food charts method
    

    //Report Section
    public function getChartReport($date){
        if($date==''){
            return redirect('food/show-calendar');
        }
        /*$currentDate = date('Y-m-d');
        $compareDate = date ("Y-m-d", strtotime("+2 day", strtotime($currentDate)));
        if(strtotime($date)>strtotime($compareDate)){
            return redirect('food/show-calendar')->with('alert','Your selected date is too future!');
        }*/

        $checkDataExistOrNot = DailyChart::where('date',$date)
                                            ->count();

        if($checkDataExistOrNot == 0){
            return redirect('food/show-calendar')->with('alert','No data has been found!');
        }

        $info = DB::table('dailychart')
                    ->join('foodlists', 'dailychart.food_id', '=', 'foodlists.id')
                    ->where('dailychart.date',$date)     
                    ->select('dailychart.*', 'foodlists.NameOfItem', 'foodlists.Quantity', 'foodlists.CategoryId','foodlists.HowManyTimes')
                    ->orderBy('foodlists.CategoryId')
                    ->get();

        $collectFoodClientInfo = FoodClient::where('Date',$date)
                                                ->get();

        $pdf = PDF::loadView('pages.food.dailyfoodchart.report.dailychartreport', ['chartInfos' => $info, 'clientInfos' => $collectFoodClientInfo, 'date' => $date]);
        return $pdf->download(date('d-M-Y',strtotime($date)).'_food_chart_report.pdf');
    }

    public function getPriceChartReport($date){
        $checkAlreadyExist = FoodMarket::where('date',$date)
                                        ->count();

        $checkDailyChartExist = DailyChart::where('date',$date)
                                            ->count();
        if($checkDailyChartExist == 0){
            return redirect('food/create-food-client/'.$date)->with('alert','Please create a food char first!');
        }

        if($checkAlreadyExist>0){
            return view('pages.food.dailyfoodchart.marketprice',['exist'=>true]);
        }

        $info = DB::table('dailychart')
                    ->join('foodlists','dailychart.food_id','=','foodlists.id')
                    ->where('dailychart.number_of_client','<>','0')
                    ->where('dailychart.Date',$date)
                    ->select('foodlists.CategoryId')
                    ->groupBy('foodlists.CategoryId')
                    ->get();
        $food[] = NULL;
        $foodAmount[] = NULL;        
        foreach($info as $data){
            $foodGroupInfo = AddFoodGroup::find($data->CategoryId);
            $food[] = $foodGroupInfo->NameOfGroup;            
            $calculateQuantitySql = DB::table('dailychart')
                                        ->join('foodlists','dailychart.food_id','=','foodlists.id')
                                        ->where('dailychart.number_of_client','<>','0')
                                        ->where('foodlists.CategoryId',$data->CategoryId)
                                        ->where('dailychart.Date',$date)
                                        ->select('foodlists.*','dailychart.number_of_client')                                        
                                        ->get();
            $foodAmountCount = 0;
            foreach ($calculateQuantitySql as $value) {
                $foodAmountCount+=($value->number_of_client*$value->Quantity*$value->HowManyTimes);       
            }
            $foodAmount[] = $foodAmountCount;
        }

        //return view('pages.food.dailyfoodchart.marketprice',['food'=>$food,'foodamount'=>$foodAmount, 'exist'=>false]);
        
        $pdf = PDF::loadView('pages.food.dailyfoodchart.report.pricechart', ['food'=>$food,'foodamount'=>$foodAmount, 'exist'=>false, 'date'=>$date]);
        return $pdf->download($date.'_price_chart_report.pdf');
    }
    //End Report Section
}