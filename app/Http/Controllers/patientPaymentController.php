<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Validator;

use App\Models\PatientInfo;
use App\Models\patientPaymentHistory;

class patientPaymentController extends Controller
{    
    public function getHome($id){
        $patient = patientInfo::where('id',$id)->get()->last();
        $payments = patientPaymentHistory::where('regNo', $id)->get();       

        return view('pages.administration.patientAdmission.payments.home',[
            'patient' => $patient,
            'payments' => $payments
            ]);
    }

    public function getCreatePatientPayment($id){

        return view('pages.administration.patientAdmission.payments.create',[
            'id' => $id
            ]);
    }

    public function postPatientPayment(Request $request){
        $user = \Auth::user()->username;
        $payment = $request->all();

        $patient = patientInfo::where('id',$payment['regNo'])->get()->last();
        $payments = patientPaymentHistory::where('regNo', $payment['regNo'])->get();

        $payment['bedCabin'] = $patient['bedCabin'];
        $payment['recievedBy'] = $user;
        $payment['posted'] = 0;

        patientPaymentHistory::create($payment);

        return view('pages.administration.patientAdmission.payments.home',[
            'patient' => $patient,
            'payments' => $payments
            ]);
    }

    public function getEditPatientPayment($id){
        $payment = patientPaymentHistory::where('id', $id)->get()->last();

        return view('pages.administration.patientAdmission.payments.edit',[
            'payment' => $payment
            ]);
    }

    public function postUpdate(Request $request){

        $rules =[
        'payment' => 'required|numeric'
        ];

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()){
            return back()->withErrors($validator);
            //return Redirect::to('nerds/' . $id . '/edit')->withErrors($validator)->withInput(Input::except('password'));
        } else {
            $user = \Auth::user()->username;
            $payment = $request->all();

            $patient = patientInfo::where('id',$payment['regNo'])->get()->last();


            $payment['bedCabin'] = $patient['bedCabin'];
            $payment['recievedBy'] = $user;
            $payment['posted'] = 0;

            $newPayment = patientPaymentHistory::find($payment['id']);
            
            $newPayment->paymentDate = $payment['paymentDate'];
            $newPayment->desc = $payment['desc'];
            $newPayment->payment = $payment['payment'];
            $newPayment->regNo = $payment['regNo'];
            $newPayment->bedCabin = $patient['bedCabin'];
            $newPayment->recievedBy = $payment['recievedBy'];
            $newPayment->posted = $payment['posted'];

            $newPayment->save();

            $payments = patientPaymentHistory::where('regNo', $payment['regNo'])->get();       

            return view('pages.administration.patientAdmission.payments.home',[
                'patient' => $patient,
                'payments' => $payments
                ]);
        }
    }

    public function getDeletePatientPayment($id){
        
        $payment = patientPaymentHistory::find($id);
        $payment->delete();

        $patient = patientInfo::where('id',$payment['regNo'])->get()->last();
        $payments = patientPaymentHistory::where('regNo', $payment['regNo'])->get();

        return view('pages.administration.patientAdmission.payments.home',[
            'patient' => $patient,
            'payments' => $payments
            ]);
    }

}
