<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\PatientInfo;
use App\Models\Bill;
use App\Models\DailyRentAndCharge;
use App\Models\dailyRentDateLog;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex(){
       

        //todays date
        $today = Carbon::today();
        $today = $today->toDateString();
        $Timetoday = strtotime($today);

        //last updated date
        $lastUpdated = DailyRentDateLog::get()->last();

        if(!$lastUpdated){
            $lastUpdated = new DailyRentDateLog;
            $lastUpdated->updatedDates = "2016-01-01";
        }

        $timeLastUpdated = strtotime($lastUpdated->updatedDates);
        
        $diffInDays = (($Timetoday - $timeLastUpdated)/(60*60*24));

        //echo $lastUpdated->updatedDates." | ".$today." | ".$diffInDays."| ";       

        if($diffInDays>0){

            $patients = PatientInfo::where('admissionStatus', 'Admitted')
            ->get();

            $today = Carbon::today();
            
            $lastUpdated = Carbon::parse($lastUpdated->updatedDates);

            $date = $lastUpdated->addday();

            while($date->lte($today)){
            	//echo $date." | ";

                foreach ($patients as $patient) {
                    if($date->gt(Carbon::parse($patient->admissionDate))){

                        $dailyRents = DailyRentAndCharge::where('pid', $patient->id)->get();

                        foreach ($dailyRents as $dailyRent){
                            $bill = [];

                            $bill['regNo'] = $dailyRent->pid;
                            $bill['bedNo'] = $patient->bedCabin;
                            $bill['masterCategory'] = $dailyRent->master_category;
                            $bill['category'] = $dailyRent->subcategory;
                            $bill['services'] = $dailyRent->services;
                            $bill['remarks'] = $dailyRent->description;
                            $bill['date'] = $date->toDateString();
                            $bill['quantity'] = $dailyRent->quantity;
                            $bill['rate'] = $dailyRent->rate;
                            $bill['vat'] = $dailyRent->vat;
                            $bill['serviceCharge'] = $dailyRent->servicecharge;
                            $bill['billBy'] = "Auto";

                            //echo $bill['services']." | ".$bill['quantity']." | ".$bill['rate']."\xA";

                            Bill::create($bill);
                        }                        
                    }
                }

                $newDate = [];
                $newDate['updatedDates'] = $date->toDateString();

                //echo $newDate['updatedDates']."\xA\xA";

                DailyRentDateLog::create($newDate);
                
                //echo "Date:".$date."  New Date:".$newDate['updatedDates']."/n   ";
                $date->addDay();
            }
        }

        //return Auth::user()->role;

        //Auth::logout();
        if(Auth::user()->role==1){
            return view('pages.admin.home');
        }
        else if(Auth::user()->role==2){
            return view('pages.food.home');
        }
        else if(Auth::user()->role==3){
            return view('pages.administration.home');
        }
        else if(Auth::user()->role==4){
            return view('pages.accounts.home');
        }
    }
}
