<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PatientInfo;
use App\Models\Bed;
use App\Models\Bill;
use App\Models\Doctors;
use App\Models\patientDiscount;
use App\Models\patientPaymentHistory;

use Carbon\Carbon;
use PDF;

class reportsController extends Controller
{
    //index
	public function getIndex(){
		return view('pages.administration.reports.home');
	}




	public function postPatientList(Request $request){
		$data = $request->all();

		if($data['fromDate'] == "" or $data['toDate'] == ""){
			$alert = "Cannot find data with empty data fields, please check input";
			return back()->withInput()->with('alert',$alert);
		}

		$patients = PatientInfo::whereBetween('admissionDate', array($data['fromDate'], $data['toDate']))->where('admissionStatus', $data['admissionStatus'])->get();

		foreach ($patients as $patient) {
			$temp = Bed::where('name', '=', $patient->bedCabin)
			->get()
			->last();
			
			$bedRate = '-';
			
			if($temp){
				$bedRate = $temp->Rate;
			}

			$patient->bedRate = $bedRate;
		}

		$count = PatientInfo::whereBetween('admissionDate', array($data['fromDate'], $data['toDate']))->count();

		$data['fromDate'] = date("d-M-Y", strtotime($data['fromDate']));
		$data['toDate'] = date("d-M-Y", strtotime($data['toDate']));

    	//return $count;
		$pdf = PDF::loadView('pages.administration.reports.patientList',[
			'patients' => $patients,
			'count' => $count,
			'from' => $data['fromDate'],
			'to' => $data['toDate']
			])->setOrientation('landscape');

		return $pdf->stream('Patient_List.pdf');
	}





	public function postDoctorList(Request $request){
		$doctors = Doctors::get();
		$count = Doctors::count();
		$pdf = PDF::loadView('pages.administration.reports.doctorList',[
			'doctors' => $doctors,
			'count' => $count
			])->setOrientation('landscape');

		return $pdf->stream('Patient_List.pdf');
	}




	public function postDiscountList(Request $request){
		$data = $request->all();

		$discounts = patientDiscount::whereBetween('date', array($data['fromDate'], $data['toDate']))
		->orderBy('regNo')
		->get();

		$regNumbers = patientDiscount::selectRaw('regNo')
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->groupBy('regNo')
		->get();

		$regNo = [];

		foreach ($regNumbers as $indx => $regNumber) {
			$regNo[$indx] = $regNumber->regNo;
		}

		$patients = PatientInfo::selectRaw('id, name, bedCabin, assignedConsultant')
		->whereIn('id', $regNo)
		->orderBy('id')
		->get();

		$consultants = PatientInfo::selectRaw('assignedConsultant')
		->whereIn('id', $regNo)
		->groupBy('assignedConsultant')
		->get();

		$total = (object) array(
			'patients' => 0,
			'hospDisc' => 0,
			'consultDisc' => 0,
			'totalDisc' => 0
			);

		/*$consultants = [];

		foreach ($patients as $patient) {
			printf($patient);
			echo "\xA\xA";
		}

		foreach($consults as $index => $consult){
			$consultants[$index] = $consult->assignedConsultant;
			printf($consult);
			echo "\xA\xA";
		}*/

		//return $consultants;

		foreach ($consultants as $consultant) {
			$consultant->patients = 0;
			$consultant->consultDisc = 0;
			$consultant->hospDisc = 0;
			$consultant->totalDisc = 0;

			foreach ($patients as $patient) {
				if($patient->assignedConsultant == $consultant->assignedConsultant){
					foreach ($discounts as $discount) {
						if($discount->regNo == $patient->id){
							$discount->hospDisc = 0;
							$discount->consultDisc = 0;
							$discount->totalDisc = 0;

							$discount->date = date("d-M-Y", strtotime($discount->date));

							if($discount->refBy == "Hospital"){
								$discount->hospDisc = $discount->hospDisc + $discount->amount;
							}
							else{
								$discount->consultDisc = $discount->consultDisc + $discount->amount;
							}

							$discount->totalDisc = $discount->totalDisc + $discount->amount;

							$discount->name = $patient->name;
							$discount->bedCabin = $patient->bedCabin;
							$discount->consultant = $patient->assignedConsultant;

							/*printf($discount->regNo.' | '.$discount->name.' | '.$discount->date.' | '.$discount->bedCabin.' | '.$discount->consultDisc.' | '.$discount->hospDisc.' | '.$discount->totalDisc.' | '.$discount->refBy);
							echo "\xA";*/


							$consultant->consultDisc = $consultant->consultDisc + $discount->consultDisc;
							$consultant->hospDisc = $consultant->hospDisc + $discount->hospDisc;
							$consultant->totalDisc = $consultant->totalDisc + $discount->totalDisc;	
						}
					}

					$consultant->patients++;
				}
			}

			/*echo "\xA";
			printf('Consultant: '.$consultant->assignedConsultant.' | '.$consultant->patients.' | '.$consultant->consultDisc.' | '.$consultant->hospDisc.' | '.$consultant->totalDisc);
			echo "\xA\xA";*/

			$total->patients = $total->patients + $consultant->patients;
			$total->consultDisc = $total->consultDisc + $consultant->consultDisc;
			$total->hospDisc = $total->hospDisc + $consultant->hospDisc;
			$total->totalDisc = $total->totalDisc + $consultant->totalDisc;
		}

		/*foreach ($discounts as $discount){
			printf($discount->regNo.' | '.$discount->name.' | '.$discount->date.' | '.$discount->bedCabin.' | '.$discount->consultDisc.' | '.$discount->hospDisc.' | '.$discount->totalDisc.' | '.$discount->refBy);
			echo "\xA\xA";
		}

		echo "\xA";

		foreach($consultants as $consult){			
			printf('Consultant: '.$consult->assignedConsultant.' | '.$consult->patients.' | '.$consult->consultDisc.' | '.$consult->hospDisc.' | '.$consult->totalDisc);
			echo "\xA\xA";
		}*/

		//print_r($total);

		//echo "\xA";

		//return $consultants;

		$pdf = PDF::loadView('pages.administration.reports.discountList',[
			'consultants' => $consultants,
			'discounts' => $discounts,
			'total' => $total,
			'from' => $data['fromDate'],
			'to' => $data['toDate']
			]);

		return $pdf->stream('Discount_List.pdf');
	}




	public function postPatientRegister(Request $request){
		$data = $request->all();
		
		$bills = Bill::whereBetween('date', array($data['fromDate'], $data['toDate']))
		->get();

		$regNumbers = Bill::selectRaw('regNo')
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->groupBy('regNo')
		->get();

		$regNo = [];

		foreach ($regNumbers as $indx => $regNumber) {
			$regNo[$indx] = $regNumber->regNo;
		}

		$patients = PatientInfo::whereIn('id', $regNo)
		->orderBy('id')
		->get();

		$count = PatientInfo::whereIn('id', $regNo)->count();

		$total = (object) array(
			'consult' => 0,
			'bedFee' => 0,
			'msc' => 0,
			'oac' => 0,
			'aFee' => 0,
			'food' => 0,
			'medicine' => 0,
			'pp' => 0,
			'inv' => 0,
			'oConsult' => 0,
			'total' => 0,
			'vat' => 0,
			'payment' => 0,
			'discount' => 0,
			'due' => 0
			);

		/*foreach ($patients as $patient) {
			print_r($patient);
			echo "\xA\xA"; 
		}*/

		//return $patients;

		foreach($patients as $patient){
			$patient->consult = 0;
			$patient->bedFee = 0;
			$patient->msc = 0;
			$patient->oac = 0;
			$patient->aFee = 0;
			$patient->food = 0;
			$patient->medicine = 0;
			$patient->pp = 0;
			$patient->inv = 0;
			$patient->oConsult = 0;
			$patient->total = 0;
			$patient->vat = 0;
			$patient->payment = 0;
			$patient->discount = 0;
			$patient->due = 0;

			$patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));

			foreach ($bills as $bill) {
				if($bill->regNo == $patient->id){

					//echo $bill->regNo." ".$bill->date." ".$bill->services."\xA\xA";

					$val = $bill->rate * $bill->quantity;
					$vat = (($bill->vat)*$val)/100;
					$sc = (($bill->serviceCharge)*$val)/100;

					if($bill->services == "Admission Fees,Cabin" or $bill->services == "Admission Fees,General bed"){
						$patient->aFee = $patient->aFee + $val + $sc;
					}
					else if($bill->masterCategory == "Consultant Services"){
						$patient->consult = $patient->consult + $val  + $sc;
					}
					else if($bill->masterCategory == "Bed & Cabin Services"){
						$patient->bedFee = $patient->bedFee + $val + $sc;
					}
					else if($bill->services == "Other Activity Charges"){
						$patient->oac = $patient->oac + $val + $sc;
					}
					else if($bill->masterCategory == "Hospital Services"){
						$patient->msc = $patient->msc + $val + $sc;
					}					
					else if($bill->category == "Food"){
						$patient->food = $patient->food + $val +  $sc;
					}
					else if($bill->category == "Medicine"){
						$patient->medicine = $patient->medicine + $val +  $sc;
					}
					else if($bill->category == "Patient Personal"){
						$patient->pp = $patient->pp + $val + $sc;
					}
					else if($bill->category == "Investigation"){
						$patient->inv = $patient->inv + $val + $sc;
					}
					else if($bill->category == "Consultancy"){
						$patient->oConsult = $patient->oConsult + $val  + $sc;
					}

					$patient->vat = $patient->vat + $vat;
					$patient->total = $patient->total + $val + $sc;

					$payment = patientPaymentHistory::selectRaw('sum(payment) totalPayment')
					->where('regNo', $patient->id)
					->whereBetween('paymentDate', array($data['fromDate'], $data['toDate']))
					->get()
					->last();

					if(!$payment->totalPayment){
						$patient->payment = 0;
					}
					else{
						$patient->payment = $payment->totalPayment;
					}

					$discount = patientDiscount::selectRaw('sum(amount) totalAmount')
					->where('regNo', $patient->id)
					->whereBetween('date', array($data['fromDate'], $data['toDate']))
					->get()
					->last();

					if(!$discount->totalAmount){
						$patient->discount = 0;
					}
					else{
						$patient->discount = $discount->totalAmount;
					}
					$patient->due = $patient->total + $patient->vat - $patient->payment - $patient->discount;
				}
			}

			$total->consult =$total->consult + $patient->consult;
			$total->bedFee = $total->bedFee + $patient->bedFee;
			$total->msc = $total->msc + $patient->msc;
			$total->oac = $total->oac + $patient->oac;
			$total->aFee = $total->aFee + $patient->aFee;
			$total->food = $total->food + $patient->food;
			$total->medicine = $total->medicine + $patient->medicine;
			$total->pp = $total->pp + $patient->pp;
			$total->inv = $total->inv + $patient->inv;
			$total->oConsult = $total->oConsult + $patient->oConsult;
			$total->total = $total->total + $patient->total;
			$total->vat = $total->vat + $patient->vat;
			$total->payment = $total->payment + $patient->payment;
			$total->discount = $total->discount + $patient->discount;
			$total->due = $total->due + $patient->due;

			/*printf('ID:'.$patient->id."\xA");
			printf("Name: ".$patient->name."\xA");
			printf("Consultant: ".$patient->consult."\xA");
			printf("Bed: ".$patient->bedFee."\xA");
			printf("MSC:".$patient->msc."\xA");
			printf("OAC: ".$patient->oac."\xA");
			printf("Admission Fee: ".$patient->aFee."\xA");
			printf("Food: ".$patient->food."\xA");
			printf("Medicine: ".$patient->medicine."\xA");
			printf("Patient Personal: ".$patient->pp."\xA");
			printf("Investigation: ".$patient->inv."\xA");
			printf("Other Consultancy: ".$patient->oConsult."\xA\xA");
			printf("Total: ".$patient->total."\xA");
			printf("VAT: ".$patient->vat."\xA");
			printf("Payment: ".$patient->payment."\xA");
			printf("Discount: ".$patient->discount."\xA");
			printf("Due: ".$patient->due."\xA\xA");*/
		}

		$from = date("d-M-Y", strtotime($data['fromDate']));
		$to = date("d-M-Y", strtotime($data['toDate']));

		$pdf = PDF::loadView('pages.administration.reports.patientRegister',[
			'patients' => $patients,
			'count' => $count,
			'total' => $total,
			'from' => $from,
			'to' => $to
			])->setOrientation('landscape');

		return $pdf->stream('Patient_Register.pdf');
	}

	

	public function postDuePatientList(Request $request){
		$data = $request->all();
		
		$bills = Bill::whereBetween('date', array($data['fromDate'], $data['toDate']))
		->get();

		$regNumbers = Bill::selectRaw('regNo')
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->groupBy('regNo')
		->get();

		$regNo = [];

		foreach ($regNumbers as $indx => $regNumber) {
			$regNo[$indx] = $regNumber->regNo;
		}

		$patients = PatientInfo::whereIn('id', $regNo)
		->orderBy('id')
		->get();

		$count = PatientInfo::whereIn('id', $regNo)->count();

		$total = (object) array(
			'consult' => 0,
			'bedFee' => 0,
			'msc' => 0,
			'oac' => 0,
			'aFee' => 0,
			'food' => 0,
			'medicine' => 0,
			'pp' => 0,
			'inv' => 0,
			'oConsult' => 0,
			'total' => 0,
			'vat' => 0,
			'payment' => 0,
			'discount' => 0,
			'due' => 0
			);

		/*foreach ($patients as $patient) {
			print_r($patient);
			echo "\xA\xA"; 
		}*/

		//return $patients;

		foreach($patients as $patient){
			$patient->consult = 0;
			$patient->bedFee = 0;
			$patient->msc = 0;
			$patient->oac = 0;
			$patient->aFee = 0;
			$patient->food = 0;
			$patient->medicine = 0;
			$patient->pp = 0;
			$patient->inv = 0;
			$patient->oConsult = 0;
			$patient->total = 0;
			$patient->vat = 0;
			$patient->payment = 0;
			$patient->discount = 0;
			$patient->due = 0;

			$patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));

			foreach ($bills as $bill) {
				if($bill->regNo == $patient->id){

					//echo $bill->regNo." ".$bill->date." ".$bill->services."\xA\xA";

					$val = $bill->rate * $bill->quantity;
					$vat = (($bill->vat)*$val)/100;
					$sc = (($bill->serviceCharge)*$val)/100;

					if($bill->services == "Admission Fees,Cabin" or $bill->services == "Admission Fees,General bed"){
						$patient->aFee = $patient->aFee + $val + $sc;
					}
					else if($bill->masterCategory == "Consultant Services"){
						$patient->consult = $patient->consult + $val  + $sc;
					}
					else if($bill->masterCategory == "Bed & Cabin Services"){
						$patient->bedFee = $patient->bedFee + $val + $sc;
					}
					else if($bill->services == "Other Activity Charges"){
						$patient->oac = $patient->oac + $val + $sc;
					}
					else if($bill->masterCategory == "Hospital Services"){
						$patient->msc = $patient->msc + $val + $sc;
					}
					else if($bill->category == "Food"){
						$patient->food = $patient->food + $val +  $sc;
					}
					else if($bill->category == "Medicine"){
						$patient->medicine = $patient->medicine + $val +  $sc;
					}
					else if($bill->category == "Patient Personal"){
						$patient->pp = $patient->pp + $val + $sc;
					}
					else if($bill->category == "Investigation"){
						$patient->inv = $patient->inv + $val + $sc;
					}
					else if($bill->category == "Consultancy"){
						$patient->oConsult = $patient->oConsult + $val  + $sc;
					}

					$patient->vat = $patient->vat + $vat;
					$patient->total = $patient->total + $val + $sc;

					$payment = patientPaymentHistory::selectRaw('sum(payment) totalPayment')
					->where('regNo', $patient->id)
					->whereBetween('paymentDate', array($data['fromDate'], $data['toDate']))
					->get()
					->last();

					if(!$payment->totalPayment){
						$patient->payment = 0;
					}
					else{
						$patient->payment = $payment->totalPayment;
					}

					$discount = patientDiscount::selectRaw('sum(amount) totalAmount')
					->where('regNo', $patient->id)
					->whereBetween('date', array($data['fromDate'], $data['toDate']))
					->get()
					->last();

					if(!$discount->totalAmount){
						$patient->discount = 0;
					}
					else{
						$patient->discount = $discount->totalAmount;
					}
					$patient->due = $patient->total + $patient->vat - $patient->payment - $patient->discount;
				}
			}
			//if patient due greater than 0
			$total->consult =$total->consult + $patient->consult;
			$total->bedFee = $total->bedFee + $patient->bedFee;
			$total->msc = $total->msc + $patient->msc;
			$total->oac = $total->oac + $patient->oac;
			$total->aFee = $total->aFee + $patient->aFee;
			$total->food = $total->food + $patient->food;
			$total->medicine = $total->medicine + $patient->medicine;
			$total->pp = $total->pp + $patient->pp;
			$total->inv = $total->inv + $patient->inv;
			$total->oConsult = $total->oConsult + $patient->oConsult;
			$total->total = $total->total + $patient->total;
			$total->vat = $total->vat + $patient->vat;
			$total->payment = $total->payment + $patient->payment;
			$total->discount = $total->discount + $patient->discount;
			$total->due = $total->due + $patient->due;

			/*printf('ID:'.$patient->id."\xA");
			printf("Name: ".$patient->name."\xA");
			printf("Consultant: ".$patient->consult."\xA");
			printf("Bed: ".$patient->bedFee."\xA");
			printf("MSC:".$patient->msc."\xA");
			printf("OAC: ".$patient->oac."\xA");
			printf("Admission Fee: ".$patient->aFee."\xA");
			printf("Food: ".$patient->food."\xA");
			printf("Medicine: ".$patient->medicine."\xA");
			printf("Patient Personal: ".$patient->pp."\xA");
			printf("Investigation: ".$patient->inv."\xA");
			printf("Other Consultancy: ".$patient->oConsult."\xA\xA");
			printf("Total: ".$patient->total."\xA");
			printf("VAT: ".$patient->vat."\xA");
			printf("Payment: ".$patient->payment."\xA");
			printf("Discount: ".$patient->discount."\xA");
			printf("Due: ".$patient->due."\xA\xA");*/
		}

		$from = date("d-M-Y", strtotime($data['fromDate']));
		$to = date("d-M-Y", strtotime($data['toDate']));

		$pdf = PDF::loadView('pages.administration.reports.duePatientList',[
			'patients' => $patients,
			'count' => $count,
			'total' => $total,
			'from' => $from,
			'to' => $to
			])->setOrientation('landscape');

		return $pdf->stream('Due_Patient_List.pdf');
	}





	public function postDiscountListWithCharges(Request $request){
		$data = $request->all();

		$discounts = patientDiscount::whereBetween('date', array($data['fromDate'], $data['toDate']))
		->orderBy('regNo')
		->get();

		$regNumbers = patientDiscount::selectRaw('regNo')
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->groupBy('regNo')
		->get();

		$regNo = [];

		foreach ($regNumbers as $indx => $regNumber) {
			$regNo[$indx] = $regNumber->regNo;
		}

		$patients = PatientInfo::selectRaw('id, name, admissionDate, assignedConsultant')
		->whereIn('id', $regNo)
		->orderBy('id')
		->get();

		$bills = Bill::where('masterCategory', 'Bed & Cabin Services')
		->whereIn('regNo', $regNo)
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->orWhere('masterCategory', 'Consultant Services')
		->whereIn('regNo', $regNo)
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->orWhere('masterCategory', 'Hospital Services')
		->whereIn('regNo', $regNo)
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->orderBy('id')
		->get();

		$consultants = PatientInfo::selectRaw('assignedConsultant')
		->whereIn('id', $regNo)
		->groupBy('assignedConsultant')
		->get();

		$total = (object) array(
			'patients' => 0,
			
			'bedCharge' => 0,
			'consultCharge' => 0,
			
			'hospDisc' => 0,
			'consultDisc' => 0,
			
			'hospital' => 0,
			'consultant' => 0
			);

		foreach ($consultants as $consultant) {
			$consultant->patients = 0;
			
			$consultant->bedCharge = 0;			
			$consultant->consultCharge = 0;		
			
			$consultant->consultDisc = 0;
			$consultant->hospDisc = 0;

			$consultant->hospital = 0;
			$consultant->consultant = 0;

			foreach ($patients as $patient) {
				if($patient->assignedConsultant == $consultant->assignedConsultant){
					$patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));

					$patient->bedCharge = 0;
					$patient->consultCharge = 0;
					
					$patient->hospDisc = 0;
					$patient->consultDisc = 0;
					
					$patient->hospital = 0;
					$patient->consultant = 0;

					foreach ($bills as $bill) {
						if($bill->regNo == $patient->id){
							$temp = $bill->rate * $bill->quantity;
							$vat = ($bill->vat * $bill->rate) / 100;
							$sc = ($bill->serviceCharge * $bill->rate) / 100;

							if($bill->masterCategory == 'Bed & Cabin Services' or $bill->masterCategory == 'Hospital Services'){
								$patient->bedCharge = $patient->bedCharge + $temp + $vat + $sc;
							}
							else if($bill->masterCategory == 'Consultant Services'){
								$patient->consultCharge = $patient->consultCharge + $temp + $vat + $sc;
							}
						}
					}

					foreach ($discounts as $discount) {
						if($discount->regNo == $patient->id){

							if($discount->refBy == "Hospital"){
								$patient->hospDisc = $patient->hospDisc + $discount->amount;
							}
							else{
								$patient->consultDisc = $patient->consultDisc + $discount->amount;
							}
							
							/*printf($discount->regNo.' | '.$discount->name.' | '.$discount->date.' | '.$discount->bedCabin.' | '.$discount->consultDisc.' | '.$discount->hospDisc.' | '.$discount->totalDisc.' | '.$discount->refBy);
							echo "\xA";*/
						}
					}

					$patient->hospital = $patient->bedCharge - $patient->hospDisc;
					$patient->consultant = $patient->consultCharge - $patient->consultDisc;

					$consultant->patients++;

					$consultant->bedCharge = $consultant->bedCharge + $patient->bedCharge;
					$consultant->consultCharge = $consultant->consultCharge + $patient->consultCharge;
					
					$consultant->hospDisc = $consultant->hospDisc + $patient->hospDisc;
					$consultant->consultDisc = $consultant->consultDisc + $patient->consultDisc;
				}
			}

			$consultant->hospital = $consultant->bedCharge - $consultant->hospDisc;
			$consultant->consultant = $consultant->consultCharge - $consultant->consultDisc;
			
			/*echo "\xA";
			printf('Consultant: '.$consultant->assignedConsultant.' | '.$consultant->patients.' | '.$consultant->consultDisc.' | '.$consultant->hospDisc.' | '.$consultant->totalDisc);
			echo "\xA\xA";*/

			$total->patients = $total->patients + $consultant->patients;

			$total->bedCharge = $total->bedCharge + $consultant->bedCharge;
			$total->consultCharge = $total->consultCharge + $consultant->consultCharge;

			$total->hospDisc = $total->hospDisc + $consultant->hospDisc;
			$total->consultDisc = $total->consultDisc + $consultant->consultDisc;

			$total->hospital = $total->bedCharge - $total->hospDisc;
			$total->consultant = $total->consultCharge - $total->consultDisc;
			
		}

		$pdf = PDF::loadView('pages.administration.reports.discountListWithCharges',[
			'consultants' => $consultants,
			'patients' => $patients,
			'total' => $total,
			'from' => $data['fromDate'],
			'to' => $data['toDate']
			]);

		return $pdf->stream('Discount_List_With_Charges.pdf');
	}



	public function postIncomeStatement(Request $request){
		$data = $request->all();
		
		$bills = Bill::where('masterCategory', 'Bed & Cabin Services')->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->orWhere('masterCategory', 'Hospital Services')->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->get();

		$regNumbers = Bill::selectRaw('regNo')
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->groupBy('regNo')
		->get();

		$regNo = [];

		foreach ($regNumbers as $indx => $regNumber) {
			$regNo[$indx] = $regNumber->regNo;
		}

		$patients = PatientInfo::whereIn('id', $regNo)
		->orderBy('id')
		->get();

		$total = (object) array(
			'patients' => 0,

			'bedFee' => 0,
			'msc' => 0,
			'oac' => 0,
			'aFee' => 0,

			'total' => 0,
			'hospDisc' => 0,
			'consultDisc' => 0,
			'gross' => 0
			);

		foreach($patients as $patient){
			$patient->bedFee = 0;
			$patient->msc = 0;
			$patient->oac = 0;
			$patient->aFee = 0;

			$patient->total = 0;
			$patient->hospDisc = 0;
			$patient->consultDisc = 0;
			$patient->gross = 0;

			$patient->admissionDate = date("d-m-Y", strtotime($patient->admissionDate));

			foreach ($bills as $bill) {
				if($bill->regNo == $patient->id){
					$val = $bill->rate * $bill->quantity;
					$vat = (($bill->vat)*$val)/100;
					$sc = (($bill->serviceCharge)*$val)/100;

					if($bill->services == "Admission Fees,Cabin" or $bill->services == "Admission Fees,General bed"){
						$patient->aFee = $patient->aFee + $val + $vat + $sc;
					}
					else if($bill->masterCategory == "Bed & Cabin Services"){
						$patient->bedFee = $patient->bedFee + $val + $vat + $sc;
					}
					else if($bill->services == "Other Activity Charges"){
						$patient->oac = $patient->oac + $val + $vat + $sc;
					}
					else if($bill->masterCategory == "Hospital Services"){
						$patient->msc = $patient->msc + $val + $vat + $sc;
					}

					$patient->total = $patient->total + $val + $vat + $sc;
				}
			}

			$hospDisc = patientDiscount::selectRaw('sum(amount) as totalAmount')
			->where('regNo', $patient->id)
			->where('refBy', 'Hospital')
			->get()
			->last();

			$consultDisc = patientDiscount::selectRaw('sum(amount) as totalAmount')
			->where('regNo', $patient->id)
			->where('refBy', '!=', 'Hospital')
			->get()
			->last();

			if($hospDisc){
				$patient->hospDisc = $hospDisc->totalAmount;
			}

			if($consultDisc){
				$patient->consultDisc = $consultDisc->totalAmount;
			}

			$patient->gross = $patient->total - $patient->hospDisc - $patient->consultDisc;



			$total->patients++;

			$total->bedFee = $total->bedFee + $patient->bedFee;
			$total->msc = $total->msc + $patient->msc;
			$total->oac = $total->oac + $patient->oac;
			$total->aFee = $total->aFee + $patient->aFee;

			$total->total = $total->total + $patient->total;

			$total->hospDisc = $total->hospDisc + $patient->hospDisc;
			$total->consultDisc = $total->consultDisc + $patient->consultDisc;

			$total->gross = $total->gross + $patient->gross;
		}

		$pdf = PDF::loadView('pages.administration.reports.incomeStatement',[
			'patients' => $patients,
			'total' => $total,
			'from' => $data['fromDate'],
			'to' => $data['toDate']
			])->setOrientation('landscape');

		return $pdf->stream('Income_Statement.pdf');
	}




	public function postIncomeStatementSum(Request $request){
		$data = $request->all();
		
		$bills = Bill::where('masterCategory', 'Bed & Cabin Services')->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->orWhere('masterCategory', 'Hospital Services')->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->get();

		$regNumbers = Bill::selectRaw('regNo')
		->whereBetween('date', array($data['fromDate'], $data['toDate']))
		->groupBy('regNo')
		->get();

		$regNo = [];

		foreach ($regNumbers as $indx => $regNumber) {
			$regNo[$indx] = $regNumber->regNo;
		}

		$patients = PatientInfo::whereIn('id', $regNo)
		->orderBy('id')
		->get();

		$consultants = PatientInfo::selectRaw('assignedConsultant')
		->whereIn('id', $regNo)
		->groupBy('assignedConsultant')
		->get();

		$total = (object) array(
			'patients' => 0,

			'bedFee' => 0,
			'msc' => 0,
			'oac' => 0,
			'aFee' => 0,

			'total' => 0,
			'hospDisc' => 0,
			'consultDisc' => 0,
			'gross' => 0
			);		

		foreach ($consultants as $consultant){

			$consultant->patients = 0;

			$consultant->bedFee = 0;
			$consultant->msc = 0;
			$consultant->oac = 0;
			$consultant->aFee = 0;

			$consultant->total = 0;
			$consultant->hospDisc = 0;
			$consultant->consultDisc = 0;
			$consultant->gross = 0;

			foreach ($patients as $patient) {
				if($patient->assignedConsultant == $consultant->assignedConsultant){
					$consultant->patients++;

					foreach ($bills as $bill) {

						if($bill->regNo == $patient->id){

							$val = $bill->rate * $bill->quantity;
							$vat = (($bill->vat)*$val)/100;
							$sc = (($bill->serviceCharge)*$val)/100;

							if($bill->services == "Admission Fees,Cabin" or $bill->services == "Admission Fees,General bed"){
								$consultant->aFee = $consultant->aFee + $val + $vat + $sc;
							}
							else if($bill->masterCategory == "Bed & Cabin Services"){
								$consultant->bedFee = $consultant->bedFee + $val + $vat + $sc;
							}
							else if($bill->services == "Other Activity Charges"){
								$consultant->oac = $consultant->oac + $val + $vat + $sc;
							}
							else if($bill->masterCategory == "Hospital Services"){
								$consultant->msc = $consultant->msc + $val + $vat + $sc;
							}

							$consultant->total = $consultant->total + $val + $vat + $sc;
						}
					}

					$hospDisc = patientDiscount::selectRaw('sum(amount) as totalAmount')
					->where('regNo', $patient->id)
					->where('refBy', 'Hospital')
					->get()
					->last();

					$consultDisc = patientDiscount::selectRaw('sum(amount) as totalAmount')
					->where('regNo', $patient->id)
					->where('refBy', '!=', 'Hospital')
					->get()
					->last();

					if($hospDisc){
						$consultant->hospDisc = $consultant->hospDisc + $hospDisc->totalAmount;
					}

					if($consultDisc){
						$consultant->consultDisc = $consultant->consultDisc + $consultDisc->totalAmount;
					}

					
				}
			}

			$consultant->gross = $consultant->gross + $consultant->total - $consultant->hospDisc - $patient->consultDisc;

			/*printf($consultant->assignedConsultant." | ".$consultant->patients." | ".$consultant->bedFee." | ");
			printf($consultant->msc." | ".$consultant->oac." | ".$consultant->aFee." | ");
			printf($consultant->total." | ".$consultant->hospDisc." | ".$consultant->consultDisc." | ".$consultant->gross."\xA\xA");*/


			$total->patients = $total->patients + $consultant->patients;

			$total->bedFee = $total->bedFee + $consultant->bedFee;
			$total->msc = $total->msc + $consultant->msc;
			$total->oac = $total->oac + $consultant->oac;
			$total->aFee = $total->aFee + $consultant->aFee;

			$total->total = $total->total + $consultant->total;

			$total->hospDisc = $total->hospDisc + $consultant->hospDisc;
			$total->consultDisc = $total->consultDisc + $consultant->consultDisc;

			$total->gross = $total->gross + $consultant->gross;
		}

		$pdf = PDF::loadView('pages.administration.reports.incomeStatementSum',[
			'consultants' => $consultants,
			'total' => $total,
			'from' => $data['fromDate'],
			'to' => $data['toDate']
			])->setOrientation('landscape');

		return $pdf->stream('Income_Statement_Sum.pdf');
	}
}
