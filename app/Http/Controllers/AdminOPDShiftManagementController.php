<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\HMS\OPD\Admin\ShiftManagementRequest;

use App\Models\TimeShift;

class AdminOPDShiftManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('pages.admin.hms.opd.shiftmanagement.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pages.admin.hms.opd.shiftmanagement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(ShiftManagementRequest $request)
    {
        $timeshift = new TimeShift;
        $timeshift->shift_name = $request->shiftname;
        $timeshift->save();

        return redirect()->back()->with('status','New time shift has been registered successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show()
    {
        $allData = TimeShift::all();
        return view('pages.admin.hms.opd.shiftmanagement.show',['allData' => $allData]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = TimeShift::find($id);
        return view('pages.admin.hms.opd.shiftmanagement.edit',['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
