<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Input;
use Validator;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OpdInfo;
use App\Models\opdShiftmdl;
use App\Models\opdVisitingFeeMdl;
use App\Models\Doctors;
use App\Models\opdServiceMdl;
use App\Models\opdAll;
use Carbon\Carbon;




class OpdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

    	$opdid=OpdInfo::all()->last();
    	$shfts=opdShiftmdl::all();
        
    	//$vfees=opdVisitingFeeMdl::select('visitingfee')->where('id','=',"1")->get();
    	//$consultants=Doctors::all('name');
    	//$services=opdServiceMdl::all();
        return view('pages.administration.opd.opdinfo',compact('opdid','shfts'));


        //return view('pages.administration.opd.create',compact('opdid','shfts','consultants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $infos=OpdInfo::all();

        return view('pages.administration.opd.home',compact('infos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $opdinfo=$request->all();
        $opdid=$request->get('opdNo');
        $name=$request->get('pName');
        $date=$request->get('date');

        $consultants=Doctors::where('name','!=','Other')
                            ->where('name','!=','')
                            ->get();



        OpdInfo::create($opdinfo);

        return view('pages.administration.opd.create',compact('opdid','consultants','name','date'));
        //return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('pages.administration.opd.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opd=OpdInfo::where('opdNo', $id)->get()->last();
        $shfts=opdShiftmdl::all();
        //$vfees=opdVisitingFeeMdl::select('visitingfee')->where('id','=',"1")->get();
        $consultants= Doctors::all('name');
        $services= opdServiceMdl::all();
        $opdInvs = opdAll::where('opdNo', $id)->get();
        $tfee=opdAll::where('opdNo',$id)->sum('total_fee');


        return view('pages.administration.opd.edit',compact('opd','shfts','consultants','services','opdInvs','tfee'));
    }





    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        

        $patient=OpdInfo::where('opdNo', $id)->get()->last();
         $data = $request->all();
         $patient->opdNo=$data['opdNo'];
         $patient->pName=$data['pName'];
         $patient->date=$data['date'];
         $patient->sex=$data['sex'];
         $patient->age=$data['age'];
         $patient->phone=$data['phone'];
         $patient->address=$data['address'];
         $patient->occupation=$data['occupation'];
         $patient->save();
         


         return back()->withInput();
    }

    












    public function report(Request $request){


     $data = $request->all();
     $from=$request->input('fromDate');
     $to=$request->input('toDate');


     if($data['fromDate'] == "" or $data['toDate'] == ""){
        $alert = "Cannot find data with empty data fields, please check input";
        return back()->withInput()->with('alert',$alert);
    }

    $opds= opdAll::whereBetween('Date', array($data['fromDate'], $data['toDate']))->get();


   //  $opd=DB::table('opdall')->join('opdinfo_tbl','opdinfo_tbl.opdNo','=','opdall.id')
   //  ->whereBetween('opdall.date', [$from, $to])
   //  ->select('opdall.*','opdinfo_tbl.pName')->get();
   // return $opd;



    
    $total=opdAll::whereBetween('Date', array($data['fromDate'], $data['toDate']))->sum('total_fee');
    $total_consultantcharge=opdAll::whereBetween('Date', array($data['fromDate'], $data['toDate']))->sum('doctors_fee');
    $total_hospitalcharge=opdAll::whereBetween('Date', array($data['fromDate'], $data['toDate']))->sum('hospital_fee');



    $pdf = PDF::loadView('pages.administration.opd.reportOpd',compact('opds','from','to','total','total_consultantcharge','total_hospitalcharge'));
        //return view('pages.administration.opd.reportOpd',compact('opds','from','to','total','total_consultantcharge','total_hospitalcharge'));
    return $pdf->stream('OpdReport.pdf');



}



public function bill($id,$words){

	
   
   $info=OpdInfo::where('opdNo',$id)->get()->last();


   $totalfee=opdAll::where('opdNo',$id)->sum('total_fee');

   $date=Carbon::now();
   $date=$date->format('d-m-Y');
   $words=ucfirst($words);
   
   //return view('pages.administration.opd.opdBill',compact('info','totalfee','date','words'));
    $pdf = PDF::loadView('pages.administration.opd.opdBill',compact('info','totalfee','date','words'));
    return $pdf->stream('OpdBill.pdf');



}


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}