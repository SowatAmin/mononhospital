<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use PDF;
use Carbon\Carbon;
use App\Models\Salary;
use App\Models\Employee;

class salaryController extends Controller
{
    public function getIndex(){

        $today = Carbon::today();

        $startYear = date("Y", strtotime($today));

        $years[0] = $startYear;

        $nextYear = $today->subYear();

        for ($i=0; $i < 30; $i++) {
            $year = date("Y", strtotime($nextYear));
            $years[$i+1] = $year;
            $nextYear = $today->subYear();
        }

        return view('pages.accounts.salary.pickMonthYear',[
            'years' => $years
            ]);
    }

    public function postMakeSalarySheet(Request $request){
        $data = $request->all();

        $month = $data['month'];
        $year = $data['year'];

        $count = Salary::where('year', $year)->where('month', $month)->count();
        
        if($count == 0){
            $employees = Employee::whereNotNull('name')->get();

            foreach($employees as $employee){
                $salary['employeeNo'] = $employee->employeeNo;
                $salary['name'] = $employee->name;
                $salary['month'] = $month;
                $salary['year'] = $year;

                $salary['basic'] = $employee->basic;
                $salary['houseRent'] = $employee->houseRent;
                $salary['medAllow'] = $employee->medAllow;
                $salary['conveyence'] = $employee->conveyence;
                $salary['gSalary'] = $employee->gSalary;

                $salary['allowPhr'] = 0; //ask monon
                $salary['allowAmount'] = $salary['allowPhr'] * 208;
                $salary['totalSalary'] = $salary['gSalary'] + $salary['allowAmount'];

                $salary['otHour'] = 0;
                $salary['festivalOT'] = 0;
                $salary['otRate'] = $salary['basic'] / 208;                
                $salary['festivalPayment'] = 0;
                $salary['otAmount'] = 0;
                $salary['otAllow'] = 0;
                $salary['totalOT'] = 0;

                $salary['othersAdd'] = 0;

                $salary['lwpHour'] = 0;
                $salary['lwpRate'] = $salary['basic'] / 208;
                $salary['lwpAmount'] = 0;
                $salary['lwpAllow'] = 0;
                $salary['totalLwp'] = 0;
                
                $salary['uaHour'] = 0;
                $salary['uaRate'] = $salary['basic'] / 208;
                $salary['uaAmount'] = 0;
                $salary['uaAllow'] = 0;

                $salary['mealDeduct'] = 0;
                $salary['fineDeduct'] = 0;
                $salary['advanceDeduct'] = 0;

                $salary['othersDeduct'] = 0;

                $salary['stamp'] = 4;

                $salary['payableSalary'] = $salary['totalSalary'] - $salary['stamp'];

                Salary::create($salary);
            }
        }

        $salaries = Salary::where('year', $year)
        ->where('month', $month)
        ->orderBy('employeeNo','asc')
        ->get();

        return view('pages.accounts.salary.salarySheet',[
            'month' => $month,
            'year' => $year,
            'salaries' =>$salaries
            ]);
    }    

    public function getEditSalary($id){
        $salary = Salary::find($id);

        return view('pages.accounts.salary.indivSalary',[
            'salary' => $salary
            ]);
    }

    public function postEditSalary(Request $request){
        $data = $request->all();

        $salary = Salary::find($data['id']);

        $salary->basic = $data['basic'];
        $salary->houseRent = $data['houseRent'];
        $salary->medAllow = $data['medAllow'];
        $salary->conveyence = $data['conveyence'];
        $salary->gSalary = $data['gSalary'];

        $salary->allowPhr = 0; //ask monon
        $salary->allowAmount = $salary->allowPhr * 208;

        $salary->totalSalary = $salary->gSalary + $salary->allowAmount;

        $salary->otHour = $data['otHour'];
        $salary->festivalOT = $data['festivalOT'];
        $salary->otRate = $salary->basic / 208;
        $salary->festivalPayment = $salary->festivalOT * $salary->otRate;
        $salary->otAmount = $salary->otHour * $salary->otRate;
        $salary->otAllow = ($salary->otHour + $salary->festivalOT) * $salary->allowPhr;
        $salary->totalOT = $salary->festivalPayment + $salary->otAmount + $salary->otAllow;

        $salary->othersAdd = $data['othersAdd'];

        $salary->lwpHour = $data['lwpHour'];
        $salary->lwpRate = $salary->basic / 208;
        $salary->lwpAmount = $salary->lwpHour * $salary->lwpRate;
        $salary->lwpAllow = $salary->lwpHour * $salary->allowPhr;
        $salary->totalLwp = $salary->lwpAllow + $salary->lwpAmount;

        $salary->uaHour = $data['uaHour'];
        $salary->uaRate = $salary->basic / 208;
        $salary->uaAmount = $salary->uaHour * $salary->uaRate;
        $salary->uaAllow = $salary->uaHour * $salary->allowPhr;
        
        $salary->mealDeduct = $data['mealDeduct'];
        $salary->fineDeduct = $data['fineDeduct'];
        $salary->advanceDeduct = $data['advanceDeduct'];        

        $salary->othersDeduct = $data['othersDeduct'];

        $salary->payableSalary = $salary->totalSalary + $salary->totalOT + $salary->othersAdd 
                                - $salary->totalLwp - $salary->uaAmount - $salary->uaAllow 
                                - $salary->mealDeduct - $salary->fineDeduct - $salary->advanceDeduct
                                - $salary->othersDeduct - $salary->stamp;
                
        $salary->save();

        $salaries = Salary::where('year', $salary->year)
        ->where('month', $salary->month)
        ->orderBy('employeeNo','asc')
        ->get();

        return view('pages.accounts.salary.salarySheet',[
            'month' => $salary->month,
            'year' => $salary->year,
            'salaries' =>$salaries
            ]);
    }

    public function getDeleteSalary($id){

        $salary = Salary::find($id);
        $month = $salary->month;
        $year = $salary->year;

        $salary->delete();

        $count = Salary::where('year', $year)->where('month', $month)->count();

        if($count == 0){
            $today = Carbon::today();

            $startYear = date("Y", strtotime($today));

            $years[0] = $startYear;

            $nextYear = $today->subYear();

            for ($i=0; $i < 30; $i++) {
                $year = date("Y", strtotime($nextYear));
                $years[$i+1] = $year;
                $nextYear = $today->subYear();
            }

            return view('pages.accounts.salary.pickMonthYear',[
                'years' => $years
                ]);
        }


        $salaries = Salary::where('year', $year)
        ->where('month', $month)
        ->orderBy('employeeNo','asc')
        ->get();

        return view('pages.accounts.salary.salarySheet',[
            'month' => $month,
            'year' => $year,
            'salaries' =>$salaries
            ]);
    }

    //reports
    public function postViewSalaries(Request $request){
        $data = $request->all();

        $count = Salary::where('year', $data['year'])->where('month', $data['month'])->count();
        
        if($count == 0){
            return "No data found, Check";
        }

        $salaries = Salary::where('year', $data['year'])
        ->where('month', $data['month'])
        ->orderBy('employeeNo','asc')
        ->get();

        $total = new Salary;

        foreach($salaries as $salary){
            $total->basic = $total->basic + $salary->basic;
            $total->houseRent = $total->houseRent + $salary->houseRent;
            $total->medAllow = $total->medAllow + $salary->medAllow;
            $total->conveyence = $total->conveyence + $salary->conveyence;
            $total->gSalary = $total->gSalary + $salary->gSalary;
            
            $total->otHour = $total->otHour + $salary->otHour;
            $total->festivalOT = $total->festivalOT + $salary->festivalOT;
            $total->otRate = $total->otRate + $salary->otRate;
            $total->festivalPayment = $total->festivalPayment + $salary->festivalPayment;
            $total->otAmount = $total->otAmount + $salary->otAmount;
            $total->otAllow = $total->otAllow + $salary->otAllow;
            $total->totalOT = $total->totalOT + $salary->totalOT;

            $total->othersAdd = $total->othersAdd + $salary->othersAdd;
            
            $total->lwpHour = $total->lwpHour + $salary->lwpHour;
            $total->lwpRate = $total->lwpRate + $salary->lwpRate;
            $total->lwpAmount = $total->lwpAmount + $salary->lwpAmount;
            $total->lwpAllow = $total->lwpAllow + $salary->lwpAllow;
            $total->totalLwp = $total->totalLwp + $salary->totalLwp;

            $total->uaHour = $total->uaHour + $salary->uaHour;
            $total->uaRate = $total->uaRate + $salary->uaRate;
            $total->uaAmount = $total->uaAmount + $salary->uaAmount;
            $total->uaAllow = $total->uaAllow + $salary->uaAllow;

            $total->mealDeduct = $total->mealDeduct + $salary->mealDeduct;
            $total->fineDeduct = $total->fineDeduct + $salary->fineDeduct;
            $total->advanceDeduct = $total->advanceDeduct + $salary->advanceDeduct;

            $total->othersDeduct = $total->othersDeduct + $salary->othersDeduct;

            $total->stamp = $total->stamp + $salary->stamp;

            $total->payableSalary = $total->payableSalary + $salary->payableSalary;            
        }

        //return $salaries;

        /*return view('pages.accounts.salary.salaryReport',[
                'salaries' => $salaries,
                'total' => $total
        ]);*/
        
        $pdf = PDF::loadView('pages.accounts.salary.salaryReport',[
                'salaries' => $salaries,
                'total' => $total
        ]);

        $size = "legal"; //array(0,0,612.00,1008.00);
        $orientation = "landscape";

        $pdf->setPaper($size, $orientation);
        
        ini_set('max_execution_time', 300);

        return $pdf->stream('Salary_Sheet.pdf');

        /*return view('pages.accounts.salary.salaryReport',[
                'salaries' => $salaries
                ]);*/
    }
}