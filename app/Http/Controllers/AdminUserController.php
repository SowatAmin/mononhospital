<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Users;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Controllers\Controller;

class AdminUserController extends Controller
{
    public function index(){
        $tableData = Users::orderBy('id','asc')->get();
        return view('pages.admin.users.home',['tableData' => $tableData]);
    }
    public function create(){
        return view('pages.admin.users.create');
    }
    
    public function store(Request $request){
        $input = $request->all();
        $input['password'] = \Hash::make($input['password']);

        Users:: create($input);

        return redirect('users')->with('status','New User has been added successfully.');    	
    }
    
    public function show(){
        
    }

    public function edit($id){
        $user = Users::find($id);
        return \View::make('users')->with('user', $user);
    }

    public function update($id){

        $rules = array(
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'username' => 'required|unique',
            'password' => 'required',
            'role'  => 'required',
            );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            redirect('pages.admin.users')
            ->withErrors($validator)
            ->withInput(Input::except('password'))
            ->with('status','Edit Not Successful.');
        }
        else {
            // store
            $user = User::find($id);
            $user->fname = Input::get('fname');
            $user->lname = Input::get('lname');
            $user->email = Input::get('email');
            $user->username = Input::get('username');
            $user->password = Input::get('password');
            $user->password = \Hash::make($user->password);
            $user->role = Input::get('role');
            $user->save();
            return redirect('pages.admin.users')->with('status','Edit successful.');
        }
            

    }

    public function destroy($id){
        $user = Users::find($id);
        $user->delete();
        
        $tableData = Users::orderBy('id','asc')->get();
        
        return view('pages.admin.users.home',['tableData' => $tableData]);
        
    }

    
}
