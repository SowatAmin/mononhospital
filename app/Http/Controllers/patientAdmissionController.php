<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Validator;

use App\Models\PatientInfo;
use App\Models\VisitorInfo;
use App\Models\Prescription;
use App\Models\InvestigationTbl;
use App\Models\Bed;
use App\Models\Diagnosis;
use App\Models\serviceMasterCategory;
use App\Models\serviceCategory;
use App\Models\Doctors;
use App\Models\Manager;
use App\Models\Bill;
use App\Models\DailyRentAndCharge;
use App\Models\patientPaymentHistory;
use App\Models\patientDiscount;

class patientAdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user()->username;
        $patientid=PatientInfo::all()->last();
        $diagnosis=Diagnosis::all(['name']);
        $mastercatagories=serviceMasterCategory::all();
        $subcategoires=serviceCategory::all();
        $mos=Doctors::select('name')->where('DocDesignation','=',"M/O")->orWhere('DocTitle','=',"M/O")->orderBy('name', 'asc')->get();
        $consultants=Doctors::all('name');
        $managers=Manager::all('name');
        $beds=Bed::select('name')->get();

        return view('pages.administration.patientAdmission.home',compact('user','diagnosis','mastercatagories','subcategoires','patientid','mos','consultants','managers','beds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $patients=PatientInfo::all();
        
        return view('pages.administration.patientAdmission.create',compact('patients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $personal=$request->all();
        //$visitor=$request->all();
        $prescription=$request->all();
        $investigations=$request->all();

        if (Input::has('visitor')) {
                // checked
            $personal['visitor'] = 1;
        }
        else {
                // unchecked
            $personal['visitor'] = 0;
        }

        if (Input::has('incomingCall')) {
                // checked
            $personal['incomingCall'] = 1;
        }
        else {
                // unchecked
            $personal['incomingCall'] = 0;
        }

        if (Input::has('outgoingCall')) {
                // checked
            $personal['outgoingCall'] = 1;
        }
        else {
                // unchecked
            $personal['outgoingCall'] = 0;
        }

        if (Input::has('extraFood')) {
                // checked
            $personal['extraFood'] = 1;
        }
        else {
                // unchecked
            $personal['extraFood'] = 0;
        }

        if (Input::has('cigarette')) {
                // checked
            $personal['cigarette'] = 1;
        }
        else {
                // unchecked
            $personal['cigarette'] = 0;
        }

        if (Input::has('policeCase')) {
                // checked
            $personal['policeCase'] = 1;
        }
        else {
                // unchecked
            $personal['policeCase'] = 0;
        }


        
        $bedcabin=$personal['bedCabin'];
        $pstatus=$personal['admissionStatus'];
        $ppid=$personal['id'];

        
        
        if($pstatus=="Admitted")
            {
        

         DB::table('beds')
                ->where('name','=',$bedcabin)
                ->update([
                    'Status' => "Occupied",
                    'RegNo' => $ppid
                    ]);

        
    
        }
    
     
        
             
            
        


        PatientInfo::create($personal);
        //VisitorInfo::create($visitor);
        Prescription::create($prescription);
        //Investigation::create($investigations);




        return redirect('/patientAdmission/'.$personal['id'].'/edit');
        //return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \Auth::user()->username;
        $bills = Bill::where('masterCategory', '1')
        ->orWhere('masterCategory', '2')
        ->orWhere('masterCategory', '3')
        ->orWhere('masterCategory', '4')
        ->get();

        foreach ($bills as $bill) {
            if($bill->masterCategory == '1'){
                $bill->masterCategory = "Bed & Cabin Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '2'){
                $bill->masterCategory = "Consultant Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '3'){
                $bill->masterCategory = "Hospital Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
            else if($bill->masterCategory == '4'){
                $bill->masterCategory = "Non Profitable Services";
                $updatedBill = Bill::find($bill->id);
                $updatedBill = $bill;
                $updatedBill->save();
            }
        }

        if($id){
            if(is_numeric($id)){
                $counter = patientInfo::where('id',$id)->count();

                if($counter == 0){
                    //need to add alert to patientAdmission homepage to use this
                    $alert = "No data was been found for ID = ".$id.", please check if you have input the correct Reg No.";
                    return back()->withInput()->with('alert',$alert);
                }
                
                $patient = patientInfo::where('id',$id)->get()->last();
                $diagnosis=Diagnosis::all(['name']);
                $mastercatagories=serviceMasterCategory::all();
                $subcategoires=serviceCategory::all();
                $mos=Doctors::select('name')->where('DocDesignation','=',"M/O")->orWhere('DocTitle','=',"M/O")->orderBy('name', 'asc')->get();
                $consultants=Doctors::all('name');
                $managers=Manager::all('name');
                $beds=Bed::select('name')->get();
                $visitors = VisitorInfo::where('pid',$id)->get();
                $dailyRents = DailyRentAndCharge::where('pid',$id)->get();                
                $bills = Bill::where('regNo',$id)->get();
                $investigations = InvestigationTbl::where('pid',$id)->get();
                $prescription = Prescription::where('id',$id)->get()->last();
                
                if(!$prescription){
                    $prescription = new Prescription;
                }


                $nonProfitBills = Bill::selectRaw('rate,quantity, vat, serviceCharge')
                ->where('regNo', $id)
                ->where('masterCategory', 'Non Profitable Services')
                ->get();

                $consultantBills = Bill::selectRaw('rate,quantity, vat, serviceCharge')
                ->where('regNo', $id)
                ->where('masterCategory', 'Consultant Services')
                ->get();

                $hospitalServices = Bill::selectRaw('rate,quantity, vat, serviceCharge')
                ->where('regNo', $id)
                ->where('masterCategory', 'Bed & Cabin Services')
                ->orWhere('masterCategory', 'Hospital Services')
                
                ->get();

                $hospitalServicesTotal = 0;
                $consultantTotal = 0;
                $nonProfitTotal = 0;

                foreach ($bills as $bill){
                    if($bill->masterCategory == 'Bed & Cabin Services' or $bill->masterCategory == 'Hospital Services'){
                        $temp = $bill->rate * $bill->quantity;
                        $vat = $temp * $bill->vat / 100;
                        $sc = $temp * $bill->serviceCharge / 100;
                        $hospitalServicesTotal = $hospitalServicesTotal + $temp + $vat +$sc;
                    }
                    else if ($bill->masterCategory == 'Consultant Services'){
                        $temp = $bill->rate * $bill->quantity;
                        $vat = $temp * $bill->vat / 100;
                        $sc = $temp * $bill->serviceCharge / 100;
                        $consultantTotal = $consultantTotal + $temp + $vat +$sc;
                    }
                    else if ($bill->masterCategory == 'Non Profitable Services'){
                        $temp = $bill->rate * $bill->quantity;
                        $vat = $temp * $bill->vat / 100;
                        $sc = $temp * $bill->serviceCharge / 100;
                        $nonProfitTotal = $nonProfitTotal + $temp + $vat +$sc;
                    }
                }

                $total = $nonProfitTotal + $consultantTotal + $hospitalServicesTotal;

                $payment = patientPaymentHistory::selectRaw('sum(payment) totalPayment')
                ->where('regNo', $id)
                ->get()
                ->last();

                if(!$payment->totalPayment){
                    $payment = 0;
                }
                else{
                    $payment = $payment->totalPayment;
                }

                $discount = patientDiscount::selectRaw('sum(amount) totalAmount')
                ->where('regNo', $id)
                ->get()
                ->last();

                if(!$discount->totalAmount){
                    $discount = 0;
                }
                else{
                    $discount = $discount->totalAmount;
                }

                $due = $total - $discount - $payment;


                return view('pages.administration.patientAdmission.edit',[
                    'user'=>$user,
                    'patient' => $patient,
                    'diagnosis' => $diagnosis,
                    'mastercatagories' => $mastercatagories,
                    'subcategoires' => $subcategoires,
                    'mos' => $mos,
                    'consultants' => $consultants,
                    'managers' => $managers,
                    'beds' => $beds,                    
                    'bills' => $bills,
                    'dailyRents' => $dailyRents,
                    'investigations' => $investigations,
                    'prescription' => $prescription,
                    'visitors' => $visitors,
                    'nonProfitBill' => $nonProfitTotal,
                    'consultantBill' => $consultantTotal,
                    'hospitalServices' => $hospitalServicesTotal,
                    'payment' => $payment,
                    'discount' => $discount,
                    'due' => $due,
                    'total' => $total
                    ]);

                //return view('pages.administration.patientAdmission.edit',compact('diagnosis','mastercatagories','subcategoires','patient','mos','consultants','managers','beds'));
}
else{
    $alert = "Data input in Reg No. = ".$id.". It is not a number, please input a number";
    return back()->with('alert',$alert);
}
}
else{
    $alert = "Cannot search for empty data, please check if you have input the correct Reg No.";
    return back()->with('alert',$alert);
}
        //return view('pages.administration.patientAdmission.edit');
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $rules =[
        'name' => 'required',
        'bedCabin',
        'admissionStatus',
        'age' => 'required|numeric',
        'sex',
        'maritalStatus',
        'presentAddress',
        'permanentAddress',
        'nationality',
        'religion',
        'bloodGroup',
        'profession',
        'guardianName' => 'required',
        'guardianRel' => 'required',
        'guardianPhone' => 'required|numeric',
        'altGuardianName' => 'required',
        'altGuardianRel' => 'required',
        'altGuardianAddress' => 'required',
        'altGuardianPhone' => 'required|numeric',
        'assignedConsultant' => 'required',
        'moOnDuty',
        'manager',
        'diagnosis',
        'bookingDate',
        'bookingTime',
        'admissionDate',
        'admissionTime',
        'releaseDate',
        'releaseTime',
        'dischargeBy',
        'visitor',
        'incomingCall',
        'outgoingCall',
        'extraFood',
        'cigarette',
        'policeCase'
        ];

        //$validator = Validator::make(Input::all(), $rules);

        // process the login
        //if ($validator->fails()){
        //    return back()->withErrors($validator);
            //return Redirect::to('nerds/' . $id . '/edit')->withErrors($validator)->withInput(Input::except('password'));
        //} else {

            // store

            $patient = PatientInfo::find($id);

            $patient->name = Input::get('name');
            $patient->bedCabin = Input::get('bedCabin');
            $patient->age = Input::get('age');
            $patient->sex = Input::get('sex');
            $patient->maritalStatus = Input::get('maritalStatus');
            $patient->presentAddress = Input::get('presentAddress');
            $patient->permanentAddress = Input::get('permanentAddress');
            $patient->nationality = Input::get('nationality');
            $patient->religion = Input::get('religion');
            $patient->bloodGroup = Input::get('bloodGroup');
            $patient->profession = Input::get('profession');
            $patient->guardianName = Input::get('guardianName');
            $patient->guardianRel = Input::get('guardianRel');
            $patient->guardianPhone = Input::get('guardianPhone');
            $patient->altGuardianName = Input::get('altGuardianName');
            $patient->altGuardianRel = Input::get('altGuardianRel'); 
            $patient->altGuardianAddress = Input::get('altGuardianAddress');
            $patient->altGuardianPhone = Input::get('altGuardianPhone');
            $patient->assignedConsultant = Input::get('assignedConsultant');
            $patient->moOnDuty = Input::get('moOnDuty');
            $patient->manager = Input::get('manager');
            $patient->diagnosis = Input::get('diagnosis');
            $patient->bookingDate = Input::get('bookingDate');
            $patient->bookingTime = Input::get('bookingTime');
            $patient->admissionDate = Input::get('admissionDate');
            $patient->admissionTime = Input::get('admissionTime');
            $patient->releaseDate = Input::get('releaseDate');
            $patient->releaseTime = Input::get('releaseTime');
            $patient->dischargeBy = Input::get('dischargeBy');
            $patient->education = Input::get('education');
            $patient->dob= Input::get('dob');
            $patient->drugabuse= Input::get('drugabuse');
            $patient->illness= Input::get('illness');

            
            $prescription = Prescription::find($id);

            $prescription->tda = Input::get('tda');
            $prescription->investigation = Input::get('investigation');
            $prescription->notes = Input::get('notes');
            $prescription->treatment=Input::get('treatment');
            $prescription->save();

            if (Input::has('visitor')) {
                // checked
                $patient->visitor = 1;
            }
            else {
                // unchecked
                $patient->visitor = 0;
            }

            if (Input::has('incomingCall')) {
                // checked
                $patient->incomingCall = 1;
            }
            else {
                // unchecked
                $patient->incomingCall = 0;
            }

            if (Input::has('outgoingCall')) {
                // checked
                $patient->outgoingCall = 1;
            }
            else {
                // unchecked
                $patient->outgoingCall = 0;
            }

            if (Input::has('extraFood')) {
                // checked
                $patient->extraFood = 1;
            }
            else {
                // unchecked
                $patient->extraFood = 0;
            }

            if (Input::has('cigarette')) {
                // checked
                $patient->cigarette = 1;
            }
            else {
                // unchecked
                $patient->cigarette = 0;
            }

            if (Input::has('policeCase')) {
                // checked
                $patient->policeCase = 1;
            }
            else {
                // unchecked
                $patient->policeCase = 0;
            }

            $patient->save();


            



            
                        // redirect
            //return Redirect::to('nerds');
            return back()->withInput();
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function savevisitor()
    {

        if(Request::ajax()) {
            $x=Input::all();
            VisitorInfo::create($x);
            return "hello";
        }
    }
}