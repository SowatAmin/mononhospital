<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Validator;

use App\Models\Doctors;

class doctorsAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctors::get();
        return view('pages.admin.doctors.home',[
                'doctors' => $doctors
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $specializations = Doctors::selectRaw('DocSpecialization')
        ->groupBy('DocSpecialization')
        ->orderBy('DocSpecialization', 'asc')
        ->get();
        
        $designations = Doctors::selectRaw('DocDesignation')
        ->groupBy('DocDesignation')
        ->orderBy('DocDesignation', 'asc')
        ->get();

        return view('pages.admin.doctors.create',[
            'specializations' => $specializations,
            'designations' => $designations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        //return $data;
        $doctor['name'] = $data['name'];
        $doctor['DocTitle'] = $data['DocTitle'];
        $doctor['DocAddress'] = $data['DocAddress'];
        $doctor['DocPhone'] = $data['DocPhone'];
        $doctor['DocSpecialization'] = $data['DocSpecialization'];
        $doctor['DocDesignation'] = $data['DocDesignation'];
        $doctor['Rate'] = $data['Rate'];
        $doctor['VatRate'] = $data['VatRate'];
        $doctor['ServiceChargeRate'] = $data['ServiceChargeRate'];

        Doctors::create($doctor);

        $doctors = Doctors::get();
        return view('pages.admin.doctors.home',[
                'doctors' => $doctors
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctor = Doctors::where('id',$id)->get()->last();
        
        $specializations = Doctors::selectRaw('DocSpecialization')
        ->groupBy('DocSpecialization')
        ->orderBy('DocSpecialization', 'asc')
        ->get();
        
        $designations = Doctors::selectRaw('DocDesignation')
        ->groupBy('DocDesignation')
        ->orderBy('DocDesignation', 'asc')
        ->get();

        return view('pages.admin.doctors.edit',[
            'doctor' => $doctor,
            'specializations' => $specializations,
            'designations' => $designations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $doctor = Doctors::find($id);

        $doctor->name = Input::get('name');
        $doctor->DocTitle = Input::get('DocTitle');
        $doctor->DocAddress = Input::get('DocAddress');
        $doctor->DocPhone = Input::get('DocPhone');
        $doctor->DocSpecialization = Input::get('DocSpecialization');
        $doctor->DocDesignation = Input::get('DocDesignation');
        $doctor->Rate = Input::get('Rate');
        $doctor->VatRate = Input::get('VatRate');
        $doctor->ServiceChargeRate = Input::get('ServiceChargeRate');

        $doctor->save();
        
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor = Doctors::find($id);
        $doctor->delete();

        // redirect
        
        $doctors = Doctors::get();
        return view('pages.admin.doctors.home',[
                'doctors' => $doctors
            ]);
    }
}
