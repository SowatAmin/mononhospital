<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Voucher;
use App\Models\VoucherType;
use App\Models\AccountType;
use Carbon\Carbon;
use PDF;



class voucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voucher=Voucher::all()->last();
        //return $voucher;


        $vouchertype=VoucherType::all();
        $accounttype=AccountType::selectRaw('parentHead')->groupBy('parentHead')->get();

        
        
        return view('pages.accounts.voucher.home',compact('voucher','vouchertype','accounttype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $today = Carbon::today();
        $today = $today->toDateString();
        

        $vouchers = Voucher::where('date', $today)->get();
        return view('pages.accounts.voucher.create',[
            'vouchers' => $vouchers
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,$words)
    {
        //$voucher = $request->all();
        //Voucher::create($voucher);



        $words=ucfirst($words);
        return $id;





        

        //return view('pages.accounts.voucher.voucherReport',compact('id','date','accountHead','amount','available_cash_bank','description','words'));
        $pdf = PDF::loadView('pages.accounts.voucher.voucherReport',compact('id','date','accountHead','amount','available_cash_bank','description','words'));
        return $pdf->stream('VoucherReport.pdf');

        return back();

        $url = '/voucher/'.$id.'/edit';
        
        return redirect($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        /*$voucher = Voucher::where('voucherNumber', $id)->get()->last();
        return "Voucher";*/

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $voucher = Voucher::where('voucherNumber', $id)->get()->last();
        $vouchertype=VoucherType::all();
        $accounttype=AccountType::selectRaw('parentHead')->groupBy('parentHead')->get();


        return view('pages.accounts.voucher.edit',[
            'voucher'    => $voucher,
            'vouchertype'=>$vouchertype,
            'accounttype'=>$accounttype               

            ]);
    }



    //admin edit 

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $voucher = Voucher::where('voucherNumber', $id)->get()->last();
        $data = $request->all();

        $voucher->voucherNumber = $data['voucherNumber'];
        $voucher->voucherType = $data['voucherType'];
        $voucher->parentHead = $data['parentHead'];
        $voucher->accountHead = $data['accountHead'];
        $voucher->date = $data['date'];
        $voucher->paymentMode = $data['paymentMode'];
        $voucher->available_cash_bank = $data['available_cash_bank'];
        $voucher->amount = $data['amount'];
        $voucher->description = $data['description'];

        $voucher->save();

        return back()->withInput();
    }


    public function report(Request $request){
     $data = $request->all();
     $from=$request->input('fromDate');
     $to=$request->input('toDate');


     if($data['fromDate'] == "" or $data['toDate'] == ""){
        $alert = "Cannot find data with empty data fields, please check input";
        return back()->withInput()->with('alert',$alert);
    }

    $vouchers=Voucher::whereBetween('Date', array($data['fromDate'], $data['toDate']))->get();
    $total=Voucher::whereBetween('Date', array($data['fromDate'], $data['toDate']))->sum('amount');
    $pdf = PDF::loadView('pages.accounts.voucher.CustomReports',compact('vouchers','from','to','total'));
    //return view('pages.accounts.voucher.CustomReports',compact('vouchers','from','to','total'));
    return $pdf->stream('VoucherReport.pdf');

}

public function view()
{
    $vouchers=Voucher::all();
    return view('pages.accounts.voucher.viewVoucher',compact('vouchers'));
}

public function invoice($id)
{
    $voucher=Voucher::where('voucherNumber',$id)->get()->last();


    $pdf = PDF::loadView('pages.accounts.voucher.voucherReport',compact('voucher'));
    //return view('pages.accounts.voucher.voucherReport',compact('voucher'));
    return $pdf->stream('VoucherReport.pdf');

}





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
public function Adminview()
{
    $vouchers=Voucher::all();
    return view('pages.admin.voucher.voucherlist',compact('vouchers'));
}


    public function destroy($id)
    {
        $voucher = Voucher::where('voucherNumber', $id)->get()->last();

        $voucher->delete();

        $today = Carbon::today();
        $today = $today->toDateString();

        $vouchers = Voucher::where('date', $today)->get();
        return view('pages.accounts.voucher.create',[
            'vouchers' => $vouchers
            ]);
    }

}
