<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeDesignation;


class employeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeId=Employee::all()->last();
        $designation=EmployeeDesignation::selectRaw('designation')->get();
       

        return view('pages.accounts.employee.home',compact('employeeId','designation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee=Employee::paginate(15);
       
        //return $employee;


         return view('pages.accounts.employee.create',compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee=$request->all();

        Employee::create($employee);

        return redirect('/employee');;
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee=Employee::where('employeeNo',$id)->get()->last();
        $designation=EmployeeDesignation::selectRaw('designation')->get();
        //return $designation;
        //return $employee;
        /*return view('pages.accounts.employee.edit',[
            'employee'=>$employee
            ]);*/
     return view('pages.accounts.employee.edit',compact('employee','designation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::where('employeeNo', $id)->get()->last();
        $data = $request->all();

        $employee->name = $data['name'];
        $employee->fathersName = $data['fathersName'];
        $employee->sex = $data['sex'];
        $employee->maritalStatus = $data['maritalStatus'];
        $employee->dob = $data['dob'];
        $employee->jDate = $data['jDate'];
        $employee->address = $data['address'];
        $employee->designation = $data['designation'];
        $employee->grade = $data['grade'];
        $employee->payType = $data['payType'];
        $employee->oId = $data['oId'];

        $employee->basic = $data['basic'];
        $employee->houseRent = $data['houseRent'];
        $employee->medAllow = $data['medAllow'];
        $employee->conveyence = $data['conveyence'];
        $employee->gSalary = $data['gSalary'];

        if (Input::has('active')) {
                // checked
            $employee->active = 1;
        }
        else {
                // unchecked
            $employee->active = 0;
        }

        if (Input::has('bonus')) {
                // checked
            $employee->bonus = 1;
        }
        else {
                // unchecked
            $employee->bonus = 0;
        }

        if (Input::has('pf')) {
                // checked
            $employee->pf = 1;
        }
        else {
                // unchecked
            $employee->pf = 0;
        }

        if (Input::has('ot')) {
                // checked
            $employee->ot = 1;
        }
        else {
                // unchecked
            $employee->ot = 0;
        }

        if (Input::has('attBonus')) {
                // checked
            $employee->attBonus = 1;
        }
        else {
                // unchecked
            $employee->attBonus = 0;
        }

        $employee->save();

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    
    public function destroy($id)
    {
        $employee = Employee::where('employeeNo', $id)->get()->last();

        $voucher->delete();

        //riderect to create page
    }
}
