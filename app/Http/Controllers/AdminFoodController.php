<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFoodGroupRequest;
use App\Http\Requests\StoreFoodItemRequest;
use App\Http\Requests\StoreDailyChartRequest;
use App\Http\Requests\StoreFoodClientRequest;
use App\Http\Requests\UpdateFoodItemRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\AddFoodGroup;
use App\Models\FoodList;
use App\Models\DailyChart;
use App\Models\FoodClient;
use App\Models\FoodKeyword;

class AdminFoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    //Start Methods for food group
    public function getIndex(){
        return view('pages.admin.food.home');
    }
    public function getFoodGroup(){    
        
        return view('pages.admin.food.foodgroup.home');
    }

    public function getAddFoodGroup(){
        return view('pages.admin.food.foodgroup.addfoodgroup');
    }

    public function getShowAllGroup(){
        $foodGroupHistory = AddFoodGroup::orderBy('NameOfGroup','asc')
                                ->get();
        return view('pages.admin.food.foodgroup.showallgroup',['foodGroupHistories' => $foodGroupHistory]);
    }

    public function postFoodGroup(StoreFoodGroupRequest $request){
        $foodgroup = new AddFoodGroup;
        $foodgroup->NameOfGroup = $request->foodgroupname;

        $foodgroup->save();

        return redirect()->back()->with('status','New food group is added successfully.');
    }

    public function getEdit($id){
        $info = AddFoodGroup::find($id);
        return view('pages.admin.food.foodgroup.edit',['groupName' => $info]);
    }

    public function postUpdate($id, StoreFoodGroupRequest $request){
        $update = AddFoodGroup::find($id);
        $update->NameOfGroup = $request->foodgroupname;
        $update->save();

        return redirect('foodadminpart/show-all-group')->with('status','Food group name is updated successfully.');
    }

    public function getDelete($id){
        $info = AddFoodGroup::find($id);        
        $info->delete();        
        return redirect()->back()->with('status','The Group has been deleted successfully!!');
    }
    //End methods for food group
    

    //Start methods for food menu
    public function getFoodItem(){        
        return view('pages.admin.food.fooditem.home');
    }

    public function getSelectDay(){
        return view('pages.admin.food.fooditem.selectday');
    }

    public function getCreateItem($day){

        $category = AddFoodGroup::orderBy('NameOfGroup')
                        ->get();
        $keyword = FoodKeyword::all();

        return view('pages.admin.food.fooditem.createitem',['day' => $day,'category' => $category,'keywords'=>$keyword]);
    }

    public function postFoodItem(StoreFoodItemRequest $request){        
        $foodlist = new FoodList;

        $foodlist->NameOfItem = $request->itemname;
        $foodlist->DayFor = $request->day;
        $foodlist->Quantity = $request->qty;
        $foodlist->HowManyTimes = $request->hmt;
        $foodlist->formula = $request->df;
        $foodlist->CategoryId = $request->itemCategory;

        $foodlist->save();

        return redirect()->back()->with('status','New food item is added successfully.');
    }

    public function getShowAllItem(){
        //$collectFoodListInfo = new FoodList;
        $foodlist = DB::table('foodlists')
                        ->join('foodgroup','foodlists.CategoryId','=','foodgroup.id')
                        ->select('foodlists.*','foodgroup.NameOfGroup')
                        ->orderBy('foodlists.DayFor')
                        ->get();
        return view('pages.admin.food.fooditem.showallitem',['foodlists' => $foodlist]);
    }

    public function getItemEditInfo($id){
        $itemInfo = Foodlist::find($id);
        $category = AddFoodGroup::orderBy('NameOfGroup')
                        ->get();
        $keyword = FoodKeyword::all();
        return view('pages.admin.food.fooditem.edit',['itemInfo' => $itemInfo, 'category' => $category, 'keywords' => $keyword]);
    }

    public function postUpdateFoodItem($id, UpdateFoodItemRequest $request){
        $checkExistence = Foodlist::where('NameOfItem',$request->itemname)
                                    ->where('DayFor',$request->day)
                                    ->where('id','<>',$id)
                                    ->count();
        if($checkExistence > 0){
            return redirect()->back()->with('alert','Your updated info is already exist!');
        }
        $itemUpdate = Foodlist::find($id);
        $itemUpdate->NameOfItem = $request->itemname;
        $itemUpdate->DayFor = $request->day;
        $itemUpdate->Quantity = $request->qty;
        $itemUpdate->HowManyTimes = $request->hmt;
        $itemUpdate->formula = $request->formula;
        $itemUpdate->CategoryId = $request->itemCategory;
        $itemUpdate->save();

        return redirect('foodadminpart/show-all-item')->with('status','The item has been updated successfully!');
    }

    public function getDeleteFoodItem($id){
        $info = Foodlist::find($id);
        $info->delete();

        return redirect('foodadminpart/show-all-item')->with('status','Item deleted successfully!');
    }
}
