<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Validator;

use App\Models\PatientInfo;
use App\Models\patientDiscount;
use App\Models\Doctors;

class patientDiscountController extends Controller
{
    public function getHome($id){
        $patient = patientInfo::where('id',$id)->get()->last();
        $discounts = patientDiscount::where('regNo', $id)->get();       

        return view('pages.administration.patientAdmission.discounts.home',[
            'patient' => $patient,
            'discounts' => $discounts
            ]);
    }

    public function getCreatePatientDiscount($id){
        $doctors = Doctors::all(['name']);

        return view('pages.administration.patientAdmission.discounts.create',[
            'id' => $id,
            'doctors' => $doctors
            ]);
    }

    public function postPatientDiscount(Request $request){
        $user = \Auth::user()->username;
        $discount = $request->all();

        $patient = patientInfo::where('id',$discount['regNo'])->get()->last();

        $discount['bedCabin'] = $patient['bedCabin'];
        $discount['discountBy'] = $user;

        patientDiscount::create($discount);  

        $discounts = patientDiscount::where('regNo', $discount['regNo'])->get();     

        return view('pages.administration.patientAdmission.discounts.home',[
            'patient' => $patient,
            'discounts' => $discounts
            ]);
    }

    public function getEditPatientDiscount($id){
        $discount = patientDiscount::where('id', $id)->get()->last();
        $doctors = Doctors::all(['name']);

        return view('pages.administration.patientAdmission.discounts.edit',[
            'discount' => $discount,
            'doctors' => $doctors
            ]);
    }

    public function postUpdate(Request $request){
        $rules =[
        'amount' => 'required|numeric'
        ];

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()){
            return back()->withErrors($validator);
        }
        else {
            $user = \Auth::user()->username;
            $discount = $request->all();

            $patient = patientInfo::where('id',$discount['regNo'])->get()->last();

            $discount['bedCabin'] = $patient['bedCabin'];
            $discount['discountBy'] = $user;

            $newDiscount = patientDiscount::find($discount['id']);

            $newDiscount->date = $discount['date'];
            $newDiscount->desc = $discount['desc'];
            $newDiscount->amount = $discount['amount'];
            $newDiscount->regNo = $discount['regNo'];
            $newDiscount->bedCabin = $patient['bedCabin'];
            $newDiscount->discountBy = $discount['discountBy'];
            $newDiscount->refBy = $discount['refBy'];

            $newDiscount->save();

            $discounts = patientDiscount::where('regNo', $discount['regNo'])->get();     

            return view('pages.administration.patientAdmission.discounts.home',[
                'patient' => $patient,
                'discounts' => $discounts
                ]);
        }
    }

    public function getDeletePatientDiscount($id){
        $discount = patientDiscount::find($id);
        $discount->delete();

        $patient = patientInfo::where('id',$discount['regNo'])->get()->last();
        $discounts = patientDiscount::where('regNo', $discount['regNo'])->get();     

            return view('pages.administration.patientAdmission.discounts.home',[
                'patient' => $patient,
                'discounts' => $discounts
                ]);
    }
}
