<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Models\AccountType;
use App\Models\Journal;
use App\Models\Journal_data;
use PDF;



class journalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$vouchertype=VoucherType::all();
        $id=Journal::all('id')->last();

        $accounttype=AccountType::selectRaw('parentHead')->groupBy('parentHead')->get();

        return view('pages.accounts.journal.home',compact('accounttype','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $journal=$request->all();
        //return $journal;
        Journal::create($journal);
        return redirect('/journal/'.$journal['journalNo'].'/edit');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
     $journals=Journal::all();
       //return $journals;
     return view('pages.accounts.journal.create',compact('journals'));
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $journal=Journal::where('id',$id)->get()->last();
        
        $journal_datas=Journal_data::where('jid',$id)->get();
        $accounttype=AccountType::selectRaw('parentHead')->groupBy('parentHead')->get();
        $credit_total=Journal_data::where('jid',$id)->sum('credit');
        $debit_total=Journal_data::where('jid',$id)->sum('debit');



        return view('pages.accounts.journal.edit',compact('journal','journal_datas','accounttype','credit_total','debit_total'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $journal = Journal::where('id', $id)->get()->last();
        $data = $request->all();
        $journal->source = $data['source'];
        $journal->date = $data['date'];
        $journal->particular = $data['particular'];
        $journal->save();
        return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function report($id){

       $journal=Journal::where('id',$id)->get()->last();

       $journal_datas=Journal_data::where('jid',$id)->get();
       $credit_total=Journal_data::where('jid',$id)->sum('credit');
       $debit_total=Journal_data::where('jid',$id)->sum('debit');
       //return view('pages.accounts.journal.reportJournal',compact('journal','journal_datas','credit_total','debit_total'));
        $pdf = PDF::loadView('pages.accounts.journal.reportJournal',compact('journal','journal_datas','credit_total','debit_total'));
        
        return $pdf->stream('Journal_Report.pdf');




   }

   public function destroy($id)
   {
        //
   }
}
