<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use PDF;
use App\Models\Voucher;
use App\Models\VoucherType;
use App\Models\AccountType;

class accountsReportsController extends Controller
{
	public function getHomepage(){
		return view('pages.accounts.reports.home');
	}


    public function postProfitAndLoss(Request $request){
    	$data = $request->all();

        $temps = AccountType::selectRaw('parentHead')->where('finalAccount', 'ProfitAndLoss Accounts')->groupBy('parentHead')->get();

        $allParentHeads = [];

        foreach ($temps as $i => $temp) {
            $allParentHeads[$i] = $temp->parentHead;
        }

        $voucherTypes = ['Debit','Credit'];

        $parentHeads = Voucher::selectRaw('parentHead')
        ->whereBetween('date', array($data['fromDate'], $data['toDate']))
        ->whereIn('parentHead', $allParentHeads)
        ->whereIn('voucherType', $voucherTypes)
        ->orderBy('parentHead','asc')
        ->groupBy('parentHead')
        ->get();

        $vouchers = Voucher::selectRaw('voucherNumber, voucherType, parentHead, accountHead, paymentMode, available_cash_bank, amount')
        ->whereBetween('date', array($data['fromDate'], $data['toDate']))
        ->whereIn('parentHead', $allParentHeads)
        ->whereIn('voucherType', $voucherTypes)
        ->orderBy('parentHead','asc')
        ->get();

        foreach ($vouchers as $voucher) {
            if($voucher->voucherType == "Debit"){
                if($voucher->available_cash_bank == "Islami Bank Krishibazar Branch"){
                    $voucher->debitIslamiBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Sonali Bank Krishibazar Branch"){
                    $voucher->debitSonaliBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "National Bank Limited"){
                    $voucher->debitNationalBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Cash in Hand"){
                    $voucher->debitCash = $voucher->amount;
                }
            }
            else if($voucher->voucherType == "Credit"){
                if($voucher->available_cash_bank == "Islami Bank Krishibazar Branch"){
                    $voucher->creditIslamiBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Sonali Bank Krishibazar Branch"){
                    $voucher->creditSonaliBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "National Bank Limited"){
                    $voucher->creditNationalBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Cash in Hand"){
                    $voucher->creditCash = $voucher->amount;
                }
            }
        }


        $total = (object) array('debit' => 0, 'credit' => 0);

        foreach ($parentHeads as $parentHead) {
            $parentHead->debit = 0;
            $parentHead->credit = 0;

            foreach ($vouchers as $voucher) {
                if($voucher->parentHead == $parentHead->parentHead){
                    if($voucher->voucherType == "Debit"){
                        $parentHead->debit = $parentHead->debit + $voucher->amount;
                    }
                    else if($voucher->voucherType == "Credit"){
                        $parentHead->credit = $parentHead->credit + $voucher->amount;
                    }
                }
            }

            $total->debit = $total->debit + $parentHead->debit;
            $total->credit = $total->credit + $parentHead->credit;
        }

        $data['fromDate'] = date("d-M-Y", strtotime($data['fromDate']));
        $data['toDate'] = date("d-M-Y", strtotime($data['toDate']));

        $pdf = PDF::loadView('pages.accounts.reports.profitAndLoss',[
            'parentHeads' => $parentHeads,
            'vouchers' => $vouchers,
            'total' => $total,
            'from' => $data['fromDate'],
            'to' => $data['toDate']
            ]);

        $size = "a4"; //array(0,0,612.00,1008.00);
        $orientation = "landscape";

        $pdf->setPaper($size, $orientation);

        return $pdf->stream('Profit_And_Loss.pdf');
    }



    public function postPaymentAndRecieved(Request $request){
        $data = $request->all();

        $voucherTypes = ['Payment','Received'];

        $parentHeads = Voucher::selectRaw('parentHead')
        ->whereBetween('date', array($data['fromDate'], $data['toDate']))
        ->whereIn('voucherType', $voucherTypes)
        ->orderBy('parentHead','asc')
        ->groupBy('parentHead')
        ->get();

        $vouchers = Voucher::selectRaw('voucherNumber, voucherType, parentHead, accountHead, paymentMode, available_cash_bank, amount')
        ->whereBetween('date', array($data['fromDate'], $data['toDate']))
        ->whereIn('voucherType', $voucherTypes)
        ->orderBy('parentHead','asc')
        ->get();

        foreach ($vouchers as $voucher) {
            if($voucher->voucherType == "Payment"){
                if($voucher->available_cash_bank == "Islami Bank Krishibazar Branch"){
                    $voucher->paymentIslamiBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Sonali Bank Krishibazar Branch"){
                    $voucher->paymentSonaliBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "National Bank Limited"){
                    $voucher->paymentNationalBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Cash in Hand"){
                    $voucher->paymentCash = $voucher->amount;
                }
            }
            else if($voucher->voucherType == "Received"){
                if($voucher->available_cash_bank == "Islami Bank Krishibazar Branch"){
                    $voucher->receivedIslamiBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Sonali Bank Krishibazar Branch"){
                    $voucher->receivedSonaliBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "National Bank Limited"){
                    $voucher->receivedNationalBank = $voucher->amount;
                }
                else if($voucher->available_cash_bank == "Cash in Hand"){
                    $voucher->receivedCash = $voucher->amount;
                }
            }
        }


        $total = (object) array('payment' => 0, 'received' => 0);

        foreach ($parentHeads as $parentHead) {
            $parentHead->payment = 0;
            $parentHead->received = 0;

            foreach ($vouchers as $voucher) {
                if($voucher->parentHead == $parentHead->parentHead){
                    if($voucher->voucherType == "Payment"){
                        $parentHead->payment = $parentHead->payment + $voucher->amount;
                    }
                    else if($voucher->voucherType == "Received"){
                        $parentHead->received = $parentHead->received + $voucher->amount;
                    }
                }
            }

            $total->payment = $total->payment + $parentHead->payment;
            $total->received = $total->received + $parentHead->received;
        }

        $data['fromDate'] = date("d-M-Y", strtotime($data['fromDate']));
        $data['toDate'] = date("d-M-Y", strtotime($data['toDate']));

        $pdf = PDF::loadView('pages.accounts.reports.paymentAndRecieved',[
            'parentHeads' => $parentHeads,
            'vouchers' => $vouchers,
            'total' => $total,
            'from' => $data['fromDate'],
            'to' => $data['toDate']
            ]);

        $size = "a4"; //array(0,0,612.00,1008.00);
        $orientation = "landscape";

        $pdf->setPaper($size, $orientation);

        return $pdf->stream('Payment_And_Recieved.pdf');
    }


    public function postBalanceSheet(Request $request){
        $data = $request->all();
        
        $accountTypes[0] = (object) array('type' => 'Assets', 'total' => 0);
        $accountTypes[1] = (object) array('type' => 'Liabilities', 'total' => 0);

        $temps = AccountType::selectRaw('type, parentHead')->where('finalAccount', 'Balance Sheet')->groupBy('parentHead')->get();
        
        $allParentHeads = [];

        foreach ($temps as $i => $temp) {
            $allParentHeads[$i] = $temp->parentHead;
        }

        $parentHeads = Voucher::selectRaw('parentHead')
        ->whereBetween('date', array($data['fromDate'], $data['toDate']))
        ->whereIn('parentHead', $allParentHeads)
        ->orderBy('parentHead','asc')
        ->groupBy('parentHead')
        ->get();

        $vouchers = Voucher::selectRaw('voucherNumber, parentHead, accountHead, paymentMode, available_cash_bank, sum(amount) as amountSum')
        ->whereBetween('date', array($data['fromDate'], $data['toDate']))
        ->whereIn('parentHead', $allParentHeads)
        ->orderBy('parentHead','asc')
        ->groupBy('accountHead')
        ->get();

        foreach ($vouchers as $voucher) {
            if($voucher->available_cash_bank == "Islami Bank Krishibazar Branch"){
                $voucher->islamiBank = $voucher->amountSum;
            }
            else if($voucher->available_cash_bank == "Sonali Bank Krishibazar Branch"){
                $voucher->sonaliBank = $voucher->amountSum;
            }
            else if($voucher->available_cash_bank == "National Bank Limited"){
                $voucher->nationalBank = $voucher->amountSum;
            }
            else if($voucher->available_cash_bank == "Cash in Hand"){
                $voucher->cash = $voucher->amountSum;
            }
        }

        $total = 0;

        foreach ($parentHeads as $parentHead) {
            $parentHead->total = 0;

            foreach ($vouchers as $voucher) {
                if($voucher->parentHead == $parentHead->parentHead){
                    $parentHead->total = $parentHead->total + $voucher->amountSum;
                }                
            }
            
            if($parentHead->parentHead == 'Cash Balance' || $parentHead->parentHead == 'Fixed Assets'){
                $parentHead->accType = "Assets";
                $accountTypes[0]->total = $accountTypes[0]->total + $parentHead->total;    
            }
            else if($parentHead->parentHead == 'ProfitAndLoss' || $parentHead->parentHead == 'Fixed Liabilities'){
                $parentHead->accType = "Liabilities";
                $accountTypes[1]->total = $accountTypes[1]->total + $parentHead->total;    
            }

            $total = $total + $parentHead->total;
        }

        $data['fromDate'] = date("d-M-Y", strtotime($data['fromDate']));
        $data['toDate'] = date("d-M-Y", strtotime($data['toDate']));

        $pdf = PDF::loadView('pages.accounts.reports.balanceSheet',[
            'accountTypes' => $accountTypes,
            'parentHeads' => $parentHeads,
            'vouchers' => $vouchers,
            'total' => $total,
            'from' => $data['fromDate'],
            'to' => $data['toDate']
            ]);

        $size = "a4"; //array(0,0,612.00,1008.00);
        $orientation = "portrait";

        $pdf->setPaper($size, $orientation);
        
        return $pdf->stream('Balance_Sheet.pdf');
    }
}
