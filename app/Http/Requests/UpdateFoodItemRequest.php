<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateFoodItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'itemname' => 'required',            
            'qty' => 'required|numeric',
            'hmt' => 'required|numeric|between:1,8',
            'formula' => 'required',
            'itemCategory' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'itemname.required' => 'Please provide a food item name',                        
            'qty.required' => 'Please provide the quantity of your item',
            'formula.required' => 'Provide a distribution formula!',
            'qty.numeric' => 'Please provie a numeric value',
            'hmt.required' => 'You must provide a serve time value',
            'hmt.numeric' => 'Please a provide a numeric value for How Many Times',
            'hmt.between' => 'The How Many Times Value Must between 1 to 3',
            'itemCategory.required' => 'You must select a category'
        ];
    }
}
