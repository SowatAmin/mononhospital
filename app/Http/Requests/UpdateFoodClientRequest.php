<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateFoodClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dhndp' => 'required_without_all:dhdp,ncml,ncmd,nsml,nsmd,nndpr,nndpl,ndpr,ndpl,ngm',
            'dhdp' => 'required_without_all:dhndp,ncml,ncmd,nsml,nsmd,nndpr,nndpl,ndpr,ndpl,ngm',
            'ncml' => 'required_without_all:dhndp,dhdp,ncmd,nsml,nsmd,nndpr,nndpl,ndpr,ndpl,ngm',
            'ncmd' => 'required_without_all:dhndp,dhdp,ncml,nsml,nsmd,nndpr,nndpl,ndpr,ndpl,ngm',
            'nsml' => 'required_without_all:dhndp,dhdp,ncml,ncmd,nsmd,nndpr,nndpl,ndpr,ndpl,ngm',
            'nsmd' => 'required_without_all:dhndp,dhdp,ncml,ncmd,nsml,nndpr,nndpl,ndpr,ndpl,ngm',
            'nndpr' => 'required_without_all:dhndp,dhdp,ncml,ncmd,nsml,nsmd,nndpl,ndpr,ndpl,ngm',
            'nndpl' => 'required_without_all:dhndp,dhdp,ncml,ncmd,nsml,nsmd,nndpr,ndpr,ndpl,ngm',
            'ndpr' => 'required_without_all:dhndp,dhdp,ncml,ncmd,nsml,nsmd,nndpr,nndpl,ndpl,ngm',
            'ndpl' => 'required_without_all:dhndp,dhdp,ncml,ncmd,nsml,nsmd,nndpr,nndpl,ndpr,ngm',
            'ngm' => 'required_without_all:dhndp,dhdp,ncml,ncmd,nsml,nsmd,nndpr,nndpl,ndpr,ndpl'
        ];
    }

    public function messages()
    {
        return [            
            'required_without_all' => 'Sorry, you don\'t have any client!!'
        ];
    }
}
