<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreFoodItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'itemname' => 'required|unique:foodlists,NameOfItem,NULL,id,DayFor,'.$this->get('day').',formula,'.$this->get('df'),
            'qty' => 'required|numeric',
            'hmt' => 'required|numeric|between:1,8',            
            'itemCategory' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'itemname.required' => 'Provide a food item name',
            'itemname.unique' => 'Your input already has been taken for this day',            
            'qty.required' => 'Provide the quantity of your item',
            'qty.numeric' => 'Provide a numeric value for quantity',
            'hmt.required' => 'You must provide a serve time value',
            'hmt.numeric' => 'Provide a numeric value for How Many Times',
            'hmt.between' => 'The How Many Times Value Must between 1 to 3',
            'itemCategory.required' => 'You must select a category'
        ];
    }
}
