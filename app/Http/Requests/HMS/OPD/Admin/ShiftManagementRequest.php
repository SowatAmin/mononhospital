<?php

namespace App\Http\Requests\HMS\OPD\Admin;

use App\Http\Requests\Request;

class ShiftManagementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shiftname' => 'required|unique:tbl_time_shift,shift_name'
        ];
    }

    public function messages()
    {
        return [
            'shiftname.required' => 'Provide a time shift name',
            'shiftname.unique' => 'Your input is already exist'
        ];
    }
}
