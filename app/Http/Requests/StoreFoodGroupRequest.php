<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreFoodGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'foodgroupname' => 'required|unique:foodgroup,NameOfGroup'
        ];
    }

    public function messages()
    {
        return [
            'foodgroupname.required' => 'Please provide a food group name',
            'foodgroupname.unique' => 'Your input already has been taken'
        ];
    }
}
