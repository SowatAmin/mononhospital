<?php



Route::get('/', 'SignInController@Index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	]);

//User management for admin
Route::resource('users', 'AdminUserController');
Route::resource('doctorsAdmin','doctorsAdminController');

//Services controls for admin
Route::get('servicesPart',function(){
	return view('pages.admin.servicesPart.home');
});

/*Route::get('logo/monon',function(){
	return view('pages.administration.patientAdmission.reports.mononLogo');
});*/



Route::controller('salary','salaryController');

Route::controller('accountsReports','accountsReportsController');




/*Route::resource('servicesPart/masterCategory','ServicesMasterCategoryController');

Route::controller('patientReports', 'patientInfoReportsController');
Route::resource('patientAdmission', 'patientAdmissionController');

Route::controller('patientPayment', 'patientPaymentController');

Route::controller('patientDiscount', 'patientDiscountController');*/
//rabbirs work 


Route::resource('servicesPart/masterCategory','ServicesMasterCategoryController');

Route::controller('patientReports', 'patientInfoReportsController');

Route::get('patientAdmission/release/{id}', function($id){
	$patient = App\Models\PatientInfo::find($id);
	$patient->admissionStatus = 'Released';


	$bedcabin=$patient->bedCabin;



	DB::table('beds')
                ->where('name','=',$bedcabin)
                ->update([
                    'Status' => "Vacant",
                    'RegNo' => 0
                    ]);




	$billNo = App\Models\PatientInfo::selectRaw('billNo')
	->orderBy('billNo', 'desc')
	->get()
	->first();
	
	if(!$billNo){
		$patient->billNo = 1;
	}
	else{
		$patient->billNo = $billNo->billNo + 1;
	}

	$patient->save();

	return back()->withInput();
});

Route::get('dailyRent/edit/{id}', function($id){
	$dailyRent = App\Models\DailyRentAndCharge::find($id);
	$mCat = App\Models\serviceMasterCategory::where('id',$dailyRent->master_category)->get()->last();
	$mastercatagories = App\Models\serviceMasterCategory::get();
	$subCatagories = App\Models\serviceCategory::where('masterCatId', $dailyRent->master_category)->get();
	

	return view('pages.administration.patientAdmission.editDaily', [
			'dailyRent' => $dailyRent,
			'mCat' => $mCat->name,
			'mastercatagories' => $mastercatagories,
			'subCatagories' => $subCatagories
		]);
});

Route::post('dailyRent/update', function(Illuminate\Http\Request $request){

	$daily = App\Models\DailyRentAndCharge::find($request['id']);

	$daily->master_category = $request['master_category'];
	$daily->subcategory = $request['subcategory'];
	$daily->services = $request['services'];
	$daily->quantity = $request['quantity'];
	$daily->rate = $request['rate'];
	$daily->vat = $request['vat'];
	$daily->servicecharge = $request['servicecharge'];
	$daily->description = $request['description'];

	if($daily->master_category == '1'){
		$patient = App\Models\PatientInfo::where('id', $daily->pid)->get()->last();
		$patient->bedCabin = $daily->services;
		$patient->save();
	}

	$daily->save();	

	return redirect('/patientAdmission/'.$request['pid'].'/edit');
});

Route::get('dailyRent/delete/{id}', function($id){
	$dailyRent = App\Models\DailyRentAndCharge::find($id);

	$dailyRent->delete();
	
	return redirect('/patientAdmission/'.$dailyRent->pid.'/edit');
});





Route::get('accountLedger/edit/{id}', function($id){
	$bill = App\Models\Bill::find($id);
	$mastercatagories = App\Models\serviceMasterCategory::get();	

	return view('pages.administration.patientAdmission.editAccoutsLedger', [
			'bill' => $bill,
			'mastercatagories' => $mastercatagories
		]);
});

Route::post('accountLedger/update', function(Illuminate\Http\Request $request){

	$account = App\Models\Bill::find($request['id']);
	
	$account->date = $request['bdate'];
	$account->masterCategory = $request['master_category_acc'];
	$account->category = $request['subcategory_acc'];
	$account->services = $request['services_acc'];
	$account->quantity = $request['quantity_acc'];
	$account->rate = $request['rate_acc'];
	$account->vat = $request['vat_acc'];
	$account->servicecharge = $request['servicecharge_acc'];
	$account->remarks = $request['remarks_acc'];

	$account->billBy = \Auth::user()->username;

	$account->save();	

	return redirect('/patientAdmission/'.$request['regNo'].'/edit');
});

Route::get('accountLedger/delete/{id}', function($id){
	$bill = App\Models\Bill::find($id);

	$bill->delete();
	
	return redirect('/patientAdmission/'.$bill->regNo.'/edit');
});




Route::resource('patientAdmission', 'patientAdmissionController');

Route::controller('patientPayment', 'patientPaymentController');

Route::controller('patientDiscount', 'patientDiscountController');

Route::controller('reports', 'reportsController');







//Shoukhin vai's work
Route::group(['middleware' => 'auth'],function(){
	//For logged in user
	Route::controller('home','HomeController');
	//For logged in user end
	
	//Admin access
	Route::group(['middleware' => 'admin'],function(){
		//foodcontroller
		Route::controller('foodadminpart','AdminFoodController');
		//endfoodcontroller
		
		//opdsectionview
		Route::get('manageopdsection',function(){
			return view('pages.admin.hms.opd.home');
		});
		//endopdsectionview
		
		//opdShiftManagement
		Route::get('opdshiftmanagement/showdata', 'AdminOPDShiftManagementController@show');				
		Route::resource('opdshiftmanagement','AdminOPDShiftManagementController');
		//endOpdShiftManagement
	});
	//Admin access end

	//Food manager access
	Route::group(['middleware' => 'fm'],function(){
		Route::controller('food','FoodController');
	});
	//Food manager access end


});
//food group delete

Route::get('delete/{id}','AdminFoodController@getDelete');

//food item delete
Route::get('Del/{id}','AdminFoodController@getDeleteFoodItem');


Route::get('ajax-subcat',function(){
	$mcat_id=Input::get('mcat_id');
	$subcategories=App\Models\serviceCategory::where('masterCatId','=',$mcat_id)->get();
	
	return Response::json($subcategories);


});



//category edit 		!!MERGE ME!!
Route::get('/edit/ajax-subcat',function(){
	$mcat_id=Input::get('mcat_id');
	$subcategories=App\Models\serviceCategory::where('masterCatId','=',$mcat_id)->get();

	return Response::json($subcategories);
	

});





Route::get('ajax-cat',function(){

	$cat_name=Input::get('cat_name');
	$tablename=Input::get('tablename');

	if($tablename=="Bed")
	{
		$service=App\Models\Bed::where('BedCabinType','=',$cat_name)->get();
		
	}
	else if($tablename=="Doctors")
	{
		$service=App\Models\Doctors::where('DocSpecialization','=',$cat_name)->get();


	}

	else if($tablename=="Hospitalservices")
	{
		$service=App\Models\hospitalservices::where('type','=',$cat_name)->get();


	}
	else
	{
		$service=App\Models\NonProfitservices::where('type','=',$cat_name)->get();
	}




	return Response::json($service);



});



//sub-category edit   !!MERGE ME!!

Route::get('/edit/ajax-cat',function(){

	$cat_name=Input::get('cat_name');
	$tablename=Input::get('tablename');

	if($tablename=="Bed")
	{
		$service=App\Models\Bed::where('BedCabinType','=',$cat_name)->get();
		
	}
	else if($tablename=="Doctors")
	{
		$service=App\Models\Doctors::where('DocSpecialization','=',$cat_name)->get();


	}

	else if($tablename=="Hospitalservices")
	{
		$service=App\Models\hospitalservices::where('type','=',$cat_name)->get();


	}
	else
	{
		$service=App\Models\NonProfitservices::where('type','=',$cat_name)->get();
	}




	return Response::json($service);



});








Route::get('save',function(){
	if(Request::ajax())
	{

		$x=Input::all();
		App\Models\VisitorInfo::create($x);
		return "Record Saved";

	}
	else
	{
		return "Error while saving record try again";
	}

	

});

//add visitor for edit    !!MERGE ME!!

Route::get('/edit/editsave',function(){
	if(Request::ajax())
	{

		$x=Input::all();
		App\Models\VisitorInfo::create($x);
		return "Record Saved";

	}
	else
	{
		return "Error while saving record try again";
	}

	

});



Route::get('save_dailyrent',function(){
	if(Request::ajax())
	{

		$y=Input::all();
		App\Models\DailyRentAndCharge::create($y);
		App\Models\Bill::create([
			'regNo'=>$y['pid'],
			'bedNo'=>$y['bedNo'],
			'masterCategory'=>$y['master_category'],
			'category'=>$y['subcategory'],
			'services'=>$y['services'],
			'date'=>$y['date'],
			'quantity'=>$y['quantity'],
			'rate'=>$y['rate'],
			'vat'=>$y['vat'],
			'serviceCharge'=>$y['servicecharge'],
			'billBy'=>$y['billBy'],
			]);
		return "Record Saved";
		

	}
	/*else
	{
		return "Error while saving record try again";
	}*/

	

});

//daily rent amd charge for edit    !!MERGE ME!!
Route::get('/edit/editsave_dailyrent',function(){
	if(Request::ajax())
	{

		$y=Input::all();
		App\Models\DailyRentAndCharge::create($y);
		App\Models\Bill::create([
			'regNo'=>$y['pid'],
			'bedNo'=>$y['bedNo'],
			'masterCategory'=>$y['master_category'],
			'category'=>$y['subcategory'],
			'services'=>$y['services'],
			'date'=>$y['date'],
			'quantity'=>$y['quantity'],
			'rate'=>$y['rate'],
			'vat'=>$y['vat'],
			'serviceCharge'=>$y['servicecharge'],
			'billBy'=>$y['billBy'],
			]);
		return "Record Saved";
		

	}
	/*else
	{
		return "Error while saving record try again";
	}*/

	

});



Route::get('saveAccountLedger',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\Bill::create($input);
		return "Record saved";

	}
	

	

});

//account ledger ADD button   !!MERGE ME!!


Route::get('/edit/editsaveAccountLedger',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\Bill::create($input);
		return "Record saved";

	}
	

	

});




Route::get('saveInvestigation',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\InvestigationTbl::create($input);
		return "Record saved";

	}




	

});

//investigation add for edit     !!MERGE ME!!
Route::get('/edit/editsaveInvestigation',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\InvestigationTbl::create($input);
		return "Record saved";

	}

});

//OPD STARTS HERE

Route::resource('opd','OPDController'); 
Route::get('opdReportShow','OPDController@show');
Route::post('opdReport','OPDController@report');


Route::get('opdBill/{id}/{words}/bill',['uses'=>'OPDController@bill']);
/*Route::get('opdBill/{words}/bill',function($words){
	return $words;
});*/










Route::get('ajax-service',function(){
	$service_name=Input::get('service_name');
	$s=App\Models\opdServiceMdl::where('serviceName','=',$service_name)->get();


	return Response::json($s);

});




Route::get('/edit/ajax-service',function(){
	$service_name=Input::get('service_name');
	$s=App\Models\opdServiceMdl::where('serviceName','=',$service_name)->get();


	return Response::json($s);

});




Route::get('saveOpdAll',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\opdAll::create($input);
		return "Record saved";

	}
	

	

});

//opd edit

Route::get('/edit/editsaveOpdAll',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\opdAll::create($input);
		return "Record saved";

	}
	

	

});

//Acounts part
Route::resource('voucher','voucherController');




//ajax for account head voucher

Route::get('ajax-phead',function(){
	$phead=Input::get('phead');

	$subcategories=App\Models\AccountType::where('parentHead','=',$phead)->get(['childHead']);

	return Response::json($subcategories);


});


Route::get('ajax-pmode',function(){
	$mode=Input::get('mode');

	if($mode=="Cash")
	{
		$subcategories=App\Models\PaymentMode::where('mode','=',"Cash")->get(['availableCash_bank']);	
	}	
	else
	{
		$subcategories=App\Models\PaymentMode::where('mode','=',"Bank")->get(['availableCash_bank']);
	}

	return Response::json($subcategories);


});



//edit for ajax for account head voucher

Route::get('/edit/ajax-phead',function(){
	$phead=Input::get('phead');

	$subcategories=App\Models\AccountType::where('parentHead','=',$phead)->get(['childHead']);

	return Response::json($subcategories);


});


Route::get('/edit/ajax-pmode',function(){
	$mode=Input::get('mode');

	if($mode=="Cash")
	{
		$subcategories=App\Models\PaymentMode::where('mode','=',"Cash")->get(['availableCash_bank']);	
	}	
	else
	{
		$subcategories=App\Models\PaymentMode::where('mode','=',"Bank")->get(['availableCash_bank']);
	}

	return Response::json($subcategories);


});


//new employee info
Route::resource('employee','employeeController');

//bed info controller
Route::resource('bed','bedController');


//journal controller
Route::resource('journal','journalController');

//journal ajax


Route::get('/save_journaldata',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\Journal_data::create($input);
		return "Record saved";

	}
	

	

});





Route::get('/edit/save_journaldata',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\Journal_data::create($input);
		return "Record saved";

	}
	

	

});






Route::get('/edit/save_journaldata',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\Journal_data::create($input);
		return "Record saved";

	}
	

	

});

Route::get('/journal/{id}/report','journalController@report');

//ajax for voucher add

Route::get('save_voucherdata',function(){
	if(Request::ajax())
	{

		$input=Input::all();
		App\Models\Voucher::create($input);
		return "Record saved";

	}
	

	

});



Route::get('/VoucherReport',function(){
	return view('pages.accounts.voucher.Report');
});


Route::post('vReport','voucherController@report');


Route::get('/ViewVoucher','voucherController@view');
Route::get('AdminViewVoucher','voucherController@Adminview');

Route::get('/voucher/{id}/invoice','voucherController@invoice');