<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    public $timestamps = false;
    protected $table='journal';
    protected $fillable=[
    'source',
    'date',
    'particular'
    ];
}
