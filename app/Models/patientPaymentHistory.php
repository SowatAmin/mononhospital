<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class patientPaymentHistory extends Model
{
	protected $table = 'patient_payment_history';
	protected $fillable=[
		'regNo',
		'paymentDate',
		'bedCabin',
		'desc',
		'payment',
		'recievedBy',
		'posted'
	];
}
