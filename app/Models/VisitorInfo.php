<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitorInfo extends Model
{
    protected $table = 'visitorinfo';

    protected $fillable=[
    'pid',
    'visitorName',
    'visitorRelation',
    'visitorPhone',
    'visitorRemark'
    ];
    public function patient()
    {
    	return $this->belongsTo('App\Models\PatientInfo');
    }
}
