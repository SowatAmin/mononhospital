<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyChart extends Model
{
    protected $table = 'dailychart';
}
