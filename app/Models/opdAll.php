<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class opdAll extends Model
{
    protected $table = 'opdall';
    protected $fillable=[
    'opdNo',
    'consultant',
    'doctors_fee',
    'hospital_fee',
    'total_fee',
    'date'

    ];
}
