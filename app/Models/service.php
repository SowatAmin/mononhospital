<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
	protected $table = 'service';

	protected $fillable = [
	'servCatId'
	'masterServCatId'
	'name'
	'type'
	'rate'
	'vat'
	'sc'
	];


}
