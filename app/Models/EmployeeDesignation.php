<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDesignation extends Model
{
    protected $table = 'designation';

    protected $fillable=[
    'designation'
    ];
}
