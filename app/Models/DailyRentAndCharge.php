<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyRentAndCharge extends Model
{
    protected $table='dailyrent';
    protected $fillable=[
    'pid',
    'master_category',
    'subcategory',
    'services',
    'quantity',
    'rate',
    'vat',
    'servicecharge',
    'description',
    'billBy',
    'bedNo',
    'date'
    ];

}

