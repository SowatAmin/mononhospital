<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journal_data extends Model
{
     public $timestamps = false;
    protected $table='journal_data';
    protected $fillable=[
    'jid',
    'acchead',
    'debit',
    'credit',
    ];

}
