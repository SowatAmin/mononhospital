<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class serviceMasterCategory extends Model
{
    protected $table = 'servicemastercategory';

    protected $fillable = [
    'name',
    'id'
    ];

    /*public function seviceCategory()
    {
        return $this->hasMany('App\Models\serviceCategory');
    }

    public function sevice()
    {
        return $this->hasManyThrough('App\Models\service', 'App\Models\serviceCategory');
    }
    */
}
