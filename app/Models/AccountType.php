<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    protected $table = 'accounttype';

    protected $fillable=[
    'id',
    'finalAccount',
    'type',
    'parentHead',
    'childHead'
    ];
}
