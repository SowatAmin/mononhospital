<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class hospitalservices extends Model
{
    protected $table = 'hospitalservices';
     protected $fillable=
     [
     'name',
     'type',
     'Rate',
     'VatRate',
     'ServiceChargeRate',
     'id'
      ];
}
