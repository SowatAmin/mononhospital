<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodMarket extends Model
{
    protected $table = 'foodmarket';
}
