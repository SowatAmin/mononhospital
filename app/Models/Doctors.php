<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    protected $table = 'doctors';

    protected $fillable=[
    'name',
    'DocTitle',
    'DocAddress',
    'DocPhone',
    'DocSpecialization',
    'DocDesignation',
    'Rate',
    'VatRate',
    'ServiceChargeRate'
    ];
}
