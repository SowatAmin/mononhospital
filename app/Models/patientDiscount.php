<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class patientDiscount extends Model
{
	protected $table = 'patient_discount';
	
	protected $fillable=[
		'regNo',
		'date',
		'bedCabin',
		'desc',
		'amount',
		'discountBy',
		'refBy'
	];
}
