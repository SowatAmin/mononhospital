<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyRentDateLog extends Model
{
    protected $table = 'dailyrentdatelog';

    protected $fillable=[
    'updatedDates'
    ];
}
