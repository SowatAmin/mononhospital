<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvestigationTbl extends Model
{
    protected $table='tbl_investigation';
    protected $fillable=[
    'pid',
    'todaydate',
    'senddate',
    'deliverydate',
    'investigationSendTime',
    'investigationDeliveryTime',
    'lab',
    'bedcabin',
    'investigation',
    'billby'
    ];
}
