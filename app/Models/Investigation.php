<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Investigation extends Model
{
   protected $table='investigations';
   protected $fillable=[
   'investigationDate',
   'investigationSendDate',
   'investigationSendTime',
   'investigationDeliveryDate',
   'investigationDeliveryTime',
   'laboratory',
   'investigations'
    ];
}
