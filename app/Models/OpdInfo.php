<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpdInfo extends Model
{
    protected $table ='opdinfo_tbl';
     protected $fillable=[
     'pName',
     'date',
     'sex',
     'age',
     'phone',
     'address',
     'occupation',
     'shift'

     ];

}
