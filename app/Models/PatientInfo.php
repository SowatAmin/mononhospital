<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientInfo extends Model
{
    protected $table = 'patientinfo';

    protected $fillable=[
    'name',
    'bedCabin',
    'admissionStatus',
    'billNo',
    'age',
    'sex',
    'maritalStatus',
    'presentAddress',
    'permanentAddress',
    'nationality',
    'religion',
    'bloodGroup',
    'profession',
    'guardianName',
    'guardianRel',
    'guardianPhone',
    'altGuardianName',
    'altGuardianRel',
    'altGuardianAddress',
    'altGuardianPhone',
    'assignedConsultant',
    'moOnDuty',
    'manager',
    'diagnosis',
    'bookingDate',
    'bookingTime',
    'admissionDate',
    'admissionTime',
    'releaseDate',
    'releaseTime',
    'dischargeBy',
    'visitor',
    'incomingCall',
    'outgoingCall',
    'extraFood',
    'cigarette',
    'policeCase',
    'education',
    'dob',
    'drugabuse',
    'illness'
    ];

   
}
