<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class opdVisitingFeeMdl extends Model
{
    protected $table ='opdvisitingfee';
     protected $fillable=[
     'visitingfee'
     ];
}
