<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
	protected $table = 'bill';

	protected $fillable = [
		'id',
		'regNo',
		'bedNo',
		'masterCategory',
		'category',
		'services',
		'remarks',
		'date',
		'quantity',
		'rate',
		'vat',
		'serviceCharge',
		'billBy'
	];
}
