<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';

    protected $fillable=[
    'employeeNo',
    'name',
    'fathersName',
    'sex',
    'maritalStatus',
    'dob',
    'jDate',
    'address',
    'designation',
    'grade',
    'section',
    'payType',
    'oId',
    'basic',
    'houseRent',
    'medAllow',
    'conveyence',
    'gSalary',
    'active',
    'bonus',
    'pf',
    'ot',
    'attBonus'
    ];
}
