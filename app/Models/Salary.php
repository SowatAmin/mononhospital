<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table='salary';
    
    protected $fillable=[
    'employeeNo',
    'name',
    'month',
    'year',
    'basic',
    'houseRent',
    'medAllow',
    'conveyence',
    'gSalary',
    'othersAdd',
    'totalOT',
    'festivalOT',
    'othersDeduct',
    'advanceDeduct',
    'mealDeduct',
    'fineDeduct',
    'totalLWP',
    'totalAbsent',

    
    'allowPhr',
    'allowAmount',
    'totalSalary',
    'otRate',
    'otAmount',
    'festivalPayment',
    'otAllow',
    'totalOT',
    'payableSalary',
    'lwpRate',
    'lwpAmount',
    'lwpAllow',
    'totalWp',
    'uaAmount',
    'uaAllow'
    ];
}
