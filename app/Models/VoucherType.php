<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherType extends Model
{
    protected $table = 'vouchertype';

    protected $fillable=[
    'id',
    'accounttype'
    ];
}
