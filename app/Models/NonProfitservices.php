<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NonProfitservices extends Model
{
    protected $table = 'nonprofitservices';
     protected $fillable=[
     'name',
     'type',
     'Rate',
     'VatRate',
     'ServiceChargeRate',
     'id'
      ];
}
