<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    
    protected $table = 'voucher';

    protected $fillable=[
    'voucherNumber',
    'voucherType',
    'parentHead',
    'accountHead',
    'date',
    'paymentMode',
    'available_cash_bank',
	'description',
	'amount'
    ];
}
