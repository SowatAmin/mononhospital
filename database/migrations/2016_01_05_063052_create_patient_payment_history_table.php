<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientPaymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_payment_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regNo');
            $table->date('paymentDate');
            $table->string('bedCabin');
            $table->string('desc');
            $table->double('payment');
            $table->string('recievedBy');
            $table->boolean('posted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patient_payment_history');
    }
}
