<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodlist', function (Blueprint $table) {
            $table->increments('id');
            $table->string('NameOfItem');
            $table->tinyInteger('GroupID');
            $table->string('Day');
            $table->string('Quantity');
            $table->string('HowManyTimes');           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('foodlist');
    }
}
