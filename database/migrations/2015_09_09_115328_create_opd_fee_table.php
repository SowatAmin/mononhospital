<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpdFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_opd_fee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patient_id');            
            $table->date('date');            
            $table->unique(array('patient_id','date'));
            $table->double('fee');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
