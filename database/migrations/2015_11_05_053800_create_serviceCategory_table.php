<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serviceCategory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('masterCatId')->unsigned()->index();
            $table->string('name');
            $table->timestamps();

            $table->foreign('masterCatId')
                ->references('id')
                ->on('serviceMasterCategory')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('serviceCategory');
    }
}
