<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpdInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opdInfo_tbl', function (Blueprint $table) {
            $table->increments('opdNo');
            $table->string('pName');
            $table->date('date');
            $table->string('sex');
            $table->integer('age');
            $table->string('phone');
            $table->string('address');
            $table->string('occupation');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opdInfo_tbl');
    }
}
