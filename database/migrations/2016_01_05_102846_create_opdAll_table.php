<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpdAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opdAll', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('opdNo');
            $table->string('consultant');
            $table->float('doctors_fee');
            $table->float('hospital_fee');
            $table->float('visitingfee');
            $table->float('opdfee');
            $table->float('total_fee');
            $table->string('services');
            $table->float('rate');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opdAll');
    }
}
