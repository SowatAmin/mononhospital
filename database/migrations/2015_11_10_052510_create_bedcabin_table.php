<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedcabinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bedcabin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bedCabinNo');
            $table->string('bedCabinType');
            $table->string('status');
            $table->float('rate');
            $table->float('vat');
            $table->float('sc');
            $table->string('floor');
            $table->string('comment');
            $table->string('regNo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bedcabin');
    }
}
