<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            
            
            $table->string('DocName');
            $table->string('DocTitle');
            $table->string('DocAddress');
            $table->string('DocPhone');
            $table->string('DocSpecialization');
            $table->string('DocDesignation');
            $table->double('DocRate');
            $table->double('DocVat');
            $table->double('DocServicecharge');
            $table->increments('id')->unsigned();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors');
    }
}
