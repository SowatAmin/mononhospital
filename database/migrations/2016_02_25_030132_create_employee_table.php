<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('employeeNo');
            
            $table->string('name');
            $table->string('fathersName');
            $table->string('sex');
            $table->string('maritalStatus');
            $table->date('dob');
            $table->date('jDate');
            $table->string('address');
            $table->string('designation');
            $table->string('grade');
            $table->string('section');
            $table->string('payType');
            $table->integer('oId');
            $table->float('basic');
            $table->float('houseRent');
            $table->float('medAllow');
            $table->float('conveyence');
            $table->float('gSalary');
            $table->boolean('active');
            $table->boolean('bonus');
            $table->boolean('pf');
            $table->boolean('ot');
            $table->boolean('attBonus');
            $table->timestamps();








            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee');
    }
}
