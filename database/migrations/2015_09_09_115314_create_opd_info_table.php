<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpdInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_opd_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('opd_id')->unique();
            $table->string('name');
            $table->integer('age');
            $table->string('sex',1);
            $table->integer('time_shift');
            $table->date('reg_date');
            $table->longtext('address');
            $table->string('phone',15);
            $table->string('occupation',50);                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
