<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodlists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('NameOfItem');
            $table->string('DayFor');
            $table->unique(array('NameOfItem','DayFor'));            
            $table->tinyInteger('Quantity');
            $table->tinyInteger('HowManyTimes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('foodlists');
    }
}
