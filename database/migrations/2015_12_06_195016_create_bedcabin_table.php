<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedcabinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beds', function (Blueprint $table) {
            $table->string('name');
             $table->string('BedCabinType');
             $table->string('Status');
             $table->float('Rate');
             $table->float('VatRate');
             $table->float('ServiceChargeRate');
             $table->string('Floor');
             $table->string('Comments');
             $table->integer('RegNo');
             $table->integer('bID');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('beds');
    }
}
