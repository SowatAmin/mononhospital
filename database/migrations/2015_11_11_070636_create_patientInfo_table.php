<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patientInfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('bedCabin');
            $table->string('admissionStatus');
            $table->integer('age');
            $table->string('sex');
            $table->string('maritalStatus');
            $table->string('presentAddress');
            $table->string('permanentAddress');
            $table->string('diagnosis');
            $table->string('nationality');
            $table->string('religion');
            $table->string('bloodGroup');
            $table->string('profession');
            $table->string('guardianName');
            $table->string('guardianRel');
            $table->string('guardianPhone');
            $table->string('altGuardianName');
            $table->string('altGuardianRel');
            $table->string('altGuardianAddress');
            $table->string('altGuardianPhone');
            $table->string('assignedConsultant');
            $table->string('moOnDuty');
            $table->string('manager');
            $table->date('bookingDate');
            $table->time('bookingTime');
            $table->date('admissionDate');
            $table->time('admissionTime');
            $table->date('releaseDate');
            $table->time('releaseTime');
            $table->integer('dischargeBy');
            $table->boolean('visitor');
            $table->boolean('incomingCall');
            $table->boolean('outgoingCall');
            $table->boolean('extraFood');
            $table->boolean('cigarette');
            $table->boolean('policeCase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patientInfo');
    }
}
