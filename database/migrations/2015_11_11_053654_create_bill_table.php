<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regNo');
            $table->string('bedNo');
            $table->integer('masterCategory');
            $table->string('category');
            $table->string('services');
            $table->string('remarks');
            $table->date('date');
            $table->integer('quantity');
            $table->float('rate');
            $table->float('vat');
            $table->float('serviceCharge');
            $table->string('billBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bill');
    }
}
