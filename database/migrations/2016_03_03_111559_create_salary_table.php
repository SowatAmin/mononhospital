<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary', function (Blueprint $table) {
             $table->increments('id');
            $table->integer('employeeNo');
            $table->string('name');
            
            $table->string('month');
            $table->string('year');

            $table->float('basic');
            $table->float('houseRent');
            $table->float('medAllow');
            $table->float('conveyence');
            $table->float('gSalary');

            $table->float('othersAdd');
            
            $table->float('festivalOT');

            $table->float('othersDeduct');
            $table->float('advanceDeduct');
            $table->float('mealDeduct');
            $table->float('fineDeduct');

            $table->float('totalLWP');
            $table->float('totalAbsent');


            $table->float('allowPhr');
            $table->float('allowAmount');
            $table->float('totalSalary');
            $table->float('otRate');
            $table->float('otAmount');
            $table->float('festivalPayment');
            $table->float('otAllow');
            $table->float('totalOT');
            $table->float('payableSalary');
            $table->float('lwpRate');
            $table->float('lwpAmount');
            $table->float('lwpAllow');
            $table->float('totalWp');
            $table->float('uaAmount');
            $table->float('uaAllow');
            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salary');
    }
}
