<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Prescription', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('tda');
            $table->string('investigation');
            $table->string('notes');
            $table->string('treatment');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Prescription');
    }
}
