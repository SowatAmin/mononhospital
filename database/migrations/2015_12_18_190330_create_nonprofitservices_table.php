<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonprofitservicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('NonprofitServices', function (Blueprint $table) {
            
            $table->string('name');
            $table->string('type');
            $table->double('Rate');
            $table->double('VatRate');
            $table->double('ServiceChargeRate');
            $table->increments('id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('NonprofitServices');
    }
}
