<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicecategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicecategory', function (Blueprint $table) {
             $table->increments('id');
            $table->integer('masterCatId')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('masterCatId')
                ->references('id')
                ->on('servicemastercategory')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicecategory');
    }
}
