<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Investigations', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->date('investigationDate');
            $table->date('investigationSendDate');
            $table->time('investigationSendTime');
            $table->date('investigationDeliveryDate');
            $table->time('investigationDeliveryTime');
            $table->string('laboratory');
            $table->string('investigations');
            $table->timestamps('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Investigations');
    }
}
