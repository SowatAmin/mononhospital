<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitorinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pid');
            $table->string('visitorName');
            $table->string('visitorRelation');
            $table->string('visitorPhone');
            $table->string('visitorRemark');
            $table->timestamps();

            $table->foreign('id')
            ->references('id')
            ->on('patientinfo')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visitorinfo');
    }
}
