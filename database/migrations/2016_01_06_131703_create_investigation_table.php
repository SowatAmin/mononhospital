<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tbl_Investigation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pid');
            $table->date('todaydate');
            $table->date('senddate');
            $table->date('deliverydate');
            $table->time('investigationSendTime');
            $table->time('investigationDeliveryTime');
            $table->string('lab');
            $table->string('investigation');
            $table->string('bedcabin');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Tbl_Investigation');
    }
}
