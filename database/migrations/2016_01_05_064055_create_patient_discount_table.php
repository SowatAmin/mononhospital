<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_discount', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regNo');
            $table->date('date');
            $table->string('bedCabin');
            $table->string('desc');
            $table->double('amount');
            $table->string('discountBy');
            $table->string('refBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patient_discount');
    }
}
