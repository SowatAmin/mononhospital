<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyFoodClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodclient', function (Blueprint $table) {
            $table->increments('id');
            $table->date('Date');
            $table->tinyInteger('dh_ndp');
            $table->tinyInteger('dh_dp');
            $table->tinyInteger('number_of_catering_meal_lunch');
            $table->tinyInteger('number_of_catering_meal_dinner');
            $table->tinyInteger('number_of_staff_meal_lunch');
            $table->tinyInteger('number_of_staff_meal_dinner');
            $table->tinyInteger('number_of_ndp_regular');           
            $table->tinyInteger('number_of_ndp_low_cost');
            $table->tinyInteger('number_of_dp_regular');
            $table->tinyInteger('number_of_dp_low_cost');
            $table->tinyInteger('number_of_guest_meal');
            $table->tinyInteger('total_patient');
            $table->tinyInteger('total_client');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
