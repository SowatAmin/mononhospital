<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);

        Model::reguard();
    }
}

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'fname'     => 'Khandakar Noushadur',
            'lname'     => 'Rahman',
            'email'     => 'noushad@monon.com',
            'username'  => 'shoukhin',
            'password'  => Hash::make('123'),
            'role'      => 1
        ));

        User::create(array(
            'fname'     => 'Food Chart',
            'lname'     => 'Creator',
            'email'     => 'food@monon.com',
            'username'  => 'food',
            'password'  => Hash::make('123'),
            'role'      => 2
        ));

        User::create(array(
            'fname'     => 'Hospital Management',
            'lname'     => 'System',
            'email'     => 'hms@monon.com',
            'username'  => 'hms',
            'password'  => Hash::make('123'),
            'role'      => 3
        ));
    }

}
